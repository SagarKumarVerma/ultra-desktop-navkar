﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraBranchEdit
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.TextLocCode = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.TextName = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEmail = New DevExpress.XtraEditors.TextEdit()
        Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.TextLocCode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEmail.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl2.Appearance.Options.UseFont = True
        Me.LabelControl2.Location = New System.Drawing.Point(18, 25)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(93, 14)
        Me.LabelControl2.TabIndex = 4
        Me.LabelControl2.Text = "Location Code: *"
        '
        'TextLocCode
        '
        Me.TextLocCode.Location = New System.Drawing.Point(131, 22)
        Me.TextLocCode.Name = "TextLocCode"
        Me.TextLocCode.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextLocCode.Properties.Appearance.Options.UseFont = True
        Me.TextLocCode.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextLocCode.Properties.Mask.EditMask = "[A-Z0-9]*"
        Me.TextLocCode.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextLocCode.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextLocCode.Properties.MaxLength = 3
        Me.TextLocCode.Size = New System.Drawing.Size(230, 20)
        Me.TextLocCode.TabIndex = 1
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(18, 51)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(96, 14)
        Me.LabelControl1.TabIndex = 6
        Me.LabelControl1.Text = "Location Name: *"
        '
        'TextName
        '
        Me.TextName.Location = New System.Drawing.Point(131, 48)
        Me.TextName.Name = "TextName"
        Me.TextName.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextName.Properties.Appearance.Options.UseFont = True
        Me.TextName.Properties.Mask.EditMask = "[a-zA-Z0-9]*"
        Me.TextName.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextName.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextName.Properties.MaxLength = 35
        Me.TextName.Size = New System.Drawing.Size(230, 20)
        Me.TextName.TabIndex = 2
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl3.Appearance.Options.UseFont = True
        Me.LabelControl3.Location = New System.Drawing.Point(18, 77)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(31, 14)
        Me.LabelControl3.TabIndex = 8
        Me.LabelControl3.Text = "Email:"
        '
        'TextEmail
        '
        Me.TextEmail.Location = New System.Drawing.Point(131, 74)
        Me.TextEmail.Name = "TextEmail"
        Me.TextEmail.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEmail.Properties.Appearance.Options.UseFont = True
        Me.TextEmail.Properties.Mask.EditMask = "[A-Z0-9]*"
        Me.TextEmail.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEmail.Properties.MaxLength = 50
        Me.TextEmail.Size = New System.Drawing.Size(230, 20)
        Me.TextEmail.TabIndex = 3
        '
        'SimpleButton2
        '
        Me.SimpleButton2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton2.Appearance.Options.UseFont = True
        Me.SimpleButton2.Location = New System.Drawing.Point(286, 100)
        Me.SimpleButton2.Name = "SimpleButton2"
        Me.SimpleButton2.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton2.TabIndex = 5
        Me.SimpleButton2.Text = "Cancel"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton1.Appearance.Options.UseFont = True
        Me.SimpleButton1.Location = New System.Drawing.Point(198, 100)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton1.TabIndex = 4
        Me.SimpleButton1.Text = "Save"
        '
        'XtraBranchEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(384, 140)
        Me.ControlBox = False
        Me.Controls.Add(Me.SimpleButton2)
        Me.Controls.Add(Me.SimpleButton1)
        Me.Controls.Add(Me.LabelControl3)
        Me.Controls.Add(Me.TextEmail)
        Me.Controls.Add(Me.LabelControl1)
        Me.Controls.Add(Me.TextName)
        Me.Controls.Add(Me.LabelControl2)
        Me.Controls.Add(Me.TextLocCode)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.Name = "XtraBranchEdit"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Location"
        CType(Me.TextLocCode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEmail.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextLocCode As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEmail As DevExpress.XtraEditors.TextEdit
    Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
End Class
