﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraRealTimePunches
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(XtraRealTimePunches))
        Me.lvwLogList = New System.Windows.Forms.ListView()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.SidePanel2 = New DevExpress.XtraEditors.SidePanel()
        Me.SimpleButtonClear = New DevExpress.XtraEditors.SimpleButton()
        Me.AxRealSvrOcxTcp1 = New AxRealSvrOcxTcpLib.AxRealSvrOcxTcp()
        Me.lblTotal = New DevExpress.XtraEditors.LabelControl()
        Me.SidePanel3 = New DevExpress.XtraEditors.SidePanel()
        Me.tcpServer1 = New tcpServer.TcpServer(Me.components)
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SidePanel2.SuspendLayout()
        CType(Me.AxRealSvrOcxTcp1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SidePanel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'lvwLogList
        '
        Me.lvwLogList.Location = New System.Drawing.Point(225, 0)
        Me.lvwLogList.Name = "lvwLogList"
        Me.lvwLogList.Size = New System.Drawing.Size(284, 229)
        Me.lvwLogList.TabIndex = 5
        Me.lvwLogList.UseCompatibleStateImageBehavior = False
        Me.lvwLogList.View = System.Windows.Forms.View.Details
        Me.lvwLogList.Visible = False
        '
        'GridControl1
        '
        Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControl1.EmbeddedNavigator.Buttons.Append.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.Edit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.Remove.Visible = False
        Me.GridControl1.Location = New System.Drawing.Point(0, 0)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(884, 302)
        Me.GridControl1.TabIndex = 2
        Me.GridControl1.UseEmbeddedNavigator = True
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.Editable = False
        '
        'SidePanel2
        '
        Me.SidePanel2.Controls.Add(Me.SimpleButtonClear)
        Me.SidePanel2.Controls.Add(Me.AxRealSvrOcxTcp1)
        Me.SidePanel2.Controls.Add(Me.lblTotal)
        Me.SidePanel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.SidePanel2.Location = New System.Drawing.Point(0, 302)
        Me.SidePanel2.Name = "SidePanel2"
        Me.SidePanel2.Size = New System.Drawing.Size(884, 59)
        Me.SidePanel2.TabIndex = 1
        Me.SidePanel2.Text = "SidePanel2"
        '
        'SimpleButtonClear
        '
        Me.SimpleButtonClear.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.SimpleButtonClear.Appearance.Options.UseFont = True
        Me.SimpleButtonClear.Location = New System.Drawing.Point(12, 15)
        Me.SimpleButtonClear.Name = "SimpleButtonClear"
        Me.SimpleButtonClear.Size = New System.Drawing.Size(101, 23)
        Me.SimpleButtonClear.TabIndex = 20
        Me.SimpleButtonClear.Text = "Clear"
        '
        'AxRealSvrOcxTcp1
        '
        Me.AxRealSvrOcxTcp1.Enabled = True
        Me.AxRealSvrOcxTcp1.Location = New System.Drawing.Point(749, 15)
        Me.AxRealSvrOcxTcp1.Name = "AxRealSvrOcxTcp1"
        Me.AxRealSvrOcxTcp1.OcxState = CType(resources.GetObject("AxRealSvrOcxTcp1.OcxState"), System.Windows.Forms.AxHost.State)
        Me.AxRealSvrOcxTcp1.Size = New System.Drawing.Size(32, 32)
        Me.AxRealSvrOcxTcp1.TabIndex = 19
        Me.AxRealSvrOcxTcp1.Visible = False
        '
        'lblTotal
        '
        Me.lblTotal.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.lblTotal.Appearance.Options.UseFont = True
        Me.lblTotal.Location = New System.Drawing.Point(165, 20)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(29, 16)
        Me.lblTotal.TabIndex = 21
        Me.lblTotal.Text = "Total"
        '
        'SidePanel3
        '
        Me.SidePanel3.Controls.Add(Me.GridControl1)
        Me.SidePanel3.Controls.Add(Me.lvwLogList)
        Me.SidePanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SidePanel3.Location = New System.Drawing.Point(0, 0)
        Me.SidePanel3.Name = "SidePanel3"
        Me.SidePanel3.Size = New System.Drawing.Size(884, 302)
        Me.SidePanel3.TabIndex = 2
        Me.SidePanel3.Text = "SidePanel3"
        '
        'tcpServer1
        '
        Me.tcpServer1.Encoding = CType(resources.GetObject("tcpServer1.Encoding"), System.Text.Encoding)
        Me.tcpServer1.IdleTime = 5
        Me.tcpServer1.IsOpen = False
        Me.tcpServer1.MaxCallbackThreads = 1000
        Me.tcpServer1.MaxSendAttempts = 3
        Me.tcpServer1.Port = 6666
        Me.tcpServer1.VerifyConnectionInterval = 0
        '
        'XtraRealTimePunches
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(884, 361)
        Me.Controls.Add(Me.SidePanel3)
        Me.Controls.Add(Me.SidePanel2)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.Name = "XtraRealTimePunches"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Real Time Monitor"
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SidePanel2.ResumeLayout(False)
        Me.SidePanel2.PerformLayout()
        CType(Me.AxRealSvrOcxTcp1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SidePanel3.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SidePanel2 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents AxRealSvrOcxTcp1 As AxRealSvrOcxTcpLib.AxRealSvrOcxTcp
    Friend WithEvents SimpleButtonClear As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents lblTotal As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents lvwLogList As System.Windows.Forms.ListView
    Friend WithEvents SidePanel3 As DevExpress.XtraEditors.SidePanel
    Private WithEvents tcpServer1 As tcpServer.TcpServer
End Class
