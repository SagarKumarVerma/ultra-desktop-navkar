﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraHomeNew
    Inherits DevExpress.XtraEditors.XtraUserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim TileItemElement1 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(XtraHomeNew))
        Dim SuperToolTip1 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem1 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim TileItemElement2 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Dim SuperToolTip2 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem2 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim TileItemElement3 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Dim SuperToolTip3 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem3 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim TileItemElement4 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Dim SuperToolTip4 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem4 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim TileItemElement5 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Dim SuperToolTip5 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem5 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim TileItemElement6 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Dim SuperToolTip6 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem6 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim TileItemElement7 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Dim TileItemElement8 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Dim Series1 As DevExpress.XtraCharts.Series = New DevExpress.XtraCharts.Series()
        Dim PieSeriesLabel1 As DevExpress.XtraCharts.PieSeriesLabel = New DevExpress.XtraCharts.PieSeriesLabel()
        Dim PieSeriesView1 As DevExpress.XtraCharts.PieSeriesView = New DevExpress.XtraCharts.PieSeriesView()
        Me.TileControl1 = New DevExpress.XtraEditors.TileControl()
        Me.TileGroup1 = New DevExpress.XtraEditors.TileGroup()
        Me.TileItemTotal = New DevExpress.XtraEditors.TileItem()
        Me.TileItemWO = New DevExpress.XtraEditors.TileItem()
        Me.TileGroup2 = New DevExpress.XtraEditors.TileGroup()
        Me.TileItemPresent = New DevExpress.XtraEditors.TileItem()
        Me.TileItemLeave = New DevExpress.XtraEditors.TileItem()
        Me.TileGroup4 = New DevExpress.XtraEditors.TileGroup()
        Me.TileItemAbsent = New DevExpress.XtraEditors.TileItem()
        Me.TileItemLate = New DevExpress.XtraEditors.TileItem()
        Me.TileItem1 = New DevExpress.XtraEditors.TileItem()
        Me.TileItem2 = New DevExpress.XtraEditors.TileItem()
        Me.SidePanel1 = New DevExpress.XtraEditors.SidePanel()
        Me.SidePanel3 = New DevExpress.XtraEditors.SidePanel()
        Me.ChartControl1 = New DevExpress.XtraCharts.ChartControl()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.lvwLogList = New System.Windows.Forms.ListView()
        Me.TimerRefreshDashBoard = New System.Windows.Forms.Timer(Me.components)
        Me.SidePanel1.SuspendLayout()
        Me.SidePanel3.SuspendLayout()
        CType(Me.ChartControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Series1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(PieSeriesLabel1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(PieSeriesView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TileControl1
        '
        Me.TileControl1.Cursor = System.Windows.Forms.Cursors.Default
        Me.TileControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.TileControl1.Groups.Add(Me.TileGroup1)
        Me.TileControl1.Groups.Add(Me.TileGroup2)
        Me.TileControl1.Groups.Add(Me.TileGroup4)
        Me.TileControl1.Location = New System.Drawing.Point(0, 0)
        Me.TileControl1.LookAndFeel.SkinName = "iMaginary"
        Me.TileControl1.LookAndFeel.UseDefaultLookAndFeel = False
        Me.TileControl1.MaxId = 8
        Me.TileControl1.Name = "TileControl1"
        Me.TileControl1.Size = New System.Drawing.Size(1432, 285)
        Me.TileControl1.TabIndex = 0
        Me.TileControl1.Text = "TileControl1"
        '
        'TileGroup1
        '
        Me.TileGroup1.Items.Add(Me.TileItemTotal)
        Me.TileGroup1.Items.Add(Me.TileItemWO)
        Me.TileGroup1.Name = "TileGroup1"
        Me.TileGroup1.Text = "TileGroup1"
        '
        'TileItemTotal
        '
        Me.TileItemTotal.AppearanceItem.Normal.Font = New System.Drawing.Font("Tahoma", 25.0!)
        Me.TileItemTotal.AppearanceItem.Normal.ForeColor = System.Drawing.Color.White
        Me.TileItemTotal.AppearanceItem.Normal.Options.UseFont = True
        Me.TileItemTotal.AppearanceItem.Normal.Options.UseForeColor = True
        TileItemElement1.Image = CType(resources.GetObject("TileItemElement1.Image"), System.Drawing.Image)
        TileItemElement1.ImageBorder = DevExpress.XtraEditors.TileItemElementImageBorderMode.None
        TileItemElement1.ImageLocation = New System.Drawing.Point(-12, 12)
        TileItemElement1.Text = ""
        TileItemElement1.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleRight
        Me.TileItemTotal.Elements.Add(TileItemElement1)
        Me.TileItemTotal.Id = 0
        Me.TileItemTotal.ItemSize = DevExpress.XtraEditors.TileItemSize.Wide
        Me.TileItemTotal.Name = "TileItemTotal"
        ToolTipTitleItem1.Text = "Click to go to Employee Master"
        SuperToolTip1.Items.Add(ToolTipTitleItem1)
        Me.TileItemTotal.SuperTip = SuperToolTip1
        '
        'TileItemWO
        '
        Me.TileItemWO.AppearanceItem.Normal.Font = New System.Drawing.Font("Tahoma", 25.0!)
        Me.TileItemWO.AppearanceItem.Normal.ForeColor = System.Drawing.Color.White
        Me.TileItemWO.AppearanceItem.Normal.Options.UseFont = True
        Me.TileItemWO.AppearanceItem.Normal.Options.UseForeColor = True
        TileItemElement2.Image = CType(resources.GetObject("TileItemElement2.Image"), System.Drawing.Image)
        TileItemElement2.ImageLocation = New System.Drawing.Point(-12, 12)
        TileItemElement2.Text = ""
        TileItemElement2.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleRight
        Me.TileItemWO.Elements.Add(TileItemElement2)
        Me.TileItemWO.Id = 3
        Me.TileItemWO.ItemSize = DevExpress.XtraEditors.TileItemSize.Wide
        Me.TileItemWO.Name = "TileItemWO"
        ToolTipTitleItem2.Text = "Click to get today's Week Off Report"
        SuperToolTip2.Items.Add(ToolTipTitleItem2)
        Me.TileItemWO.SuperTip = SuperToolTip2
        '
        'TileGroup2
        '
        Me.TileGroup2.Items.Add(Me.TileItemPresent)
        Me.TileGroup2.Items.Add(Me.TileItemLeave)
        Me.TileGroup2.Name = "TileGroup2"
        '
        'TileItemPresent
        '
        Me.TileItemPresent.AppearanceItem.Normal.Font = New System.Drawing.Font("Tahoma", 25.0!)
        Me.TileItemPresent.AppearanceItem.Normal.ForeColor = System.Drawing.Color.White
        Me.TileItemPresent.AppearanceItem.Normal.Options.UseFont = True
        Me.TileItemPresent.AppearanceItem.Normal.Options.UseForeColor = True
        TileItemElement3.Image = CType(resources.GetObject("TileItemElement3.Image"), System.Drawing.Image)
        TileItemElement3.ImageLocation = New System.Drawing.Point(-12, 12)
        TileItemElement3.Text = ""
        TileItemElement3.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleRight
        Me.TileItemPresent.Elements.Add(TileItemElement3)
        Me.TileItemPresent.Id = 4
        Me.TileItemPresent.ItemSize = DevExpress.XtraEditors.TileItemSize.Wide
        Me.TileItemPresent.Name = "TileItemPresent"
        ToolTipTitleItem3.Text = "Click to get today's Present Report"
        SuperToolTip3.Items.Add(ToolTipTitleItem3)
        Me.TileItemPresent.SuperTip = SuperToolTip3
        '
        'TileItemLeave
        '
        Me.TileItemLeave.AppearanceItem.Normal.Font = New System.Drawing.Font("Tahoma", 25.0!)
        Me.TileItemLeave.AppearanceItem.Normal.Options.UseFont = True
        TileItemElement4.Image = CType(resources.GetObject("TileItemElement4.Image"), System.Drawing.Image)
        TileItemElement4.ImageLocation = New System.Drawing.Point(-12, 12)
        TileItemElement4.Text = ""
        TileItemElement4.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleRight
        Me.TileItemLeave.Elements.Add(TileItemElement4)
        Me.TileItemLeave.Id = 5
        Me.TileItemLeave.ItemSize = DevExpress.XtraEditors.TileItemSize.Wide
        Me.TileItemLeave.Name = "TileItemLeave"
        ToolTipTitleItem4.Text = "Click to get today's Leave Report"
        SuperToolTip4.Items.Add(ToolTipTitleItem4)
        Me.TileItemLeave.SuperTip = SuperToolTip4
        '
        'TileGroup4
        '
        Me.TileGroup4.Items.Add(Me.TileItemAbsent)
        Me.TileGroup4.Items.Add(Me.TileItemLate)
        Me.TileGroup4.Name = "TileGroup4"
        '
        'TileItemAbsent
        '
        Me.TileItemAbsent.AppearanceItem.Normal.Font = New System.Drawing.Font("Tahoma", 25.0!)
        Me.TileItemAbsent.AppearanceItem.Normal.Options.UseFont = True
        TileItemElement5.Image = CType(resources.GetObject("TileItemElement5.Image"), System.Drawing.Image)
        TileItemElement5.ImageLocation = New System.Drawing.Point(-12, 12)
        TileItemElement5.Text = ""
        TileItemElement5.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleRight
        Me.TileItemAbsent.Elements.Add(TileItemElement5)
        Me.TileItemAbsent.Id = 6
        Me.TileItemAbsent.ItemSize = DevExpress.XtraEditors.TileItemSize.Wide
        Me.TileItemAbsent.Name = "TileItemAbsent"
        ToolTipTitleItem5.Text = "Click to get today's Absent Report"
        SuperToolTip5.Items.Add(ToolTipTitleItem5)
        Me.TileItemAbsent.SuperTip = SuperToolTip5
        '
        'TileItemLate
        '
        Me.TileItemLate.AppearanceItem.Normal.Font = New System.Drawing.Font("Tahoma", 25.0!)
        Me.TileItemLate.AppearanceItem.Normal.Options.UseFont = True
        TileItemElement6.Image = CType(resources.GetObject("TileItemElement6.Image"), System.Drawing.Image)
        TileItemElement6.ImageLocation = New System.Drawing.Point(-12, 12)
        TileItemElement6.Text = ""
        TileItemElement6.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleRight
        Me.TileItemLate.Elements.Add(TileItemElement6)
        Me.TileItemLate.Id = 7
        Me.TileItemLate.ItemSize = DevExpress.XtraEditors.TileItemSize.Wide
        Me.TileItemLate.Name = "TileItemLate"
        ToolTipTitleItem6.Text = "Click to get today's Late Report"
        SuperToolTip6.Items.Add(ToolTipTitleItem6)
        Me.TileItemLate.SuperTip = SuperToolTip6
        '
        'TileItem1
        '
        TileItemElement7.Text = "TileItem1"
        Me.TileItem1.Elements.Add(TileItemElement7)
        Me.TileItem1.Id = 4
        Me.TileItem1.ItemSize = DevExpress.XtraEditors.TileItemSize.Wide
        Me.TileItem1.Name = "TileItem1"
        '
        'TileItem2
        '
        TileItemElement8.Text = "TileItem2"
        Me.TileItem2.Elements.Add(TileItemElement8)
        Me.TileItem2.Id = 5
        Me.TileItem2.ItemSize = DevExpress.XtraEditors.TileItemSize.Wide
        Me.TileItem2.Name = "TileItem2"
        '
        'SidePanel1
        '
        Me.SidePanel1.Controls.Add(Me.SidePanel3)
        Me.SidePanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SidePanel1.Location = New System.Drawing.Point(0, 285)
        Me.SidePanel1.Name = "SidePanel1"
        Me.SidePanel1.Size = New System.Drawing.Size(1432, 238)
        Me.SidePanel1.TabIndex = 1
        Me.SidePanel1.Text = "SidePanel1"
        '
        'SidePanel3
        '
        Me.SidePanel3.Controls.Add(Me.ChartControl1)
        Me.SidePanel3.Controls.Add(Me.GridControl1)
        Me.SidePanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SidePanel3.Location = New System.Drawing.Point(0, 0)
        Me.SidePanel3.Name = "SidePanel3"
        Me.SidePanel3.Size = New System.Drawing.Size(1432, 238)
        Me.SidePanel3.TabIndex = 23
        Me.SidePanel3.Text = "SidePanel3"
        '
        'ChartControl1
        '
        Me.ChartControl1.DataBindings = Nothing
        Me.ChartControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ChartControl1.Legend.Name = "Default Legend"
        Me.ChartControl1.Location = New System.Drawing.Point(749, 0)
        Me.ChartControl1.LookAndFeel.SkinName = "iMaginary"
        Me.ChartControl1.LookAndFeel.UseDefaultLookAndFeel = False
        Me.ChartControl1.Name = "ChartControl1"
        PieSeriesLabel1.TextPattern = "{A} {V}"
        Series1.Label = PieSeriesLabel1
        Series1.LegendName = "Default Legend"
        Series1.Name = "Series1"
        Series1.View = PieSeriesView1
        Me.ChartControl1.SeriesSerializable = New DevExpress.XtraCharts.Series() {Series1}
        Me.ChartControl1.Size = New System.Drawing.Size(683, 238)
        Me.ChartControl1.TabIndex = 2
        '
        'GridControl1
        '
        Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Left
        Me.GridControl1.EmbeddedNavigator.Buttons.Append.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.Edit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.Remove.Visible = False
        Me.GridControl1.Location = New System.Drawing.Point(0, 0)
        Me.GridControl1.LookAndFeel.SkinName = "iMaginary"
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(749, 238)
        Me.GridControl1.TabIndex = 1
        Me.GridControl1.UseEmbeddedNavigator = True
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsView.ShowGroupPanel = False
        '
        'lvwLogList
        '
        Me.lvwLogList.Location = New System.Drawing.Point(1148, 103)
        Me.lvwLogList.Name = "lvwLogList"
        Me.lvwLogList.Size = New System.Drawing.Size(284, 229)
        Me.lvwLogList.TabIndex = 4
        Me.lvwLogList.UseCompatibleStateImageBehavior = False
        Me.lvwLogList.View = System.Windows.Forms.View.Details
        Me.lvwLogList.Visible = False
        '
        'TimerRefreshDashBoard
        '
        Me.TimerRefreshDashBoard.Enabled = True
        Me.TimerRefreshDashBoard.Interval = 60000
        '
        'XtraHomeNew
        '
        Me.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.Appearance.Options.UseBackColor = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.SidePanel1)
        Me.Controls.Add(Me.TileControl1)
        Me.Controls.Add(Me.lvwLogList)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.Name = "XtraHomeNew"
        Me.Size = New System.Drawing.Size(1432, 523)
        Me.SidePanel1.ResumeLayout(False)
        Me.SidePanel3.ResumeLayout(False)
        CType(PieSeriesLabel1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(PieSeriesView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Series1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ChartControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TileControl1 As DevExpress.XtraEditors.TileControl
    Friend WithEvents TileGroup1 As DevExpress.XtraEditors.TileGroup
    Friend WithEvents TileItemTotal As DevExpress.XtraEditors.TileItem
    Friend WithEvents TileItemWO As DevExpress.XtraEditors.TileItem
    Friend WithEvents TileGroup2 As DevExpress.XtraEditors.TileGroup
    Friend WithEvents TileItemPresent As DevExpress.XtraEditors.TileItem
    Friend WithEvents TileItem1 As DevExpress.XtraEditors.TileItem
    Friend WithEvents TileItemLeave As DevExpress.XtraEditors.TileItem
    Friend WithEvents TileItem2 As DevExpress.XtraEditors.TileItem
    Friend WithEvents TileGroup4 As DevExpress.XtraEditors.TileGroup
    Friend WithEvents TileItemAbsent As DevExpress.XtraEditors.TileItem
    Friend WithEvents TileItemLate As DevExpress.XtraEditors.TileItem
    Friend WithEvents SidePanel1 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents lvwLogList As System.Windows.Forms.ListView
    Friend WithEvents SidePanel3 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents ChartControl1 As DevExpress.XtraCharts.ChartControl
    Friend WithEvents TimerRefreshDashBoard As System.Windows.Forms.Timer
    Public WithEvents GridControl1 As DevExpress.XtraGrid.GridControl

End Class
