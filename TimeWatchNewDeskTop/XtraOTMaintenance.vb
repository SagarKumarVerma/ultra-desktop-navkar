﻿Imports System.Data.SqlClient
Imports DevExpress.LookAndFeel
Imports System.Data.OleDb
Imports DevExpress.XtraEditors
Imports DevExpress.XtraGrid.Views.Base
Imports System.Text

Public Class XtraOTMaintenance
    Dim ulf As UserLookAndFeel
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Dim datamaintain As Boolean = True
    Public Shared DateEditAtt As String
    Public Shared paycodeformaintaince As String
    Public Shared cardnoformaintaince As String
    Public Shared nameformaintaince As String
    Dim g_WhereClause As String = ""
    Dim WhichReport As Integer
    Public Sub New()
        InitializeComponent()
        If Common.servername = "Access" Then
            'Me.LeaveApplication1TableAdapter1.Fill(Me.SSSDBDataSet.LeaveApplication1)
            'GridControl1.DataSource = SSSDBDataSet.EmployeeGroup1
        Else
            'LeaveApplicationTableAdapter.Connection.ConnectionString = Common.ConnectionString '"Data Source=DESKTOP-PG6NDV8;Initial Catalog=SSSDB;Integrated Security=True"
            'Me.LeaveApplicationTableAdapter.Fill(Me.SSSDBDataSet.LeaveApplication)
            'GridControl1.DataSource = SSSDBDataSet.EmployeeGroup
            TblEmployeeTableAdapter.Connection.ConnectionString = Common.ConnectionString
        End If
        Common.SetGridFont(GridView1, New Font("Tahoma", 11))
        Common.SetGridFont(GridViewComp, New System.Drawing.Font("Tahoma", 10))
        Common.SetGridFont(GridViewEmp, New System.Drawing.Font("Tahoma", 10))
        Common.SetGridFont(GridViewCat, New System.Drawing.Font("Tahoma", 10))
        Common.SetGridFont(GridViewDept, New System.Drawing.Font("Tahoma", 10))
        Common.SetGridFont(GridViewGrade, New System.Drawing.Font("Tahoma", 10))
        Common.SetGridFont(GridViewBranch, New System.Drawing.Font("Tahoma", 10))
    End Sub
    Private Sub XtraDataMaintenance_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Me.Width = Common.NavWidth 'Me.Parent.Width
        Me.Height = Common.NavHeight 'Me.Parent.Height
        'SplitContainerControl1.Width = Common.splitforMasterMenuWidth 'SplitContainerControl1.Parent.Width
        SplitContainerControl1.SplitterPosition = Common.SplitterPosition '(SplitContainerControl1.Parent.Width) * 85 / 100

        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True
        'If Common.servername = "Access" Then
        '    Me.TblEmployee1TableAdapter1.Fill(Me.SSSDBDataSet.TblEmployee1)
        '    LookUpEdit1.Properties.DataSource = SSSDBDataSet.TblEmployee1
        'Else
        '    Me.TblEmployeeTableAdapter.Fill(Me.SSSDBDataSet.TblEmployee)
        '    LookUpEdit1.Properties.DataSource = SSSDBDataSet.TblEmployee
        'End If
        'LookUpEdit1.Properties.DataSource = Common.EmpNonAdmin
        setDefault()
    End Sub
    Private Sub setDefault()
        GridViewComp.ClearSelection()

        GridControlEmp.DataSource = Common.EmpNonAdmin
        GridControlBranch.DataSource = Common.LocationNonAdmin
        GridControlComp.DataSource = Common.CompanyNonAdmin

        If Common.servername = "Access" Then
            Me.TblDepartment1TableAdapter1.Fill(Me.SSSDBDataSet.tblDepartment1)
            GridControlDept.DataSource = SSSDBDataSet.tblDepartment1

            Me.TblCatagory1TableAdapter1.Fill(Me.SSSDBDataSet.tblCatagory1)
            GridControlCat.DataSource = SSSDBDataSet.tblCatagory1

            Me.TblGrade1TableAdapter1.Fill(Me.SSSDBDataSet.tblGrade1)
            GridControlGrade.DataSource = SSSDBDataSet.tblGrade1
        Else
            TblDepartmentTableAdapter.Connection.ConnectionString = Common.ConnectionString
            Me.TblDepartmentTableAdapter.Fill(Me.SSSDBDataSet.tblDepartment)
            GridControlDept.DataSource = SSSDBDataSet.tblDepartment

            TblCatagoryTableAdapter.Connection.ConnectionString = Common.ConnectionString
            Me.TblCatagoryTableAdapter.Fill(Me.SSSDBDataSet.tblCatagory)
            GridControlCat.DataSource = SSSDBDataSet.tblCatagory

            TblGradeTableAdapter.Connection.ConnectionString = Common.ConnectionString
            Me.TblGradeTableAdapter.Fill(Me.SSSDBDataSet.tblGrade)
            GridControlGrade.DataSource = SSSDBDataSet.tblGrade
        End If
        DateEdit1.DateTime = New DateTime(DateTime.Now.Year, DateTime.Now.Month, 1)
        DateEdit2.EditValue = Now
        TextEdit2.Text = ""
    End Sub
    Private Sub setSelectValue()
        If Not datamaintain Then Return
        datamaintain = False

        Dim adap, adap1 As SqlDataAdapter
        Dim adapA, adapA1 As OleDbDataAdapter
        Dim ds As DataSet = New DataSet
        Dim ds1 As DataSet

        'If LookUpEdit1.EditValue.ToString.Trim <> "" Then
        '            Dim sSql1 As String = "select EMPNAME, PRESENTCARDNO, (select GroupName from EmployeeGroup where EmployeeGroup.GroupId = TblEmployee.EmployeeGroupId) as EmpGrp, (select DEPARTMENTNAME from tblDepartment where tblDepartment.DEPARTMENTCODE = TblEmployee.DEPARTMENTCODE) as DEPName, (select CATAGORYNAME from tblCatagory WHERE tblCatagory.CAT = TblEmployee.CAT) as CATName, DESIGNATION  from TblEmployee where PAYCODE = '" & LookUpEdit1.EditValue.ToString.Trim & "'"
        '            sSql1 = "select EMPNAME, PRESENTCARDNO, " &
        '"(SELECT COMPANYNAME from tblCompany WHERE tblCompany.COMPANYCODE = TblEmployee.COMPANYCODE) as CompName, " &
        '"(select GroupName from EmployeeGroup where EmployeeGroup.GroupId = TblEmployee.EmployeeGroupId) as EmpGrp," &
        '"(select DEPARTMENTNAME from tblDepartment where tblDepartment.DEPARTMENTCODE = TblEmployee.DEPARTMENTCODE) as DEPName, " &
        '"(select CATAGORYNAME from tblCatagory WHERE tblCatagory.CAT = TblEmployee.CAT) as CATName, " &
        '"(select GradeName from tblGrade WHERE tblGrade.GradeCode = TblEmployee.GradeCode) as GrdName, DESIGNATION  from TblEmployee where PAYCODE = '" & LookUpEdit1.EditValue.ToString.Trim & "'"
        '            If Common.servername = "Access" Then
        '                sSql1 = "select T.EMPNAME, T.PRESENTCARDNO, C.COMPANYNAME  as CompName,  T.DESIGNATION, E.GroupName as EmpGrp, D.DEPARTMENTNAME as DEPName, cat.CATAGORYNAME as CATName, grd.GradeName as GrdName  from TblEmployee T,  tblCompany C, EmployeeGroup E, tblDepartment D, tblCatagory cat, tblGrade grd where C.COMPANYCODE = T.COMPANYCODE  and E.GroupId = T.EmployeeGroupId and D.DEPARTMENTCODE = T.DEPARTMENTCODE and cat.CAT = T.CAT and grd.GradeCode = T.GradeCode and T.Paycode='" & LookUpEdit1.EditValue.ToString.Trim & "'"
        '                adapA1 = New OleDbDataAdapter(sSql1, Common.con1)
        '                ds1 = New DataSet
        '                adapA1.Fill(ds1)
        '            Else
        '                adap1 = New SqlDataAdapter(sSql1, Common.con)
        '                ds1 = New DataSet
        '                adap1.Fill(ds1)
        '            End If
        Dim strsortorder As String = ""
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim strsql As String = ""
        If Common.servername = "Access" Then
            strsql = "select tbltimeregister.ManOTDuration,tbltimeregister.OTApprove,tblCatagory.CatagoryName, tblDivision.DivisionName, tbltimeregister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   TblTimeRegister.DateOffice, tblTimeRegister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName," &
       " tblTimeRegister.In1,tblTimeRegister.Out1,tblTimeRegister.In2,tblTimeRegister.Out2,tblTimeRegister.HoursWorked,tblTimeRegister.Status,tblTimeRegister.LATEARRIVAL,tblTimeRegister.EarlyDeparture,tblTimeRegister.SHIFTATTENDED," &
       " tblTimeRegister.OtDuration,tblTimeRegister.OtAmount," &
       " tblTimeRegister.PresentValue,tblTimeRegister.AbsentValue,tblTimeRegister.LeaveValue,tblTimeRegister.Holiday_Value,tblTimeRegister.Wo_Value, tbltimeregister.REASON " &
       " from tblCatagory,tblDivision,  tblTimeRegister,tblEmployee,tblCompany ,tblDepartment" &
       " Where tblCatagory.Cat = tblEmployee.Cat And tblTimeRegister.DateOffice Between #" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "# AND #" & DateEdit2.DateTime.ToString("yyyy-MM-dd") & "#   AND (tblTimeRegister.PayCode = tblEmployee.PayCode)" &
       " And tblEmployee.CompanyCode = tblcompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode &
       " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & strsortorder & ",tblTimeRegister.DateOffice"
            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(ds1)
        Else
            strsql = "select tbltimeregister.ManOTDuration,tbltimeregister.OTApprove,tblCatagory.CatagoryName, tblDivision.DivisionName, tbltimeregister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   TblTimeRegister.DateOffice, tblTimeRegister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName," &
        " tblTimeRegister.In1,tblTimeRegister.Out1,tblTimeRegister.In2,tblTimeRegister.Out2,tblTimeRegister.HoursWorked,tblTimeRegister.Status,tblTimeRegister.LATEARRIVAL,tblTimeRegister.EarlyDeparture,tblTimeRegister.SHIFTATTENDED," &
        " tblTimeRegister.OtDuration,tblTimeRegister.OtAmount," &
        " tblTimeRegister.PresentValue,tblTimeRegister.AbsentValue,tblTimeRegister.LeaveValue,tblTimeRegister.Holiday_Value,tblTimeRegister.Wo_Value,  tbltimeregister.REASON " &
        " from tblCatagory,tblDivision,  tblTimeRegister,tblEmployee,tblCompany ,tblDepartment" &
        " Where tblCatagory.Cat = tblEmployee.Cat And tblTimeRegister.DateOffice Between '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' AND '" & DateEdit2.DateTime.ToString("yyyy-MM-dd") & "' AND (tblTimeRegister.PayCode = tblEmployee.PayCode)" &
        " And tblEmployee.CompanyCode = tblcompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode &
        " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & strsortorder & ",tblTimeRegister.DateOffice"
            ' " And (tblemployee.LeavingDate>='" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' or tblemployee.LeavingDate is null)" & _               
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(ds1)
        End If

        If ds1.Tables(0).Rows.Count = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>No Data Available </size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            setDefault()
            datamaintain = True
            Exit Sub
        Else

        End If
        'End If
        datamaintain = True
    End Sub
    Private Sub LookUpEdit1_Leave(sender As System.Object, e As System.EventArgs)
        'setSelectValue()
        setGridData()
    End Sub
    Private Sub setGridData()
        Me.Cursor = Cursors.WaitCursor
        'Dim dt As DataTable = New DataTable
        'dt.Columns.Add("Date")
        'dt.Columns.Add("Shift")
        'dt.Columns.Add("Status")
        'dt.Columns.Add("Late Arr")
        'dt.Columns.Add("Earlt Dep")
        'dt.Columns.Add("In Time")
        'dt.Columns.Add("Lunch Out")
        'dt.Columns.Add("Lunch In")
        'dt.Columns.Add("Out Time")
        'dt.Columns.Add("Over Time")
        'Dim startdate As DateTime
        'Dim enddate As DateTime
        'If Common.IsNepali = "Y" Then
        '    Dim DC As New DateConverter()
        '    Try
        '        'Dim tmp As DateTime = DC.ToAD(New Date(ComboBoxEditNepaliYear.EditValue, ComboBoxEditNEpaliMonth.SelectedIndex + 1, 1))
        '        Dim tmp As DateTime = DC.ToAD(ComboBoxEditNepaliYear.EditValue & "-" & ComboBoxEditNEpaliMonth.SelectedIndex + 1 & "-" & 1)
        '        TextEdit1.Text = tmp.Year
        '        ComboBoxEdit2.SelectedIndex = tmp.Month
        '        'startdate = DC.ToAD(New Date(ComboBoxEditNepaliYear.EditValue, ComboBoxEditNEpaliMonth.SelectedIndex + 1, 1)) 'Convert.ToDateTime(tmp.Year & "-" & tmp.Month & "-01 00:00:00")
        '        startdate = DC.ToAD(ComboBoxEditNepaliYear.EditValue & "-" & ComboBoxEditNEpaliMonth.SelectedIndex + 1 & "-" & 1)
        '        enddate = startdate.AddMonths(1) 'DC.ToAD(New Date(ComboBoxEditNepaliYear.EditValue, ComboBoxEditNEpaliMonth.SelectedIndex + 2, 1)) 'Convert.ToDateTime(tmp.Year & "-" & tmp.Month + 1 & "-01 00:00:00")
        '        '
        '    Catch ex As Exception
        '        XtraMessageBox.Show(ulf, "<size=10>Invalid Process From Date</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        '        ComboBoxEditNepaliYear.Select()
        '        Exit Sub
        '    End Try
        'Else
        '    'MsgBox(TextEdit1.Text.Trim & "-" & ComboBoxEdit2.SelectedIndex + 1 & "-01 00:00:00")
        '    startdate = Convert.ToDateTime(TextEdit1.Text.Trim & "-" & ComboBoxEdit2.SelectedIndex + 1 & "-01 00:00:00")
        '    'enddate = Convert.ToDateTime(TextEdit1.Text.Trim & "-" & ComboBoxEdit2.SelectedIndex + 1 & "-01 00:00:00")
        '    'MsgBox(startdate.ToString("yyyy-MM-dd HH:mm:ss"))
        '    enddate = startdate.AddDays(System.DateTime.DaysInMonth(TextEdit1.Text.Trim, ComboBoxEdit2.SelectedIndex + 1)) '.AddDays(-1)
        'End If
        Set_Filter_Crystal()
        Dim strsortorder As String = ""
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim gridtblregisterselet As String = ""
        If Common.servername = "Access" Then
            gridtblregisterselet = "select tbltimeregister.ManOTDuration,tbltimeregister.OTApprove,tblCatagory.CatagoryName, tblDivision.DivisionName, tbltimeregister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   TblTimeRegister.DateOffice, tblTimeRegister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName," &
           " tblTimeRegister.In1,tblTimeRegister.Out1,tblTimeRegister.In2,tblTimeRegister.Out2,tblTimeRegister.HOURSWORKED,tblTimeRegister.Status,tblTimeRegister.LATEARRIVAL,tblTimeRegister.EarlyDeparture,tblTimeRegister.SHIFTATTENDED," &
           " tblTimeRegister.OtDuration,tblTimeRegister.OtAmount," &
           " tblTimeRegister.PresentValue,tblTimeRegister.AbsentValue,tblTimeRegister.LeaveValue,tblTimeRegister.Holiday_Value,tblTimeRegister.Wo_Value, tbltimeregister.REASON,tblTimeRegister.OutWorkDuration, tblTimeRegister.OTApprove, tblTimeRegister.ManOTDuration " &
           " from tblCatagory,tblDivision,  tblTimeRegister,tblEmployee,tblCompany ,tblDepartment" &
           " Where tblCatagory.Cat = tblEmployee.Cat And tblTimeRegister.DateOffice Between #" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "# AND #" & DateEdit2.DateTime.ToString("yyyy-MM-dd") & "#   AND (tblTimeRegister.PayCode = tblEmployee.PayCode)" &
           " And tblEmployee.CompanyCode = tblcompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode &
           " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause)
            ' "select DateOFFICE, SHIFT, STATUS, LATEARRIVAL, EARLYDEPARTURE, IN1, OUT1, IN2, OUT2, OTDURATION ,[ManOTDuration],[OTApprove]  from tblTimeRegister where PAYCODE='" & LookUpEdit1.EditValue.ToString.Trim & "' and FORMAT(DateOFFICE,'yyyy-MM-dd 00:00:00') >='" & startdate.ToString("yyyy-MM-dd HH:mm:ss") & "' and FORMAT(DateOFFICE,'yyyy-MM-dd 00:00:00') < '" & enddate.ToString("yyyy-MM-dd HH:mm:ss") & "' order	BY DateOFFICE ASC "
        Else
            gridtblregisterselet = "select tbltimeregister.ManOTDuration,tbltimeregister.OTApprove,tblCatagory.CatagoryName, tblDivision.DivisionName, tbltimeregister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   TblTimeRegister.DateOffice, tblTimeRegister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName," &
            " tblTimeRegister.In1,tblTimeRegister.Out1,tblTimeRegister.In2,tblTimeRegister.Out2,tblTimeRegister.HOURSWORKED,tblTimeRegister.Status,tblTimeRegister.LATEARRIVAL,tblTimeRegister.EarlyDeparture,tblTimeRegister.SHIFTATTENDED," &
            " tblTimeRegister.OtDuration,tblTimeRegister.OtAmount," &
            " tblTimeRegister.PresentValue,tblTimeRegister.AbsentValue,tblTimeRegister.LeaveValue,tblTimeRegister.Holiday_Value,tblTimeRegister.Wo_Value,  tbltimeregister.REASON,tblTimeRegister.OutWorkDuration, tblTimeRegister.OTApprove, tblTimeRegister.ManOTDuration " &
            " from tblCatagory,tblDivision,  tblTimeRegister,tblEmployee,tblCompany ,tblDepartment" &
            " Where tblCatagory.Cat = tblEmployee.Cat And tblTimeRegister.DateOffice Between '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' AND '" & DateEdit2.DateTime.ToString("yyyy-MM-dd") & "' AND (tblTimeRegister.PayCode = tblEmployee.PayCode)" &
            " And tblEmployee.CompanyCode = tblcompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode &
            " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause)
            ' "select DateOFFICE, SHIFT, STATUS, LATEARRIVAL, EARLYDEPARTURE, IN1, OUT1, IN2, OUT2, OTDURATION ,[ManOTDuration],[OTApprove]  from tblTimeRegister where PAYCODE='" & LookUpEdit1.EditValue.ToString.Trim & "' and DateOFFICE >='" & startdate.ToString("yyyy-MM-dd HH:mm:ss") & "' and DateOFFICE <'" & enddate.ToString("yyyy-MM-dd HH:mm:ss") & "'"
        End If
        Dim WTDataTable As DataTable
        If Common.servername = "Access" Then
            Dim dataAdapter As New OleDbDataAdapter(gridtblregisterselet, Common.con1)
            WTDataTable = New DataTable("tblTimeRegister")
            dataAdapter.Fill(WTDataTable)
        Else
            Dim dataAdapter As New SqlClient.SqlDataAdapter(gridtblregisterselet, Common.con)
            WTDataTable = New DataTable("tblTimeRegister")
            dataAdapter.Fill(WTDataTable)
        End If
        GridControl1.DataSource = WTDataTable
        Me.Cursor = Cursors.Default
    End Sub
    Private Sub TextEdit1_Leave(sender As System.Object, e As System.EventArgs)
        'setSelectValue()
        setGridData()
    End Sub
    Private Sub ComboBoxEdit2_Leave(sender As System.Object, e As System.EventArgs)
        'setSelectValue()
        setGridData()
    End Sub
    Private Sub GridView1_EditFormShowing(sender As System.Object, e As DevExpress.XtraGrid.Views.Grid.EditFormShowingEventArgs) Handles GridView1.EditFormShowing
        'If Common.DataMaintenance = "N" Then
        '    e.Allow = False
        '    Exit Sub
        'End If
        'Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
        'DateEditAtt = row("DateOFFICE").ToString.Trim
        'paycodeformaintaince = LookUpEdit1.EditValue.ToString.Trim
        'cardnoformaintaince = lblCardNum.Text.Trim
        'nameformaintaince = lblName.Text.Trim
        'e.Allow = False
        ''MsgBox(DateEditAtt)
        ''XtraDataMaintenanceEdit.ShowDialog()
        'setGridData()
    End Sub
    Private Sub GridView1_CustomColumnDisplayText(sender As System.Object, e As DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs) Handles GridView1.CustomColumnDisplayText
        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
        Dim view As ColumnView = TryCast(sender, ColumnView)
        'If e.Column.FieldName = "LATEARRIVAL" Then
        '    If row("LATEARRIVAL").ToString.Trim = "0" Or row("LATEARRIVAL").ToString.Trim = "" Then
        '        e.DisplayText = ""
        '    Else
        '        MsgBox(row("LATEARRIVAL").ToString.Trim)
        '        e.DisplayText = (Convert.ToInt32(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "LATEARRIVAL").ToString().Trim) \ 60).ToString("00") & ":" & (Convert.ToInt32(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "LATEARRIVAL").ToString().Trim) Mod 60).ToString("00")
        '    End If
        'End If
        If e.Column.FieldName = "LATEARRIVAL" Then
            If view.GetListSourceRowCellValue(e.ListSourceRowIndex, "LATEARRIVAL").ToString().Trim = "0" Or view.GetListSourceRowCellValue(e.ListSourceRowIndex, "LATEARRIVAL").ToString().Trim = "" Then
                e.DisplayText = ""
            Else
                'MsgBox(row("LATEARRIVAL").ToString.Trim)
                e.DisplayText = (Convert.ToInt32(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "LATEARRIVAL").ToString().Trim) \ 60).ToString("00") & ":" & (Convert.ToInt32(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "LATEARRIVAL").ToString().Trim) Mod 60).ToString("00")
            End If
        End If

        If e.Column.FieldName = "EARLYDEPARTURE" Then
            If view.GetListSourceRowCellValue(e.ListSourceRowIndex, "EARLYDEPARTURE").ToString().Trim = "0" Or view.GetListSourceRowCellValue(e.ListSourceRowIndex, "EARLYDEPARTURE").ToString().Trim = "" Then
                e.DisplayText = ""
            Else
                e.DisplayText = (Convert.ToInt32(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "EARLYDEPARTURE").ToString().Trim) \ 60).ToString("00") & ":" & (Convert.ToInt32(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "EARLYDEPARTURE").ToString().Trim) Mod 60).ToString("00")
            End If
        End If

        If e.Column.FieldName = "OtDuration" Then
            If view.GetListSourceRowCellValue(e.ListSourceRowIndex, "OtDuration").ToString().Trim = "0" Or view.GetListSourceRowCellValue(e.ListSourceRowIndex, "OtDuration").ToString().Trim = "" Then
                e.DisplayText = ""
            Else
                e.DisplayText = (Convert.ToInt32(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "OtDuration").ToString().Trim) \ 60).ToString("00") & ":" & (Convert.ToInt32(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "OtDuration").ToString().Trim) Mod 60).ToString("00")
            End If
        End If

        If e.Column.FieldName = "ManOTDuration" Then
            If view.GetListSourceRowCellValue(e.ListSourceRowIndex, "ManOTDuration").ToString().Trim = "0" Or view.GetListSourceRowCellValue(e.ListSourceRowIndex, "ManOTDuration").ToString().Trim = "" Then
                e.DisplayText = ""
            Else
                e.DisplayText = (Convert.ToInt32(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "ManOTDuration").ToString().Trim) \ 60).ToString("00") & ":" & (Convert.ToInt32(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "ManOTDuration").ToString().Trim) Mod 60).ToString("00")
            End If
        End If

        If e.Column.FieldName = "HOURSWORKED" Then
            If view.GetListSourceRowCellValue(e.ListSourceRowIndex, "HOURSWORKED").ToString().Trim = "0" Or view.GetListSourceRowCellValue(e.ListSourceRowIndex, "HOURSWORKED").ToString().Trim = "" Then
                e.DisplayText = ""
            Else
                e.DisplayText = (Convert.ToInt32(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "HOURSWORKED").ToString().Trim) \ 60).ToString("00") & ":" & (Convert.ToInt32(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "HOURSWORKED").ToString().Trim) Mod 60).ToString("00")
            End If
        End If

        If Common.IsNepali = "Y" Then
            'Me.Cursor = Cursors.WaitCursor
            'If e.Column.FieldName = "DateOFFICE" Then
            '    If row("DateOFFICE").ToString.Trim <> "" Then
            '        Dim DC As New DateConverter()
            '        Dim dt As DateTime = Convert.ToDateTime(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "DateOFFICE").ToString().Trim)
            '        Try
            '            Dim Vstart As String = DC.ToBS(New Date(dt.Year, dt.Month, dt.Day))
            '            Dim dojTmp() As String = Vstart.Split("-")
            '            e.DisplayText = dojTmp(2) & "-" & ComboBoxEditNEpaliMonth.Properties.Items(dojTmp(1) - 1).ToString & "-" & dojTmp(0)
            '        Catch ex As Exception
            '        End Try
            '    End If
            'End If
            'Me.Cursor = Cursors.Default
        End If
    End Sub
    Private Sub SimpleButtonExcel_Click(sender As System.Object, e As System.EventArgs)
        XtraRosterUpload.ShowDialog()
    End Sub

    Private Sub GridView1_FocusedRowChanged(sender As Object, e As FocusedRowChangedEventArgs) Handles GridView1.FocusedRowChanged
        Dim Mot As String = GridView1.GetRowCellValue(e.FocusedRowHandle, "ManOTDuration").ToString.Trim
        If Mot = "" Or Mot = "0" Then
            TextMOT.Text = "00:00"
        Else
            TextMOT.Text = Convert.ToInt32(Mot \ 60).ToString("00") & ":" & Convert.ToInt32(Mot Mod 60).ToString("00")
        End If
    End Sub

    Private Sub SimpleButtonAdd_Click(sender As Object, e As EventArgs) Handles SimpleButtonAdd.Click
        Dim tmp() As String = TextMOT.Text.Trim.Split(":")
        Dim mot As Integer = tmp(0) * 60 + tmp(1)
        GridView1.SetFocusedRowCellValue("ManOTDuration", mot)
    End Sub

    Private Sub SimpleButtonSave_Click(sender As Object, e As EventArgs) Handles SimpleButtonSave.Click
        Dim tmpcount As Integer = 0
        For i As Integer = 0 To GridView1.DataRowCount - 1
            Dim Mot As String = GridView1.GetRowCellValue(i, "ManOTDuration").ToString.Trim
            If Mot <> "" Then
                tmpcount = tmpcount + 1
                Dim paycode As String = GridView1.GetRowCellValue(i, "PayCode").ToString.Trim ' LookUpEdit1.EditValue.ToString.Trim
                Dim DateOFFICE As String = Convert.ToDateTime(GridView1.GetRowCellValue(i, "DateOffice").ToString.Trim).ToString("yyyy-MM-dd HH:mm:ss")
                Dim ssql As String = "update tblTimeRegister set ManOTDuration='" & Mot & "', OTApprove='Y' where paycode ='" & paycode & "' and DateOFFICE='" & DateOFFICE & "'"
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                Dim cmd As SqlCommand
                cmd = New SqlCommand(ssql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
        Next
        If tmpcount > 0 Then
            XtraMessageBox.Show(ulf, "<size=10>Saved Successfully</size>", "<size=9>iAS</size>")
        End If
    End Sub
    Private Sub Set_Filter_Crystal() 'Optional ByVal Selection As String
        'On Error GoTo ErrorGen
        'Dim i As Integer
        Dim mCompanyString As String, mDepartmentString As String, mCatagoryString As String
        Dim mEmployeeString As String, r_CompantStr As String, mShiftString As String, mLocationString As String
        Dim mDivisionString As String, mGradeString As String, mActive As String

        mCompanyString = ""
        mActive = ""
        mDepartmentString = ""
        mCatagoryString = ""
        mEmployeeString = ""
        mShiftString = ""
        mDivisionString = ""
        mGradeString = ""
        mLocationString = ""
        Dim g_CompanyNames As String = ""
        'For CompanyCode
        If GridViewComp.GetSelectedRows.Count = 0 Then
            GridViewComp.SelectAll()
        End If
        Dim selectedRowsComp As Integer() = GridViewComp.GetSelectedRows()
        Dim resultComp As Object() = New Object(selectedRowsComp.Length - 1) {}
        For i As Integer = 0 To selectedRowsComp.Length - 1
            Dim rowHandle As Integer = selectedRowsComp(i)
            If Not GridViewComp.IsGroupRow(rowHandle) Then
                resultComp(i) = GridViewComp.GetRowCellValue(rowHandle, "COMPANYCODE")
                If i <> 0 Then
                    mCompanyString = mCompanyString + " OR "
                End If
                mCompanyString = mCompanyString + "tblEmployee.CompanyCode = "
                mCompanyString = mCompanyString + "'" + resultComp(i) + "'"
                g_CompanyNames = g_CompanyNames & GridViewComp.GetRowCellValue(rowHandle, "COMPANYNAME") & ", "
            End If
        Next
        If GridViewComp.GetSelectedRows.Count = 0 Then
            GridViewComp.ClearSelection()
        End If
        CommonReport.g_CompanyNames = g_CompanyNames.Trim.TrimStart(",").TrimEnd(",")
        'For i = 0 To (LstCompanyTarget.ListCount - 1)
        '    LstCompanyTarget.ListIndex = i
        '    If i <> 0 Then
        '        mCompanyString = mCompanyString + " OR "
        '    End If
        '    mCompanyString = mCompanyString + "tblEmployee.CompanyCode = "
        '    mCompanyString = mCompanyString + "'" + Left(LstCompanyTarget.Text, 3) + "'"
        '    g_CompanyNames = g_CompanyNames & Trim(Mid(LstCompanyTarget.Text, 6)) & ", "
        'Next i

        'For DepartmentCode
        Dim selectedRowsDept As Integer() = GridViewDept.GetSelectedRows()
        Dim resultDept As Object() = New Object(selectedRowsDept.Length - 1) {}
        For i As Integer = 0 To selectedRowsDept.Length - 1
            Dim rowHandle As Integer = selectedRowsDept(i)
            If Not GridViewDept.IsGroupRow(rowHandle) Then
                resultDept(i) = GridViewDept.GetRowCellValue(rowHandle, "DEPARTMENTCODE")
                If i <> 0 Then
                    mDepartmentString = mDepartmentString + " OR "
                End If
                mDepartmentString = mDepartmentString + "tblEmployee.DepartmentCode = "
                mDepartmentString = mDepartmentString + "'" + resultDept(i) + "'"
            End If
        Next

        'For CatagoryCode
        Dim selectedRowsCat As Integer() = GridViewCat.GetSelectedRows()
        Dim resultCat As Object() = New Object(selectedRowsCat.Length - 1) {}
        For i As Integer = 0 To selectedRowsCat.Length - 1
            Dim rowHandle As Integer = selectedRowsCat(i)
            If Not GridViewCat.IsGroupRow(rowHandle) Then
                resultCat(i) = GridViewCat.GetRowCellValue(rowHandle, "CAT")
                If i <> 0 Then
                    mCatagoryString = mCatagoryString + " OR "
                End If
                mCatagoryString = mCatagoryString + "tblEmployee.CAT = "
                mCatagoryString = mCatagoryString + "'" + resultCat(i) + "'"
            End If
        Next

        ' For Grade Code
        Dim selectedRowsGrade As Integer() = GridViewGrade.GetSelectedRows()
        Dim resultGrade As Object() = New Object(selectedRowsGrade.Length - 1) {}
        For i As Integer = 0 To selectedRowsGrade.Length - 1
            Dim rowHandle As Integer = selectedRowsGrade(i)
            If Not GridViewGrade.IsGroupRow(rowHandle) Then
                resultGrade(i) = GridViewGrade.GetRowCellValue(rowHandle, "GradeCode")
                If i <> 0 Then
                    mGradeString = mGradeString + " OR "
                End If
                mGradeString = mGradeString + "tblEmployee.GradeCode = "
                mGradeString = mGradeString + "'" + resultGrade(i) + "'"
            End If
        Next

        'For EmployeeCode  - Check for late arrival
        Dim selectedRowsEmp As Integer() = GridViewEmp.GetSelectedRows()
        Dim resultEmp As Object() = New Object(selectedRowsEmp.Length - 1) {}
        For i As Integer = 0 To selectedRowsEmp.Length - 1
            Dim rowHandle As Integer = selectedRowsEmp(i)
            If Not GridViewEmp.IsGroupRow(rowHandle) Then
                resultEmp(i) = GridViewEmp.GetRowCellValue(rowHandle, "PAYCODE")
                Dim emp As String = resultEmp(i)
                If i <> 0 Then
                    mEmployeeString = mEmployeeString + " OR "
                End If
                mEmployeeString = mEmployeeString & "tblEmployee.Paycode  = "
                mEmployeeString = mEmployeeString & "'" & emp & "'"
            End If
        Next

        'For Location
        Dim selectedRowsLocation As Integer() = GridViewBranch.GetSelectedRows()
        Dim resultLocation As Object() = New Object(selectedRowsLocation.Length - 1) {}
        For i As Integer = 0 To selectedRowsLocation.Length - 1
            Dim rowHandle As Integer = selectedRowsLocation(i)
            If Not GridViewBranch.IsGroupRow(rowHandle) Then
                resultLocation(i) = GridViewBranch.GetRowCellValue(rowHandle, "BRANCHCODE")
                If i <> 0 Then
                    mLocationString = mLocationString + " OR "
                End If
                If WhichReport = 1 Then
                    mLocationString = mLocationString + "tblEmployee.BRANCHCODE = "
                Else
                    mLocationString = mLocationString + "tblEmployee.BRANCHCODE = "
                End If
                mLocationString = mLocationString + "'" + resultLocation(i) + "'"
            End If
        Next

        If CheckEdit1.Checked And CheckEdit2.Checked Then
            mActive = ""
        Else
            If CheckEdit1.Checked Then
                mActive = "tblEmployee.Active = 'Y'"
            End If
            If CheckEdit2.Checked Then
                mActive = "tblEmployee.Active = 'N'"
            End If
        End If
        Dim mCondition As String
        'Dim g_WhereClause As String
        mCondition = IIf(Len(Trim(mEmployeeString)) = 0, "(", "(" & Trim(mEmployeeString) & ") AND (") &
                     IIf(Len(Trim(mCatagoryString)) = 0, "", " " & Trim(mCatagoryString) & ") AND (") &
                     IIf(Len(Trim(mDepartmentString)) = 0, "", " " & Trim(mDepartmentString) & ") AND (") &
                     IIf(Len(Trim(mCompanyString)) = 0, "", " " & Trim(mCompanyString) & ") AND (") &
                     IIf(Len(Trim(mShiftString)) = 0, "", " " & Trim(mShiftString) & ") AND (") &
                     IIf(Len(Trim(mGradeString)) = 0, "", " " & Trim(mGradeString) & ") AND (") &
                     IIf(Len(Trim(mLocationString)) = 0, "", " " & Trim(mLocationString) & ") AND (") &
                     IIf(Len(Trim(mActive)) = 0, "", " " & Trim(mActive) & ") AND (")

        If mCondition = "(" Then
            mCondition = ""
        End If
        If Not Len(Trim(mCondition)) = 0 Then
            mCondition = mCondition.Remove(mCondition.Trim.Length - 5)
            'mCondition = LTrim(mCondition, Len(Trim(mCondition)) - 5)
        End If
        g_WhereClause = mCondition

        If Common.USERTYPE <> "A" Then
            Dim emp() As String = Common.Auth_Branch.Split(",")
            Dim ls As New List(Of String)()
            For x As Integer = 0 To emp.Length - 1
                ls.Add(emp(x).Trim)
            Next
            If g_WhereClause = "" Then
                g_WhereClause = " TBLEmployee.BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "')"
            Else
                g_WhereClause = g_WhereClause & " and TBLEmployee.BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "')"
            End If

            Dim com() As String = Common.auth_comp.Split(",")
            Dim lsc As New List(Of String)()
            For x As Integer = 0 To com.Length - 1
                lsc.Add(com(x).Trim)
            Next
            If g_WhereClause = "" Then
                g_WhereClause = " TBLEmployee.COMPANYCODE IN ('" & String.Join("', '", lsc.ToArray()) & "')"
            Else
                g_WhereClause = g_WhereClause & " and TBLEmployee.COMPANYCODE IN ('" & String.Join("', '", lsc.ToArray()) & "')"
            End If
        End If

        Exit Sub
        'ErrorGen:
        '        MsgBox(Err.Description, vbInformation, "TimeWatch")
    End Sub

    Private Sub BtnLoad_Click(sender As Object, e As EventArgs) Handles BtnLoad.Click
        setGridData()
    End Sub
    Private Sub PopupContainerEditEmp_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEditEmp.QueryResultValue
        Dim selectedRows() As Integer = GridViewEmp.GetSelectedRows
        Dim sb As StringBuilder = New StringBuilder
        For Each selectionRow As Integer In selectedRows
            Dim a As System.Data.DataRowView = GridViewEmp.GetRow(selectionRow)
            If (sb.ToString.Length > 0) Then
                sb.Append(", ")
            End If
            sb.Append(a.Item("PAYCODE"))
        Next
        e.Value = sb.ToString
    End Sub
    Private Sub PopupContainerEditComp_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEditComp.QueryResultValue
        Dim selectedRows() As Integer = GridViewComp.GetSelectedRows
        Dim sb As StringBuilder = New StringBuilder
        For Each selectionRow As Integer In selectedRows
            Dim a As System.Data.DataRowView = GridViewComp.GetRow(selectionRow)
            If (sb.ToString.Length > 0) Then
                sb.Append(", ")
            End If
            sb.Append(a.Item("COMPANYCODE"))
        Next
        e.Value = sb.ToString
    End Sub
    Private Sub PopupContainerEditDept_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEditDept.QueryResultValue
        Dim selectedRows() As Integer = GridViewDept.GetSelectedRows
        Dim sb As StringBuilder = New StringBuilder
        For Each selectionRow As Integer In selectedRows
            Dim a As System.Data.DataRowView = GridViewDept.GetRow(selectionRow)
            If (sb.ToString.Length > 0) Then
                sb.Append(", ")
            End If
            sb.Append(a.Item("DEPARTMENTCODE"))
        Next
        e.Value = sb.ToString
    End Sub
    Private Sub PopupContainerEditCat_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEditCat.QueryResultValue
        Dim selectedRows() As Integer = GridViewCat.GetSelectedRows
        Dim sb As StringBuilder = New StringBuilder
        For Each selectionRow As Integer In selectedRows
            Dim a As System.Data.DataRowView = GridViewCat.GetRow(selectionRow)
            If (sb.ToString.Length > 0) Then
                sb.Append(", ")
            End If
            sb.Append(a.Item("CAT"))
        Next
        e.Value = sb.ToString
    End Sub
    Private Sub PopupContainerEditGrade_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEditGrade.QueryResultValue
        Dim selectedRows() As Integer = GridViewGrade.GetSelectedRows
        Dim sb As StringBuilder = New StringBuilder
        For Each selectionRow As Integer In selectedRows
            Dim a As System.Data.DataRowView = GridViewGrade.GetRow(selectionRow)
            If (sb.ToString.Length > 0) Then
                sb.Append(", ")
            End If
            sb.Append(a.Item("GradeCode"))
        Next
        e.Value = sb.ToString
    End Sub
    Private Sub PopupContainerEditLocation_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEditLocation.QueryResultValue
        Dim selectedRows() As Integer = GridViewBranch.GetSelectedRows
        Dim sb As StringBuilder = New StringBuilder
        For Each selectionRow As Integer In selectedRows
            Dim a As System.Data.DataRowView = GridViewBranch.GetRow(selectionRow)
            If (sb.ToString.Length > 0) Then
                sb.Append(", ")
            End If
            sb.Append(a.Item("BRANCHCODE"))
        Next
        e.Value = sb.ToString
    End Sub
End Class
