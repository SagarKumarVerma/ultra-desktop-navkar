﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports System.ComponentModel
Imports DevExpress.XtraGrid.Columns
Imports System.Data.SqlClient
Imports System.Text
Imports System.Data.OleDb
Imports DevExpress.XtraSplashScreen
Imports System.Net
Imports System.Text.RegularExpressions
Imports System.Net.Mail

Public Class XtraReportEmailSetting
    Dim ulf As UserLookAndFeel
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Dim g_WhereClause As String
    Public Sub New()
        InitializeComponent()
    End Sub
    Private Sub XtraEmailSetting_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True

        Me.Width = Common.NavWidth 'Me.Parent.Width
        Me.Height = Common.NavHeight 'Me.Parent.Height
        SplitContainerControl1.SplitterPosition = Common.SplitterPosition '(SplitContainerControl1.Parent.Width) * 85 / 100

        If Common.servername = "Access" Then
            Me.TblCompany1TableAdapter1.Fill(Me.SSSDBDataSet.tblCompany1)
            GridControlComp.DataSource = SSSDBDataSet.tblCompany1

            'Me.TblEmployee1TableAdapter1.Fill(Me.SSSDBDataSet.TblEmployee1)
            'GridControlEmp.DataSource = SSSDBDataSet.TblEmployee1

            Me.TblDepartment1TableAdapter1.Fill(Me.SSSDBDataSet.tblDepartment1)
            GridControlDept.DataSource = SSSDBDataSet.tblDepartment1

            Me.Tblbranch1TableAdapter1.Fill(Me.SSSDBDataSet.tblbranch1)
            GridControlBranch.DataSource = SSSDBDataSet.tblbranch1

            Me.ReportType1TableAdapter1.Fill(Me.SSSDBDataSet.ReportType1)
            GridControlReports.DataSource = SSSDBDataSet.ReportType1
        Else
            TblCompanyTableAdapter.Connection.ConnectionString = Common.ConnectionString
            Me.TblCompanyTableAdapter.Fill(Me.SSSDBDataSet.tblCompany)
            GridControlComp.DataSource = SSSDBDataSet.tblCompany

            'TblEmployeeTableAdapter.Connection.ConnectionString = Common.ConnectionString
            'Me.TblEmployeeTableAdapter.Fill(Me.SSSDBDataSet.TblEmployee)
            'GridControlEmp.DataSource = SSSDBDataSet.TblEmployee

            TblDepartmentTableAdapter.Connection.ConnectionString = Common.ConnectionString
            Me.TblDepartmentTableAdapter.Fill(Me.SSSDBDataSet.tblDepartment)
            GridControlDept.DataSource = SSSDBDataSet.tblDepartment

            TblbranchTableAdapter.Connection.ConnectionString = Common.ConnectionString
            Me.TblbranchTableAdapter.Fill(Me.SSSDBDataSet.tblbranch)
            GridControlBranch.DataSource = SSSDBDataSet.tblbranch

            ReportTypeTableAdapter.Connection.ConnectionString = Common.ConnectionString
            Me.ReportTypeTableAdapter.Fill(Me.SSSDBDataSet.ReportType)
            GridControlReports.DataSource = SSSDBDataSet.ReportType
        End If
        'DateEdit1.DateTime = New DateTime(DateTime.Now.Year, DateTime.Now.Month, 1)
        Common.SetGridFont(GridViewComp, New System.Drawing.Font("Tahoma", 10))
        Common.SetGridFont(GridViewEmp, New System.Drawing.Font("Tahoma", 10))
        Common.SetGridFont(GridViewDept, New System.Drawing.Font("Tahoma", 10))
        Common.SetGridFont(GridViewBranch, New System.Drawing.Font("Tahoma", 10))
        Common.SetGridFont(GridViewReports, New System.Drawing.Font("Tahoma", 10))
        setEmpGrid()
        setformdata()
        setBranchGrid()
        LoadEmailSetting()
    End Sub
    Private Sub LoadEmailSetting()
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim ds As DataSet = New DataSet
        Dim sSql As String = "select * from tblMailSetup"

        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(ds)
        End If
        If ds.Tables(0).Rows.Count > 0 Then
            TextServer.Text = ds.Tables(0).Rows(0).Item("Server").ToString.Trim
            TextSender.Text = ds.Tables(0).Rows(0).Item("sender").ToString.Trim
            ComboType.Text = ds.Tables(0).Rows(0).Item("Type").ToString.Trim
            'TextType.Text = ds.Tables(0).Rows(0).Item("Type").ToString.Trim
            TextUser.Text = ds.Tables(0).Rows(0).Item("username").ToString.Trim
            TextPwd.Text = ds.Tables(0).Rows(0).Item("password").ToString.Trim
            'Dim pop3 As String = 
            'Dim RunTime As String
            If ds.Tables(0).Rows(0).Item("IsEnable").ToString.Trim = "Y" Then
                ToggleSwitch1.IsOn = True
            Else
                ToggleSwitch1.IsOn = False
            End If
            TextPort.Text = ds.Tables(0).Rows(0).Item("Port").ToString.Trim
            TextSenderName.Text = ds.Tables(0).Rows(0).Item("SenderName").ToString.Trim
            TextCC.Text = ds.Tables(0).Rows(0).Item("CCMail").ToString.Trim
        Else
            TextServer.Text = ""
            TextSender.Text = ""
            TextType.Text = ""
            TextUser.Text = ""
            TextPwd.Text = ""
            ToggleSwitch1.IsOn = False
            TextPort.Text = ""
            TextSenderName.Text = ""
            TextCC.Text = ""
        End If
    End Sub
    Private Sub PopupContainerEditEmp_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEditEmp.QueryResultValue
        Dim selectedRows() As Integer = GridViewEmp.GetSelectedRows
        Dim sb As StringBuilder = New StringBuilder
        For Each selectionRow As Integer In selectedRows
            Dim a As System.Data.DataRowView = GridViewEmp.GetRow(selectionRow)
            If (sb.ToString.Length > 0) Then
                sb.Append(", ")
            End If
            sb.Append(a.Item("PAYCODE"))
        Next
        e.Value = sb.ToString
    End Sub
    Private Sub PopupContainerEditComp_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEditComp.QueryResultValue
        Dim selectedRows() As Integer = GridViewComp.GetSelectedRows
        Dim sb As StringBuilder = New StringBuilder
        For Each selectionRow As Integer In selectedRows
            Dim a As System.Data.DataRowView = GridViewComp.GetRow(selectionRow)
            If (sb.ToString.Length > 0) Then
                sb.Append(", ")
            End If
            sb.Append(a.Item("COMPANYCODE"))
        Next
        e.Value = sb.ToString
    End Sub
    Private Sub PopupContainerEditDept_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEditDept.QueryResultValue
        Dim selectedRows() As Integer = GridViewDept.GetSelectedRows
        Dim sb As StringBuilder = New StringBuilder
        For Each selectionRow As Integer In selectedRows
            Dim a As System.Data.DataRowView = GridViewDept.GetRow(selectionRow)
            If (sb.ToString.Length > 0) Then
                sb.Append(", ")
            End If
            sb.Append(a.Item("DEPARTMENTCODE"))
        Next
        e.Value = sb.ToString
    End Sub
    Private Sub PopupContainerEditLocation_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEditLocation.QueryResultValue
        Dim selectedRows() As Integer = GridViewBranch.GetSelectedRows
        Dim sb As StringBuilder = New StringBuilder
        For Each selectionRow As Integer In selectedRows
            Dim a As System.Data.DataRowView = GridViewBranch.GetRow(selectionRow)
            If (sb.ToString.Length > 0) Then
                sb.Append(", ")
            End If
            sb.Append(a.Item("BRANCHCODE"))
        Next
        e.Value = sb.ToString
    End Sub
    Private Sub PopupContainerEditReports_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEditReports.QueryResultValue
        Dim selectedRows() As Integer = GridViewReports.GetSelectedRows
        Dim sb As StringBuilder = New StringBuilder
        For Each selectionRow As Integer In selectedRows
            Dim a As System.Data.DataRowView = GridViewReports.GetRow(selectionRow)
            If (sb.ToString.Length > 0) Then
                sb.Append(", ")
            End If
            sb.Append(a.Item("ReportType"))
        Next
        e.Value = sb.ToString
    End Sub
    Private Sub PopupContainerEditEmp_QueryPopUp(sender As System.Object, e As System.ComponentModel.CancelEventArgs) Handles PopupContainerEditEmp.QueryPopUp
        Dim val As Object = PopupContainerEditEmp.EditValue
        If (val Is Nothing) Then
            GridViewEmp.ClearSelection()
        Else
            'Dim texts() As String = val.ToString.Split(Microsoft.VisualBasic.ChrW(44))
            Dim texts() As String = val.ToString.Split(",")
            For Each text As String In texts
                If text.Trim.Length = 1 Then
                    text = text.Trim & "  "
                ElseIf text.Trim.Length = 2 Then
                    text = text.Trim & " "
                End If
                'MsgBox(text & "  " & text.Length & " " & GridView1.LocateByValue("SHIFT", text))
                Dim rowHandle As Integer = GridViewEmp.LocateByValue("PAYCODE", text.Trim)
                GridViewEmp.SelectRow(rowHandle)
            Next
        End If
    End Sub
    Private Sub PopupContainerEditComp_QueryPopUp(sender As System.Object, e As System.ComponentModel.CancelEventArgs) Handles PopupContainerEditComp.QueryPopUp
        Dim val As Object = PopupContainerEditComp.EditValue
        If (val Is Nothing) Then
            GridViewComp.ClearSelection()
        Else
            'Dim texts() As String = val.ToString.Split(Microsoft.VisualBasic.ChrW(44))
            Dim texts() As String = val.ToString.Split(",")
            For Each text As String In texts
                If text.Trim.Length = 1 Then
                    text = text.Trim & "  "
                ElseIf text.Trim.Length = 2 Then
                    text = text.Trim & " "
                End If
                'MsgBox(text & "  " & text.Length & " " & GridView1.LocateByValue("SHIFT", text))
                Dim rowHandle As Integer = GridViewComp.LocateByValue("COMPANYCODE", text.Trim)
                GridViewComp.SelectRow(rowHandle)
            Next
        End If
    End Sub
    Private Sub PopupContainerEditDept_QueryPopUp(sender As System.Object, e As System.ComponentModel.CancelEventArgs) Handles PopupContainerEditDept.QueryPopUp
        Dim val As Object = PopupContainerEditDept.EditValue
        If (val Is Nothing) Then
            GridViewDept.ClearSelection()
        Else
            Dim texts() As String = val.ToString.Split(",")
            For Each text As String In texts
                If text.Trim.Length = 1 Then
                    text = text.Trim & "  "
                ElseIf text.Trim.Length = 2 Then
                    text = text.Trim & " "
                End If
                Dim rowHandle As Integer = GridViewDept.LocateByValue("DEPARTMENTCODE", text.Trim)
                GridViewDept.SelectRow(rowHandle)
            Next
        End If
    End Sub
    Private Sub PopupContainerEditLocation_QueryPopUp(sender As System.Object, e As System.ComponentModel.CancelEventArgs) Handles PopupContainerEditLocation.QueryPopUp
        Dim val As Object = PopupContainerEditLocation.EditValue
        If (val Is Nothing) Then
            GridViewBranch.ClearSelection()
        Else
            Dim texts() As String = val.ToString.Split(",")
            For Each text As String In texts
                If text.Trim.Length = 1 Then
                    text = text.Trim & "  "
                ElseIf text.Trim.Length = 2 Then
                    text = text.Trim & " "
                End If
                Dim rowHandle As Integer = GridViewBranch.LocateByValue("BRANCHCODE", text.Trim)
                GridViewBranch.SelectRow(rowHandle)
            Next
        End If
    End Sub
    Private Sub PopupContainerEditReports_QueryPopUp(sender As System.Object, e As System.ComponentModel.CancelEventArgs) Handles PopupContainerEditReports.QueryPopUp
        Dim val As Object = PopupContainerEditReports.EditValue
        If (val Is Nothing) Then
            GridViewReports.ClearSelection()
        Else
            Dim texts() As String = val.ToString.Split(",")
            For Each text As String In texts
                If text.Trim.Length = 1 Then
                    text = text.Trim & "  "
                ElseIf text.Trim.Length = 2 Then
                    text = text.Trim & " "
                End If
                Dim rowHandle As Integer = GridViewReports.LocateByValue("ReportType", text.Trim)
                GridViewReports.SelectRow(rowHandle)
            Next
        End If
    End Sub
    Private Sub setEmpGrid()
        Dim gridtblregisterselet As String
        gridtblregisterselet = "select * from TblEmployee where E_MAIL1 <> '' and ACTIVE='Y'"
        Dim WTDataTable As DataTable
        If Common.servername = "Access" Then
            Dim dataAdapter As New OleDbDataAdapter(gridtblregisterselet, Common.con1)
            WTDataTable = New DataTable("TblEmployee")
            dataAdapter.Fill(WTDataTable)
        Else
            Dim dataAdapter As New SqlClient.SqlDataAdapter(gridtblregisterselet, Common.con)
            WTDataTable = New DataTable("TblEmployee")
            dataAdapter.Fill(WTDataTable)
        End If
        GridControlEmp.DataSource = WTDataTable
    End Sub
    Private Sub setBranchGrid()
        Dim gridtblregisterselet As String
        gridtblregisterselet = "select * from tblbranch where Email <> ''"
        Dim WTDataTable As DataTable
        If Common.servername = "Access" Then
            Dim dataAdapter As New OleDbDataAdapter(gridtblregisterselet, Common.con1)
            WTDataTable = New DataTable("tblbranch")
            dataAdapter.Fill(WTDataTable)
        Else
            Dim dataAdapter As New SqlClient.SqlDataAdapter(gridtblregisterselet, Common.con)
            WTDataTable = New DataTable("tblbranch")
            dataAdapter.Fill(WTDataTable)
        End If
        GridControlBranch.DataSource = WTDataTable
    End Sub
    Private Sub setformdata()
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim ds As DataSet = New DataSet
        Dim sSql As String = "select * from ReportEmailSetting"
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(ds)
        End If
        If ds.Tables(0).Rows.Count > 0 Then
            PopupContainerEditComp.EditValue = ds.Tables(0).Rows(0).Item("CompanyCode").ToString.Trim
            PopupContainerEditLocation.EditValue = ds.Tables(0).Rows(0).Item("BranchCode").ToString.Trim
            PopupContainerEditDept.EditValue = ds.Tables(0).Rows(0).Item("DepartmentCode").ToString.Trim
            PopupContainerEditReports.EditValue = ds.Tables(0).Rows(0).Item("ReportType").ToString.Trim
            PopupContainerEditEmp.EditValue = ds.Tables(0).Rows(0).Item("Paycode").ToString.Trim
            TextEdit3.Text = ds.Tables(0).Rows(0).Item("SentTime").ToString.Trim
        Else
            PopupContainerEditComp.EditValue = ""
            PopupContainerEditLocation.EditValue = ""
            PopupContainerEditDept.EditValue = ""
            PopupContainerEditReports.EditValue = ""
            PopupContainerEditEmp.EditValue = ""
            TextEdit3.Text = "10:00"
        End If
    End Sub
    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click
        If PopupContainerEditLocation.EditValue.ToString.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Please select Location</size>", "Error")
            PopupContainerEditLocation.Select()
            Exit Sub
        End If
        If PopupContainerEditReports.EditValue.ToString.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Please select Report Type</size>", "Error")
            PopupContainerEditReports.Select()
            Exit Sub
        End If
        Dim CompanyCode As String = PopupContainerEditComp.EditValue.ToString
        Dim BranchCode As String = PopupContainerEditLocation.EditValue.ToString
        Dim DepartmentCode As String = PopupContainerEditDept.EditValue.ToString
        Dim ReportType As String = PopupContainerEditReports.EditValue.ToString
        Dim Paycode As String = PopupContainerEditEmp.EditValue.ToString
        Dim SentTime As String = TextEdit3.Text.Trim
        Dim LastModifiedBy As String = "admin"
        Dim LastModifiedDate As String = Now.ToString("yyyy-MM-dd HH:mm:ss")

        Set_Filter_Crystal()

        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim ds As DataSet = New DataSet
        Dim sSql As String = "select * from ReportEmailSetting"
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(ds)
        End If
        If ds.Tables(0).Rows.Count > 0 Then
            'update
            'sSql = "Update ReportEmailSetting set CompanyCode = '" & CompanyCode & "', BranchCode  = '" & BranchCode & "', DepartmentCode = '" & DepartmentCode & "', ReportType = '" & ReportType & "', Paycode = '" & Paycode & "', SentTime = '" & SentTime & "', LastModifiedBy = '" & LastModifiedBy & "', LastModifiedDate = '" & LastModifiedDate & "', WhereClause = '" & g_WhereClause & "'"
            sSql = "Update ReportEmailSetting set BranchCode  = '" & BranchCode & "', ReportType = '" & ReportType & "', SentTime = '" & SentTime & "', LastModifiedBy = '" & LastModifiedBy & "', LastModifiedDate = '" & LastModifiedDate & "'"
        Else
            'insert
            'sSql = "insert into ReportEmailSetting (CompanyCode,BranchCode,DepartmentCode,ReportType,Paycode,SentTime,LastModifiedBy,LastModifiedDate, WhereClause) VALUES" & _
            '    "('" & CompanyCode & "','" & BranchCode & "','" & DepartmentCode & "','" & ReportType & "','" & Paycode & "','" & SentTime & "','" & LastModifiedBy & "','" & LastModifiedDate & "','" & g_WhereClause & "')"
            sSql = "insert into ReportEmailSetting (BranchCode,ReportType,SentTime,LastModifiedBy,LastModifiedDate) VALUES" & _
               "('" & BranchCode & "','" & ReportType & "','" & SentTime & "','" & LastModifiedBy & "','" & LastModifiedDate & "')"
        End If

        If Common.servername = "Access" Then
            If Common.con1.State <> ConnectionState.Open Then
                Common.con1.Open()
            End If
            cmd1 = New OleDbCommand(sSql, Common.con1)
            cmd1.ExecuteNonQuery()
            If Common.con1.State <> ConnectionState.Closed Then
                Common.con1.Close()
            End If
        Else
            If Common.con.State <> ConnectionState.Open Then
                Common.con.Open()
            End If
            cmd = New SqlCommand(sSql, Common.con)
            cmd.ExecuteNonQuery()
            If Common.con.State <> ConnectionState.Closed Then
                Common.con.Close()
            End If
        End If
        Common.LogPost("Auto Report Email Setting Save")
        XtraMessageBox.Show(ulf, "<size=10>Saved Successfully</size>", "<size=9>Success</size>")
        setformdata()
    End Sub
    Private Sub Set_Filter_Crystal() 'Optional ByVal Selection As String
        'On Error GoTo ErrorGen
        'Dim i As Integer
        Dim mCompanyString As String, mDepartmentString As String
        Dim mLocationString As String
        Dim mActive As String

        mCompanyString = ""
        mActive = ""
        mDepartmentString = ""
        mLocationString = ""
        Dim g_CompanyNames As String = ""
        'For CompanyCode
        Dim selectedRowsComp As Integer() = GridViewComp.GetSelectedRows()
        Dim resultComp As Object() = New Object(selectedRowsComp.Length - 1) {}
        For i As Integer = 0 To selectedRowsComp.Length - 1
            Dim rowHandle As Integer = selectedRowsComp(i)
            If Not GridViewComp.IsGroupRow(rowHandle) Then
                resultComp(i) = GridViewComp.GetRowCellValue(rowHandle, "COMPANYCODE")
                If i <> 0 Then
                    mCompanyString = mCompanyString + " OR "
                End If
                mCompanyString = mCompanyString + "tblEmployee.CompanyCode = "
                mCompanyString = mCompanyString + "'" + resultComp(i) + "'"
                g_CompanyNames = g_CompanyNames & GridViewComp.GetRowCellValue(rowHandle, "COMPANYNAME") & ", "
            End If
        Next

        'For DepartmentCode
        Dim selectedRowsDept As Integer() = GridViewDept.GetSelectedRows()
        Dim resultDept As Object() = New Object(selectedRowsDept.Length - 1) {}
        For i As Integer = 0 To selectedRowsDept.Length - 1
            Dim rowHandle As Integer = selectedRowsDept(i)
            If Not GridViewDept.IsGroupRow(rowHandle) Then
                resultDept(i) = GridViewDept.GetRowCellValue(rowHandle, "DEPARTMENTCODE")
                If i <> 0 Then
                    mDepartmentString = mDepartmentString + " OR "
                End If
                mDepartmentString = mDepartmentString + "tblEmployee.DepartmentCode = "
                mDepartmentString = mDepartmentString + "'" + resultDept(i) + "'"
            End If
        Next

        'For Location
        Dim selectedRowsLocation As Integer() = GridViewBranch.GetSelectedRows()
        Dim resultLocation As Object() = New Object(selectedRowsLocation.Length - 1) {}
        For i As Integer = 0 To selectedRowsLocation.Length - 1
            Dim rowHandle As Integer = selectedRowsLocation(i)
            If Not GridViewBranch.IsGroupRow(rowHandle) Then
                resultLocation(i) = GridViewBranch.GetRowCellValue(rowHandle, "BRANCHCODE")
                If i <> 0 Then
                    mLocationString = mLocationString + " OR "
                End If
                mLocationString = mLocationString + "tblEmployee.BRANCHCODE = "
                mLocationString = mLocationString + "'" + resultLocation(i) + "'"
            End If
        Next

        mActive = "tblEmployee.Active = 'Y'"
        Dim mCondition As String
        'Dim g_WhereClause As String
        mCondition = IIf(Len(Trim(mDepartmentString)) = 0, "", " " & Trim(mDepartmentString) & ") AND (") & _
                     IIf(Len(Trim(mCompanyString)) = 0, "", " " & Trim(mCompanyString) & ") AND (") & _
                     IIf(Len(Trim(mLocationString)) = 0, "", " " & Trim(mLocationString) & ") AND (") & _
                     IIf(Len(Trim(mActive)) = 0, "", " " & Trim(mActive) & ") AND (")

        If mCondition = "(" Then
            mCondition = ""
        End If
        If Not Len(Trim(mCondition)) = 0 Then
            mCondition = mCondition.Remove(mCondition.Trim.Length - 5)
            'mCondition = LTrim(mCondition, Len(Trim(mCondition)) - 5)
        End If
        g_WhereClause = mCondition
        Exit Sub
        'ErrorGen:
        '        MsgBox(Err.Description, vbInformation, "TimeWatch")
    End Sub
    Private Sub SimpleButton2_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton2.Click
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim ds As DataSet = New DataSet
        Dim sSql As String = "select * from tblMailSetup"

        Dim Server As String = TextServer.Text.Trim
        Dim senderDB As String = TextSender.Text.Trim
        Dim type As String = ComboType.Text.Trim 'TextType.Text.Trim
        Dim username As String = TextUser.Text.Trim
        Dim password As String = TextPwd.Text.Trim
        'Dim pop3 As String = 
        'Dim RunTime As String
        Dim IsEnable As String
        If ToggleSwitch1.IsOn = True Then
            IsEnable = "Y"
        Else
            IsEnable = "N"
        End If
        Dim Port As String = TextPort.Text.Trim
        Dim SenderName As String = TextSenderName.Text.Trim
        Dim CCMail As String = TextCC.Text.Trim
        'Dim HRMail As String

        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(ds)
        End If
        If ds.Tables(0).Rows.Count > 0 Then
            'update
            sSql = "update tblMailSetup set [Server]='" & Server & "', [sender]='" & senderDB & "', [type]='" & type & "', [username]='" & username & "', [password]='" & password & "', [IsEnable]='" & IsEnable & "', [Port] = '" & Port & "', [SenderName]='" & SenderName & "', [CCMail]='" & CCMail & "'"
        Else
            'insert
            sSql = "INSERT into tblMailSetup ([Server], [sender], [type], [username], [password], [IsEnable], [Port], [SenderName], [CCMail]) VALUES('" & Server & "','" & senderDB & "','" & type & "','" & username & "','" & password & "','" & IsEnable & "','" & Port & "','" & SenderName & "','" & CCMail & "')"
        End If
        If Common.servername = "Access" Then
            If Common.con1.State <> ConnectionState.Open Then
                Common.con1.Open()
            End If
            cmd1 = New OleDbCommand(sSql, Common.con1)
            cmd1.ExecuteNonQuery()
            If Common.con1.State <> ConnectionState.Closed Then
                Common.con1.Close()
            End If
        Else
            If Common.con.State <> ConnectionState.Open Then
                Common.con.Open()
            End If
            cmd = New SqlCommand(sSql, Common.con)
            cmd.ExecuteNonQuery()
            If Common.con.State <> ConnectionState.Closed Then
                Common.con.Close()
            End If
        End If
        Common.LogPost("Email Setting Save")
        XtraMessageBox.Show(ulf, "<size=10>Saved Successfully</size>", "<size=9>Success</size>")
    End Sub
    Private Sub SimpleButtonTest_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonTest.Click
        If TextEditTest.Text.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Please Enter Valid Email Id</size>", "<size=9>Error</size>")
            Exit Sub
        End If
        If emailaddresscheck(TextEditTest.Text.Trim) = False Then
            XtraMessageBox.Show(ulf, "<size=10>Please Enter Valid Email Id</size>", "<size=9>Error</size>")
            Exit Sub
        End If
        Me.Cursor = Cursors.WaitCursor
        Dim Server As String = TextServer.Text.Trim
        Dim senderDB As String = TextSender.Text.Trim
        Dim type As String = TextType.Text.Trim
        Dim username As String = TextUser.Text.Trim
        Dim password As String = TextPwd.Text.Trim
        Dim IsEnable As Boolean
        If ToggleSwitch1.IsOn = True Then
            IsEnable = True
        Else
            IsEnable = False
        End If
        Dim Port As String = TextPort.Text.Trim
        Dim SenderName As String = TextSenderName.Text.Trim

        Dim Smtp_Server As New SmtpClient
        Dim e_mail As New MailMessage()
        Dim attachment As System.Net.Mail.Attachment
        Smtp_Server.UseDefaultCredentials = False

        'Smtp_Server.Credentials = New Net.NetworkCredential(("nittslam@gmail.com"), ("umawidnitinbs4"))
        Smtp_Server.Credentials = New Net.NetworkCredential(senderDB, password)
        Smtp_Server.Port = Convert.ToInt16(Port) '587
        Smtp_Server.EnableSsl = IsEnable 'True
        Smtp_Server.Host = Server '"smtp.gmail.com"
        e_mail = New MailMessage()
        'e_mail.From = New MailAddress("nittslam@gmail.com")
        e_mail.From = New MailAddress(senderDB)
        'e_mail.To.Add("nittslam@gmail.com")
        Try
            e_mail.To.Add(TextEditTest.Text.Trim)
            If TextCC.Text.Trim <> "" Then
                e_mail.CC.Add(TextCC.Text.Trim)
            End If
            e_mail.IsBodyHtml = True
            e_mail.IsBodyHtml = True
            e_mail.Subject = "iAS Test Mail"
            e_mail.Body = "This is test mail sent from iAS on " & Now.ToString("dd/MM/yyyy")
            Smtp_Server.Send(e_mail)
        Catch ex As Exception
            Me.Cursor = Cursors.Default
            XtraMessageBox.Show(ulf, "<size=10>" & ex.Message.ToString.Trim & "</size>", "<size=9>iAS</size>")
            'XtraMessageBox.Show(ulf, "<size=10>Mail Not Sent. Please Check the Settings</size>", "<size=9>iAS</size>")
            Exit Sub
        End Try
        Common.LogPost("Test Mail Sent")
        Me.Cursor = Cursors.Default
        XtraMessageBox.Show(ulf, "<size=10>Mail Sent</size>", "<size=9>Success</size>")
    End Sub
    Private Function emailaddresscheck(ByVal emailaddress As String) As Boolean
        Dim pattern As String = "^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"
        Dim emailAddressMatch As Match = Regex.Match(emailaddress, pattern)
        If emailAddressMatch.Success Then
            Return True
        Else
            Return False
        End If
    End Function
End Class
