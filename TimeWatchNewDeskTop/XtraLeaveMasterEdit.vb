﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports System.ComponentModel
Imports DevExpress.XtraGrid.Columns
Imports System.Data.SqlClient
Imports System.Text
Imports System.Data.OleDb
Public Class XtraLeaveMasterEdit
    Dim LeaveId As String
    'Dim res_man As ResourceManager     'declare Resource manager to access to specific cultureinfo
    'Dim cul As CultureInfo     'declare culture info
    'Dim servername As String
    'Dim Common.connectionString As String
    Dim ulf As UserLookAndFeel
    'Dim Common.con As SqlCommon.connection
    'Dim Common.con1 As OleDbCommon.connection
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Dim adap, adap1, adap2, adap3 As SqlDataAdapter
    Dim adapA, adapA1, adapA2 As OleDbDataAdapter
    Dim ds, ds1, ds2, ds3 As DataSet
    Public Sub New()
        InitializeComponent()
    End Sub
   
    Private Sub XtraLeaveMasterEdit_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        LeaveId = XtraLeaveMaster.LEAVEFIELD
        'MsgBox(GrpId)

        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True

        If LeaveId.Length = 0 Then
            SetDefaultValue()
        Else
            SetFormValue()
        End If


    End Sub
    Private Sub SetDefaultValue()
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim ds As DataSet
        ds = New DataSet
        Dim sSql As String = "SELECT max(LEAVEFIELD) as LEAVE from tblLeaveMaster "
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(ds)
        End If
        'MsgBox(ds.Tables(0).Rows(0).Item("LEAVE").ToString)
        If ds.Tables(0).Rows(0).Item("LEAVE").ToString.Trim = "" Then
            TextEdit1.Text = "L01"
        Else
            TextEdit1.Text = "L" & (ds.Tables(0).Rows(0).Item("LEAVE").ToString.Substring(1, 2) + 1).ToString("00")
        End If
        TextEdit2.Text = ""
        TextEdit3.Text = ""
        ComboBoxEdit1.SelectedIndex = 0
        ToggleSwitch1.IsOn = False
        ToggleSwitch2.IsOn = False
        ToggleSwitch3.IsOn = False
        ToggleSwitch4.IsOn = False
        ToggleSwitch5.IsOn = False
        GroupControl1.Visible = False
        TextEdit4.Text = 0.0
        TextEdit5.Text = 0.0
        TextEdit6.Text = 0.0
        TextEdit7.Text = 0.0
        TextEdit8.Text = 0.0
        TextEdit9.Text = 0.0
        CheckEdit1.Checked = True
        LabelControl10.Visible = False
        TextEdit4.Visible = False
        TogglePre.IsOn = False
        TogglePost.IsOn = False
    End Sub
    Private Sub SetFormValue()
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim ds As DataSet
        ds = New DataSet
        'Dim sSql As String = "SELECT LEAVECODE,LEAVEDESCRIPTION,ISOFFINCLUDE,ISHOLIDAYINCLUDE,ISLEAVEACCRUAL,LEAVETYPE,SMIN," & _
        '    "SMAX,PRESENT,LEAVE,LEAVELIMIT,FIXED,isMonthly,isCompOffType,ExpiryDays from tblLeaveMaster where LEAVEFIELD='" & LeaveId & "'"
        Dim sSql As String = "SELECT * from tblLeaveMaster where LEAVEFIELD='" & LeaveId & "'"
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(ds)
        End If
        TextEdit1.Text = LeaveId
        TextEdit2.Text = ds.Tables(0).Rows(0).Item("LEAVECODE").ToString
        TextEdit3.Text = ds.Tables(0).Rows(0).Item("LEAVEDESCRIPTION").ToString
        If ds.Tables(0).Rows(0).Item("LEAVETYPE").ToString = "L" Then
            ComboBoxEdit1.SelectedIndex = 0
        ElseIf ds.Tables(0).Rows(0).Item("LEAVETYPE").ToString = "A" Then
            ComboBoxEdit1.SelectedIndex = 1
        ElseIf ds.Tables(0).Rows(0).Item("LEAVETYPE").ToString = "P" Then
            ComboBoxEdit1.SelectedIndex = 2
        End If

        If ds.Tables(0).Rows(0).Item("ISOFFINCLUDE").ToString = "Y" Then
            ToggleSwitch1.IsOn = True
        ElseIf ds.Tables(0).Rows(0).Item("ISOFFINCLUDE").ToString = "N" Then
            ToggleSwitch1.IsOn = False
        End If

        If ds.Tables(0).Rows(0).Item("ISHOLIDAYINCLUDE").ToString = "Y" Then
            ToggleSwitch2.IsOn = True
        ElseIf ds.Tables(0).Rows(0).Item("ISHOLIDAYINCLUDE").ToString = "N" Then
            ToggleSwitch2.IsOn = False
        End If

        If ds.Tables(0).Rows(0).Item("ISLEAVEACCRUAL").ToString = "Y" Then
            ToggleSwitch3.IsOn = True
            GroupControl1.Visible = True
        ElseIf ds.Tables(0).Rows(0).Item("ISLEAVEACCRUAL").ToString = "N" Then
            ToggleSwitch3.IsOn = False
            GroupControl1.Visible = False
        End If

        If ds.Tables(0).Rows(0).Item("isCompOffType").ToString = "Y" Then
            ToggleSwitch4.IsOn = True
            TextEdit4.Visible = True
            LabelControl10.Visible = True
        ElseIf ds.Tables(0).Rows(0).Item("isCompOffType").ToString = "N" Then
            ToggleSwitch4.IsOn = False
            TextEdit4.Visible = False
            LabelControl10.Visible = False
        End If

        If ds.Tables(0).Rows(0).Item("isMonthly").ToString = "Y" Then
            ToggleSwitch5.IsOn = True
        ElseIf ds.Tables(0).Rows(0).Item("isMonthly").ToString = "N" Then
            ToggleSwitch5.IsOn = False
        End If

        TextEdit4.Text = ds.Tables(0).Rows(0).Item("ExpiryDays").ToString
        If ds.Tables(0).Rows(0).Item("FIXED").ToString = "Y" Then
            CheckEdit2.Checked = True
            TextEdit7.Visible = False
            TextEdit8.Visible = False
            TextEdit9.Visible = False
            LabelControl13.Visible = False
            LabelControl14.Visible = False
            LabelControl15.Visible = False
        Else
            CheckEdit1.Checked = True
            TextEdit7.Visible = True
            TextEdit8.Visible = True
            TextEdit9.Visible = True
            LabelControl13.Visible = True
            LabelControl14.Visible = True
            LabelControl15.Visible = True
        End If
        TextEdit5.Text = ds.Tables(0).Rows(0).Item("SMIN").ToString
        TextEdit6.Text = ds.Tables(0).Rows(0).Item("SMAX").ToString
        TextEdit7.Text = ds.Tables(0).Rows(0).Item("PRESENT").ToString
        TextEdit8.Text = ds.Tables(0).Rows(0).Item("LEAVE").ToString
        TextEdit9.Text = ds.Tables(0).Rows(0).Item("LEAVELIMIT").ToString
        TogglePre.EditValue = ds.Tables(0).Rows(0).Item("CheckPreFix").ToString
        TogglePost.EditValue = ds.Tables(0).Rows(0).Item("CheckPostFix").ToString
    End Sub

    Private Sub SimpleButton2_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton2.Click
        Me.Close()
    End Sub

    Private Sub ToggleSwitch3_Toggled(sender As System.Object, e As System.EventArgs) Handles ToggleSwitch3.Toggled
        If ToggleSwitch3.IsOn = True Then
            GroupControl1.Visible = True
        Else
            GroupControl1.Visible = False
        End If
    End Sub

    Private Sub ToggleSwitch4_Toggled(sender As System.Object, e As System.EventArgs) Handles ToggleSwitch4.Toggled
        If ToggleSwitch4.IsOn = True Then
            LabelControl10.Visible = True
            TextEdit4.Visible = True
        Else
            LabelControl10.Visible = False
            TextEdit4.Visible = False
        End If
    End Sub

    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click
        If TextEdit2.Text = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Leave Code Cannot be Empty</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            TextEdit2.Select()
            Exit Sub
        End If
        Dim LEAVEFIELD As String = TextEdit1.Text
        Dim LEAVECODE As String = TextEdit2.Text
        Dim LEAVEDESCRIPTION As String = TextEdit3.Text
        Dim ISOFFINCLUDE As String
        If ToggleSwitch1.IsOn = True Then
            ISOFFINCLUDE = "Y"
        Else
            ISOFFINCLUDE = "N"
        End If
        Dim ISHOLIDAYINCLUDE As String
        If ToggleSwitch2.IsOn = True Then
            ISHOLIDAYINCLUDE = "Y"
        Else
            ISHOLIDAYINCLUDE = "N"
        End If
        Dim ISLEAVEACCRUAL As String
        If ToggleSwitch3.IsOn = True Then
            ISLEAVEACCRUAL = "Y"
        Else
            ISLEAVEACCRUAL = "N"
        End If

        Dim LEAVETYPE As String
        If ComboBoxEdit1.SelectedIndex = 0 Then
            LEAVETYPE = "L"
        ElseIf ComboBoxEdit1.SelectedIndex = 1 Then
            LEAVETYPE = "A"
        ElseIf ComboBoxEdit1.SelectedIndex = 2 Then
            LEAVETYPE = "P"
        End If

        Dim SMIN As String = TextEdit5.Text
        Dim SMAX As String = TextEdit6.Text
        Dim PRESENT As String = TextEdit7.Text
        Dim LEAVE As String = TextEdit8.Text
        Dim LEAVELIMIT As String = TextEdit9.Text
        Dim FIXED As String
        If CheckEdit1.Checked = True Then
            FIXED = "N"
        Else
            FIXED = "Y"
        End If
        Dim isMonthly As String
        If ToggleSwitch5.IsOn = True Then
            isMonthly = "Y"
        Else
            isMonthly = "N"
        End If
        Dim isCompOffType As String
        If ToggleSwitch4.IsOn = True Then
            isCompOffType = "Y"
        Else
            isCompOffType = "N"
        End If
        Dim ExpiryDays As String = TextEdit4.Text

        Dim CheckPreFix As String = TogglePre.EditValue
        Dim CheckPostFix As String = TogglePost.EditValue

        Dim sSql As String
        If LeaveId = "" Then
            'insert
            sSql = "insert INTO tblLeaveMaster (LEAVEFIELD,LEAVECODE,LEAVEDESCRIPTION,ISOFFINCLUDE,ISHOLIDAYINCLUDE,ISLEAVEACCRUAL,LEAVETYPE,SMIN,SMAX,PRESENT,LEAVE,LEAVELIMIT,FIXED,isMonthly,isCompOffType,ExpiryDays,LastModifiedBy,LastModifiedDate, CheckPreFix,CheckPostFix) " & _
                "VALUES('" & LEAVEFIELD & "','" & LEAVECODE & "','" & LEAVEDESCRIPTION & "','" & ISOFFINCLUDE & "','" & ISHOLIDAYINCLUDE & "','" & ISLEAVEACCRUAL & "','" & LEAVETYPE & "','" & SMIN & "','" & SMAX & "','" & PRESENT & "','" & LEAVE & "','" & LEAVELIMIT & "','" & FIXED & "','" & isMonthly & "','" & isCompOffType & "','" & ExpiryDays & "','admin','" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "','" & CheckPreFix & "','" & CheckPostFix & "')"
            Common.LogPost("Leave Master Add; LeaveCode='" & LEAVECODE)
        Else
            'update
            sSql = "UPDATE tblLeaveMaster set LEAVECODE = '" & LEAVECODE & "',LEAVEDESCRIPTION = '" & LEAVEDESCRIPTION & "', ISOFFINCLUDE = '" & ISOFFINCLUDE & "'," & _
                "ISHOLIDAYINCLUDE = '" & ISHOLIDAYINCLUDE & "',ISLEAVEACCRUAL = '" & ISLEAVEACCRUAL & "',LEAVETYPE = '" & LEAVETYPE & "',SMIN = '" & SMIN & "'," & _
                "SMAX = '" & SMAX & "',PRESENT = '" & PRESENT & "',LEAVE = '" & LEAVE & "',LEAVELIMIT = '" & LEAVELIMIT & "',FIXED = '" & FIXED & "',isMonthly = '" & isMonthly & "'," & _
                "isCompOffType = '" & isCompOffType & "',ExpiryDays = '" & ExpiryDays & "',LastModifiedBy = 'admin',LastModifiedDate = '" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "', " & _
                "CheckPreFix='" & CheckPreFix & "', CheckPostFix='" & CheckPostFix & "' " & _
                " where LEAVEFIELD = '" & LEAVEFIELD & "'"
            Common.LogPost("Leave Master Update; LeaveCode='" & LEAVECODE)
        End If
        If Common.servername = "Access" Then
            If Common.con1.State <> ConnectionState.Open Then
                Common.con1.Open()
            End If
            cmd1 = New OleDbCommand(sSql, Common.con1)
            cmd1.ExecuteNonQuery()
            Common.con1.Close()
        Else
            If Common.con.State <> ConnectionState.Open Then
                Common.con.Open()
            End If
            cmd = New SqlCommand(sSql, Common.con)
            cmd.ExecuteNonQuery()
            Common.con.Close()
        End If
        Me.Close()
    End Sub

    Private Sub CheckEdit2_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckEdit2.CheckedChanged
        If CheckEdit2.Checked = True Then
            TextEdit7.Visible = False
            TextEdit8.Visible = False
            TextEdit9.Visible = False
            LabelControl13.Visible = False
            LabelControl14.Visible = False
            LabelControl15.Visible = False
        Else
            TextEdit7.Visible = True
            TextEdit8.Visible = True
            TextEdit9.Visible = True
            LabelControl13.Visible = True
            LabelControl14.Visible = True
            LabelControl15.Visible = True
        End If
    End Sub
End Class