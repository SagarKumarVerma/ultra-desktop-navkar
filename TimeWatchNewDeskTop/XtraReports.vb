﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports System.ComponentModel
Imports DevExpress.XtraGrid.Columns
Imports System.Data.SqlClient
Imports System.Text
Imports System.Data.OleDb
Imports DevExpress.XtraSplashScreen

Public Class XtraReports
    Dim servername As String
    Dim ConnectionString As String
    Dim ulf As UserLookAndFeel
    Dim con As SqlConnection
    Dim con1 As OleDbConnection
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Dim adap, adap1, adap2 As SqlDataAdapter
    Dim adapA, adapA1 As OleDbDataAdapter
    Dim ds, ds1, ds2 As DataSet
    Public Sub New()
        InitializeComponent()
        'Dim fs As FileStream = New FileStream("db.txt", FileMode.Open, FileAccess.Read)
        'Dim sr As StreamReader = New StreamReader(fs)
        'Dim str As String
        'Dim str1() As String
        'Do While sr.Peek <> -1
        '    str = sr.ReadLine
        '    str1 = str.Split(",")
        '    servername = str1(0)
        'Loop
        'sr.Close()
        'fs.Close()


        If Common.servername = "Access" Then
            Me.TblEmployee1TableAdapter1.Fill(Me.SSSDBDataSet.TblEmployee1)
            GridControl2.DataSource = SSSDBDataSet.TblEmployee1
        Else
            TblEmployeeTableAdapter.Connection.ConnectionString = Common.ConnectionString '"Data Source=DESKTOP-PG6NDV8;Initial Catalog=SSSDB;Integrated Security=True"
            Me.TblEmployeeTableAdapter.Fill(Me.SSSDBDataSet.TblEmployee)
            GridControl2.DataSource = SSSDBDataSet.TblEmployee
        End If
        Common.SetGridFont(GridView1, New Font("Tahoma", 11))
    End Sub
    Private Sub XtraReports_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True

        Me.Width = My.Computer.Screen.WorkingArea.Width
        SplitContainerControl1.Width = My.Computer.Screen.WorkingArea.Width
        SplitContainerControl1.SplitterPosition = (My.Computer.Screen.WorkingArea.Width) * 90 / 100
        'SidePanel2.Height = SidePanel2.Parent.Height

        'Dim dt As DataTable = New DataTable
        'dt.Columns.Add("Sr No")
        'dt.Columns.Add("License Plate")
        'dt.Columns.Add("Start")
        'dt.Columns.Add("End")
        'dt.Columns.Add("Running Time")
        'dt.Columns.Add("Distance (M)")
        'dt.Columns.Add("Start Address")
        'dt.Columns.Add("End Address")


        'Dim datase As DataSet = New DataSet()
        'datase.Tables.Add(dt)
        'GridControl1.DataSource = dt
        'GridControl1.Width = SplitContainerControl1.Panel1.Width

        DateEdit1.DateTime = New DateTime(DateTime.Now.Year, DateTime.Now.Month, 1)
        DateEdit2.EditValue = Now

    End Sub
    Private Sub PopupContainerEdit1_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEdit1.QueryResultValue
        Dim selectedRows() As Integer = GridView2.GetSelectedRows
        Dim sb As StringBuilder = New StringBuilder
        For Each selectionRow As Integer In selectedRows
            Dim a As System.Data.DataRowView = GridView2.GetRow(selectionRow)
            If (sb.ToString.Length > 0) Then
                sb.Append(", ")
            End If
            sb.Append(a.Item("PAYCODE"))
        Next
        e.Value = sb.ToString
    End Sub

    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click
        DevExpress.XtraSplashScreen.SplashScreenManager.ShowForm(Me, GetType(WaitForm1), True, True, False)
        Me.Cursor = Cursors.WaitCursor
        Dim PAYCODE() As String = {} 'PopupContainerEdit1.EditValue.ToString.Split(",")
        Dim paycodelist As New List(Of String)()
        Dim sSql As String = "select DISTINCT (TblEmployee.PAYCODE) from tblTimeRegister, TblEmployee where TblEmployee.PAYCODE = tblTimeRegister.PAYCODE"
        If PopupContainerEdit1.EditValue = "" Then
            ds = New DataSet
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
            
            'MsgBox(ds.Tables(0).Rows.Count)
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                paycodelist.Add(ds.Tables(0).Rows(i).Item(0).ToString)
            Next
            PAYCODE = paycodelist.ToArray
        Else
            PAYCODE = PopupContainerEdit1.EditValue.ToString.Split(",")
        End If
        'MsgBox(PAYCODE.Length)

        Dim dt As DataTable = New DataTable
        dt.Columns.Add("Pay Code")
        dt.Columns.Add("Card Number")
        dt.Columns.Add("Date")
        dt.Columns.Add("Day")
        dt.Columns.Add("Status")
        dt.Columns.Add("Shift")
        dt.Columns.Add("In Time")
        dt.Columns.Add("Lunch Out")
        dt.Columns.Add("Lunch In")
        dt.Columns.Add("Out Time")
        dt.Columns.Add("Late Arrival")
        dt.Columns.Add("Early Depature")
        dt.Columns.Add("Hours Worked")
        dt.Columns.Add("Overtime")
        dt.Columns.Add("Over Stay")
        dt.Columns.Add("Early Arrival")
        dt.Columns.Add("Excess Lunch")

        Dim CardNumber As String = ""
        Dim IN1 As String = ""
        Dim OUT2 As String = ""
        Dim HOURSWORKED As String = ""
        Dim LATEARRIVAL As String = ""
        Dim EARLYDEPARTURE As String = ""
        Dim OTDURATION As String = ""
        Dim EXCLUNCHHOURS As String = ""
        Dim DateOFFICE As String = ""
        Dim STATUS As String = ""
        Dim SHIFT As String = ""
        Dim Day As String = ""
        Dim IN2 As String = ""
        Dim OUT1 As String = ""
        Dim EARLYARRIVAL As String = ""
        Dim OSDURATION As String = ""

        For i As Integer = 0 To PAYCODE.Length - 1
            ds = New DataSet
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter("select TblEmployee.PRESENTCARDNO,tblTimeRegister.IN1, tblTimeRegister.OUT2, tblTimeRegister.HOURSWORKED, tblTimeRegister.LATEARRIVAL, tblTimeRegister.OTDURATION, tblTimeRegister.DateOFFICE, tblTimeRegister.STATUS, tblTimeRegister.SHIFT, tblTimeRegister.IN2, tblTimeRegister.OUT1, tblTimeRegister.EARLYARRIVAL, tblTimeRegister.EARLYDEPARTURE, tblTimeRegister.OSDURATION, tblTimeRegister.EXCLUNCHHOURS from tblTimeRegister, TblEmployee where TblEmployee.PAYCODE = tblTimeRegister.PAYCODE and tblTimeRegister.PAYCODE = '" & PAYCODE(i).Trim & "' and FORMAT(tblTimeRegister.DateOFFICE,'yyyy-MM-dd HH:mm:ss.000') >= '" & DateEdit1.DateTime.ToString("yyyy-MM-dd 00:00:00.000") & "' and FORMAT(tblTimeRegister.DateOFFICE,'yyyy-MM-dd HH:mm:ss') <= '" & DateEdit2.DateTime.ToString("yyyy-MM-dd 00:00:00") & "' order	BY DateOFFICE ASC", Common.con1)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter("select TblEmployee.PRESENTCARDNO,tblTimeRegister.IN1, tblTimeRegister.OUT2, tblTimeRegister.HOURSWORKED, tblTimeRegister.LATEARRIVAL, tblTimeRegister.OTDURATION, tblTimeRegister.DateOFFICE, tblTimeRegister.STATUS, tblTimeRegister.SHIFT, tblTimeRegister.IN2, tblTimeRegister.OUT1, tblTimeRegister.EARLYARRIVAL, tblTimeRegister.EARLYDEPARTURE, tblTimeRegister.OSDURATION , tblTimeRegister.EXCLUNCHHOURS from tblTimeRegister, TblEmployee where TblEmployee.PAYCODE = tblTimeRegister.PAYCODE and tblTimeRegister.PAYCODE = '" & PAYCODE(i).Trim & "' and tblTimeRegister.DateOFFICE >= '" & DateEdit1.DateTime.ToString("yyyy-MM-dd 00:00:00.000") & "' and tblTimeRegister.DateOFFICE <= '" & DateEdit2.DateTime.ToString("yyyy-MM-dd 00:00:00.000") & "'", Common.con)
                adap.Fill(ds)
            End If
            For j As Integer = 0 To ds.Tables(0).Rows.Count - 1
                CardNumber = ds.Tables(0).Rows(j).Item("PRESENTCARDNO").ToString
                Try
                    IN1 = Convert.ToDateTime(ds.Tables(0).Rows(j).Item("IN1").ToString).ToString("HH:mm")
                Catch
                    IN1 = ""
                End Try

                Try
                    IN2 = Convert.ToDateTime(ds.Tables(0).Rows(j).Item("IN2").ToString).ToString("HH:mm")
                Catch
                    IN2 = ""
                End Try
                Try
                    OUT1 = Convert.ToDateTime(ds.Tables(0).Rows(j).Item("OUT1").ToString).ToString("HH:mm")
                Catch
                    OUT1 = ""
                End Try
                Try
                    OUT2 = Convert.ToDateTime(ds.Tables(0).Rows(j).Item("OUT2").ToString).ToString("HH:mm")
                Catch
                    OUT2 = ""
                End Try
                If ds.Tables(0).Rows(j).Item("HOURSWORKED").ToString = "" Then
                    HOURSWORKED = ""
                Else
                    HOURSWORKED = (Convert.ToInt32(ds.Tables(0).Rows(j).Item("HOURSWORKED").ToString) \ 60).ToString("00") & ":" & (Convert.ToInt32(ds.Tables(0).Rows(j).Item("HOURSWORKED").ToString) Mod 60).ToString("00")
                End If
                If ds.Tables(0).Rows(j).Item("LATEARRIVAL").ToString = "" Then
                    LATEARRIVAL = ""
                Else
                    LATEARRIVAL = (Convert.ToInt32(ds.Tables(0).Rows(j).Item("LATEARRIVAL").ToString) \ 60).ToString("00") & ":" & (Convert.ToInt32(ds.Tables(0).Rows(j).Item("LATEARRIVAL").ToString) Mod 60).ToString("00")
                End If

                If ds.Tables(0).Rows(j).Item("EARLYDEPARTURE").ToString = "" Then
                    EARLYDEPARTURE = ""
                Else
                    EARLYDEPARTURE = (Convert.ToInt32(ds.Tables(0).Rows(j).Item("EARLYDEPARTURE").ToString) \ 60).ToString("00") & ":" & (Convert.ToInt32(ds.Tables(0).Rows(j).Item("EARLYDEPARTURE").ToString) Mod 60).ToString("00")
                End If

                If ds.Tables(0).Rows(j).Item("EARLYARRIVAL").ToString = "" Then
                    EARLYARRIVAL = ""
                Else
                    EARLYARRIVAL = (Convert.ToInt32(ds.Tables(0).Rows(j).Item("EARLYARRIVAL").ToString) \ 60).ToString("00") & ":" & (Convert.ToInt32(ds.Tables(0).Rows(j).Item("EARLYARRIVAL").ToString) Mod 60).ToString("00")
                End If

                If ds.Tables(0).Rows(j).Item("OSDURATION").ToString = "" Then
                    OSDURATION = ""
                Else
                    OSDURATION = (Convert.ToInt32(ds.Tables(0).Rows(j).Item("OSDURATION").ToString) \ 60).ToString("00") & ":" & (Convert.ToInt32(ds.Tables(0).Rows(j).Item("OSDURATION").ToString) Mod 60).ToString("00")
                End If

                If ds.Tables(0).Rows(j).Item("OTDURATION").ToString = "" Then
                    OTDURATION = ""
                Else
                    OTDURATION = (Convert.ToInt32(ds.Tables(0).Rows(j).Item("OTDURATION").ToString) \ 60).ToString("00") & ":" & (Convert.ToInt32(ds.Tables(0).Rows(j).Item("OTDURATION").ToString) Mod 60).ToString("00")
                End If

                If ds.Tables(0).Rows(j).Item("EXCLUNCHHOURS").ToString = "" Then
                    EXCLUNCHHOURS = ""
                Else
                    EXCLUNCHHOURS = (Convert.ToInt32(ds.Tables(0).Rows(j).Item("EXCLUNCHHOURS").ToString) \ 60).ToString("00") & ":" & (Convert.ToInt32(ds.Tables(0).Rows(j).Item("EXCLUNCHHOURS").ToString) Mod 60).ToString("00")
                End If

                'MsgBox("2")
                Try
                    DateOFFICE = Convert.ToDateTime(ds.Tables(0).Rows(j).Item("DateOFFICE").ToString).ToString("dd-MM")
                    Day = Convert.ToDateTime(ds.Tables(0).Rows(j).Item("DateOFFICE").ToString).DayOfWeek.ToString.Substring(0, 3)
                Catch
                    DateOFFICE = ""
                    Day = ""
                End Try

                STATUS = ds.Tables(0).Rows(j).Item("STATUS").ToString
                SHIFT = ds.Tables(0).Rows(j).Item("SHIFT").ToString

                dt.Rows.Add(PAYCODE(i), CardNumber, DateOFFICE, Day, STATUS, SHIFT, IN1, OUT1, IN2, OUT2, LATEARRIVAL, EARLYDEPARTURE, HOURSWORKED, OTDURATION, OSDURATION, EARLYARRIVAL, EXCLUNCHHOURS)
            Next
            'MsgBox(ds.Tables(0).Rows.Count)
        Next
        Dim datase As DataSet = New DataSet()
        datase.Tables.Add(dt)
        GridControl1.DataSource = dt
        GridControl1.Width = SplitContainerControl1.Panel1.Width
        GridView1.OptionsView.AllowCellMerge = True

        VGridControl1.DataSource = dt
        Me.Cursor = Cursors.Default
        SplashScreenManager.CloseForm(False)
    End Sub

    Private Sub GridView1_CellMerge(sender As System.Object, e As DevExpress.XtraGrid.Views.Grid.CellMergeEventArgs) Handles GridView1.CellMerge
        If (e.Column.FieldName = "In Time") Then
            e.Handled = True
            e.Merge = False
        End If
        If (e.Column.FieldName = "Out Time") Then
            e.Handled = True
            e.Merge = False
        End If
        If (e.Column.FieldName = "Late") Then
            e.Handled = True
            e.Merge = False
        End If
        If (e.Column.FieldName = "Hours Worked") Then
            e.Handled = True
            e.Merge = False
        End If
        If (e.Column.FieldName = "Hours Worked") Then
            e.Handled = True
            e.Merge = False
        End If
        If (e.Column.FieldName = "Overtime") Then
            e.Handled = True
            e.Merge = False
        End If
        If (e.Column.FieldName = "Status") Then
            e.Handled = True
            e.Merge = False
        End If
        If (e.Column.FieldName = "Shift") Then
            e.Handled = True
            e.Merge = False
        End If

        If (e.Column.FieldName = "Lunch Out") Then
            e.Handled = True
            e.Merge = False
        End If
        If (e.Column.FieldName = "Lunch In") Then
            e.Handled = True
            e.Merge = False
        End If
        If (e.Column.FieldName = "Late Arrival") Then
            e.Handled = True
            e.Merge = False
        End If
        If (e.Column.FieldName = "Early Depature") Then
            e.Handled = True
            e.Merge = False
        End If
        If (e.Column.FieldName = "Early Arrival") Then
            e.Handled = True
            e.Merge = False
        End If
        If (e.Column.FieldName = "Day") Then
            e.Handled = True
            e.Merge = False
        End If
        If (e.Column.FieldName = "Early Arrival") Then
            e.Handled = True
            e.Merge = False
        End If
        If (e.Column.FieldName = "Over Stay") Then
            e.Handled = True
            e.Merge = False
        End If

        If (e.Column.FieldName = "Excess Lunch") Then
            e.Handled = True
            e.Merge = False
        End If
      
        'If Not Object.Equals(e.CellValue1, e.CellValue2) Then
        '    Return
        'End If

        'Dim id1 As String = CType(GridView1.GetRowCellValue(e.RowHandle1, "Pay Code"), String)
        'Dim id2 As String = CType(GridView1.GetRowCellValue(e.RowHandle2, "Pay Code"), String)
        'If Not Object.Equals(e.CellValue1, e.CellValue2) Then
        '    Return
        'End If

        'e.Merge = (id1 = id2)
    End Sub

    Private Sub SimpleButton2_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton2.Click
        If GridView1.DataRowCount = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>No date to export</size>", "Information")
            Exit Sub
        End If

        GridView1.ExportToXlsx(".\Performance Register.xlsx")

        Dim sr As New StreamReader(".\Performance Register.xlsx")
        Dim text As String = sr.ReadToEnd()
        sr.Close()
        Dim sw As New StreamWriter(".\Performance Register.xlsx")
        Dim rundatetime As String = "Run date time : " & Now.ToString("dd-MM-yyyy HH:mm") & vbCrLf
        Dim caption2 As String = "Performance register for " & DateEdit1.DateTime.ToString("dd-MM-yyyy") & " to " & DateEdit2.DateTime.ToString("dd-MM-yyyy") & vbCrLf
        sw.WriteLine(rundatetime)
        sw.WriteLine(caption2 & vbCrLf)
        sw.WriteLine(text) ' Add original content back
        sw.Close()
    End Sub

    Private Sub SimpleButton3_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton3.Click
        If GridView1.DataRowCount = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>No date to export</size>", "Information")
            Exit Sub
        End If

        GridView1.ExportToText(".\Performance Register.txt")

        Dim sr As New StreamReader(".\Performance Register.txt")
        Dim text As String = sr.ReadToEnd()
        sr.Close()
        Dim sw As New StreamWriter(".\Performance Register.txt")
        Dim rundatetime As String = "Run date time : " & Now.ToString("dd-MM-yyyy HH:mm") & vbCrLf
        Dim caption2 As String = "Performance register for " & DateEdit1.DateTime.ToString("dd-MM-yyyy") & " to " & DateEdit2.DateTime.ToString("dd-MM-yyyy") & vbCrLf
        sw.WriteLine(rundatetime)
        sw.WriteLine(caption2 & vbCrLf)
        sw.WriteLine(text) ' Add original content back
        sw.Close()
        If XtraMessageBox.Show(ulf, "<size=10>File is stroted in installed location as Performance Register.txt" & vbCrLf & "Do you want to open the file?</size>", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
            Process.Start(".\Performance Register.txt")
        End If

    End Sub

    Private Sub SimpleButton4_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton4.Click
        If GridView1.DataRowCount = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>No date to export</size>", "Information")
            Exit Sub
        End If
        GridView1.ExportToPdf(".\Performance Register.pdf")

        Dim sr As New StreamReader(".\Performance Register.pdf")
        Dim text As String = sr.ReadToEnd()
        sr.Close()
        Dim sw As New StreamWriter(".\Performance Register.pdf")
        Dim rundatetime As String = "Run date time : " & Now.ToString("dd-MM-yyyy HH:mm") & vbCrLf
        Dim caption2 As String = "Performance register for " & DateEdit1.DateTime.ToString("dd-MM-yyyy") & " to " & DateEdit2.DateTime.ToString("dd-MM-yyyy") & vbCrLf
        sw.WriteLine(rundatetime)
        sw.WriteLine(caption2 & vbCrLf)
        sw.WriteLine(text) ' Add original content back
        sw.Close()

    End Sub

    Private Sub SimpleButton5_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton5.Click
        If GridView1.DataRowCount = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>No date to export</size>", "Information")
            Exit Sub
        End If
        GridView1.ExportToCsv(".\Performance Register.csv")

        Dim sr As New StreamReader(".\Performance Register.csv")
        Dim text As String = sr.ReadToEnd()
        sr.Close()
        Dim sw As New StreamWriter(".\Performance Register.csv")
        Dim rundatetime As String = "Run date time : " & Now.ToString("dd-MM-yyyy HH:mm") & vbCrLf
        Dim caption2 As String = "Performance register for " & DateEdit1.DateTime.ToString("dd-MM-yyyy") & " to " & DateEdit2.DateTime.ToString("dd-MM-yyyy") & vbCrLf
        sw.WriteLine(rundatetime)
        sw.WriteLine(caption2 & vbCrLf)
        sw.WriteLine(text) ' Add original content back
        sw.Close()

        If XtraMessageBox.Show(ulf, "<size=10>File is stroted in installed location as Performance Register.csv" & vbCrLf & "Do you want to open the file?</size>", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
            Process.Start(".\Performance Register.csv")
        End If

    End Sub
End Class
