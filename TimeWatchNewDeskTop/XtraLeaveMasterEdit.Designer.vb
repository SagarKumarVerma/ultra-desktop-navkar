﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraLeaveMasterEdit
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEdit1 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEdit2 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit3 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.ComboBoxEdit1 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.ToggleSwitch1 = New DevExpress.XtraEditors.ToggleSwitch()
        Me.ToggleSwitch2 = New DevExpress.XtraEditors.ToggleSwitch()
        Me.ToggleSwitch3 = New DevExpress.XtraEditors.ToggleSwitch()
        Me.ToggleSwitch4 = New DevExpress.XtraEditors.ToggleSwitch()
        Me.ToggleSwitch5 = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEdit4 = New DevExpress.XtraEditors.TextEdit()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.TextEdit9 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEdit8 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit7 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEdit6 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit5 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.CheckEdit2 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit1 = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.TogglePre = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
        Me.TogglePost = New DevExpress.XtraEditors.ToggleSwitch()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleSwitch1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleSwitch2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleSwitch3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleSwitch4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleSwitch5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.TextEdit9.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit8.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TogglePre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TogglePost.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton1.Appearance.Options.UseFont = True
        Me.SimpleButton1.Location = New System.Drawing.Point(435, 282)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton1.TabIndex = 14
        Me.SimpleButton1.Text = "Save"
        '
        'SimpleButton2
        '
        Me.SimpleButton2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton2.Appearance.Options.UseFont = True
        Me.SimpleButton2.Location = New System.Drawing.Point(516, 282)
        Me.SimpleButton2.Name = "SimpleButton2"
        Me.SimpleButton2.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton2.TabIndex = 15
        Me.SimpleButton2.Text = "Cancel"
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(12, 25)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(60, 14)
        Me.LabelControl1.TabIndex = 29
        Me.LabelControl1.Text = "Leave Field"
        '
        'TextEdit1
        '
        Me.TextEdit1.Enabled = False
        Me.TextEdit1.Location = New System.Drawing.Point(102, 22)
        Me.TextEdit1.Name = "TextEdit1"
        Me.TextEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEdit1.Properties.Appearance.Options.UseFont = True
        Me.TextEdit1.Properties.MaxLength = 25
        Me.TextEdit1.Size = New System.Drawing.Size(72, 20)
        Me.TextEdit1.TabIndex = 1
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl2.Appearance.Options.UseFont = True
        Me.LabelControl2.Location = New System.Drawing.Point(12, 51)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(75, 14)
        Me.LabelControl2.TabIndex = 30
        Me.LabelControl2.Text = "Leave Code *"
        '
        'TextEdit2
        '
        Me.TextEdit2.Location = New System.Drawing.Point(102, 48)
        Me.TextEdit2.Name = "TextEdit2"
        Me.TextEdit2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEdit2.Properties.Appearance.Options.UseFont = True
        Me.TextEdit2.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextEdit2.Properties.MaxLength = 3
        Me.TextEdit2.Size = New System.Drawing.Size(72, 20)
        Me.TextEdit2.TabIndex = 2
        '
        'TextEdit3
        '
        Me.TextEdit3.Location = New System.Drawing.Point(102, 74)
        Me.TextEdit3.Name = "TextEdit3"
        Me.TextEdit3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEdit3.Properties.Appearance.Options.UseFont = True
        Me.TextEdit3.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextEdit3.Properties.Mask.EditMask = "[A-z ]*"
        Me.TextEdit3.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEdit3.Properties.MaxLength = 20
        Me.TextEdit3.Size = New System.Drawing.Size(152, 20)
        Me.TextEdit3.TabIndex = 3
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl3.Appearance.Options.UseFont = True
        Me.LabelControl3.Location = New System.Drawing.Point(12, 77)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(60, 14)
        Me.LabelControl3.TabIndex = 32
        Me.LabelControl3.Text = "Description"
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl4.Appearance.Options.UseFont = True
        Me.LabelControl4.Location = New System.Drawing.Point(12, 103)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(64, 14)
        Me.LabelControl4.TabIndex = 34
        Me.LabelControl4.Text = "Leave Type"
        '
        'ComboBoxEdit1
        '
        Me.ComboBoxEdit1.Location = New System.Drawing.Point(102, 100)
        Me.ComboBoxEdit1.Name = "ComboBoxEdit1"
        Me.ComboBoxEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEdit1.Properties.Appearance.Options.UseFont = True
        Me.ComboBoxEdit1.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.ComboBoxEdit1.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboBoxEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEdit1.Properties.DropDownRows = 3
        Me.ComboBoxEdit1.Properties.Items.AddRange(New Object() {"Leave", "Absent", "Present"})
        Me.ComboBoxEdit1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.ComboBoxEdit1.Size = New System.Drawing.Size(152, 20)
        Me.ComboBoxEdit1.TabIndex = 4
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl5.Appearance.Options.UseFont = True
        Me.LabelControl5.Location = New System.Drawing.Point(12, 131)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(105, 14)
        Me.LabelControl5.TabIndex = 37
        Me.LabelControl5.Text = "Weekly Off Include"
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl6.Appearance.Options.UseFont = True
        Me.LabelControl6.Location = New System.Drawing.Point(12, 162)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(82, 14)
        Me.LabelControl6.TabIndex = 38
        Me.LabelControl6.Text = "Holiday Include"
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl7.Appearance.Options.UseFont = True
        Me.LabelControl7.Location = New System.Drawing.Point(12, 193)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(52, 14)
        Me.LabelControl7.TabIndex = 39
        Me.LabelControl7.Text = "Is Accrual"
        '
        'LabelControl8
        '
        Me.LabelControl8.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl8.Appearance.Options.UseFont = True
        Me.LabelControl8.Location = New System.Drawing.Point(12, 224)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(97, 14)
        Me.LabelControl8.TabIndex = 40
        Me.LabelControl8.Text = "Is CompOff Type "
        '
        'ToggleSwitch1
        '
        Me.ToggleSwitch1.Location = New System.Drawing.Point(159, 126)
        Me.ToggleSwitch1.Name = "ToggleSwitch1"
        Me.ToggleSwitch1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleSwitch1.Properties.Appearance.Options.UseFont = True
        Me.ToggleSwitch1.Properties.OffText = "Off"
        Me.ToggleSwitch1.Properties.OnText = "On"
        Me.ToggleSwitch1.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ToggleSwitch1.Size = New System.Drawing.Size(95, 25)
        Me.ToggleSwitch1.TabIndex = 5
        '
        'ToggleSwitch2
        '
        Me.ToggleSwitch2.Location = New System.Drawing.Point(159, 157)
        Me.ToggleSwitch2.Name = "ToggleSwitch2"
        Me.ToggleSwitch2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleSwitch2.Properties.Appearance.Options.UseFont = True
        Me.ToggleSwitch2.Properties.OffText = "Off"
        Me.ToggleSwitch2.Properties.OnText = "On"
        Me.ToggleSwitch2.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ToggleSwitch2.Size = New System.Drawing.Size(95, 25)
        Me.ToggleSwitch2.TabIndex = 6
        '
        'ToggleSwitch3
        '
        Me.ToggleSwitch3.Location = New System.Drawing.Point(159, 188)
        Me.ToggleSwitch3.Name = "ToggleSwitch3"
        Me.ToggleSwitch3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleSwitch3.Properties.Appearance.Options.UseFont = True
        Me.ToggleSwitch3.Properties.OffText = "Off"
        Me.ToggleSwitch3.Properties.OnText = "On"
        Me.ToggleSwitch3.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ToggleSwitch3.Size = New System.Drawing.Size(95, 25)
        Me.ToggleSwitch3.TabIndex = 7
        '
        'ToggleSwitch4
        '
        Me.ToggleSwitch4.Location = New System.Drawing.Point(159, 219)
        Me.ToggleSwitch4.Name = "ToggleSwitch4"
        Me.ToggleSwitch4.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleSwitch4.Properties.Appearance.Options.UseFont = True
        Me.ToggleSwitch4.Properties.OffText = "Off"
        Me.ToggleSwitch4.Properties.OnText = "On"
        Me.ToggleSwitch4.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ToggleSwitch4.Size = New System.Drawing.Size(95, 25)
        Me.ToggleSwitch4.TabIndex = 9
        '
        'ToggleSwitch5
        '
        Me.ToggleSwitch5.Location = New System.Drawing.Point(428, 188)
        Me.ToggleSwitch5.Name = "ToggleSwitch5"
        Me.ToggleSwitch5.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleSwitch5.Properties.Appearance.Options.UseFont = True
        Me.ToggleSwitch5.Properties.OffText = "Off"
        Me.ToggleSwitch5.Properties.OnText = "On"
        Me.ToggleSwitch5.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ToggleSwitch5.Size = New System.Drawing.Size(95, 25)
        Me.ToggleSwitch5.TabIndex = 8
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl9.Appearance.Options.UseFont = True
        Me.LabelControl9.Location = New System.Drawing.Point(281, 193)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(122, 14)
        Me.LabelControl9.TabIndex = 46
        Me.LabelControl9.Text = "Monthly Leave Accrual"
        '
        'LabelControl10
        '
        Me.LabelControl10.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl10.Appearance.Options.UseFont = True
        Me.LabelControl10.Location = New System.Drawing.Point(272, 224)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(70, 14)
        Me.LabelControl10.TabIndex = 47
        Me.LabelControl10.Text = "Expires After"
        Me.LabelControl10.Visible = False
        '
        'TextEdit4
        '
        Me.TextEdit4.EditValue = "000.00"
        Me.TextEdit4.Location = New System.Drawing.Point(357, 222)
        Me.TextEdit4.Name = "TextEdit4"
        Me.TextEdit4.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEdit4.Properties.Appearance.Options.UseFont = True
        Me.TextEdit4.Properties.Mask.EditMask = "f"
        Me.TextEdit4.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEdit4.Properties.MaxLength = 6
        Me.TextEdit4.Size = New System.Drawing.Size(70, 20)
        Me.TextEdit4.TabIndex = 10
        Me.TextEdit4.Visible = False
        '
        'GroupControl1
        '
        Me.GroupControl1.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.GroupControl1.AppearanceCaption.Options.UseFont = True
        Me.GroupControl1.Controls.Add(Me.TextEdit9)
        Me.GroupControl1.Controls.Add(Me.LabelControl15)
        Me.GroupControl1.Controls.Add(Me.TextEdit8)
        Me.GroupControl1.Controls.Add(Me.TextEdit7)
        Me.GroupControl1.Controls.Add(Me.LabelControl13)
        Me.GroupControl1.Controls.Add(Me.LabelControl14)
        Me.GroupControl1.Controls.Add(Me.TextEdit6)
        Me.GroupControl1.Controls.Add(Me.TextEdit5)
        Me.GroupControl1.Controls.Add(Me.LabelControl12)
        Me.GroupControl1.Controls.Add(Me.LabelControl11)
        Me.GroupControl1.Controls.Add(Me.CheckEdit2)
        Me.GroupControl1.Controls.Add(Me.CheckEdit1)
        Me.GroupControl1.Location = New System.Drawing.Point(281, 12)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(322, 155)
        Me.GroupControl1.TabIndex = 13
        Me.GroupControl1.Text = "Sanction Limit"
        '
        'TextEdit9
        '
        Me.TextEdit9.EditValue = "000.00"
        Me.TextEdit9.Location = New System.Drawing.Point(234, 116)
        Me.TextEdit9.Name = "TextEdit9"
        Me.TextEdit9.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEdit9.Properties.Appearance.Options.UseFont = True
        Me.TextEdit9.Properties.Mask.EditMask = "f"
        Me.TextEdit9.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEdit9.Properties.MaxLength = 6
        Me.TextEdit9.Size = New System.Drawing.Size(70, 20)
        Me.TextEdit9.TabIndex = 7
        '
        'LabelControl15
        '
        Me.LabelControl15.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl15.Appearance.Options.UseFont = True
        Me.LabelControl15.Location = New System.Drawing.Point(123, 119)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(93, 14)
        Me.LabelControl15.TabIndex = 55
        Me.LabelControl15.Text = "Max Accrual Limit"
        '
        'TextEdit8
        '
        Me.TextEdit8.EditValue = "00.00"
        Me.TextEdit8.Location = New System.Drawing.Point(234, 90)
        Me.TextEdit8.Name = "TextEdit8"
        Me.TextEdit8.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEdit8.Properties.Appearance.Options.UseFont = True
        Me.TextEdit8.Properties.Mask.EditMask = "f"
        Me.TextEdit8.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEdit8.Properties.MaxLength = 5
        Me.TextEdit8.Size = New System.Drawing.Size(70, 20)
        Me.TextEdit8.TabIndex = 6
        '
        'TextEdit7
        '
        Me.TextEdit7.EditValue = "00.00"
        Me.TextEdit7.Location = New System.Drawing.Point(75, 90)
        Me.TextEdit7.Name = "TextEdit7"
        Me.TextEdit7.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEdit7.Properties.Appearance.Options.UseFont = True
        Me.TextEdit7.Properties.Mask.EditMask = "f"
        Me.TextEdit7.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEdit7.Properties.MaxLength = 5
        Me.TextEdit7.Size = New System.Drawing.Size(70, 20)
        Me.TextEdit7.TabIndex = 5
        '
        'LabelControl13
        '
        Me.LabelControl13.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl13.Appearance.Options.UseFont = True
        Me.LabelControl13.Location = New System.Drawing.Point(179, 93)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(32, 14)
        Me.LabelControl13.TabIndex = 52
        Me.LabelControl13.Text = "Leave"
        '
        'LabelControl14
        '
        Me.LabelControl14.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl14.Appearance.Options.UseFont = True
        Me.LabelControl14.Location = New System.Drawing.Point(17, 93)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(42, 14)
        Me.LabelControl14.TabIndex = 51
        Me.LabelControl14.Text = "Present"
        '
        'TextEdit6
        '
        Me.TextEdit6.EditValue = "00.00"
        Me.TextEdit6.Location = New System.Drawing.Point(234, 64)
        Me.TextEdit6.Name = "TextEdit6"
        Me.TextEdit6.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEdit6.Properties.Appearance.Options.UseFont = True
        Me.TextEdit6.Properties.Mask.EditMask = "f"
        Me.TextEdit6.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEdit6.Properties.MaxLength = 5
        Me.TextEdit6.Size = New System.Drawing.Size(70, 20)
        Me.TextEdit6.TabIndex = 4
        '
        'TextEdit5
        '
        Me.TextEdit5.EditValue = "00.00"
        Me.TextEdit5.Location = New System.Drawing.Point(75, 64)
        Me.TextEdit5.Name = "TextEdit5"
        Me.TextEdit5.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEdit5.Properties.Appearance.Options.UseFont = True
        Me.TextEdit5.Properties.Mask.EditMask = "f"
        Me.TextEdit5.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEdit5.Properties.MaxLength = 5
        Me.TextEdit5.Size = New System.Drawing.Size(70, 20)
        Me.TextEdit5.TabIndex = 3
        '
        'LabelControl12
        '
        Me.LabelControl12.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl12.Appearance.Options.UseFont = True
        Me.LabelControl12.Location = New System.Drawing.Point(179, 67)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(21, 14)
        Me.LabelControl12.TabIndex = 31
        Me.LabelControl12.Text = "Max"
        '
        'LabelControl11
        '
        Me.LabelControl11.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl11.Appearance.Options.UseFont = True
        Me.LabelControl11.Location = New System.Drawing.Point(17, 67)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(18, 14)
        Me.LabelControl11.TabIndex = 30
        Me.LabelControl11.Text = "Min"
        '
        'CheckEdit2
        '
        Me.CheckEdit2.Location = New System.Drawing.Point(141, 39)
        Me.CheckEdit2.Name = "CheckEdit2"
        Me.CheckEdit2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEdit2.Properties.Appearance.Options.UseFont = True
        Me.CheckEdit2.Properties.Caption = "Fixed"
        Me.CheckEdit2.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEdit2.Properties.RadioGroupIndex = 0
        Me.CheckEdit2.Size = New System.Drawing.Size(75, 19)
        Me.CheckEdit2.TabIndex = 2
        Me.CheckEdit2.TabStop = False
        '
        'CheckEdit1
        '
        Me.CheckEdit1.EditValue = True
        Me.CheckEdit1.Location = New System.Drawing.Point(17, 39)
        Me.CheckEdit1.Name = "CheckEdit1"
        Me.CheckEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEdit1.Properties.Appearance.Options.UseFont = True
        Me.CheckEdit1.Properties.Caption = "Carried"
        Me.CheckEdit1.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEdit1.Properties.RadioGroupIndex = 0
        Me.CheckEdit1.Size = New System.Drawing.Size(75, 19)
        Me.CheckEdit1.TabIndex = 1
        '
        'LabelControl16
        '
        Me.LabelControl16.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl16.Appearance.Options.UseFont = True
        Me.LabelControl16.Location = New System.Drawing.Point(12, 255)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(69, 14)
        Me.LabelControl16.TabIndex = 49
        Me.LabelControl16.Text = "Check PreFix"
        '
        'TogglePre
        '
        Me.TogglePre.Location = New System.Drawing.Point(159, 250)
        Me.TogglePre.Name = "TogglePre"
        Me.TogglePre.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TogglePre.Properties.Appearance.Options.UseFont = True
        Me.TogglePre.Properties.OffText = "Off"
        Me.TogglePre.Properties.OnText = "On"
        Me.TogglePre.Properties.ValueOff = "N"
        Me.TogglePre.Properties.ValueOn = "Y"
        Me.TogglePre.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.TogglePre.Size = New System.Drawing.Size(95, 25)
        Me.TogglePre.TabIndex = 11
        '
        'LabelControl17
        '
        Me.LabelControl17.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl17.Appearance.Options.UseFont = True
        Me.LabelControl17.Location = New System.Drawing.Point(12, 286)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(75, 14)
        Me.LabelControl17.TabIndex = 51
        Me.LabelControl17.Text = "Check PostFix"
        '
        'TogglePost
        '
        Me.TogglePost.Location = New System.Drawing.Point(159, 281)
        Me.TogglePost.Name = "TogglePost"
        Me.TogglePost.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TogglePost.Properties.Appearance.Options.UseFont = True
        Me.TogglePost.Properties.OffText = "Off"
        Me.TogglePost.Properties.OnText = "On"
        Me.TogglePost.Properties.ValueOff = "N"
        Me.TogglePost.Properties.ValueOn = "Y"
        Me.TogglePost.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.TogglePost.Size = New System.Drawing.Size(95, 25)
        Me.TogglePost.TabIndex = 12
        '
        'XtraLeaveMasterEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(616, 325)
        Me.ControlBox = False
        Me.Controls.Add(Me.LabelControl17)
        Me.Controls.Add(Me.TogglePost)
        Me.Controls.Add(Me.LabelControl16)
        Me.Controls.Add(Me.TogglePre)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.TextEdit4)
        Me.Controls.Add(Me.LabelControl10)
        Me.Controls.Add(Me.LabelControl9)
        Me.Controls.Add(Me.ToggleSwitch5)
        Me.Controls.Add(Me.ToggleSwitch4)
        Me.Controls.Add(Me.ToggleSwitch3)
        Me.Controls.Add(Me.ToggleSwitch2)
        Me.Controls.Add(Me.ToggleSwitch1)
        Me.Controls.Add(Me.LabelControl8)
        Me.Controls.Add(Me.LabelControl7)
        Me.Controls.Add(Me.LabelControl6)
        Me.Controls.Add(Me.LabelControl5)
        Me.Controls.Add(Me.ComboBoxEdit1)
        Me.Controls.Add(Me.LabelControl4)
        Me.Controls.Add(Me.TextEdit3)
        Me.Controls.Add(Me.LabelControl3)
        Me.Controls.Add(Me.TextEdit2)
        Me.Controls.Add(Me.LabelControl2)
        Me.Controls.Add(Me.TextEdit1)
        Me.Controls.Add(Me.LabelControl1)
        Me.Controls.Add(Me.SimpleButton2)
        Me.Controls.Add(Me.SimpleButton1)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.Name = "XtraLeaveMasterEdit"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Leave Master"
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleSwitch1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleSwitch2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleSwitch3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleSwitch4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleSwitch5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.TextEdit9.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit8.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TogglePre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TogglePost.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEdit1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEdit2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ComboBoxEdit1 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ToggleSwitch1 As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents ToggleSwitch2 As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents ToggleSwitch3 As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents ToggleSwitch4 As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents ToggleSwitch5 As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEdit4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CheckEdit2 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit1 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents TextEdit9 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEdit8 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit7 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEdit6 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit5 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TogglePre As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TogglePost As DevExpress.XtraEditors.ToggleSwitch
End Class
