﻿Imports System.Resources
Imports System.Globalization
Imports System.Data.SqlClient
Imports System.IO
Imports System.Data.OleDb
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.Net.NetworkInformation
Imports System.Text.RegularExpressions

Public Class XtraProfessionalTaxSlabEdit
    Dim ulf As UserLookAndFeel
    Dim ID_NO As Integer
    Private Sub SimpleButtonSave_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonSave.Click
   
        Me.Cursor = Cursors.WaitCursor
        Dim LOWERLIMIT As String = TextEditLowerLimit.Text.Trim
        Dim UPPERLIMIT As String = TextEditUpperLimit.Text.Trim
        Dim TAXAMOUNT As String = TextEditTaxAmt.Text.Trim
        Dim BRANCHCODE As String = GridLookUpEditBranch.EditValue
        'Dim FIXED As String
        'Dim gender As String

        Dim cmd As New SqlCommand
        Dim cmd1 As New OleDbCommand

        Dim sSql1 As String
        If XtraIncomTaxSlab.ITax_ID_NO = "" Then
            sSql1 = "insert into PROFESSIONALTAX (ID_NO,LOWERLIMIT,UPPERLIMIT,TAXAMOUNT,BRANCHCODE) " & _
                "values ('" & ID_NO & "','" & LOWERLIMIT & "','" & UPPERLIMIT & "','" & TAXAMOUNT & "','" & BRANCHCODE & "')"
        Else
            sSql1 = "UPDATE PROFESSIONALTAX SET LOWERLIMIT='" & LOWERLIMIT & "', UPPERLIMIT='" & UPPERLIMIT & "', TAXAMOUNT='" & TAXAMOUNT & "' , BRANCHCODE='" & BRANCHCODE & "' where ID_NO='" & ID_NO & "'"
        End If
        If Common.servername = "Access" Then
            Try
                If Common.con1.State <> ConnectionState.Open Then
                    Common.con1.Open()
                End If
                cmd1 = New OleDbCommand(sSql1, Common.con1)
                cmd1.ExecuteNonQuery()
                If Common.con1.State <> ConnectionState.Closed Then
                    Common.con1.Close()
                End If
            Catch ex As Exception
                If Common.con1.State <> ConnectionState.Closed Then
                    Common.con1.Close()
                End If
            End Try
        Else
            If Common.con.State <> ConnectionState.Open Then
                Common.con.Open()
            End If
            cmd = New SqlCommand(sSql1, Common.con)
            cmd.ExecuteNonQuery()
            If Common.con.State <> ConnectionState.Closed Then
                Common.con.Close()
            End If
        End If
        Me.Cursor = Cursors.Default
        XtraMessageBox.Show(ulf, "<size=10>Saved Successfully</size>", "<size=10>iAS</size>")
        Me.Close()
    End Sub

    Private Sub XtraCompanyInfo_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True
        'MAC =  getMacAddress()

        GetDefault()

    End Sub
    Private Sub GetDefault()
        GridLookUpEditBranch.Properties.DataSource = Common.LocationNonAdmin
        If XtraProfessionalTaxSlab.PTax_ID_NO = "" Then
            TextEditLowerLimit.Text = "0"
            TextEditUpperLimit.Text = "0"
            TextEditTaxAmt.Text = "0"
            Dim sSql As String = "Select * from PROFESSIONALTAX order by id_no desc"
            Dim adap As SqlDataAdapter
            Dim adapA As OleDbDataAdapter
            Dim dsL As DataSet = New DataSet
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(dsL)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(dsL)
            End If
            If dsL.Tables(0).Rows.Count = 0 Then
                ID_NO = 1
            Else
                ID_NO = dsL.Tables(0).Rows(0)("ID_NO") + 1
            End If
        Else
            Dim adap As SqlDataAdapter
            Dim adapA As OleDbDataAdapter
            Dim dsL As DataSet = New DataSet
            Dim sSql As String = "select * from PROFESSIONALTAX where ID_NO = " & XtraProfessionalTaxSlab.PTax_ID_NO
            'If Common.checkOpenedFromLicense = "Admin" Then
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(dsL)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(dsL)
            End If

            If dsL.Tables(0).Rows.Count > 0 Then
                TextEditLowerLimit.Text = dsL.Tables(0).Rows(0).Item("LOWERLIMIT").ToString.Trim
                TextEditUpperLimit.Text = dsL.Tables(0).Rows(0).Item("UPPERLIMIT").ToString.Trim
                TextEditTaxAmt.Text = dsL.Tables(0).Rows(0).Item("TAXAMOUNT").ToString.Trim
                GridLookUpEditBranch.EditValue = dsL.Tables(0).Rows(0).Item("BRANCHCODE").ToString
            End If
        End If
    End Sub
    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click
        Me.Close()
    End Sub
End Class