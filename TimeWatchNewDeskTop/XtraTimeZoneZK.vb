﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports System.ComponentModel
Imports DevExpress.XtraGrid.Columns
Imports System.Data.SqlClient
Imports System.Text
Imports System.Data.OleDb
Imports DevExpress.XtraGrid.Views.Base
Public Class XtraTimeZoneZK
    Dim ulf As UserLookAndFeel
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Public Shared TZID As String
    Public Shared TZIP As String
    Public Shared TZTime As String
    Public Shared TZDeviceType As String
    Public Sub New()
        InitializeComponent()
        Common.SetGridFont(GridView3, New Font("Tahoma", 10))
        Common.SetGridFont(GridView1, New Font("Tahoma", 10))
        Common.SetGridFont(GridViewEmp, New Font("Tahoma", 10))
    End Sub
    Private Sub XtraTimeZone_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True

        'If Common.servername = "Access" Then
        '    Me.TblMachine1TableAdapter1.Fill(Me.SSSDBDataSet.tblMachine1)
        '    GridControl1.DataSource = SSSDBDataSet.tblMachine1
        '    Me.TblEmployee1TableAdapter1.Fill(Me.SSSDBDataSet.TblEmployee1)
        '    GridControlEmp.DataSource = SSSDBDataSet.TblEmployee1
        'Else
        '    TblMachineTableAdapter.Connection.ConnectionString = Common.ConnectionString
        '    TblEmployeeTableAdapter.Connection.ConnectionString = Common.ConnectionString
        '    Me.TblMachineTableAdapter.Fill(Me.SSSDBDataSet.tblMachine)
        '    GridControl1.DataSource = SSSDBDataSet.tblMachine
        '    Me.TblEmployeeTableAdapter.Fill(Me.SSSDBDataSet.TblEmployee)
        '    GridControlEmp.DataSource = SSSDBDataSet.TblEmployee
        'End If
        GridControlEmp.DataSource = Common.EmpNonAdmin
        GridControl1.DataSource = Common.MachineNonAdmin

        GridControl3.DataSource = Nothing
        TextEditTZ1.Text = ""
        TextEditTZ2.Text = ""
        TextEditTZ3.Text = ""
        PopupContainerEditEmp.EditValue = ""
        setDeviceGrid()
    End Sub
    Private Sub setDeviceGrid()
        Dim gridtblregisterselet As String
        If Common.USERTYPE = "A" Then
            gridtblregisterselet = "select * from tblMachine where DeviceType = 'ZK(TFT)' or DeviceType='Bio-1Pro/ATF305Pro/ATF686Pro' "
        Else
            'gridtblregisterselet = "select * from tblMachine where DeviceType = 'ZK(TFT)' or DeviceType='Bio-1Pro/ATF305Pro/ATF686Pro' "
            Dim emp() As String = Common.Auth_Branch.Split(",")
            Dim ls As New List(Of String)()
            For x As Integer = 0 To emp.Length - 1
                ls.Add(emp(x).Trim)
            Next
            gridtblregisterselet = "select tblMachine.ID_NO, tblMachine.A_R, tblMachine.IN_OUT,tblMachine.DeviceType,tblMachine.LOCATION,tblMachine.branch,tblMachine.commkey,tblMachine.MAC_ADDRESS,tblMachine.Purpose,tblMachine.LastModifiedBy,tblMachine.LastModifiedDate from tblMachine, tblbranch where tblMachine.branch=tblbranch.BRANCHNAME and tblbranch.BRANCHCODE in ('" & String.Join("', '", ls.ToArray()) & "') and DeviceType = 'ZK(TFT)' or DeviceType='Bio-1Pro/ATF305Pro/ATF686Pro' "
        End If

        Dim WTDataTable As DataTable
        If Common.servername = "Access" Then
            Dim dataAdapter As New OleDbDataAdapter(gridtblregisterselet, Common.con1)
            WTDataTable = New DataTable("tblMachine")
            dataAdapter.Fill(WTDataTable)
        Else
            Dim dataAdapter As New SqlClient.SqlDataAdapter(gridtblregisterselet, Common.con)
            WTDataTable = New DataTable("tblMachine")
            dataAdapter.Fill(WTDataTable)
        End If
        GridControl1.DataSource = WTDataTable
    End Sub
    Private Sub PopupContainerEditEmp_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEditEmp.QueryResultValue
        Dim selectedRows() As Integer = GridViewEmp.GetSelectedRows
        Dim sb As StringBuilder = New StringBuilder
        For Each selectionRow As Integer In selectedRows
            Dim a As System.Data.DataRowView = GridViewEmp.GetRow(selectionRow)
            If (sb.ToString.Length > 0) Then
                sb.Append(", ")
            End If
            sb.Append(a.Item("PAYCODE"))
        Next
        e.Value = sb.ToString
    End Sub
    Private Sub PopupContainerEditEmp_QueryPopUp(sender As System.Object, e As System.ComponentModel.CancelEventArgs) Handles PopupContainerEditEmp.QueryPopUp
        Dim val As Object = PopupContainerEditEmp.EditValue
        If (val Is Nothing) Then
            GridViewEmp.ClearSelection()
        Else
            Dim texts() As String = val.ToString.Split(",")
            For Each text As String In texts
                If text.Trim.Length = 1 Then
                    text = text.Trim & "  "
                ElseIf text.Trim.Length = 2 Then
                    text = text.Trim & " "
                End If
                'MsgBox(text & "  " & text.Length & " " & GridView1.LocateByValue("SHIFT", text))
                Dim rowHandle As Integer = GridViewEmp.LocateByValue("PAYCODE", text.Trim)
                GridViewEmp.SelectRow(rowHandle)
            Next
        End If
    End Sub
    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click
        Me.Close()
    End Sub
    Private Sub btnGetTZ_Click(sender As System.Object, e As System.EventArgs) Handles btnGetTZ.Click
        If GridView1.GetSelectedRows.Count = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>Please select the Machine</size>", "<size=9>Error</size>")
            Exit Sub
        ElseIf GridView1.GetSelectedRows.Count > 1 Then
            XtraMessageBox.Show(ulf, "<size=10>Please select One Machine at a Time</size>", "<size=9>Error</size>")
            Exit Sub
        End If
        Me.Cursor = Cursors.WaitCursor
        GetTZ()
        Me.Cursor = Cursors.Default
    End Sub
    Private Sub GetTZ()
        GridControl3.DataSource = Nothing
        Dim dt As DataTable = New DataTable
        dt.Columns.Add("Device IP")
        dt.Columns.Add("TimeZone ID")
        dt.Columns.Add("SUN Start")
        dt.Columns.Add("SUN End")

        dt.Columns.Add("MON Start")
        dt.Columns.Add("MON End")

        dt.Columns.Add("TUE Start")
        dt.Columns.Add("TUE End")

        dt.Columns.Add("WED Start")
        dt.Columns.Add("WED End")

        dt.Columns.Add("THU Start")
        dt.Columns.Add("THU End")

        dt.Columns.Add("FRI Start")
        dt.Columns.Add("FRI End")

        dt.Columns.Add("SAT Start")
        dt.Columns.Add("SAT End")

        'dt.Rows.Add(ds.Tables(0).Rows(i).Item("HDate").ToString.Split(" ")(0), ds.Tables(0).Rows(i).Item("HOLIDAY").ToString, ds.Tables(0).Rows(i).Item("companycode").ToString)

        Dim m As Integer
        'Dim e As Integer
        Dim lngMachineNum As Long
        Dim mCommKey As Long
        Dim lpszIPAddress As String
        Dim mKey As String
        Dim vnMachineNumber As Long
        Dim vnCommPort As Long
        Dim vnCommBaudrate As Long
        Dim vstrTelNumber As String
        Dim vnWaitDialTime As Long
        Dim vnLicense As Long
        Dim vpszIPAddress As String
        Dim vpszNetPort As Long
        Dim vpszNetPassword As Long
        Dim vnTimeOut As Long
        Dim vnProtocolType As Long
        Dim strDateTime As String
        Dim vRet As Long
        Dim nCommHandleIndex As Long
        Dim vPrivilege As Long


        Dim sSql As String = ""
        Dim selectedRows As Integer() = GridView1.GetSelectedRows()
        Dim result As Object() = New Object(selectedRows.Length - 1) {}
        Dim LstMachineId As String
        Dim commkey As Integer
        For i = 0 To selectedRows.Length - 1
            Dim rowHandle As Integer = selectedRows(i)
            If Not GridView1.IsGroupRow(rowHandle) Then
                LstMachineId = GridView1.GetRowCellValue(rowHandle, "ID_NO").ToString.Trim

                If GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Bio-1/3/4/5/6/7/ATF-203/ATF-395/ACR-872" Then
                    ''sSql = "select * from tblmachine where ID_NO='" & LstMachineId & "'"
                    'Dim bConn As Boolean = False
                    'vnMachineNumber = LstMachineId
                    'vnLicense = 1261 '1789 '
                    'lpszIPAddress = GridView1.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim 'Trim(rstm!Location)
                    'vpszIPAddress = Trim(lpszIPAddress)
                    'vpszNetPort = CLng("5005")
                    'vpszNetPassword = CLng("0")
                    'vnTimeOut = CLng("5000")
                    'vnProtocolType = 0 'PROTOCOL_TCPIP
                    ''bConn = FK623Attend.ConnectNet(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
                    'If GridView1.GetRowCellValue(rowHandle, "A_R").ToString.Trim = "S" Then
                    '    vRet = FK_ConnectUSB(vnMachineNumber, vnLicense)
                    'Else
                    '    vpszIPAddress = Trim(lpszIPAddress)
                    '    vpszNetPort = CLng("5005")
                    '    vpszNetPassword = CLng("0")
                    '    vnTimeOut = CLng("5000")
                    '    vnProtocolType = 0
                    '    vRet = FK_ConnectNet(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
                    '    nCommHandleIndex = vRet
                    'End If
                    'If vRet > 0 Then
                    '    bConn = True
                    'Else
                    '    bConn = False
                    'End If
                    'If bConn Then

                    '    Dim bytTimeZone(SIZE_TIME_ZONE_STRUCT - 1) As Byte
                    '    Dim vTimeZone As TIME_ZONE
                    '    Dim TimeZoneId As Integer
                    '    Dim nFKRetCode As Integer

                    '    nFKRetCode = FK_EnableDevice(vRet, 0)
                    '    If nFKRetCode <> RUN_SUCCESS Then
                    '        Continue For
                    '    End If
                    '    For x As Integer = 0 To 150
                    '        TimeZoneId = 1 'cmbTZN.SelectedIndex + 1                       
                    '        vTimeZone.Init()
                    '        vTimeZone.TimeZoneId = TimeZoneId
                    '        ConvertStructureToByteArray(vTimeZone, bytTimeZone)
                    '        nFKRetCode = FK_HS_GetTimeZone(vRet, bytTimeZone)
                    '        If nFKRetCode = RUN_SUCCESS Then
                    '            vTimeZone = ConvertByteArrayToStructure(bytTimeZone, GetType(TIME_ZONE))
                    '            'ShowTimeZoneValue(vTimeZone)

                    '            'mtxtStartHour(k).Text = Convert.ToString(vTimeZone.TimeSlots(k).StartHour)
                    '            'mtxtStartMinute(k).Text = Convert.ToString(vTimeZone.TimeSlots(k).StartMinute)
                    '            'mtxtEndHour(k).Text = Convert.ToString(vTimeZone.TimeSlots(k).EndHour)
                    '            'mtxtEndMinute(k).Text = Convert.ToString(vTimeZone.TimeSlots(k).EndMinute)

                    '            dt.Rows.Add(vpszIPAddress, x, Convert.ToString(vTimeZone.TimeSlots(0).StartHour), Convert.ToString(vTimeZone.TimeSlots(0).StartMinute), _
                    '                        Convert.ToString(vTimeZone.TimeSlots(1).StartHour), Convert.ToString(vTimeZone.TimeSlots(1).StartMinute), _
                    '                        Convert.ToString(vTimeZone.TimeSlots(2).StartHour), Convert.ToString(vTimeZone.TimeSlots(2).StartMinute), _
                    '                        Convert.ToString(vTimeZone.TimeSlots(3).StartHour), Convert.ToString(vTimeZone.TimeSlots(3).StartMinute), _
                    '                        Convert.ToString(vTimeZone.TimeSlots(4).StartHour), Convert.ToString(vTimeZone.TimeSlots(4).StartMinute), _
                    '                        Convert.ToString(vTimeZone.TimeSlots(5).StartHour), Convert.ToString(vTimeZone.TimeSlots(5).StartMinute)) ' _
                    '            ' Convert.ToString(vTimeZone.TimeSlots(6).StartHour), Convert.ToString(vTimeZone.TimeSlots(6).StartMinute))
                    '        Else
                    '            ' lblMessage.Text = ReturnResultPrint(nFKRetCode)
                    '        End If
                    '    Next

                    '    FK_EnableDevice(vRet, 1)
                    'Else
                    '    XtraMessageBox.Show(ulf, "<size=10>Device No: " & LstMachineId & " Not connected..</size>", "Information")
                    'End If
                    ' ''FK623Attend.Disconnect
                    'FK_DisConnect(nCommHandleIndex)
                ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "ZK(TFT)" Or GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Bio-1Pro/ATF305Pro/ATF686Pro" Then
                    commkey = Convert.ToInt32(GridView1.GetRowCellValue(rowHandle, "commkey").ToString.Trim)
                    Dim sdwEnrollNumber As String
                    Dim sName As String = ""
                    Dim sPassword As String = ""
                    Dim iPrivilege As Integer
                    Dim idwFingerIndex As Integer
                    Dim sTmpData As String = ""
                    Dim sEnabled As String = ""
                    Dim bEnabled As Boolean = False
                    Dim iflag As Integer

                    lpszIPAddress = GridView1.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim 'Trim(rstm!Location)
                    vpszIPAddress = Trim(lpszIPAddress)

                    Dim bIsConnected = False
                    Dim iMachineNumber As Integer = result(i)
                    Dim idwErrorCode As Integer
                    Dim com As Common = New Common
                    Dim axCZKEM1 As New zkemkeeper.CZKEM
                    axCZKEM1.SetCommPassword(Convert.ToInt32(commkey))  'to check device commkey and db commkey matches
                    bIsConnected = axCZKEM1.Connect_Net(vpszIPAddress, 4370)
                    If bIsConnected = True Then
                        'axCZKEM1.EnableDevice(iMachineNumber, False)
                        'loop for timezone id, ZK support only 50 timezone IDs
                        For x As Integer = 1 To 50
                            Dim sTimeZone As String = ""
                            If axCZKEM1.GetTZInfo(iMachineNumber, x, sTimeZone) Then
                                Dim array As String() = New String(sTimeZone.Length / 2 - 1) {}
                                Dim i1 As Integer, j As Integer = 0
                                i1 = 0
                                While (i1 + 2) <= sTimeZone.Length AndAlso sTimeZone.Length >= i1
                                    array(j) = sTimeZone.Substring(i1, 2)
                                    j += 1
                                    i1 = i1 + 2
                                End While
                                'If array(0) = "00" And array(1) = "00" And array(2) = "23" And array(3) = "59" And _
                                '     array(4) = "00" And array(5) = "00" And array(6) = "23" And array(7) = "59" And _
                                '      array(8) = "00" And array(9) = "00" And array(10) = "23" And array(11) = "59" And _
                                '       array(12) = "00" And array(13) = "00" And array(14) = "23" And array(15) = "59" And _
                                '     array(16) = "00" And array(17) = "00" And array(18) = "23" And array(19) = "59" And _
                                '      array(20) = "00" And array(21) = "00" And array(22) = "23" And array(23) = "59" And _
                                '       array(24) = "00" And array(25) = "00" And array(26) = "23" And array(27) = "59" Then
                                'Else
                                dt.Rows.Add(vpszIPAddress, x, array(0) & ":" & array(1), array(2) & ":" & array(3), array(4) & ":" & array(5), _
                                        array(6) & ":" & array(7), array(8) & ":" & array(9), array(10) & ":" & array(11), array(12) & ":" & array(13), _
                                        array(14) & ":" & array(15), array(16) & ":" & array(17), array(18) & ":" & array(19), array(20) & ":" & array(21), _
                                        array(22) & ":" & array(23), array(24) & ":" & array(25), array(26) & ":" & array(27))
                                'End If
                            Else
                                axCZKEM1.GetLastError(idwErrorCode)
                                'axCZKEM1.EnableDevice(iMachineNumber, True)
                                Return
                            End If
                            'axCZKEM1.EnableDevice(iMachineNumber, True)
                        Next
                        'axCZKEM1.EnableDevice(iMachineNumber, True)
                    Else
                        axCZKEM1.GetLastError(idwErrorCode)
                        XtraMessageBox.Show(ulf, "<size=10>Device No: " & LstMachineId & " Not connected..</size>", "Information")
                        Continue For
                    End If
                End If
            End If
        Next i
        Dim datase As DataSet = New DataSet()
        datase.Tables.Add(dt)
        GridControl3.DataSource = dt
        GridView3.Columns(0).Width = 120
    End Sub

    Private Sub btnSaveTZ_Click(sender As System.Object, e As System.EventArgs) Handles btnSaveTZ.Click
        Dim m As Integer
        'Dim e As Integer
        Dim lngMachineNum As Long
        Dim mCommKey As Long
        Dim lpszIPAddress As String
        Dim mKey As String
        Dim vnMachineNumber As Long
        Dim vnCommPort As Long
        Dim vnCommBaudrate As Long
        Dim vstrTelNumber As String
        Dim vnWaitDialTime As Long
        Dim vnLicense As Long
        Dim vpszIPAddress As String
        Dim vpszNetPort As Long
        Dim vpszNetPassword As Long
        Dim vnTimeOut As Long
        Dim vnProtocolType As Long
        Dim strDateTime As String
        Dim vRet As Long
        Dim vPrivilege As Long

        If GridView1.GetSelectedRows.Count = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>Please select the Machine</size>", "<size=9>Error</size>")
            Exit Sub
        End If

        If GridViewEmp.GetSelectedRows.Count = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>Please select Employee</size>", "<size=9>Error</size>")
            Exit Sub
        End If
        Dim iTZ1 As Integer '= Convert.ToInt32(TextEditTZ1.Text.Trim)
        Dim iTZ2 As Integer '= Convert.ToInt32(TextEditTZ2.Text.Trim)
        Dim iTZ3 As Integer '= Convert.ToInt32(TextEditTZ3.Text.Trim)
        If TextEditTZ1.Text.Trim = "" Then
            iTZ1 = 0
        Else
            iTZ1 = Convert.ToInt32(TextEditTZ1.Text.Trim)
        End If
        If TextEditTZ2.Text.Trim = "" Then
            iTZ2 = 0
        Else
            iTZ2 = Convert.ToInt32(TextEditTZ2.Text.Trim)
        End If
        If TextEditTZ3.Text.Trim = "" Then
            iTZ3 = 0
        Else
            iTZ3 = Convert.ToInt32(TextEditTZ3.Text.Trim)
        End If

        If iTZ1 < 0 OrElse iTZ1 > 50 OrElse iTZ2 < 0 OrElse iTZ2 > 50 OrElse iTZ3 < 0 OrElse iTZ3 > 50 Then
            XtraMessageBox.Show(ulf, "<size=10>Timezone index error!</size>", "<size=9>Error</size>")
            Exit Sub
        End If
        Dim sSql As String = ""
        Dim selectedRows As Integer() = GridView1.GetSelectedRows()
        Dim result As Object() = New Object(selectedRows.Length - 1) {}
        Dim LstMachineId As String
        Dim commkey As Integer
        For i = 0 To selectedRows.Length - 1
            Dim rowHandle As Integer = selectedRows(i)
            If Not GridView1.IsGroupRow(rowHandle) Then
                LstMachineId = GridView1.GetRowCellValue(rowHandle, "ID_NO").ToString.Trim
                Common.LogPost("Employee TimeZone Set in Device; Device Id='" & LstMachineId)
                If GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Bio-1/3/4/5/6/7/ATF-203/ATF-395/ACR-872" Then
                    'sSql = "select * from tblmachine where ID_NO='" & LstMachineId & "'"
                    'Dim bConn As Boolean = False
                    'vnMachineNumber = LstMachineId
                    'vnLicense = 1261 '1789 '
                    'lpszIPAddress = GridView1.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim 'Trim(rstm!Location)
                    'vpszIPAddress = Trim(lpszIPAddress)
                    'vpszNetPort = CLng("5005")
                    'vpszNetPassword = CLng("0")
                    'vnTimeOut = CLng("5000")
                    'vnProtocolType = 0 'PROTOCOL_TCPIP
                    ''bConn = FK623Attend.ConnectNet(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
                    'If GridView1.GetRowCellValue(rowHandle, "A_R").ToString.Trim = "S" Then
                    '    vRet = FK_ConnectUSB(vnMachineNumber, vnLicense)
                    '    'vstrTelNumber = ""
                    '    'vnWaitDialTime = 0
                    '    'vnCommPort = GridView1.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim 'Val(Trim(rstm!Location))
                    '    'vnCommBaudrate = 38400
                    '    ''*************************
                    'Else
                    '    vpszIPAddress = Trim(lpszIPAddress)
                    '    vpszNetPort = CLng("5005")
                    '    vpszNetPassword = CLng("0")
                    '    vnTimeOut = CLng("5000")
                    '    vnProtocolType = 0
                    '    vRet = FK_ConnectNet(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
                    'End If
                    'If vRet > 0 Then
                    '    bConn = True
                    'Else
                    '    bConn = False
                    'End If
                    'If bConn Then
                    '    Dim selectedRowsEmp As Integer() = GridView2.GetSelectedRows()
                    '    Dim resultEmp As Object() = New Object(selectedRowsEmp.Length - 1) {}
                    '    For x As Integer = 0 To selectedRowsEmp.Length - 1
                    '        'LstEmployeesTarget.ListIndex = e
                    '        Dim rowHandleEmp As Integer = selectedRowsEmp(x)
                    '        If Not GridView2.IsGroupRow(rowHandleEmp) Then
                    '            'MsgBox(GridView2.GetRowCellValue(rowHandleEmp, "EnrollNumber").ToString.Trim)
                    '            'MsgBox(GridView2.GetRowCellValue(rowHandleEmp, "FingerNumber").ToString.Trim)
                    '            Dim EnrollNumber As String = GridView2.GetRowCellValue(rowHandleEmp, "EnrollNumber").ToString.Trim
                    '            Dim FingerNumber As Long = GridView2.GetRowCellValue(rowHandleEmp, "FingerNumber").ToString.Trim
                    '            XtraMaster.LabelControl4.Text = "Uploading Template " & EnrollNumber
                    '            Application.DoEvents()

                    '            cmdSetAllEnrollData_bio_All(vnMachineNumber, EnrollNumber, FingerNumber)
                    '        End If
                    '    Next x
                    'Else
                    '    'MsgBox("Device No: " & LstMachineId & " Not connected..")
                    '    XtraMessageBox.Show(ulf, "<size=10>Device No: " & LstMachineId & " Not connected..</size>", "Information")
                    'End If
                    ''FK623Attend.Disconnect
                    'FK_DisConnect(vRet)
                ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "ZK(TFT)" Or GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Bio-1Pro/ATF305Pro/ATF686Pro" Then
                    Dim sdwEnrollNumber As String
                    Dim sName As String = ""
                    Dim sPassword As String = ""
                    Dim iPrivilege As Integer
                    Dim idwFingerIndex As Integer
                    Dim sTmpData As String = ""
                    Dim sEnabled As String = ""
                    Dim bEnabled As Boolean = False
                    Dim iflag As Integer
                    commkey = Convert.ToInt32(GridView1.GetRowCellValue(rowHandle, "commkey").ToString.Trim)
                    lpszIPAddress = GridView1.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim 'Trim(rstm!Location)
                    vpszIPAddress = Trim(lpszIPAddress)

                    Dim bIsConnected = False
                    Dim iMachineNumber As Integer = result(i)
                    Dim idwErrorCode As Integer
                    Dim com As Common = New Common
                    Dim axCZKEM1 As New zkemkeeper.CZKEM
                    axCZKEM1.SetCommPassword(Convert.ToInt32(commkey))  'to check device commkey and db commkey matches
                    bIsConnected = axCZKEM1.Connect_Net(vpszIPAddress, 4370)
                    If bIsConnected = True Then
                        'axCZKEM1.EnableDevice(iMachineNumber, False)
                        'loop for emp
                        Dim selectedRowsEmp As Integer() = GridViewEmp.GetSelectedRows()
                        Dim resultEmp As Object() = New Object(selectedRowsEmp.Length - 1) {}
                        For x As Integer = 0 To selectedRowsEmp.Length - 1
                            'LstEmployeesTarget.ListIndex = e
                            Dim rowHandleEmp As Integer = selectedRowsEmp(x)
                            If Not GridViewEmp.IsGroupRow(rowHandleEmp) Then
                                Dim mUser_ID As String = GridViewEmp.GetRowCellValue(rowHandleEmp, "PRESENTCARDNO").ToString.Trim()
                                XtraMasterTest.LabelControlStatus.Text = "Setting TimeZone for " & GridViewEmp.GetRowCellValue(rowHandleEmp, "PAYCODE").ToString.Trim()
                                Application.DoEvents()
                                sdwEnrollNumber = Convert.ToInt64(mUser_ID)
                                Dim sTZs As String = iTZ1.ToString() & ":" & iTZ2.ToString() & ":" & iTZ3.ToString() & ":" & "1"
                                Dim vRetT As Boolean = axCZKEM1.SetUserTZStr(iMachineNumber, sdwEnrollNumber, sTZs)
                                If vRetT = True Then
                                    axCZKEM1.RefreshData(iMachineNumber)
                                End If
                                'Dim s As String = "select EMPNAME from TblEmployee where PRESENTCARDNO = '" & mUser_ID & "'"
                                'Dim adapZ As SqlDataAdapter
                                'Dim adapAZ As OleDbDataAdapter
                                'Dim dstmp As DataSet = New DataSet
                                'If Common.servername = "Access" Then
                                '    adapAZ = New OleDbDataAdapter(s, Common.con1)
                                '    adapAZ.Fill(dstmp)
                                'Else
                                '    adapZ = New SqlDataAdapter(s, Common.con)
                                '    adapZ.Fill(dstmp)
                                'End If
                                ''Dim FpNo As String = GridViewEmp.GetRowCellValue(rowHandleEmp, "FingerNumber").ToString.Trim()
                                'sSql = "select * from fptable where enrollnumber='" & mUser_ID & "' and FingerNumber=" & FpNo & " order by FingerNumber"
                                'If Common.servername = "Access" Then
                                '    adapA = New OleDbDataAdapter(sSql, Common.con1)
                                '    adapA.Fill(RsFp)
                                'Else
                                '    adap = New SqlDataAdapter(sSql, Common.con)
                                '    adap.Fill(RsFp)
                                'End If


                                'If dstmp.Tables(0).Rows.Count > 0 Then
                                '    sName = dstmp.Tables(0).Rows(0)("EMPNAME").ToString().Trim()
                                'Else
                                '    sName = ""
                                'End If
                                'idwFingerIndex = Convert.ToInt32(RsFp.Tables(0).Rows(0)("FingerNumber").ToString().Trim())
                                'sTmpData = RsFp.Tables(0).Rows(0)("Template").ToString().Trim()
                                'iPrivilege = Convert.ToInt32(RsFp.Tables(0).Rows(0)("Privilege").ToString().Trim()) 'told by ajitesh(used for admin)
                                'sPassword = RsFp.Tables(0).Rows(0)("Password").ToString().Trim()
                                'sEnabled = "true" 'RsFp.Tables(0).Rows(0)(6).ToString().Trim()
                                'iflag = 0 'Convert.ToInt32(RsFp.Tables(0).Rows(0)(7).ToString())' as per old s/w
                                'Dim icard As String = RsFp.Tables(0).Rows(0)("CardNumber").ToString().Trim()

                                'If sEnabled.ToString().ToLower() = "true" Then
                                '    bEnabled = True
                                'Else
                                '    bEnabled = False
                                'End If
                                'Dim iBackupNumber As String
                                'If icard <> "" Then
                                '    iBackupNumber = idwFingerIndex
                                '    axCZKEM1.RefreshData(iMachineNumber)
                                '    'xxen = True
                                '    axCZKEM1.CardNumber(0) = icard
                                '    axCZKEM1.SSR_SetUserInfo(iMachineNumber, sdwEnrollNumber, sName, sPassword, iPrivilege, True)
                                '    'Sleep(100)
                                '    'TZS = "1:2:3"
                                '    'vRet = axCZKEM1.SetUserTmpExStr(iMachineNumber, sdwEnrollNumber, iBackupNumber, 0, sTmpData)
                                '    ''Sleep (200)
                                '    'If vRet = False Then
                                '    '    Sleep(200)
                                '    '    vRet = axCZKEM1.SetUserTmpExStr(iMachineNumber, sdwEnrollNumber, iBackupNumber, 0, sTmpData)
                                '    'End If
                                '    If vRet = False Then
                                '    End If
                                '    axCZKEM1.RefreshData(iMachineNumber)
                                'Else
                                '    If axCZKEM1.SSR_SetUserInfo(iMachineNumber, sdwEnrollNumber, sName, sPassword, iPrivilege, bEnabled) Then 'upload user information to the device
                                '        If idwFingerIndex = 50 Then
                                '            axCZKEM1.SetUserFaceStr(iMachineNumber, sdwEnrollNumber, idwFingerIndex, sTmpData, sTmpData.Length)  'face
                                '        Else
                                '            axCZKEM1.SetUserTmpExStr(iMachineNumber, sdwEnrollNumber, idwFingerIndex, iflag, sTmpData) 'upload templates information to the device
                                '        End If
                                '    Else
                                '        axCZKEM1.GetLastError(idwErrorCode)
                                '        MsgBox("Operation failed,ErrorCode=" & idwErrorCode.ToString(), MsgBoxStyle.Exclamation, "Error")
                                '        axCZKEM1.EnableDevice(iMachineNumber, True)
                                '        'Cursor = Cursors.Default
                                '        Return
                                '    End If
                                'End If

                            End If
                        Next
                        'axCZKEM1.EnableDevice(iMachineNumber, True)
                    Else
                        axCZKEM1.GetLastError(idwErrorCode)
                        Continue For
                    End If
                End If

            End If
        Next i
        XtraMasterTest.LabelControlStatus.Text = ""
        Application.DoEvents()
        XtraMessageBox.Show(ulf, "<size=10>Task Completed</size>", "<size=9>Information</size>")
    End Sub
    Private Sub GridView3_EditFormShowing(sender As System.Object, e As DevExpress.XtraGrid.Views.Grid.EditFormShowingEventArgs) Handles GridView3.EditFormShowing
        Dim row As System.Data.DataRow = GridView3.GetDataRow(GridView3.FocusedRowHandle)
        TZID = row("TimeZone ID").ToString.Trim
        TZIP = row("Device IP").ToString.Trim
        TZTime = row("SUN Start").ToString.Trim & " " & row("SUN End").ToString.Trim & " " & _
            row("MON Start").ToString.Trim & " " & row("MON End").ToString.Trim & " " & _
            row("TUE Start").ToString.Trim & " " & row("TUE End").ToString.Trim & " " & _
            row("WED Start").ToString.Trim & " " & row("WED End").ToString.Trim & " " & _
            row("THU Start").ToString.Trim & " " & row("THU End").ToString.Trim & " " & _
            row("FRI Start").ToString.Trim & " " & row("FRI End").ToString.Trim & " " & _
            row("SAT Start").ToString.Trim & " " & row("SAT End").ToString.Trim

        TZDeviceType = ""
        e.Allow = False
        XtraTimeZoneEditZK.ShowDialog()
        Me.Cursor = Cursors.WaitCursor
        GetTZ()
        Me.Cursor = Cursors.Default
    End Sub
End Class