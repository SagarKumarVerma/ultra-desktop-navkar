﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid.Views.Base
Imports DevExpress.XtraEditors.Repository
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports System.Text.RegularExpressions

Public Class XtraPayrollSetup
    Dim ulf As UserLookAndFeel
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Public Sub New()
        InitializeComponent()
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True

        If Common.servername = "Access" Then
            Me.PaY_FORMULA1TableAdapter1.Fill(Me.SSSDBDataSet.PAY_FORMULA1)
            Me.TblLeaveMaster1TableAdapter1.Fill(Me.SSSDBDataSet.tblLeaveMaster1)
            'GridControl1.DataSource = SSSDBDataSet.PAY_FORMULA1
        Else
            PAY_FORMULATableAdapter.Connection.ConnectionString = Common.ConnectionString
            Me.PAY_FORMULATableAdapter.Fill(Me.SSSDBDataSet.PAY_FORMULA)

            TblLeaveMasterTableAdapter.Connection.ConnectionString = Common.ConnectionString
            Me.TblLeaveMasterTableAdapter.Fill(Me.SSSDBDataSet.tblLeaveMaster)
            'GridControl1.DataSource = SSSDBDataSet.PAY_FORMULA
        End If
        Common.SetGridFont(GridView1, New Font("Tahoma", 10))
    End Sub
    Private Sub XtraPayrollSetup_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Me.Width = Common.NavWidth 'Me.Parent.Width
        Me.Height = Common.NavHeight 'Me.Parent.Height
        SplitContainerControl1.SplitterPosition = Common.SplitterPosition '(SplitContainerControl1.Parent.Width) * 85 / 100

        If Common.servername = "Access" Then
            Me.PaY_FORMULA1TableAdapter1.Fill(Me.SSSDBDataSet.PAY_FORMULA1)
            GridLookUpEditFormulaE1.Properties.DataSource = SSSDBDataSet.PAY_FORMULA1
            GridLookUpEditFormulaE2.Properties.DataSource = SSSDBDataSet.PAY_FORMULA1
            GridLookUpEditFormulaE3.Properties.DataSource = SSSDBDataSet.PAY_FORMULA1
            GridLookUpEditFormulaE4.Properties.DataSource = SSSDBDataSet.PAY_FORMULA1
            GridLookUpEditFormulaE5.Properties.DataSource = SSSDBDataSet.PAY_FORMULA1
            GridLookUpEditFormulaE6.Properties.DataSource = SSSDBDataSet.PAY_FORMULA1
            GridLookUpEditFormulaE7.Properties.DataSource = SSSDBDataSet.PAY_FORMULA1
            GridLookUpEditFormulaE8.Properties.DataSource = SSSDBDataSet.PAY_FORMULA1
            GridLookUpEditFormulaE9.Properties.DataSource = SSSDBDataSet.PAY_FORMULA1
            GridLookUpEditFormulaE10.Properties.DataSource = SSSDBDataSet.PAY_FORMULA1

            GridLookUpEditFormulaD1.Properties.DataSource = SSSDBDataSet.PAY_FORMULA1
            GridLookUpEditFormulaD2.Properties.DataSource = SSSDBDataSet.PAY_FORMULA1
            GridLookUpEditFormulaD3.Properties.DataSource = SSSDBDataSet.PAY_FORMULA1
            GridLookUpEditFormulaD4.Properties.DataSource = SSSDBDataSet.PAY_FORMULA1
            GridLookUpEditFormulaD5.Properties.DataSource = SSSDBDataSet.PAY_FORMULA1
            GridLookUpEditFormulaD6.Properties.DataSource = SSSDBDataSet.PAY_FORMULA1
            GridLookUpEditFormulaD7.Properties.DataSource = SSSDBDataSet.PAY_FORMULA1
            GridLookUpEditFormulaD8.Properties.DataSource = SSSDBDataSet.PAY_FORMULA1
            GridLookUpEditFormulaD9.Properties.DataSource = SSSDBDataSet.PAY_FORMULA1
            GridLookUpEditFormulaD10.Properties.DataSource = SSSDBDataSet.PAY_FORMULA1
            GridLookUpEditFormulaD11.Properties.DataSource = SSSDBDataSet.PAY_FORMULA1

            Me.TblLeaveMaster1TableAdapter1.Fill(Me.SSSDBDataSet.tblLeaveMaster1)
            'GridControlLeave.DataSource = SSSDBDataSet.tblLeaveMaster1
        Else
            PAY_FORMULATableAdapter.Connection.ConnectionString = Common.ConnectionString
            Me.PAY_FORMULATableAdapter.Fill(Me.SSSDBDataSet.PAY_FORMULA)
            GridLookUpEditFormulaE1.Properties.DataSource = SSSDBDataSet.PAY_FORMULA
            GridLookUpEditFormulaE2.Properties.DataSource = SSSDBDataSet.PAY_FORMULA
            GridLookUpEditFormulaE3.Properties.DataSource = SSSDBDataSet.PAY_FORMULA
            GridLookUpEditFormulaE4.Properties.DataSource = SSSDBDataSet.PAY_FORMULA
            GridLookUpEditFormulaE5.Properties.DataSource = SSSDBDataSet.PAY_FORMULA
            GridLookUpEditFormulaE6.Properties.DataSource = SSSDBDataSet.PAY_FORMULA
            GridLookUpEditFormulaE7.Properties.DataSource = SSSDBDataSet.PAY_FORMULA
            GridLookUpEditFormulaE8.Properties.DataSource = SSSDBDataSet.PAY_FORMULA
            GridLookUpEditFormulaE9.Properties.DataSource = SSSDBDataSet.PAY_FORMULA
            GridLookUpEditFormulaE10.Properties.DataSource = SSSDBDataSet.PAY_FORMULA

            GridLookUpEditFormulaD1.Properties.DataSource = SSSDBDataSet.PAY_FORMULA
            GridLookUpEditFormulaD2.Properties.DataSource = SSSDBDataSet.PAY_FORMULA
            GridLookUpEditFormulaD3.Properties.DataSource = SSSDBDataSet.PAY_FORMULA
            GridLookUpEditFormulaD4.Properties.DataSource = SSSDBDataSet.PAY_FORMULA
            GridLookUpEditFormulaD5.Properties.DataSource = SSSDBDataSet.PAY_FORMULA
            GridLookUpEditFormulaD6.Properties.DataSource = SSSDBDataSet.PAY_FORMULA
            GridLookUpEditFormulaD7.Properties.DataSource = SSSDBDataSet.PAY_FORMULA
            GridLookUpEditFormulaD8.Properties.DataSource = SSSDBDataSet.PAY_FORMULA
            GridLookUpEditFormulaD9.Properties.DataSource = SSSDBDataSet.PAY_FORMULA
            GridLookUpEditFormulaD10.Properties.DataSource = SSSDBDataSet.PAY_FORMULA
            GridLookUpEditFormulaD11.Properties.DataSource = SSSDBDataSet.PAY_FORMULA

            Me.TblLeaveMasterTableAdapter.Fill(Me.SSSDBDataSet.tblLeaveMaster)
            'GridControlLeave.DataSource = SSSDBDataSet.tblLeaveMaster
        End If
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim dsPAY_SETUP As DataSet = New DataSet
        Dim sSql As String = "select * from PAY_SETUP"
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(dsPAY_SETUP)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(dsPAY_SETUP)
        End If
        loadLeaveGrid()
        loadPFPage(dsPAY_SETUP)
        LoadESIPage(dsPAY_SETUP)
        loadAllowed(dsPAY_SETUP)
        loadAllValue(dsPAY_SETUP)
        If Common.IsFullPayroll = True Then
            XtraTabPage2.PageVisible = True
            XtraTabPage3.PageVisible = True
            XtraTabPage4.PageVisible = True
            XtraTabPage5.PageVisible = True
        Else
            XtraTabPage2.PageVisible = False
            XtraTabPage3.PageVisible = False
            XtraTabPage4.PageVisible = False
            XtraTabPage5.PageVisible = False
        End If
    End Sub
    Private Sub loadLeaveGrid()
        Dim gridselet As String = "Select * from tblLeaveMaster where leavetype='L' Order By LeaveField"
        If Common.servername = "Access" Then
            Dim dataAdapter As New OleDbDataAdapter(gridselet, Common.con1)
            Dim WTDataTable As New DataTable("tblLeaveMaster")
            dataAdapter.Fill(WTDataTable)
            GridControlLeave.DataSource = WTDataTable
        Else
            Dim dataAdapter As New SqlClient.SqlDataAdapter(gridselet, Common.con)
            Dim WTDataTable As New DataTable("tblLeaveMaster")
            dataAdapter.Fill(WTDataTable)
            GridControlLeave.DataSource = WTDataTable
        End If
    End Sub
    Private Sub loadPFPage(ds As DataSet)
        For n = 0 To Len(ds.Tables(0).Rows(0).Item("PFREL").ToString.Trim)
            If n = 0 Then
                CheckEditPFBasic.EditValue = ds.Tables(0).Rows(0).Item("PFREL").ToString.Trim.Substring(n, 1)
            End If
            If n = 1 Then
                CheckEditPFHRA.EditValue = ds.Tables(0).Rows(0).Item("PFREL").ToString.Trim.Substring(n, 1)
            End If

            If n = 2 Then
                CheckEditPFDA.EditValue = ds.Tables(0).Rows(0).Item("PFREL").ToString.Trim.Substring(n, 1)
            End If
            If n = 3 Then
                CheckEditPFConv.EditValue = ds.Tables(0).Rows(0).Item("PFREL").ToString.Trim.Substring(n, 1)
            End If
            If n = 4 Then
                CheckEditPFMedical.EditValue = ds.Tables(0).Rows(0).Item("PFREL").ToString.Trim.Substring(n, 1)
            End If

            If n = 5 Then
                CheckEditPFE1.EditValue = ds.Tables(0).Rows(0).Item("PFREL").ToString.Trim.Substring(n, 1)
            End If
            If n = 6 Then
                CheckEditPFE2.EditValue = ds.Tables(0).Rows(0).Item("PFREL").ToString.Trim.Substring(n, 1)
            End If
            If n = 7 Then
                CheckEditPFE3.EditValue = ds.Tables(0).Rows(0).Item("PFREL").ToString.Trim.Substring(n, 1)
            End If
            If n = 8 Then
                CheckEditPFE4.EditValue = ds.Tables(0).Rows(0).Item("PFREL").ToString.Trim.Substring(n, 1)
            End If
            If n = 9 Then
                CheckEditPFE5.EditValue = ds.Tables(0).Rows(0).Item("PFREL").ToString.Trim.Substring(n, 1)
            End If
            If n = 10 Then
                CheckEditPFE6.EditValue = ds.Tables(0).Rows(0).Item("PFREL").ToString.Trim.Substring(n, 1)
            End If
            If n = 11 Then
                CheckEditPFE7.EditValue = ds.Tables(0).Rows(0).Item("PFREL").ToString.Trim.Substring(n, 1)
            End If
            If n = 12 Then
                CheckEditPFE8.EditValue = ds.Tables(0).Rows(0).Item("PFREL").ToString.Trim.Substring(n, 1)
            End If
            If n = 13 Then
                CheckEditPFE9.EditValue = ds.Tables(0).Rows(0).Item("PFREL").ToString.Trim.Substring(n, 1)
            End If
            If n = 14 Then
                CheckEditPFE10.EditValue = ds.Tables(0).Rows(0).Item("PFREL").ToString.Trim.Substring(n, 1)
            End If
        Next
    End Sub
    Private Sub LoadESIPage(ds As DataSet)
        For n = 0 To Len(ds.Tables(0).Rows(0).Item("ESIREL").ToString.Trim)
            If n = 0 Then
                CheckEditESIBasic.EditValue = ds.Tables(0).Rows(0).Item("ESIREL").ToString.Trim.Substring(n, 1)
            End If

            If n = 1 Then
                CheckEditESIHRA.EditValue = ds.Tables(0).Rows(0).Item("ESIREL").ToString.Trim.Substring(n, 1)
            End If
            If n = 2 Then
                CheckEditESIDA.EditValue = ds.Tables(0).Rows(0).Item("ESIREL").ToString.Trim.Substring(n, 1)
            End If
            If n = 3 Then
                CheckEditESIConv.EditValue = ds.Tables(0).Rows(0).Item("ESIREL").ToString.Trim.Substring(n, 1)
            End If
            If n = 4 Then
                CheckEditESIMedical.EditValue = ds.Tables(0).Rows(0).Item("ESIREL").ToString.Trim.Substring(n, 1)
            End If
            If n = 5 Then
                CheckEditESIE1.EditValue = ds.Tables(0).Rows(0).Item("ESIREL").ToString.Trim.Substring(n, 1)
            End If
            If n = 6 Then
                CheckEditESIE2.EditValue = ds.Tables(0).Rows(0).Item("ESIREL").ToString.Trim.Substring(n, 1)
            End If
            If n = 7 Then
                CheckEditESIE3.EditValue = ds.Tables(0).Rows(0).Item("ESIREL").ToString.Trim.Substring(n, 1)
            End If
            If n = 8 Then
                CheckEditESIE4.EditValue = ds.Tables(0).Rows(0).Item("ESIREL").ToString.Trim.Substring(n, 1)
            End If
            If n = 9 Then
                CheckEditESIE5.EditValue = ds.Tables(0).Rows(0).Item("ESIREL").ToString.Trim.Substring(n, 1)
            End If
            If n = 10 Then
                CheckEditESIE6.EditValue = ds.Tables(0).Rows(0).Item("ESIREL").ToString.Trim.Substring(n, 1)
            End If
            If n = 11 Then
                CheckEditESIE7.EditValue = ds.Tables(0).Rows(0).Item("ESIREL").ToString.Trim.Substring(n, 1)
            End If
            If n = 12 Then
                CheckEditESIE8.EditValue = ds.Tables(0).Rows(0).Item("ESIREL").ToString.Trim.Substring(n, 1)
            End If
            If n = 13 Then
                CheckEditESIE9.EditValue = ds.Tables(0).Rows(0).Item("ESIREL").ToString.Trim.Substring(n, 1)
            End If
            If n = 14 Then
                CheckEditESIE10.EditValue = ds.Tables(0).Rows(0).Item("ESIREL").ToString.Trim.Substring(n, 1)
            End If
            If n = 15 Then
                CheckEditESIEOT.EditValue = ds.Tables(0).Rows(0).Item("ESIREL").ToString.Trim.Substring(n, 1)
            End If
        Next
    End Sub
    Private Sub loadAllowed(ds As DataSet)
        For n = 0 To Len(ds.Tables(0).Rows(0).Item("REIMBREL").ToString.Trim)
            If n = 0 Then
                CheckEditAllowReConveyance.EditValue = ds.Tables(0).Rows(0).Item("REIMBREL").ToString.Trim.Substring(n, 1)
            End If

            If n = 1 Then
                CheckEditAllowReMedical.EditValue = ds.Tables(0).Rows(0).Item("REIMBREL").ToString.Trim.Substring(n, 1)
            End If
            If n = 2 Then
                CheckEditAllowReE1.EditValue = ds.Tables(0).Rows(0).Item("REIMBREL").ToString.Trim.Substring(n, 1)
            End If
            If n = 3 Then
                CheckEditAllowReE2.EditValue = ds.Tables(0).Rows(0).Item("REIMBREL").ToString.Trim.Substring(n, 1)
            End If
            If n = 4 Then
                CheckEditAllowReE3.EditValue = ds.Tables(0).Rows(0).Item("REIMBREL").ToString.Trim.Substring(n, 1)
            End If
            If n = 5 Then
                CheckEditAllowReE4.EditValue = ds.Tables(0).Rows(0).Item("REIMBREL").ToString.Trim.Substring(n, 1)
            End If
            If n = 6 Then
                CheckEditAllowReE5.EditValue = ds.Tables(0).Rows(0).Item("REIMBREL").ToString.Trim.Substring(n, 1)
            End If
            If n = 7 Then
                CheckEditAllowReE6.EditValue = ds.Tables(0).Rows(0).Item("REIMBREL").ToString.Trim.Substring(n, 1)
            End If
            If n = 8 Then
                CheckEditAllowReE7.EditValue = ds.Tables(0).Rows(0).Item("REIMBREL").ToString.Trim.Substring(n, 1)
            End If
            If n = 9 Then
                CheckEditAllowReE8.EditValue = ds.Tables(0).Rows(0).Item("REIMBREL").ToString.Trim.Substring(n, 1)
            End If
            If n = 10 Then
                CheckEditAllowReE9.EditValue = ds.Tables(0).Rows(0).Item("REIMBREL").ToString.Trim.Substring(n, 1)
            End If
            If n = 11 Then
                CheckEditAllowReE10.EditValue = ds.Tables(0).Rows(0).Item("REIMBREL").ToString.Trim.Substring(n, 1)
            End If
        Next

        For n = 0 To Len(ds.Tables(0).Rows(0).Item("LEAVEREL").ToString.Trim)
            If n = 0 Then
                CheckEditAllowLeaveBasic.EditValue = ds.Tables(0).Rows(0).Item("LEAVEREL").ToString.Trim.Substring(n, 1)
            End If
            If n = 1 Then
                CheckEditAllowLeaveHRA.EditValue = ds.Tables(0).Rows(0).Item("LEAVEREL").ToString.Trim.Substring(n, 1)
            End If

            If n = 2 Then
                CheckEditAllowLeaveDA.EditValue = ds.Tables(0).Rows(0).Item("LEAVEREL").ToString.Trim.Substring(n, 1)
            End If
            If n = 3 Then
                CheckEditAllowLeaveConveyance.EditValue = ds.Tables(0).Rows(0).Item("LEAVEREL").ToString.Trim.Substring(n, 1)
            End If
            If n = 4 Then
                CheckEditAllowLeaveMedical.EditValue = ds.Tables(0).Rows(0).Item("LEAVEREL").ToString.Trim.Substring(n, 1)
            End If
            If n = 5 Then
                CheckEditAllowLeaveE1.EditValue = ds.Tables(0).Rows(0).Item("LEAVEREL").ToString.Trim.Substring(n, 1)
            End If
            If n = 6 Then
                CheckEditAllowLeaveE2.EditValue = ds.Tables(0).Rows(0).Item("LEAVEREL").ToString.Trim.Substring(n, 1)
            End If
            If n = 7 Then
                CheckEditAllowLeaveE3.EditValue = ds.Tables(0).Rows(0).Item("LEAVEREL").ToString.Trim.Substring(n, 1)
            End If
            If n = 8 Then
                CheckEditAllowLeaveE4.EditValue = ds.Tables(0).Rows(0).Item("LEAVEREL").ToString.Trim.Substring(n, 1)
            End If
            If n = 9 Then
                CheckEditAllowLeaveE5.EditValue = ds.Tables(0).Rows(0).Item("LEAVEREL").ToString.Trim.Substring(n, 1)
            End If
            If n = 10 Then
                CheckEditAllowLeaveE6.EditValue = ds.Tables(0).Rows(0).Item("LEAVEREL").ToString.Trim.Substring(n, 1)
            End If
            If n = 11 Then
                CheckEditAllowLeaveE7.EditValue = ds.Tables(0).Rows(0).Item("LEAVEREL").ToString.Trim.Substring(n, 1)
            End If
            If n = 12 Then
                CheckEditAllowLeaveE8.EditValue = ds.Tables(0).Rows(0).Item("LEAVEREL").ToString.Trim.Substring(n, 1)
            End If
            If n = 13 Then
                CheckEditAllowLeaveE9.EditValue = ds.Tables(0).Rows(0).Item("LEAVEREL").ToString.Trim.Substring(n, 1)
            End If
            If n = 14 Then
                CheckEditAllowLeaveE10.EditValue = ds.Tables(0).Rows(0).Item("LEAVEREL").ToString.Trim.Substring(n, 1)
            End If
        Next

        Dim val As Object = ds.Tables(0).Rows(0).Item("LVFIELD").ToString.Trim
        If (val Is Nothing) Then
            GridViewLeave.ClearSelection()
        Else
            'Dim texts() As String = val.ToString.Split(Microsoft.VisualBasic.ChrW(44))
            Dim texts() As String = val.ToString.Split(".")
            For Each text As String In texts
                If text.Trim.Length = 1 Then
                    text = text.Trim & "  "
                ElseIf text.Trim.Length = 2 Then
                    text = text.Trim & " "
                End If
                'MsgBox(text & "  " & text.Length & " " & GridView1.LocateByValue("SHIFT", text))
                Dim rowHandle As Integer = GridViewLeave.LocateByValue("LEAVEFIELD", text.Trim)
                GridViewLeave.SelectRow(rowHandle)
            Next
        End If
    End Sub
    Private Sub loadAllValue(ds As DataSet)
        TextEditPFLimite.Text = ds.Tables(0).Rows(0).Item("PFLIMIT").ToString.Trim
        TextEditPFPRErDec.Text = ds.Tables(0).Rows(0).Item("PF").ToString.Trim

        TextEditPFEPRDec.Text = ds.Tables(0).Rows(0).Item("EPF").ToString.Trim
        TextEditPFFPRDec.Text = ds.Tables(0).Rows(0).Item("FPF").ToString.Trim
        TextEditPFPFDec.Text = ds.Tables(0).Rows(0).Item("PFE").ToString.Trim
        TextEditPFVPFDec.Text = ds.Tables(0).Rows(0).Item("VPF").ToString.Trim
        TextEditPFRound.Text = ds.Tables(0).Rows(0).Item("PFRND").ToString.Trim


        TextEditESILimite.Text = ds.Tables(0).Rows(0).Item("ESILIMIT").ToString.Trim
        TextEditESIErDec.Text = ds.Tables(0).Rows(0).Item("ESI").ToString.Trim
        TextEditESIEpDec.Text = ds.Tables(0).Rows(0).Item("ESIE").ToString.Trim
        TextEditESIRound.Text = ds.Tables(0).Rows(0).Item("ESIRND").ToString.Trim
        TextEditDescp1.Text = ds.Tables(0).Rows(0).Item("VDES_1").ToString.Trim
        GridLookUpEditFormulaD1.EditValue = ds.Tables(0).Rows(0).Item("VDT_1").ToString.Trim
        TextEditRate1.Text = ds.Tables(0).Rows(0).Item("VD_1").ToString.Trim
        TextEditDescp2.Text = ds.Tables(0).Rows(0).Item("VDES_2").ToString.Trim
        GridLookUpEditFormulaD2.EditValue = ds.Tables(0).Rows(0).Item("VDT_2").ToString.Trim
        TextEditRate2.Text = ds.Tables(0).Rows(0).Item("VD_2").ToString.Trim
        TextEditDescp3.Text = ds.Tables(0).Rows(0).Item("VDES_3").ToString.Trim
        GridLookUpEditFormulaD3.EditValue = ds.Tables(0).Rows(0).Item("VDT_3").ToString.Trim
        TextEditRate3.Text = ds.Tables(0).Rows(0).Item("VD_3").ToString.Trim
        TextEditDescp4.Text = ds.Tables(0).Rows(0).Item("VDES_4").ToString.Trim
        GridLookUpEditFormulaD4.EditValue = ds.Tables(0).Rows(0).Item("VDT_4").ToString.Trim
        TextEditRate4.Text = ds.Tables(0).Rows(0).Item("VD_4").ToString.Trim
        TextEditDescp5.Text = ds.Tables(0).Rows(0).Item("VDES_5").ToString.Trim
        GridLookUpEditFormulaD5.EditValue = ds.Tables(0).Rows(0).Item("VDT_5").ToString.Trim
        TextEditRate5.Text = ds.Tables(0).Rows(0).Item("VD_5").ToString.Trim
        TextEditDescp6.Text = ds.Tables(0).Rows(0).Item("VDES_6").ToString.Trim
        GridLookUpEditFormulaD6.EditValue = ds.Tables(0).Rows(0).Item("VDT_6").ToString.Trim
        TextEditRate6.Text = ds.Tables(0).Rows(0).Item("VD_6").ToString.Trim
        TextEditDescp7.Text = ds.Tables(0).Rows(0).Item("VDES_7").ToString.Trim
        GridLookUpEditFormulaD7.EditValue = ds.Tables(0).Rows(0).Item("VDT_7").ToString.Trim
        TextEditRate7.Text = ds.Tables(0).Rows(0).Item("VD_7").ToString.Trim
        TextEditDescp8.Text = ds.Tables(0).Rows(0).Item("VDES_8").ToString.Trim
        GridLookUpEditFormulaD8.EditValue = ds.Tables(0).Rows(0).Item("VDT_8").ToString.Trim
        TextEditRate8.Text = ds.Tables(0).Rows(0).Item("VD_8").ToString.Trim
        TextEditDescp9.Text = ds.Tables(0).Rows(0).Item("VDES_9").ToString.Trim
        GridLookUpEditFormulaD9.EditValue = ds.Tables(0).Rows(0).Item("VDT_9").ToString.Trim
        TextEditRate9.Text = ds.Tables(0).Rows(0).Item("VD_9").ToString.Trim
        TextEditDescp10.Text = ds.Tables(0).Rows(0).Item("VDES_10").ToString.Trim
        GridLookUpEditFormulaD10.EditValue = ds.Tables(0).Rows(0).Item("VDT_10").ToString.Trim
        TextEditRate10.Text = ds.Tables(0).Rows(0).Item("VD_10").ToString.Trim
        TextEditDescpE1.Text = ds.Tables(0).Rows(0).Item("VIES_1").ToString.Trim
        GridLookUpEditFormulaE1.EditValue = ds.Tables(0).Rows(0).Item("VIT_1").ToString.Trim
        TextEditRateE1.Text = ds.Tables(0).Rows(0).Item("VI_1").ToString.Trim
        TextEditDescpE2.Text = ds.Tables(0).Rows(0).Item("VIES_2").ToString.Trim
        GridLookUpEditFormulaE2.EditValue = ds.Tables(0).Rows(0).Item("VIT_2").ToString.Trim
        TextEditRateE2.Text = ds.Tables(0).Rows(0).Item("VI_2").ToString.Trim
        TextEditDescpE3.Text = ds.Tables(0).Rows(0).Item("VIES_3").ToString.Trim
        GridLookUpEditFormulaE3.EditValue = ds.Tables(0).Rows(0).Item("VIT_3").ToString.Trim
        TextEditRateE3.Text = ds.Tables(0).Rows(0).Item("VI_3").ToString.Trim
        TextEditDescpE4.Text = ds.Tables(0).Rows(0).Item("VIES_4").ToString.Trim
        GridLookUpEditFormulaE4.EditValue = ds.Tables(0).Rows(0).Item("VIT_4").ToString.Trim
        TextEditRateE4.Text = ds.Tables(0).Rows(0).Item("VI_4").ToString.Trim
        TextEditDescpE5.Text = ds.Tables(0).Rows(0).Item("VIES_5").ToString.Trim
        GridLookUpEditFormulaE5.EditValue = ds.Tables(0).Rows(0).Item("VIT_5").ToString.Trim
        TextEditRateE5.Text = ds.Tables(0).Rows(0).Item("VI_5").ToString.Trim
        TextEditDescpE6.Text = ds.Tables(0).Rows(0).Item("VIES_6").ToString.Trim
        GridLookUpEditFormulaE6.EditValue = ds.Tables(0).Rows(0).Item("VIT_6").ToString.Trim
        TextEditRateE6.Text = ds.Tables(0).Rows(0).Item("VI_6").ToString.Trim
        TextEditDescpE7.Text = ds.Tables(0).Rows(0).Item("VIES_7").ToString.Trim
        GridLookUpEditFormulaE7.EditValue = ds.Tables(0).Rows(0).Item("VIT_7").ToString.Trim
        TextEditRateE7.Text = ds.Tables(0).Rows(0).Item("VI_7").ToString.Trim
        TextEditDescpE8.Text = ds.Tables(0).Rows(0).Item("VIES_8").ToString.Trim
        GridLookUpEditFormulaE8.EditValue = ds.Tables(0).Rows(0).Item("VIT_8").ToString.Trim
        TextEditRateE8.Text = ds.Tables(0).Rows(0).Item("VI_8").ToString.Trim
        TextEditDescpE9.Text = ds.Tables(0).Rows(0).Item("VIES_9").ToString.Trim
        GridLookUpEditFormulaE9.EditValue = ds.Tables(0).Rows(0).Item("VIT_9").ToString.Trim
        TextEditRateE9.Text = ds.Tables(0).Rows(0).Item("VI_9").ToString.Trim
        TextEditDescpE10.Text = ds.Tables(0).Rows(0).Item("VIES_10").ToString.Trim
        GridLookUpEditFormulaE10.EditValue = ds.Tables(0).Rows(0).Item("VIT_10").ToString.Trim
        TextEditRateE10.Text = ds.Tables(0).Rows(0).Item("VI_10").ToString.Trim
        TextEditTDSE.Text = ds.Tables(0).Rows(0).Item("TDS_RATE").ToString.Trim
        GridLookUpEditFormulaD11.EditValue = ds.Tables(0).Rows(0).Item("TDS_F").ToString.Trim
        TextEditBonusMinWorking.Text = ds.Tables(0).Rows(0).Item("BONUS_WORKDAYS").ToString.Trim
        TextEditBonusWageLimit.Text = ds.Tables(0).Rows(0).Item("BONUS_WAGELIMIT").ToString.Trim
        TextEditBonusAmtLimit.Text = ds.Tables(0).Rows(0).Item("BONUS_LIMIT").ToString.Trim
        CheckEditBonusAllOnArr.EditValue = ds.Tables(0).Rows(0).Item("BONUS_ARRIER").ToString.Trim
        TextEditBonusRate.Text = ds.Tables(0).Rows(0).Item("BONUS_RATE").ToString.Trim
        TextEditPF02.Text = ds.Tables(0).Rows(0).Item("PFAC02").ToString.Trim
        TextEditPF21.Text = ds.Tables(0).Rows(0).Item("PFAC21").ToString.Trim
        TextEditPF22.Text = ds.Tables(0).Rows(0).Item("PFAC22").ToString.Trim
        CheckEditBR.EditValue = ds.Tables(0).Rows(0).Item("BASIC_RND").ToString.Trim
        CheckEditDA.EditValue = ds.Tables(0).Rows(0).Item("DA_RND").ToString.Trim
        CheckEditHRA.EditValue = ds.Tables(0).Rows(0).Item("HRA_RND").ToString.Trim
        CheckEditCR.EditValue = ds.Tables(0).Rows(0).Item("CONV_RND").ToString.Trim

        CheckEditMR.EditValue = ds.Tables(0).Rows(0).Item("MEDICAL_RND").ToString.Trim
        CheckEditRndE1.EditValue = ds.Tables(0).Rows(0).Item("EARN1_RND").ToString.Trim
        CheckEditRndE2.EditValue = ds.Tables(0).Rows(0).Item("EARN2_RND").ToString.Trim
        CheckEditRndE3.EditValue = ds.Tables(0).Rows(0).Item("EARN3_RND").ToString.Trim
        CheckEditRndE4.EditValue = ds.Tables(0).Rows(0).Item("EARN4_RND").ToString.Trim
        CheckEditRndE5.EditValue = ds.Tables(0).Rows(0).Item("EARN5_RND").ToString.Trim
        CheckEditRndE6.EditValue = ds.Tables(0).Rows(0).Item("EARN6_RND").ToString.Trim
        CheckEditRndE7.EditValue = ds.Tables(0).Rows(0).Item("EARN7_RND").ToString.Trim
        CheckEditRndE8.EditValue = ds.Tables(0).Rows(0).Item("EARN8_RND").ToString.Trim
        CheckEditRndE9.EditValue = ds.Tables(0).Rows(0).Item("EARN9_RND").ToString.Trim
        CheckEditRndE10.EditValue = ds.Tables(0).Rows(0).Item("EARN10_RND").ToString.Trim
        CheckEditRndD11.EditValue = ds.Tables(0).Rows(0).Item("TDS_RND").ToString.Trim
        'Dim PROF_TAX_RND As String
        CheckEditRndD1.EditValue = ds.Tables(0).Rows(0).Item("DED1_RND").ToString.Trim
        CheckEditRndD2.EditValue = ds.Tables(0).Rows(0).Item("DED2_RND").ToString.Trim
        CheckEditRndD3.EditValue = ds.Tables(0).Rows(0).Item("DED3_RND").ToString.Trim
        CheckEditRndD4.EditValue = ds.Tables(0).Rows(0).Item("DED4_RND").ToString.Trim
        CheckEditRndD5.EditValue = ds.Tables(0).Rows(0).Item("DED5_RND").ToString.Trim
        CheckEditRndD6.EditValue = ds.Tables(0).Rows(0).Item("DED6_RND").ToString.Trim
        CheckEditRndD7.EditValue = ds.Tables(0).Rows(0).Item("DED7_RND").ToString.Trim
        CheckEditRndD8.EditValue = ds.Tables(0).Rows(0).Item("DED8_RND").ToString.Trim
        CheckEditRndD9.EditValue = ds.Tables(0).Rows(0).Item("DED9_RND").ToString.Trim
        CheckEditRndD10.EditValue = ds.Tables(0).Rows(0).Item("DED10_RND").ToString.Trim
        CheckEditOT.EditValue = ds.Tables(0).Rows(0).Item("OTAMT_RND").ToString.Trim
        TextEdit1.Text = ds.Tables(0).Rows(0).Item("GRADUTY_YRLIMIT").ToString.Trim

        TextEditGratuityFormula.Text = ds.Tables(0).Rows(0).Item("GRADUTY_FORMULA").ToString.Trim

        CheckEditAllowPF.EditValue = ds.Tables(0).Rows(0).Item("PF_ALLOWED").ToString.Trim
        CheckEditAllowVPF.EditValue = ds.Tables(0).Rows(0).Item("VPF_ALLOWED").ToString.Trim
        CheckEditAllowESI.EditValue = ds.Tables(0).Rows(0).Item("ESI_ALLOWED").ToString.Trim
        CheckEditAllowProfTax.EditValue = ds.Tables(0).Rows(0).Item("PROF_TAX_ALLOWED").ToString.Trim
        CheckEditAllowGratuity.EditValue = ds.Tables(0).Rows(0).Item("GRADUTY_ALLOWED").ToString.Trim
        CheckEditAllowBonus.EditValue = ds.Tables(0).Rows(0).Item("BONUS_ALLOWED").ToString.Trim

        CheckEditchklvPF.EditValue = ds.Tables(0).Rows(0).Item("LEAVEPF").ToString.Trim

        TextEditGrossBasic.Text = ds.Tables(0).Rows(0).Item("gbasic").ToString.Trim
        TextEditGrossDA.Text = ds.Tables(0).Rows(0).Item("gda").ToString.Trim
        TextEditGrossHRA.Text = ds.Tables(0).Rows(0).Item("ghra").ToString.Trim
        TextEditGrossConv.Text = ds.Tables(0).Rows(0).Item("gconv").ToString.Trim
        TextEditGrossMed.Text = ds.Tables(0).Rows(0).Item("gmed").ToString.Trim
        TextEditGrossEar01.Text = ds.Tables(0).Rows(0).Item("gEARN1").ToString.Trim
        TextEditGrossEar02.Text = ds.Tables(0).Rows(0).Item("gEARN2").ToString.Trim
        TextEditGrossEar03.Text = ds.Tables(0).Rows(0).Item("gEARN3").ToString.Trim
        TextEditGrossEar04.Text = ds.Tables(0).Rows(0).Item("gEARN4").ToString.Trim
        TextEditGrossEar05.Text = ds.Tables(0).Rows(0).Item("gEARN5").ToString.Trim
        TextEditGrossEar06.Text = ds.Tables(0).Rows(0).Item("gEARN6").ToString.Trim
        TextEditGrossEar07.Text = ds.Tables(0).Rows(0).Item("gEARN7").ToString.Trim
        TextEditGrossEar08.Text = ds.Tables(0).Rows(0).Item("gEARN8").ToString.Trim
        TextEditGrossEar09.Text = ds.Tables(0).Rows(0).Item("gEARN9").ToString.Trim
        TextEditGrossEar10.Text = ds.Tables(0).Rows(0).Item("gEARN10").ToString.Trim

    End Sub
    Private Sub SimpleButtonSave_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonSave.Click
        Dim PFLIMIT As String = TextEditPFLimite.Text.Trim
        Dim PF As String = TextEditPFPRErDec.Text.Trim
        Dim EPF As String = TextEditPFEPRDec.Text.Trim
        Dim FPF As String = TextEditPFFPRDec.Text.Trim
        Dim PFE As String = TextEditPFPFDec.Text.Trim
        Dim VPF As String = TextEditPFVPFDec.Text.Trim
        Dim PFRND As String = TextEditPFRound.Text.Trim

        Dim PFREL As String = CheckEditPFBasic.EditValue & CheckEditPFHRA.EditValue & CheckEditPFDA.EditValue & CheckEditPFConv.EditValue & CheckEditPFMedical.EditValue & CheckEditPFE1.EditValue & CheckEditPFE2.EditValue & CheckEditPFE3.EditValue & CheckEditPFE4.EditValue & CheckEditPFE5.EditValue & CheckEditPFE6.EditValue & CheckEditPFE7.EditValue & CheckEditPFE8.EditValue & CheckEditPFE9.EditValue & CheckEditPFE10.EditValue

        Dim ESILIMIT As String = TextEditESILimite.Text.Trim
        Dim ESI As String = TextEditESIErDec.Text.Trim
        Dim ESIE As String = TextEditESIEpDec.Text.Trim
        Dim ESIRND As String = TextEditESIRound.Text.Trim
        Dim VDES_1 As String = TextEditDescp1.Text.Trim
        Dim VDT_1 As String = GridLookUpEditFormulaD1.EditValue
        Dim VD_1 As String = TextEditRate1.Text.Trim
        Dim VDES_2 As String = TextEditDescp2.Text.Trim
        Dim VDT_2 As String = GridLookUpEditFormulaD2.EditValue
        Dim VD_2 As String = TextEditRate2.Text.Trim
        Dim VDES_3 As String = TextEditDescp3.Text.Trim
        Dim VDT_3 As String = GridLookUpEditFormulaD3.EditValue
        Dim VD_3 As String = TextEditRate3.Text.Trim
        Dim VDES_4 As String = TextEditDescp4.Text.Trim
        Dim VDT_4 As String = GridLookUpEditFormulaD4.EditValue
        Dim VD_4 As String = TextEditRate4.Text.Trim
        Dim VDES_5 As String = TextEditDescp5.Text.Trim
        Dim VDT_5 As String = GridLookUpEditFormulaD5.EditValue
        Dim VD_5 As String = TextEditRate5.Text.Trim
        Dim VDES_6 As String = TextEditDescp6.Text.Trim
        Dim VDT_6 As String = GridLookUpEditFormulaD6.EditValue
        Dim VD_6 As String = TextEditRate6.Text.Trim
        Dim VDES_7 As String = TextEditDescp7.Text.Trim
        Dim VDT_7 As String = GridLookUpEditFormulaD7.EditValue
        Dim VD_7 As String = TextEditRate7.Text.Trim
        Dim VDES_8 As String = TextEditDescp8.Text.Trim
        Dim VDT_8 As String = GridLookUpEditFormulaD8.EditValue
        Dim VD_8 As String = TextEditRate8.Text.Trim
        Dim VDES_9 As String = TextEditDescp9.Text.Trim
        Dim VDT_9 As String = GridLookUpEditFormulaD9.EditValue
        Dim VD_9 As String = TextEditRate9.Text.Trim
        Dim VDES_10 As String = TextEditDescp10.Text.Trim
        Dim VDT_10 As String = GridLookUpEditFormulaD10.EditValue
        Dim VD_10 As String = TextEditRate10.Text.Trim
        Dim VIES_1 As String = TextEditDescpE1.Text.Trim
        Dim VIT_1 As String = GridLookUpEditFormulaE1.EditValue
        Dim VI_1 As String = TextEditRateE1.Text.Trim
        Dim VIES_2 As String = TextEditDescpE2.Text.Trim
        Dim VIT_2 As String = GridLookUpEditFormulaE2.EditValue
        Dim VI_2 As String = TextEditRateE2.Text.Trim
        Dim VIES_3 As String = TextEditDescpE3.Text.Trim
        Dim VIT_3 As String = GridLookUpEditFormulaE3.EditValue
        Dim VI_3 As String = TextEditRateE3.Text.Trim
        Dim VIES_4 As String = TextEditDescpE4.Text.Trim
        Dim VIT_4 As String = GridLookUpEditFormulaE4.EditValue
        Dim VI_4 As String = TextEditRateE4.Text.Trim
        Dim VIES_5 As String = TextEditDescpE5.Text.Trim
        Dim VIT_5 As String = GridLookUpEditFormulaE5.EditValue
        Dim VI_5 As String = TextEditRateE5.Text.Trim
        Dim VIES_6 As String = TextEditDescpE6.Text.Trim
        Dim VIT_6 As String = GridLookUpEditFormulaE6.EditValue
        Dim VI_6 As String = TextEditRateE6.Text.Trim
        Dim VIES_7 As String = TextEditDescpE7.Text.Trim
        Dim VIT_7 As String = GridLookUpEditFormulaE7.EditValue
        Dim VI_7 As String = TextEditRateE7.Text.Trim
        Dim VIES_8 As String = TextEditDescpE8.Text.Trim
        Dim VIT_8 As String = GridLookUpEditFormulaE8.EditValue
        Dim VI_8 As String = TextEditRateE8.Text.Trim
        Dim VIES_9 As String = TextEditDescpE9.Text.Trim
        Dim VIT_9 As String = GridLookUpEditFormulaE9.EditValue
        Dim VI_9 As String = TextEditRateE9.Text.Trim
        Dim VIES_10 As String = TextEditDescpE10.Text.Trim
        Dim VIT_10 As String = GridLookUpEditFormulaE10.EditValue
        Dim VI_10 As String = TextEditRateE10.Text.Trim
        Dim TDS_RATE As String = TextEditTDSE.Text.Trim
        Dim TDS_F As String = GridLookUpEditFormulaD11.EditValue
        Dim BONUS_WORKDAYS As String = TextEditBonusMinWorking.Text.Trim
        Dim BONUS_WAGELIMIT As String = TextEditBonusWageLimit.Text.Trim
        Dim BONUS_LIMIT As String = TextEditBonusAmtLimit.Text.Trim
        Dim BONUS_ARRIER As String = CheckEditBonusAllOnArr.EditValue
        Dim BONUS_RATE As String = TextEditBonusRate.Text.Trim
        Dim PFAC02 As String = TextEditPF02.Text.Trim
        Dim PFAC21 As String = TextEditPF21.Text.Trim
        Dim PFAC22 As String = TextEditPF22.Text.Trim
        Dim BASIC_RND As String = CheckEditBR.EditValue
        Dim DA_RND As String = CheckEditDA.EditValue
        Dim HRA_RND As String = CheckEditHRA.EditValue
        Dim CONV_RND As String = CheckEditCR.EditValue
        Dim MEDICAL_RND As String = CheckEditMR.EditValue
        Dim EARN1_RND As String = CheckEditRndE1.EditValue
        Dim EARN2_RND As String = CheckEditRndE2.EditValue
        Dim EARN3_RND As String = CheckEditRndE3.EditValue
        Dim EARN4_RND As String = CheckEditRndE4.EditValue
        Dim EARN5_RND As String = CheckEditRndE5.EditValue
        Dim EARN6_RND As String = CheckEditRndE6.EditValue
        Dim EARN7_RND As String = CheckEditRndE7.EditValue
        Dim EARN8_RND As String = CheckEditRndE8.EditValue
        Dim EARN9_RND As String = CheckEditRndE9.EditValue
        Dim EARN10_RND As String = CheckEditRndE10.EditValue
        Dim TDS_RND As String = CheckEditRndD11.EditValue
        'Dim PROF_TAX_RND As String
        Dim DED1_RND As String = CheckEditRndD1.EditValue
        Dim DED2_RND As String = CheckEditRndD2.EditValue
        Dim DED3_RND As String = CheckEditRndD3.EditValue
        Dim DED4_RND As String = CheckEditRndD4.EditValue
        Dim DED5_RND As String = CheckEditRndD5.EditValue
        Dim DED6_RND As String = CheckEditRndD6.EditValue
        Dim DED7_RND As String = CheckEditRndD7.EditValue
        Dim DED8_RND As String = CheckEditRndD8.EditValue
        Dim DED9_RND As String = CheckEditRndD9.EditValue
        Dim DED10_RND As String = CheckEditRndD10.EditValue
        Dim OTAMT_RND As String = CheckEditOT.EditValue
        Dim GRADUTY_YRLIMIT As String = TextEdit1.Text.Trim
        Dim tempfor As String = Common.ArrangeFormula(TextEditGratuityFormula.Text.Trim, ulf)
        Dim GRADUTY_FORMULA As String = tempfor
        'Dim otLogic As String
        'Dim otLogic1 As String
        'Dim otperDay As String
        'Dim otperWeek As String
        'Dim procDate As String
        Dim PF_ALLOWED As String = CheckEditAllowPF.EditValue
        Dim VPF_ALLOWED As String = CheckEditAllowVPF.EditValue
        Dim ESI_ALLOWED As String = CheckEditAllowESI.EditValue
        Dim PROF_TAX_ALLOWED As String = CheckEditAllowProfTax.EditValue
        Dim GRADUTY_ALLOWED As String = CheckEditAllowGratuity.EditValue
        Dim BONUS_ALLOWED As String = CheckEditAllowBonus.EditValue


        Dim REIMBREL As String = CheckEditAllowReConveyance.EditValue & CheckEditAllowReMedical.EditValue & CheckEditAllowReE1.EditValue & CheckEditAllowReE2.EditValue & CheckEditAllowReE3.EditValue & CheckEditAllowReE4.EditValue & CheckEditAllowReE5.EditValue & CheckEditAllowReE6.EditValue & CheckEditAllowReE7.EditValue & CheckEditAllowReE8.EditValue & CheckEditAllowReE9.EditValue & CheckEditAllowReE10.EditValue
        Dim LEAVEREL As String = CheckEditAllowLeaveBasic.EditValue & CheckEditAllowLeaveHRA.EditValue & CheckEditAllowLeaveDA.EditValue & CheckEditAllowLeaveConveyance.EditValue & CheckEditAllowLeaveMedical.EditValue & CheckEditAllowLeaveE1.EditValue & CheckEditAllowLeaveE2.EditValue & CheckEditAllowLeaveE3.EditValue & CheckEditAllowLeaveE4.EditValue & CheckEditAllowLeaveE5.EditValue & CheckEditAllowLeaveE6.EditValue & CheckEditAllowLeaveE7.EditValue & CheckEditAllowLeaveE8.EditValue & CheckEditAllowLeaveE9.EditValue & CheckEditAllowLeaveE10.EditValue


        Dim LEAVEPF As String = CheckEditchklvPF.EditValue
        Dim LVFIELD As String = ""
        Dim selectedRows As Integer() = GridViewLeave.GetSelectedRows()
        Dim result As Object() = New Object(selectedRows.Length - 1) {}
        For i As Integer = 0 To selectedRows.Length - 1
            Dim rowHandle As Integer = selectedRows(i)
            If Not GridView1.IsGroupRow(rowHandle) Then
                LVFIELD = LVFIELD & "." & Trim(GridViewLeave.GetRowCellValue(rowHandle, "LEAVEFIELD").ToString.Trim)
            End If
        Next
        LVFIELD = LVFIELD.Trim(".")
        Dim gbasic As String = TextEditGrossBasic.Text.Trim
        Dim gda As String = TextEditGrossDA.Text.Trim
        Dim ghra As String = TextEditGrossHRA.Text.Trim
        Dim gconv As String = TextEditGrossConv.Text.Trim
        Dim gmed As String = TextEditGrossMed.Text.Trim
        Dim gEARN1 As String = TextEditGrossEar01.Text.Trim
        Dim gEARN2 As String = TextEditGrossEar02.Text.Trim
        Dim gEARN3 As String = TextEditGrossEar03.Text.Trim
        Dim gEARN4 As String = TextEditGrossEar04.Text.Trim
        Dim gEARN5 As String = TextEditGrossEar05.Text.Trim
        Dim gEARN6 As String = TextEditGrossEar06.Text.Trim
        Dim gEARN7 As String = TextEditGrossEar07.Text.Trim
        Dim gEARN8 As String = TextEditGrossEar08.Text.Trim
        Dim gEARN9 As String = TextEditGrossEar09.Text.Trim
        Dim gEARN10 As String = TextEditGrossEar10.Text.Trim
        Dim ESIREL As String = CheckEditESIBasic.EditValue & CheckEditESIHRA.EditValue & CheckEditESIDA.EditValue & CheckEditESIConv.EditValue & CheckEditESIMedical.EditValue & CheckEditESIE1.EditValue & CheckEditESIE2.EditValue & CheckEditESIE3.EditValue & CheckEditESIE4.EditValue & CheckEditESIE5.EditValue & CheckEditESIE6.EditValue & CheckEditESIE7.EditValue & CheckEditESIE8.EditValue & CheckEditESIE9.EditValue & CheckEditESIE10.EditValue & CheckEditESIEOT.EditValue

        If Val(TextEditGrossBasic.Text) + Val(TextEditGrossDA.Text) + Val(TextEditGrossHRA.Text) + Val(TextEditGrossConv.Text) + Val(TextEditGrossMed.Text) + Val(TextEditGrossEar01.Text) + Val(TextEditGrossEar02.Text) + Val(TextEditGrossEar03.Text) + Val(TextEditGrossEar04.Text) + Val(TextEditGrossEar05.Text) + Val(TextEditGrossEar06.Text) + Val(TextEditGrossEar07.Text) + Val(TextEditGrossEar08.Text) + Val(TextEditGrossEar09.Text) + Val(TextEditGrossEar10.Text) > 100 Then
            XtraMessageBox.Show(ulf, "<size=10>Gross Bifercastion not Valid</size>", "<size=9>Error</size>")
            Exit Sub
        End If

        'Dim sSql As String = "Update Pay_Setup set PFLIMIT='" & PFLIMIT & "', PF='" & PF & "', EPF='" & EPF & "', FPF='" & FPF & "', " & _
        '                    "PFE='" & PFE & "', VPF='" & VPF & "', PFRND='" & PFRND & "', PFREL='" & PFREL & "', ESILIMIT='" & ESILIMIT & _
        '                    "', ESI='" & ESI & "', ESIE='" & ESIE & "', ESIRND='" & ESIRND & "', VDES_1='" & VDES_1 & "', VDT_1='" & VDT_1 & _
        '                    "', VD_1='" & VD_1 & "', VDES_2='" & VDES_2 & "', VDT_2='" & VDT_2 & "', VD_2='" & VD_2 & "', VDES_3='" & VDES_3 & _
        '                    "', VDT_3='" & VDT_3 & "', VD_3='" & VD_3 & "', VDES_4='" & VDES_4 & "', VDT_4='" & VDT_4 & "', VD_4='" & VD_4 & _
        '                    "', VDES_5='" & VDES_5 & "', VDT_5='" & VDT_5 & "', VD_5='" & VD_5 & "', VDES_6='" & VDES_6 & _
        '                    "', VDT_6='" & VDT_6 & "', VD_6='" & VD_6 & "', VDES_7='" & VDES_7 & "', VDT_7='" & VDT_7 & _
        '                    "', VD_7='" & VD_7 & "', VDES_8='" & VDES_8 & "', VDT_8='" & VDT_8 & "', VD_8='" & VD_8 & _
        '                    "', VDES_9='" & VDES_9 & "', VDT_9='" & VDT_9 & "', VD_9='" & VD_9 & "', VDES_10='" & VDES_10 & _
        '                    "', VDT_10='" & VDT_10 & "', VD_10='" & VD_10 & "', VIES_1='" & VIES_1 & "', VIT_1='" & VIT_1 & _
        '                    "', VI_1='" & VI_1 & "', VIES_2='" & VIES_2 & "', VIT_2='" & VIT_2 & "', VI_2='" & VI_2 & _
        '                    "', VIES_3='" & VIES_3 & "', VIT_3='" & VIT_3 & "', VI_3='" & VI_3 & "', VIES_4='" & VIES_4 & _
        '                    "', VIT_4='" & VIT_4 & "', VI_4='" & VI_4 & "', VIES_5='" & VIES_5 & "', VIT_5='" & VIT_5 & _
        '                    "', VI_5='" & VI_5 & "', VIES_6='" & VIES_6 & "', VIT_6='" & VIT_6 & "', VI_6='" & VI_6 & _
        '                    "', VIES_7='" & VIES_7 & "', VIT_7='" & VIT_7 & "', VI_7='" & VI_7 & "', VIES_8='" & VIES_8 & _
        '                    "', VIT_8='" & VIT_8 & "', VI_8='" & VI_8 & "', VIES_9='" & VIES_9 & "', VIT_9='" & VIT_9 & _
        '                    "', VI_9='" & VI_9 & "', VIES_10='" & VIES_10 & "', VIT_10='" & VIT_10 & "', VI_10='" & VI_10 & _
        '                    "', TDS_RATE='" & TDS_RATE & "', TDS_F='" & TDS_F & "', BONUS_WORKDAYS='" & BONUS_WORKDAYS & _
        '                    "', BONUS_WAGELIMIT='" & BONUS_WAGELIMIT & "', BONUS_LIMIT='" & BONUS_LIMIT & _
        '                    "', BONUS_ARRIER='" & BONUS_ARRIER & "', BONUS_RATE='" & BONUS_RATE & "', PFAC02='" & PFAC02 & _
        '                    "', PFAC21='" & PFAC21 & "', PFAC22='" & PFAC22 & "', BASIC_RND='" & BASIC_RND & "', DA_RND='" & DA_RND & _
        '                    "', HRA_RND='" & HRA_RND & "', CONV_RND='" & CONV_RND & "', MEDICAL_RND='" & MEDICAL_RND & _
        '                    "', EARN1_RND='" & EARN1_RND & "', EARN2_RND='" & EARN2_RND & "', EARN3_RND='" & EARN3_RND & _
        '                    "', EARN4_RND='" & EARN4_RND & "', EARN5_RND='" & EARN5_RND & "', EARN6_RND='" & EARN6_RND & _
        '                    "', EARN7_RND='" & EARN7_RND & "', EARN8_RND='" & EARN8_RND & "', EARN9_RND='" & EARN9_RND & _
        '                    "', EARN10_RND='" & EARN10_RND & "', TDS_RND='" & TDS_RND & "', DED1_RND='" & DED1_RND & _
        '                    "', DED2_RND='" & DED2_RND & "', DED3_RND='" & DED3_RND & "', DED4_RND='" & DED4_RND & _
        '                    "', DED5_RND='" & DED5_RND & "', DED6_RND='" & DED6_RND & "', DED7_RND='" & DED7_RND & _
        '                    "', DED8_RND='" & DED8_RND & "', DED9_RND='" & DED9_RND & "', DED10_RND='" & DED10_RND & _
        '                    "', OTAMT_RND='" & OTAMT_RND & "', GRADUTY_YRLIMIT='" & GRADUTY_YRLIMIT & "', GRADUTY_FORMULA='" & GRADUTY_FORMULA & _
        '                    "', PF_ALLOWED='" & PF_ALLOWED & "', VPF_ALLOWED='" & VPF_ALLOWED & "', ESI_ALLOWED='" & ESI_ALLOWED & _
        '                    "', PROF_TAX_ALLOWED='" & PROF_TAX_ALLOWED & "', GRADUTY_ALLOWED='" & GRADUTY_ALLOWED & "', BONUS_ALLOWED='" & BONUS_ALLOWED & _
        '                    "', REIMBREL='" & REIMBREL & "', LEAVEREL='" & LEAVEREL & "', LEAVEPF='" & LEAVEPF & "', LVFIELD='" & LVFIELD & _
        '                    "', gbasic='" & gbasic & "', gda='" & gda & "', ghra='" & ghra & "', gconv='" & gconv & "', gmed='" & gmed & _
        '                    "', gEARN1='" & gEARN1 & "', gEARN2='" & gEARN2 & "', gEARN3='" & gEARN3 & "', gEARN4='" & gEARN4 & "', gEARN5='" & gEARN5 & _
        '                    "', gEARN6='" & gEARN6 & "', gEARN7='" & gEARN7 & "', gEARN8='" & gEARN8 & "', gEARN9='" & gEARN9 & "', gEARN10='" & gEARN10 & _
        '                    "', ESIREL='" & ESIREL & "'"

        Dim sSql As String = "Update Pay_Setup set PFLIMIT='" & PFLIMIT & "', PF='" & PF & "', EPF='" & EPF & "', FPF='" & FPF & "', " & _
                           "PFE='" & PFE & "', VPF='" & VPF & "', PFRND='" & PFRND & "', PFREL='" & PFREL & "', ESILIMIT='" & ESILIMIT & _
                           "', ESI='" & ESI & "', ESIE='" & ESIE & "', ESIRND='" & ESIRND & "', VDES_1='" & VDES_1 & "', VDT_1='" & VDT_1 & _
                           "', VD_1='" & VD_1 & "', VDES_2='" & VDES_2 & "', VDT_2='" & VDT_2 & "', VD_2='" & VD_2 & "', VDES_3='" & VDES_3 & _
                           "', VDT_3='" & VDT_3 & "', VD_3='" & VD_3 & "', VDES_4='" & VDES_4 & "', VDT_4='" & VDT_4 & "', VD_4='" & VD_4 & _
                           "', VDES_5='" & VDES_5 & "', VDT_5='" & VDT_5 & "', VD_5='" & VD_5 & "', VDES_6='" & VDES_6 & _
                           "', VDT_6='" & VDT_6 & "', VD_6='" & VD_6 & "', VDES_7='" & VDES_7 & "', VDT_7='" & VDT_7 & _
                           "', VD_7='" & VD_7 & "', VDES_8='" & VDES_8 & "', VDT_8='" & VDT_8 & "', VD_8='" & VD_8 & _
                           "', VDES_9='" & VDES_9 & "', VDT_9='" & VDT_9 & "', VD_9='" & VD_9 & "', VDES_10='" & VDES_10 & _
                           "', VDT_10='" & VDT_10 & "', VD_10='" & VD_10 & "', VIES_1='" & VIES_1 & "', VIT_1='" & VIT_1 & _
                           "', VI_1='" & VI_1 & "', VIES_2='" & VIES_2 & "', VIT_2='" & VIT_2 & "', VI_2='" & VI_2 & _
                           "', VIES_3='" & VIES_3 & "', VIT_3='" & VIT_3 & "', VI_3='" & VI_3 & "', VIES_4='" & VIES_4 & _
                           "', VIT_4='" & VIT_4 & "', VI_4='" & VI_4 & "', VIES_5='" & VIES_5 & "', VIT_5='" & VIT_5 & _
                           "', VI_5='" & VI_5 & "', VIES_6='" & VIES_6 & "', VIT_6='" & VIT_6 & "', VI_6='" & VI_6 & _
                           "', VIES_7='" & VIES_7 & "', VIT_7='" & VIT_7 & "', VI_7='" & VI_7 & "', VIES_8='" & VIES_8 & _
                           "', VIT_8='" & VIT_8 & "', VI_8='" & VI_8 & "', VIES_9='" & VIES_9 & "', VIT_9='" & VIT_9 & _
                           "', VI_9='" & VI_9 & "', VIES_10='" & VIES_10 & "', VIT_10='" & VIT_10 & "', VI_10='" & VI_10 & _
                           "', TDS_RATE='" & TDS_RATE & "', TDS_F='" & TDS_F & "', BONUS_WORKDAYS='" & BONUS_WORKDAYS & _
                           "', BONUS_WAGELIMIT='" & BONUS_WAGELIMIT & "'"

        Dim sSql1 As String = "Update Pay_Setup set BONUS_LIMIT='" & BONUS_LIMIT & _
                           "', BONUS_ARRIER='" & BONUS_ARRIER & "', BONUS_RATE='" & BONUS_RATE & "', PFAC02='" & PFAC02 & _
                           "', PFAC21='" & PFAC21 & "', PFAC22='" & PFAC22 & "', BASIC_RND='" & BASIC_RND & "', DA_RND='" & DA_RND & _
                           "', HRA_RND='" & HRA_RND & "', CONV_RND='" & CONV_RND & "', MEDICAL_RND='" & MEDICAL_RND & _
                           "', EARN1_RND='" & EARN1_RND & "', EARN2_RND='" & EARN2_RND & "', EARN3_RND='" & EARN3_RND & _
                           "', EARN4_RND='" & EARN4_RND & "', EARN5_RND='" & EARN5_RND & "', EARN6_RND='" & EARN6_RND & _
                           "', EARN7_RND='" & EARN7_RND & "', EARN8_RND='" & EARN8_RND & "', EARN9_RND='" & EARN9_RND & _
                           "', EARN10_RND='" & EARN10_RND & "', TDS_RND='" & TDS_RND & "', DED1_RND='" & DED1_RND & _
                           "', DED2_RND='" & DED2_RND & "', DED3_RND='" & DED3_RND & "', DED4_RND='" & DED4_RND & _
                           "', DED5_RND='" & DED5_RND & "', DED6_RND='" & DED6_RND & "', DED7_RND='" & DED7_RND & _
                           "', DED8_RND='" & DED8_RND & "', DED9_RND='" & DED9_RND & "', DED10_RND='" & DED10_RND & _
                           "', OTAMT_RND='" & OTAMT_RND & "', GRADUTY_YRLIMIT='" & GRADUTY_YRLIMIT & "', GRADUTY_FORMULA='" & GRADUTY_FORMULA & _
                           "', PF_ALLOWED='" & PF_ALLOWED & "', VPF_ALLOWED='" & VPF_ALLOWED & "', ESI_ALLOWED='" & ESI_ALLOWED & _
                           "', PROF_TAX_ALLOWED='" & PROF_TAX_ALLOWED & "', GRADUTY_ALLOWED='" & GRADUTY_ALLOWED & "', BONUS_ALLOWED='" & BONUS_ALLOWED & _
                           "', REIMBREL='" & REIMBREL & "', LEAVEREL='" & LEAVEREL & "', LEAVEPF='" & LEAVEPF & "', LVFIELD='" & LVFIELD & _
                           "', gbasic='" & gbasic & "', gda='" & gda & "', ghra='" & ghra & "', gconv='" & gconv & "', gmed='" & gmed & _
                           "', gEARN1='" & gEARN1 & "', gEARN2='" & gEARN2 & "', gEARN3='" & gEARN3 & "', gEARN4='" & gEARN4 & "', gEARN5='" & gEARN5 & _
                           "', gEARN6='" & gEARN6 & "', gEARN7='" & gEARN7 & "', gEARN8='" & gEARN8 & "', gEARN9='" & gEARN9 & "', gEARN10='" & gEARN10 & _
                           "', ESIREL='" & ESIREL & "'"

        If Common.servername = "Access" Then
            If Common.con1.State <> ConnectionState.Open Then
                Common.con1.Open()
            End If
            cmd1 = New OleDbCommand(sSql, Common.con1)
            cmd1.ExecuteNonQuery()
            cmd1 = New OleDbCommand(sSql1, Common.con1)
            cmd1.ExecuteNonQuery()
            If Common.con1.State <> ConnectionState.Closed Then
                Common.con1.Close()
            End If
        Else
            If Common.con.State <> ConnectionState.Open Then
                Common.con.Open()
            End If
            cmd = New SqlCommand(sSql, Common.con)
            cmd.ExecuteNonQuery()
            cmd = New SqlCommand(sSql1, Common.con)
            cmd.ExecuteNonQuery()
            If Common.con.State <> ConnectionState.Closed Then
                Common.con.Close()
            End If
        End If
        XtraMessageBox.Show(ulf, "<size=10>Saved Successfully</size>", "<size=9>Success</size>")
    End Sub

    Private Sub PictureEdit1_Click(sender As System.Object, e As System.EventArgs) Handles PictureEdit1.Click
        GridLookUpEditFormulaE1.EditValue = ""
    End Sub
    Private Sub PictureEdit2_Click(sender As System.Object, e As System.EventArgs) Handles PictureEdit2.Click
        GridLookUpEditFormulaE2.EditValue = ""
    End Sub
    Private Sub PictureEdit3_Click(sender As System.Object, e As System.EventArgs) Handles PictureEdit3.Click
        GridLookUpEditFormulaE3.EditValue = ""
    End Sub
    Private Sub PictureEdit4_Click(sender As System.Object, e As System.EventArgs) Handles PictureEdit4.Click
        GridLookUpEditFormulaE4.EditValue = ""
    End Sub
    Private Sub PictureEdit5_Click(sender As System.Object, e As System.EventArgs) Handles PictureEdit5.Click
        GridLookUpEditFormulaE5.EditValue = ""
    End Sub
    Private Sub PictureEdit6_Click(sender As System.Object, e As System.EventArgs) Handles PictureEdit6.Click
        GridLookUpEditFormulaE6.EditValue = ""
    End Sub
    Private Sub PictureEdit7_Click(sender As System.Object, e As System.EventArgs) Handles PictureEdit7.Click
        GridLookUpEditFormulaE7.EditValue = ""
    End Sub
    Private Sub PictureEdit8_Click(sender As System.Object, e As System.EventArgs) Handles PictureEdit8.Click
        GridLookUpEditFormulaE8.EditValue = ""
    End Sub
    Private Sub PictureEdit9_Click(sender As System.Object, e As System.EventArgs) Handles PictureEdit9.Click
        GridLookUpEditFormulaE9.EditValue = ""
    End Sub
    Private Sub PictureEdit10_Click(sender As System.Object, e As System.EventArgs) Handles PictureEdit10.Click
        GridLookUpEditFormulaE10.EditValue = ""
    End Sub
    Private Sub PictureEdit11_Click(sender As System.Object, e As System.EventArgs) Handles PictureEdit11.Click
        GridLookUpEditFormulaD1.EditValue = ""
    End Sub
    Private Sub PictureEdit12_Click(sender As System.Object, e As System.EventArgs) Handles PictureEdit12.Click
        GridLookUpEditFormulaD2.EditValue = ""
    End Sub
    Private Sub PictureEdit13_Click(sender As System.Object, e As System.EventArgs) Handles PictureEdit13.Click
        GridLookUpEditFormulaD3.EditValue = ""
    End Sub
    Private Sub PictureEdit14_Click(sender As System.Object, e As System.EventArgs) Handles PictureEdit14.Click
        GridLookUpEditFormulaD4.EditValue = ""
    End Sub
    Private Sub PictureEdit15_Click(sender As System.Object, e As System.EventArgs) Handles PictureEdit15.Click
        GridLookUpEditFormulaD5.EditValue = ""
    End Sub
    Private Sub PictureEdit16_Click(sender As System.Object, e As System.EventArgs) Handles PictureEdit16.Click
        GridLookUpEditFormulaD6.EditValue = ""
    End Sub
    Private Sub PictureEdit17_Click(sender As System.Object, e As System.EventArgs) Handles PictureEdit17.Click
        GridLookUpEditFormulaD7.EditValue = ""
    End Sub
    Private Sub PictureEdit18_Click(sender As System.Object, e As System.EventArgs) Handles PictureEdit18.Click
        GridLookUpEditFormulaD8.EditValue = ""
    End Sub
    Private Sub PictureEdit19_Click(sender As System.Object, e As System.EventArgs) Handles PictureEdit19.Click
        GridLookUpEditFormulaD9.EditValue = ""
    End Sub
    Private Sub PictureEdit20_Click(sender As System.Object, e As System.EventArgs) Handles PictureEdit20.Click
        GridLookUpEditFormulaD10.EditValue = ""
    End Sub
    Private Sub PictureEdit21_Click(sender As System.Object, e As System.EventArgs) Handles PictureEdit21.Click
        GridLookUpEditFormulaD11.EditValue = ""
    End Sub
End Class
