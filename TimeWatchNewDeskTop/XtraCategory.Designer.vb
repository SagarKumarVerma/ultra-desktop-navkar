﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraCategory
    Inherits DevExpress.XtraEditors.XtraUserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(XtraCategory))
        Me.SplitContainerControl1 = New DevExpress.XtraEditors.SplitContainerControl()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colCAT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTextEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.colCATAGORYNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTextEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.colLateVerification = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemCheckEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.colEveryInterval = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDeductFrom = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemComboBox1 = New DevExpress.XtraEditors.Repository.RepositoryItemComboBox()
        Me.colFromLeave = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemGridLookUpEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit()
        Me.TblLeaveMasterBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SSSDBDataSet = New iAS.SSSDBDataSet()
        Me.RepositoryItemGridLookUpEdit1View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colLEAVEFIELD = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLEAVECODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLEAVEDESCRIPTION = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colISOFFINCLUDE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colISHOLIDAYINCLUDE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colISLEAVEACCRUAL = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLEAVETYPE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSMIN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSMAX = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPRESENT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLEAVE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLEAVELIMIT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colFIXED = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colisMonthly = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colisCompOffType = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colExpiryDays = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colFromLeave1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLateDays = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTextEdit3 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.colDeductDay = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTextEdit4 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.colMaxLateDur = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTextEdit5 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.colLastModifiedBy = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLastModifiedDate = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemToggleSwitch1 = New DevExpress.XtraEditors.Repository.RepositoryItemToggleSwitch()
        Me.MemoEdit1 = New DevExpress.XtraEditors.MemoEdit()
        Me.TblCatagoryBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TblCatagoryTableAdapter = New iAS.SSSDBDataSetTableAdapters.tblCatagoryTableAdapter()
        Me.TblCatagory1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblCatagory1TableAdapter()
        Me.TblLeaveMasterTableAdapter = New iAS.SSSDBDataSetTableAdapters.tblLeaveMasterTableAdapter()
        Me.TblLeaveMaster1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblLeaveMaster1TableAdapter()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl1.SuspendLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTextEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTextEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemComboBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemGridLookUpEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblLeaveMasterBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemGridLookUpEdit1View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTextEdit3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTextEdit4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTextEdit5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemToggleSwitch1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblCatagoryBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SplitContainerControl1
        '
        Me.SplitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainerControl1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainerControl1.Name = "SplitContainerControl1"
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.GridControl1)
        Me.SplitContainerControl1.Panel1.Text = "Panel1"
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.MemoEdit1)
        Me.SplitContainerControl1.Panel2.Text = "Panel2"
        Me.SplitContainerControl1.Size = New System.Drawing.Size(1145, 568)
        Me.SplitContainerControl1.SplitterPosition = 1036
        Me.SplitContainerControl1.TabIndex = 0
        Me.SplitContainerControl1.Text = "SplitContainerControl1"
        '
        'GridControl1
        '
        Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControl1.EmbeddedNavigator.Buttons.Append.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.Edit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = False
        Me.GridControl1.Location = New System.Drawing.Point(0, 0)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTextEdit1, Me.RepositoryItemTextEdit2, Me.RepositoryItemToggleSwitch1, Me.RepositoryItemComboBox1, Me.RepositoryItemGridLookUpEdit1, Me.RepositoryItemTextEdit3, Me.RepositoryItemTextEdit4, Me.RepositoryItemTextEdit5, Me.RepositoryItemCheckEdit1})
        Me.GridControl1.Size = New System.Drawing.Size(1036, 568)
        Me.GridControl1.TabIndex = 1
        Me.GridControl1.UseEmbeddedNavigator = True
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Appearance.TopNewRow.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.GridView1.Appearance.TopNewRow.ForeColor = System.Drawing.Color.Blue
        Me.GridView1.Appearance.TopNewRow.Options.UseFont = True
        Me.GridView1.Appearance.TopNewRow.Options.UseForeColor = True
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colCAT, Me.colCATAGORYNAME, Me.colLateVerification, Me.colEveryInterval, Me.colDeductFrom, Me.colFromLeave, Me.colFromLeave1, Me.colLateDays, Me.colDeductDay, Me.colMaxLateDur, Me.colLastModifiedBy, Me.colLastModifiedDate})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.NewItemRowText = "Click here to add new Category"
        Me.GridView1.OptionsBehavior.EditingMode = DevExpress.XtraGrid.Views.Grid.GridEditingMode.EditForm
        Me.GridView1.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top
        Me.GridView1.OptionsView.ShowFooter = True
        '
        'colCAT
        '
        Me.colCAT.ColumnEdit = Me.RepositoryItemTextEdit1
        Me.colCAT.FieldName = "CAT"
        Me.colCAT.Name = "colCAT"
        Me.colCAT.Visible = True
        Me.colCAT.VisibleIndex = 0
        '
        'RepositoryItemTextEdit1
        '
        Me.RepositoryItemTextEdit1.AutoHeight = False
        Me.RepositoryItemTextEdit1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.RepositoryItemTextEdit1.MaxLength = 3
        Me.RepositoryItemTextEdit1.Name = "RepositoryItemTextEdit1"
        '
        'colCATAGORYNAME
        '
        Me.colCATAGORYNAME.ColumnEdit = Me.RepositoryItemTextEdit2
        Me.colCATAGORYNAME.FieldName = "CATAGORYNAME"
        Me.colCATAGORYNAME.Name = "colCATAGORYNAME"
        Me.colCATAGORYNAME.Visible = True
        Me.colCATAGORYNAME.VisibleIndex = 1
        '
        'RepositoryItemTextEdit2
        '
        Me.RepositoryItemTextEdit2.AutoHeight = False
        Me.RepositoryItemTextEdit2.MaxLength = 35
        Me.RepositoryItemTextEdit2.Name = "RepositoryItemTextEdit2"
        '
        'colLateVerification
        '
        Me.colLateVerification.ColumnEdit = Me.RepositoryItemCheckEdit1
        Me.colLateVerification.FieldName = "LateVerification"
        Me.colLateVerification.Name = "colLateVerification"
        Me.colLateVerification.OptionsEditForm.UseEditorColRowSpan = False
        Me.colLateVerification.Visible = True
        Me.colLateVerification.VisibleIndex = 2
        '
        'RepositoryItemCheckEdit1
        '
        Me.RepositoryItemCheckEdit1.AutoHeight = False
        Me.RepositoryItemCheckEdit1.Name = "RepositoryItemCheckEdit1"
        Me.RepositoryItemCheckEdit1.ValueChecked = "Y"
        Me.RepositoryItemCheckEdit1.ValueGrayed = "N"
        Me.RepositoryItemCheckEdit1.ValueUnchecked = "N"
        '
        'colEveryInterval
        '
        Me.colEveryInterval.ColumnEdit = Me.RepositoryItemCheckEdit1
        Me.colEveryInterval.FieldName = "EveryInterval"
        Me.colEveryInterval.Name = "colEveryInterval"
        Me.colEveryInterval.Visible = True
        Me.colEveryInterval.VisibleIndex = 3
        '
        'colDeductFrom
        '
        Me.colDeductFrom.ColumnEdit = Me.RepositoryItemComboBox1
        Me.colDeductFrom.FieldName = "DeductFrom"
        Me.colDeductFrom.Name = "colDeductFrom"
        Me.colDeductFrom.Visible = True
        Me.colDeductFrom.VisibleIndex = 4
        '
        'RepositoryItemComboBox1
        '
        Me.RepositoryItemComboBox1.AutoHeight = False
        Me.RepositoryItemComboBox1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemComboBox1.Items.AddRange(New Object() {"Leave", "Attendance"})
        Me.RepositoryItemComboBox1.Name = "RepositoryItemComboBox1"
        '
        'colFromLeave
        '
        Me.colFromLeave.ColumnEdit = Me.RepositoryItemGridLookUpEdit1
        Me.colFromLeave.FieldName = "FromLeave"
        Me.colFromLeave.Name = "colFromLeave"
        Me.colFromLeave.Visible = True
        Me.colFromLeave.VisibleIndex = 5
        '
        'RepositoryItemGridLookUpEdit1
        '
        Me.RepositoryItemGridLookUpEdit1.AutoHeight = False
        Me.RepositoryItemGridLookUpEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemGridLookUpEdit1.DataSource = Me.TblLeaveMasterBindingSource
        Me.RepositoryItemGridLookUpEdit1.DisplayMember = "LEAVEFIELD"
        Me.RepositoryItemGridLookUpEdit1.KeyMember = "LEAVEFIELD;LEAVECODE"
        Me.RepositoryItemGridLookUpEdit1.Name = "RepositoryItemGridLookUpEdit1"
        Me.RepositoryItemGridLookUpEdit1.NullText = ""
        Me.RepositoryItemGridLookUpEdit1.View = Me.RepositoryItemGridLookUpEdit1View
        '
        'TblLeaveMasterBindingSource
        '
        Me.TblLeaveMasterBindingSource.DataMember = "tblLeaveMaster"
        Me.TblLeaveMasterBindingSource.DataSource = Me.SSSDBDataSet
        '
        'SSSDBDataSet
        '
        Me.SSSDBDataSet.DataSetName = "SSSDBDataSet"
        Me.SSSDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'RepositoryItemGridLookUpEdit1View
        '
        Me.RepositoryItemGridLookUpEdit1View.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colLEAVEFIELD, Me.colLEAVECODE, Me.colLEAVEDESCRIPTION, Me.colISOFFINCLUDE, Me.colISHOLIDAYINCLUDE, Me.colISLEAVEACCRUAL, Me.colLEAVETYPE, Me.colSMIN, Me.colSMAX, Me.colPRESENT, Me.colLEAVE, Me.colLEAVELIMIT, Me.colFIXED, Me.colisMonthly, Me.colisCompOffType, Me.colExpiryDays})
        Me.RepositoryItemGridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.RepositoryItemGridLookUpEdit1View.Name = "RepositoryItemGridLookUpEdit1View"
        Me.RepositoryItemGridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.RepositoryItemGridLookUpEdit1View.OptionsView.ShowGroupPanel = False
        '
        'colLEAVEFIELD
        '
        Me.colLEAVEFIELD.FieldName = "LEAVEFIELD"
        Me.colLEAVEFIELD.Name = "colLEAVEFIELD"
        Me.colLEAVEFIELD.Visible = True
        Me.colLEAVEFIELD.VisibleIndex = 0
        '
        'colLEAVECODE
        '
        Me.colLEAVECODE.FieldName = "LEAVECODE"
        Me.colLEAVECODE.Name = "colLEAVECODE"
        Me.colLEAVECODE.Visible = True
        Me.colLEAVECODE.VisibleIndex = 1
        '
        'colLEAVEDESCRIPTION
        '
        Me.colLEAVEDESCRIPTION.FieldName = "LEAVEDESCRIPTION"
        Me.colLEAVEDESCRIPTION.Name = "colLEAVEDESCRIPTION"
        '
        'colISOFFINCLUDE
        '
        Me.colISOFFINCLUDE.FieldName = "ISOFFINCLUDE"
        Me.colISOFFINCLUDE.Name = "colISOFFINCLUDE"
        '
        'colISHOLIDAYINCLUDE
        '
        Me.colISHOLIDAYINCLUDE.FieldName = "ISHOLIDAYINCLUDE"
        Me.colISHOLIDAYINCLUDE.Name = "colISHOLIDAYINCLUDE"
        '
        'colISLEAVEACCRUAL
        '
        Me.colISLEAVEACCRUAL.FieldName = "ISLEAVEACCRUAL"
        Me.colISLEAVEACCRUAL.Name = "colISLEAVEACCRUAL"
        '
        'colLEAVETYPE
        '
        Me.colLEAVETYPE.FieldName = "LEAVETYPE"
        Me.colLEAVETYPE.Name = "colLEAVETYPE"
        '
        'colSMIN
        '
        Me.colSMIN.FieldName = "SMIN"
        Me.colSMIN.Name = "colSMIN"
        '
        'colSMAX
        '
        Me.colSMAX.FieldName = "SMAX"
        Me.colSMAX.Name = "colSMAX"
        '
        'colPRESENT
        '
        Me.colPRESENT.FieldName = "PRESENT"
        Me.colPRESENT.Name = "colPRESENT"
        '
        'colLEAVE
        '
        Me.colLEAVE.FieldName = "LEAVE"
        Me.colLEAVE.Name = "colLEAVE"
        '
        'colLEAVELIMIT
        '
        Me.colLEAVELIMIT.FieldName = "LEAVELIMIT"
        Me.colLEAVELIMIT.Name = "colLEAVELIMIT"
        '
        'colFIXED
        '
        Me.colFIXED.FieldName = "FIXED"
        Me.colFIXED.Name = "colFIXED"
        '
        'colisMonthly
        '
        Me.colisMonthly.FieldName = "isMonthly"
        Me.colisMonthly.Name = "colisMonthly"
        '
        'colisCompOffType
        '
        Me.colisCompOffType.FieldName = "isCompOffType"
        Me.colisCompOffType.Name = "colisCompOffType"
        '
        'colExpiryDays
        '
        Me.colExpiryDays.FieldName = "ExpiryDays"
        Me.colExpiryDays.Name = "colExpiryDays"
        '
        'colFromLeave1
        '
        Me.colFromLeave1.ColumnEdit = Me.RepositoryItemGridLookUpEdit1
        Me.colFromLeave1.FieldName = "FromLeave1"
        Me.colFromLeave1.Name = "colFromLeave1"
        Me.colFromLeave1.Visible = True
        Me.colFromLeave1.VisibleIndex = 6
        '
        'colLateDays
        '
        Me.colLateDays.ColumnEdit = Me.RepositoryItemTextEdit3
        Me.colLateDays.FieldName = "LateDays"
        Me.colLateDays.Name = "colLateDays"
        Me.colLateDays.Visible = True
        Me.colLateDays.VisibleIndex = 7
        '
        'RepositoryItemTextEdit3
        '
        Me.RepositoryItemTextEdit3.AutoHeight = False
        Me.RepositoryItemTextEdit3.Mask.EditMask = "##"
        Me.RepositoryItemTextEdit3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.RepositoryItemTextEdit3.MaxLength = 2
        Me.RepositoryItemTextEdit3.Name = "RepositoryItemTextEdit3"
        '
        'colDeductDay
        '
        Me.colDeductDay.ColumnEdit = Me.RepositoryItemTextEdit4
        Me.colDeductDay.FieldName = "DeductDay"
        Me.colDeductDay.Name = "colDeductDay"
        Me.colDeductDay.Visible = True
        Me.colDeductDay.VisibleIndex = 8
        '
        'RepositoryItemTextEdit4
        '
        Me.RepositoryItemTextEdit4.AutoHeight = False
        Me.RepositoryItemTextEdit4.Mask.EditMask = "n2"
        Me.RepositoryItemTextEdit4.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.RepositoryItemTextEdit4.MaxLength = 4
        Me.RepositoryItemTextEdit4.Name = "RepositoryItemTextEdit4"
        '
        'colMaxLateDur
        '
        Me.colMaxLateDur.ColumnEdit = Me.RepositoryItemTextEdit5
        Me.colMaxLateDur.FieldName = "MaxLateDur"
        Me.colMaxLateDur.Name = "colMaxLateDur"
        Me.colMaxLateDur.Visible = True
        Me.colMaxLateDur.VisibleIndex = 9
        '
        'RepositoryItemTextEdit5
        '
        Me.RepositoryItemTextEdit5.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTextEdit5.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.RepositoryItemTextEdit5.Name = "RepositoryItemTextEdit5"
        '
        'colLastModifiedBy
        '
        Me.colLastModifiedBy.FieldName = "LastModifiedBy"
        Me.colLastModifiedBy.Name = "colLastModifiedBy"
        '
        'colLastModifiedDate
        '
        Me.colLastModifiedDate.FieldName = "LastModifiedDate"
        Me.colLastModifiedDate.Name = "colLastModifiedDate"
        '
        'RepositoryItemToggleSwitch1
        '
        Me.RepositoryItemToggleSwitch1.AutoHeight = False
        Me.RepositoryItemToggleSwitch1.Name = "RepositoryItemToggleSwitch1"
        Me.RepositoryItemToggleSwitch1.OffText = "Off"
        Me.RepositoryItemToggleSwitch1.OnText = "On"
        '
        'MemoEdit1
        '
        Me.MemoEdit1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MemoEdit1.EditValue = resources.GetString("MemoEdit1.EditValue")
        Me.MemoEdit1.Location = New System.Drawing.Point(0, 0)
        Me.MemoEdit1.Name = "MemoEdit1"
        Me.MemoEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.MemoEdit1.Properties.Appearance.Options.UseFont = True
        Me.MemoEdit1.Properties.MaxLength = 100000
        Me.MemoEdit1.Properties.ReadOnly = True
        Me.MemoEdit1.Size = New System.Drawing.Size(101, 568)
        Me.MemoEdit1.TabIndex = 2
        '
        'TblCatagoryBindingSource
        '
        Me.TblCatagoryBindingSource.DataMember = "tblCatagory"
        Me.TblCatagoryBindingSource.DataSource = Me.SSSDBDataSet
        '
        'TblCatagoryTableAdapter
        '
        Me.TblCatagoryTableAdapter.ClearBeforeFill = True
        '
        'TblCatagory1TableAdapter1
        '
        Me.TblCatagory1TableAdapter1.ClearBeforeFill = True
        '
        'TblLeaveMasterTableAdapter
        '
        Me.TblLeaveMasterTableAdapter.ClearBeforeFill = True
        '
        'TblLeaveMaster1TableAdapter1
        '
        Me.TblLeaveMaster1TableAdapter1.ClearBeforeFill = True
        '
        'XtraCategory
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.SplitContainerControl1)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.Name = "XtraCategory"
        Me.Size = New System.Drawing.Size(1145, 568)
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl1.ResumeLayout(False)
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTextEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTextEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemComboBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemGridLookUpEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblLeaveMasterBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemGridLookUpEdit1View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTextEdit3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTextEdit4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTextEdit5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemToggleSwitch1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblCatagoryBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SplitContainerControl1 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents MemoEdit1 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents TblCatagoryBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SSSDBDataSet As iAS.SSSDBDataSet
    Friend WithEvents colCAT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCATAGORYNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLateVerification As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colEveryInterval As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDeductFrom As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colFromLeave As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colFromLeave1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLateDays As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDeductDay As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colMaxLateDur As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLastModifiedBy As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLastModifiedDate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TblCatagoryTableAdapter As iAS.SSSDBDataSetTableAdapters.tblCatagoryTableAdapter
    Friend WithEvents TblCatagory1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.tblCatagory1TableAdapter
    Friend WithEvents RepositoryItemTextEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents RepositoryItemTextEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents RepositoryItemToggleSwitch1 As DevExpress.XtraEditors.Repository.RepositoryItemToggleSwitch
    Friend WithEvents RepositoryItemComboBox1 As DevExpress.XtraEditors.Repository.RepositoryItemComboBox
    Friend WithEvents RepositoryItemGridLookUpEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit
    Friend WithEvents RepositoryItemGridLookUpEdit1View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents TblLeaveMasterBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblLeaveMasterTableAdapter As iAS.SSSDBDataSetTableAdapters.tblLeaveMasterTableAdapter
    Friend WithEvents TblLeaveMaster1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.tblLeaveMaster1TableAdapter
    Friend WithEvents colLEAVEFIELD As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLEAVECODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLEAVEDESCRIPTION As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colISOFFINCLUDE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colISHOLIDAYINCLUDE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colISLEAVEACCRUAL As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLEAVETYPE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSMIN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSMAX As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPRESENT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLEAVE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLEAVELIMIT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colFIXED As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colisMonthly As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colisCompOffType As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colExpiryDays As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemTextEdit3 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents RepositoryItemTextEdit4 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents RepositoryItemTextEdit5 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents RepositoryItemCheckEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit

End Class
