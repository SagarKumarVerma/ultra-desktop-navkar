﻿Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports System.ComponentModel
Imports DevExpress.XtraGrid.Columns
Imports System.Data.SqlClient
Imports System.Text
Imports System.Data.OleDb
Imports System.Resources
Imports DevExpress.LookAndFeel
Imports System.Globalization
Imports System.IO
Imports DevExpress.XtraEditors

Public Class XtraCategoryEdit
    'Dim res_man As ResourceManager     'declare Resource manager to access to specific cultureinfo
    'Dim cul As CultureInfo     'declare culture info
    'Dim servername As String
    'Dim ConnectionString As String
    Dim ulf As UserLookAndFeel
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Dim adap, adap1 As SqlDataAdapter
    Dim adapA As OleDbDataAdapter
    Dim ds, ds1 As DataSet
    Dim CtId As String

    Public Sub New()
        InitializeComponent()
        If Common.servername = "Access" Then
            'ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\TimeWatch.mdb;Persist Security Info=True;Jet OLEDB:Database Password=SSS"
            'con1 = New OleDbConnection(ConnectionString)
            Me.TblLeaveMaster1TableAdapter1.Fill(Me.SSSDBDataSet.tblLeaveMaster1)
            GridLookUpEdit1.Properties.DataSource = SSSDBDataSet.tblLeaveMaster1
            GridLookUpEdit2.Properties.DataSource = SSSDBDataSet.tblLeaveMaster1
        Else
            'ConnectionString = "Data Source=" & servername & ";Initial Catalog=SSSDB;Integrated Security=True"
            'con = New SqlConnection(ConnectionString)
            TblLeaveMasterTableAdapter.Connection.ConnectionString = Common.ConnectionString
            Me.TblLeaveMasterTableAdapter.Fill(Me.SSSDBDataSet.tblLeaveMaster)
            GridLookUpEdit1.Properties.DataSource = SSSDBDataSet.tblLeaveMaster
            GridLookUpEdit2.Properties.DataSource = SSSDBDataSet.tblLeaveMaster
        End If
    End Sub
    Private Sub XtraCategory_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Dim xe As XtraCategory = New XtraCategory
        CtId = XtraCategory.CatId
        'for xtramessage box
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True

        'res_man = New ResourceManager("TimeWatchNewDeskTop.Res", GetType(XtraCategory).Assembly)
        'cul = CultureInfo.CreateSpecificCulture("en")

        'GridView1.Columns.Item(0).Caption = Common.res_man.GetString("gradecode", Common.cul)
        'GridView1.Columns.Item(1).Caption = Common.res_man.GetString("gradename", Common.cul)

        If CtId.Length = 0 Then
            SetDefaultValue()
        Else
            SetFormValue()
        End If

        If ToggleSwitch1.IsOn = True Then
            GroupControl1.Enabled = True
        ElseIf ToggleSwitch1.IsOn = False Then
            GroupControl1.Enabled = False
        End If
        If CheckEdit1.Checked = True Then
            GridLookUpEdit1.Enabled = True
            GridLookUpEdit2.Enabled = True
        ElseIf CheckEdit2.Checked = True Then
            GridLookUpEdit1.Enabled = False
            GridLookUpEdit2.Enabled = False
        End If
    End Sub

    Private Sub SetDefaultValue()
        TextEdit1.Enabled = True
        TextEdit1.Text = ""
        TextEdit2.Text = ""
        ToggleSwitch1.IsOn = False
        TextEdit3.Text = "00:00"
        TextEdit4.Text = ""
        ToggleSwitch2.IsOn = False
        CheckEdit2.Checked = True
        GridLookUpEdit1.EditValue = ""
        GridLookUpEdit2.EditValue = ""
        TextEdit5.Text = "0.00"
    End Sub
    Private Sub SetFormValue()
        TextEdit1.Enabled = False
        ds1 = New DataSet
        If Common.servername = "Access" Then
            Dim a As OleDbDataAdapter = New OleDbDataAdapter("select * from tblCatagory where CAT = '" & CtId & "' ", Common.con1)
            a.Fill(ds1)
        Else
            adap1 = New SqlDataAdapter("select * from tblCatagory where CAT = '" & CtId & "' ", Common.con)
            adap1.Fill(ds1)
        End If

        TextEdit1.Text = ds1.Tables(0).Rows(0).Item("CAT").ToString.Trim
        TextEdit2.Text = ds1.Tables(0).Rows(0).Item("CATAGORYNAME").ToString.Trim
        If ds1.Tables(0).Rows(0).Item("LateVerification").ToString.Trim = "Y" Then
            ToggleSwitch1.IsOn = True
        ElseIf ds1.Tables(0).Rows(0).Item("LateVerification").ToString.Trim = "N" Then
            ToggleSwitch1.IsOn = False
        End If
        TextEdit3.Text = (ds1.Tables(0).Rows(0).Item("MaxLateDur").ToString.Trim \ 60) & ":" & ds1.Tables(0).Rows(0).Item("MaxLateDur").ToString.Trim Mod 60
        TextEdit4.Text = ds1.Tables(0).Rows(0).Item("LateDays").ToString.Trim
        If ds1.Tables(0).Rows(0).Item("EveryInterval").ToString.Trim = "Y" Then
            ToggleSwitch2.IsOn = True
        ElseIf ds1.Tables(0).Rows(0).Item("EveryInterval").ToString.Trim = "N" Then
            ToggleSwitch2.IsOn = False
        End If
        If ds1.Tables(0).Rows(0).Item("DeductFrom").ToString.Trim = "A" Then
            CheckEdit2.Checked = True
        ElseIf ds1.Tables(0).Rows(0).Item("DeductFrom").ToString.Trim = "L" Then
            CheckEdit1.Checked = True
        End If
        GridLookUpEdit1.EditValue = ds1.Tables(0).Rows(0).Item("FromLeave").ToString
        GridLookUpEdit2.EditValue = ds1.Tables(0).Rows(0).Item("FromLeave1").ToString
        TextEdit5.Text = ds1.Tables(0).Rows(0).Item("DeductDay").ToString

    End Sub
    Private Sub SimpleButton2_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton2.Click
        Me.Close()
    End Sub

    Private Sub ToggleSwitch1_Toggled(sender As System.Object, e As System.EventArgs) Handles ToggleSwitch1.Toggled
        If ToggleSwitch1.IsOn = True Then
            GroupControl1.Enabled = True
        ElseIf ToggleSwitch1.IsOn = False Then
            GroupControl1.Enabled = False
        End If
    End Sub

    Private Sub CheckEdit1_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckEdit1.CheckedChanged
        If CheckEdit1.Checked = True Then
            GridLookUpEdit1.Enabled = True
            GridLookUpEdit2.Enabled = True
        End If
    End Sub

    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click
        Dim CAT As String = TextEdit1.Text.Trim
        Dim CATAGORYNAME As String = TextEdit2.Text
        Dim LateVerification As Char = ""
        If ToggleSwitch1.IsOn = True Then
            LateVerification = "Y"
        ElseIf ToggleSwitch1.IsOn = False Then
            LateVerification = "N"
        End If
        Dim MaxLateDur As Double = 0
        Dim MD() As String = TextEdit3.Text.Split(":")
        MaxLateDur = (MD(0) * 60) + MD(1)
        Dim LateDays As Double = 0
        If TextEdit4.Text = "" Then
            LateDays = 0
        Else
            LateDays = TextEdit4.Text.Trim
        End If
        Dim EveryInterval As Char = ""
        If ToggleSwitch2.IsOn = True Then
            EveryInterval = "Y"
        ElseIf ToggleSwitch2.IsOn = False Then
            EveryInterval = "N"
        End If
        Dim DeductFrom As Char = ""
        If CheckEdit1.Checked = True Then
            DeductFrom = "L"
        ElseIf CheckEdit2.Checked = True Then
            DeductFrom = "A"
        End If
        Dim FromLeave As String = GridLookUpEdit1.EditValue
        Dim FromLeave1 As String = GridLookUpEdit2.EditValue
        Dim DeductDay As Double = TextEdit5.Text.Trim


        If CtId = "" Then
            'insert
            If Common.servername = "Access" Then

                adapA = New OleDbDataAdapter("select CAT from tblCatagory where CAT = '" & CAT & "' ", Common.con1)
                ds1 = New DataSet
                adapA.Fill(ds1)
                If ds1.Tables(0).Rows.Count > 0 Then
                    XtraMessageBox.Show(ulf, "<size=10>Dupplicate Category Code</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    TextEdit1.Select()
                    Exit Sub
                End If

                adapA = New OleDbDataAdapter("select CAT from tblCatagory where CATAGORYNAME = '" & CATAGORYNAME & "' ", Common.con1)
                ds1 = New DataSet
                adapA.Fill(ds1)
                If ds1.Tables(0).Rows.Count > 0 Then
                    XtraMessageBox.Show(ulf, "<size=10>Dupplicate Category Name</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    TextEdit2.Select()
                    Exit Sub
                End If
                If Common.con1.State <> ConnectionState.Open Then
                    Common.con1.Open()
                End If
                cmd1 = New OleDbCommand("INSERT INTO tblCatagory (CAT,CATAGORYNAME,LateVerification,EveryInterval,DeductFrom,FromLeave,FromLeave1,LateDays,DeductDay,MaxLateDur,LastModifiedBy,LastModifiedDate) VALUES ('" & CAT & "','" & CATAGORYNAME & "','" & LateVerification & "','" & EveryInterval & "','" & DeductFrom & "','" & FromLeave & "','" & FromLeave1 & "','" & LateDays & "','" & DeductDay & "','" & MaxLateDur & "','admin','" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "')", Common.con1)
                cmd1.ExecuteNonQuery()
                Common.con1.Close()
                Common.LogPost("Category Add; Category Code='" & CAT)
                Me.Close()
            Else

                adap = New SqlDataAdapter("select CAT from tblCatagory where CAT = '" & CAT & "' ", Common.con)
                ds = New DataSet
                adap.Fill(ds)
                If ds.Tables(0).Rows.Count > 0 Then
                    XtraMessageBox.Show(ulf, "<size=10>Dupplicate Category Code</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    TextEdit1.Select()
                    Exit Sub
                End If

                adap = New SqlDataAdapter("select CAT from tblCatagory where CATAGORYNAME = '" & CATAGORYNAME & "' ", Common.con)
                ds = New DataSet
                adap.Fill(ds)
                If ds.Tables(0).Rows.Count > 0 Then
                    XtraMessageBox.Show(ulf, "<size=10>Dupplicate Category Name</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    TextEdit2.Select()
                    Exit Sub
                End If
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                cmd = New SqlCommand("INSERT INTO tblCatagory (CAT,CATAGORYNAME,LateVerification,EveryInterval,DeductFrom,FromLeave,FromLeave1,LateDays,DeductDay,MaxLateDur,LastModifiedBy,LastModifiedDate) VALUES ('" & CAT & "','" & CATAGORYNAME & "','" & LateVerification & "','" & EveryInterval & "','" & DeductFrom & "','" & FromLeave & "','" & FromLeave1 & "','" & LateDays & "','" & DeductDay & "','" & MaxLateDur & "','admin','" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "')", Common.con)
                cmd.ExecuteNonQuery()
                Common.con.Close()
                Common.LogPost("Category Add; Category Code='" & CAT)
                Me.Close()
            End If
        Else
            'update
            If Common.servername = "Access" Then
                If Common.con1.State <> ConnectionState.Open Then
                    Common.con1.Open()
                End If
                cmd1 = New OleDbCommand("update tblCatagory set CATAGORYNAME = '" & CATAGORYNAME & "', LateVerification ='" & LateVerification & "', EveryInterval = '" & EveryInterval & "', DeductFrom = '" & DeductFrom & "', FromLeave = '" & FromLeave & "', FromLeave1 = '" & FromLeave1 & "', LateDays = '" & LateDays & "', DeductDay = '" & DeductDay & "', MaxLateDur = '" & MaxLateDur & "', LastModifiedBy = 'admin', LastModifiedDate = '" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "'  where CAT = '" & CAT & "'", Common.con1)
                cmd1.ExecuteNonQuery()
                Common.con1.Close()
                Common.LogPost("Category Update; Category Code='" & CAT)
                Me.Close()
            Else
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                cmd = New SqlCommand("update tblCatagory set CATAGORYNAME = '" & CATAGORYNAME & "', LateVerification ='" & LateVerification & "', EveryInterval = '" & EveryInterval & "', DeductFrom = '" & DeductFrom & "', FromLeave = '" & FromLeave & "', FromLeave1 = '" & FromLeave1 & "', LateDays = '" & LateDays & "', DeductDay = '" & DeductDay & "', MaxLateDur = '" & MaxLateDur & "', LastModifiedBy = 'admin', LastModifiedDate = '" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "'  where CAT = '" & CAT & "'", Common.con)
                cmd.ExecuteNonQuery()
                Common.con.Close()
                Common.LogPost("Category Update; Category Code='" & CAT)
                Me.Close()
            End If
        End If
    End Sub
    Private Sub CheckEdit2_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckEdit2.CheckedChanged
        If CheckEdit2.Checked = True Then
            GridLookUpEdit1.Enabled = False
            GridLookUpEdit2.Enabled = False
        End If
    End Sub

    Private Sub SimpleButton3_Click(sender As System.Object, e As System.EventArgs)
        GridLookUpEdit2.EditValue = ""
    End Sub

    Private Sub PictureEdit1_EditValueChanged(sender As System.Object, e As System.EventArgs) Handles PictureEdit1.EditValueChanged

    End Sub

    Private Sub PictureEdit1_Click(sender As System.Object, e As System.EventArgs) Handles PictureEdit1.Click
        GridLookUpEdit2.EditValue = ""
    End Sub
End Class