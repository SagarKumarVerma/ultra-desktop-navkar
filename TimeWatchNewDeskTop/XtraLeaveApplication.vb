﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports System.ComponentModel
Imports DevExpress.XtraGrid.Columns
Imports System.Data.SqlClient
Imports System.Text
Imports System.Data.OleDb
Imports DevExpress.Utils
Imports DevExpress.XtraGrid.Views.Base

Public Class XtraLeaveApplication
    Dim ulf As UserLookAndFeel
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Dim leaveFlage As Boolean = True
    Public Sub New()
        InitializeComponent()
        If Common.servername = "Access" Then
            Me.LeaveApplication1TableAdapter1.Fill(Me.SSSDBDataSet.LeaveApplication1)
            'Me.TblEmployee1TableAdapter1.Fill(Me.SSSDBDataSet.TblEmployee1)
        Else
            LeaveApplicationTableAdapter.Connection.ConnectionString = Common.ConnectionString '"Data Source=DESKTOP-PG6NDV8;Initial Catalog=SSSDB;Integrated Security=True"
            Me.LeaveApplicationTableAdapter.Fill(Me.SSSDBDataSet.LeaveApplication)
            TblLeaveMasterTableAdapter.Connection.ConnectionString = Common.ConnectionString
            'TblEmployeeTableAdapter.Connection.ConnectionString = Common.ConnectionString
            'Me.TblEmployeeTableAdapter.Fill(Me.SSSDBDataSet.TblEmployee)
        End If
        Common.SetGridFont(GridView1, New Font("Tahoma", 11))
    End Sub
    Private Sub XtraLeaveApplication_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Me.Width = Common.NavWidth 'Me.Parent.Width
        Me.Height = Common.NavHeight 'Me.Parent.Height
        'SplitContainerControl1.Width = Common.splitforMasterMenuWidth 'SplitContainerControl1.Parent.Width
        SplitContainerControl1.SplitterPosition = Common.SplitterPosition '(SplitContainerControl1.Parent.Width) * 85 / 100

        If Common.servername = "Access" Then
            Me.TblLeaveMaster1TableAdapter1.Fill(Me.SSSDBDataSet.tblLeaveMaster1)
            Me.LeaveApplication1TableAdapter1.Fill(Me.SSSDBDataSet.LeaveApplication1)
            'Me.TblEmployee1TableAdapter1.Fill(Me.SSSDBDataSet.TblEmployee1)
            'LookUpEdit1.Properties.DataSource = SSSDBDataSet.TblEmployee1
        Else
            Me.TblLeaveMasterTableAdapter.Fill(Me.SSSDBDataSet.tblLeaveMaster)
            TblEmployeeTableAdapter.Connection.ConnectionString = Common.ConnectionString
            'Me.TblEmployeeTableAdapter.Fill(Me.SSSDBDataSet.TblEmployee)
            'LookUpEdit1.Properties.DataSource = SSSDBDataSet.TblEmployee
        End If
        LookUpEdit1.Properties.DataSource = Common.EmpNonAdmin

        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True
        setDefault()
    End Sub
    Private Sub CheckEdit2_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckEdit2.CheckedChanged
        If CheckEdit2.Checked = True Then
            CheckEdit5.Visible = True
            CheckEdit6.Visible = True
        Else
            CheckEdit5.Visible = False
            CheckEdit6.Visible = False
        End If
    End Sub
    Private Sub TextEdit2_Leave(sender As System.Object, e As System.EventArgs) Handles TextEdit2.Leave
        If Not leaveFlage Then Return
        leaveFlage = False
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim ds As DataSet = New DataSet
        Dim sSql As String = "select PAYCODE from TblEmployee where PAYCODE = '" & TextEdit2.Text.Trim & "'"

        If TextEdit2.Text.Trim <> "" Then
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
            If ds.Tables(0).Rows.Count = 0 Then
                If TextEdit2.Text.Trim <> "" Then
                    XtraMessageBox.Show(ulf, "<size=10>No such Employee</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    TextEdit2.Select()
                    setDefault()
                    leaveFlage = True
                    Exit Sub
                End If
            End If
        End If
        leaveFlage = True
    End Sub
    Private Sub DateEdit2_Leave(sender As System.Object, e As System.EventArgs) Handles DateEdit2.Leave
        If DateEdit2.DateTime.ToString("yyyy-MM-dd") = "0001-01-01" Then
            DateEdit2.EditValue = DateEdit1.EditValue
            'MsgBox(DateEdit2.DateTime.ToString)
        End If
        If DateEdit1.EditValue > DateEdit2.EditValue Then
            DateEdit1.EditValue = DateEdit2.EditValue
        End If
        If Not leaveFlage Then Return
        leaveFlage = False
        setSelectValue()
        leaveFlage = True
    End Sub
    Private Sub setDefault()
        TextEdit2.Text = ""
        LookUpEdit1.EditValue = ""
        lblCardNum.Text = ""
        lblCat.Text = ""
        lblComp.Text = ""
        lblDept.Text = ""
        lblDesi.Text = ""
        lblGrade.Text = ""
        lblName.Text = ""
        lblEmpGrp.Text = ""
        DateEdit1.EditValue = Now
        DateEdit2.EditValue = Now
        DateEdit3.EditValue = Now
        GridLookUpEditLeave.EditValue = ""
        TextEdit1.Text = ""
        CheckEdit4.Checked = True
        CheckEdit5.Visible = False
        CheckEdit6.Visible = False
        SimpleButton1.Enabled = False
        LabelControl13.Visible = False
        ComboCmpAgn.Visible = False

        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim ds As DataSet = New DataSet
        Dim sSql As String = "select max(VOUCHER_NO) from tblTimeRegister "
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(ds)
        End If
        'MsgBox(ds.Tables(0).Rows(0).Item(0))
        If ds.Tables(0).Rows.Count = 0 Or ds.Tables(0).Rows(0).Item(0).ToString.Trim = "" Then
            lblVoucher.Text = "0000000001"
        Else
            Dim voucher_num As Integer = Convert.ToInt32(ds.Tables(0).Rows(0).Item(0).ToString.Trim) + 1
            lblVoucher.Text = (Convert.ToInt32(ds.Tables(0).Rows(0).Item(0).ToString.Trim) + 1).ToString("0000000000")
        End If
        If Common.IsNepali = "Y" Then
            ComboNepaliYearFrm.Visible = True
            ComboNEpaliMonthFrm.Visible = True
            ComboNepaliDateFrm.Visible = True
            ComboNepaliYearTo.Visible = True
            ComboNEpaliMonthTo.Visible = True
            ComboNepaliDateTo.Visible = True
            ComboNepaliYearApp.Visible = True
            ComboNEpaliMonthApp.Visible = True
            ComboNepaliDateApp.Visible = True
            DateEdit1.Visible = False
            DateEdit2.Visible = False
            DateEdit3.Visible = False

            Dim DC As New DateConverter()
            Dim doj As String = DC.ToBS(New Date(DateEdit1.DateTime.Year, DateEdit1.DateTime.Month, DateEdit1.DateTime.Day))
            Dim dojTmp() As String = doj.Split("-")
            ComboNepaliYearFrm.EditValue = dojTmp(0)
            ComboNEpaliMonthFrm.SelectedIndex = dojTmp(1) - 1
            ComboNepaliDateFrm.EditValue = dojTmp(2)

            doj = DC.ToBS(New Date(DateEdit2.DateTime.Year, DateEdit2.DateTime.Month, DateEdit2.DateTime.Day))           
            dojTmp = doj.Split("-")
            ComboNepaliYearTo.EditValue = dojTmp(0)
            ComboNEpaliMonthTo.SelectedIndex = dojTmp(1) - 1
            ComboNepaliDateTo.EditValue = dojTmp(2)

            doj = DC.ToBS(New Date(DateEdit3.DateTime.Year, DateEdit3.DateTime.Month, DateEdit3.DateTime.Day))
            dojTmp = doj.Split("-")
            ComboNepaliYearApp.EditValue = dojTmp(0)
            ComboNEpaliMonthApp.SelectedIndex = dojTmp(1) - 1
            ComboNepaliDateApp.EditValue = dojTmp(2)
        Else
            ComboNepaliYearFrm.Visible = False
            ComboNEpaliMonthFrm.Visible = False
            ComboNepaliDateFrm.Visible = False
            ComboNepaliYearTo.Visible = False
            ComboNEpaliMonthTo.Visible = False
            ComboNepaliDateTo.Visible = False
            ComboNepaliYearApp.Visible = False
            ComboNEpaliMonthApp.Visible = False
            ComboNepaliDateApp.Visible = False
            DateEdit1.Visible = True
            DateEdit2.Visible = True
            DateEdit3.Visible = True
        End If

    End Sub
    Private Sub setSelectValue()
        Dim adap, adap1 As SqlDataAdapter
        Dim adapA, adapA1 As OleDbDataAdapter
        Dim ds As DataSet = New DataSet
        Dim ds1 As DataSet
        Dim sSql As String = "select PAYCODE from LeaveApplication where PAYCODE = '" & LookUpEdit1.EditValue.ToString.Trim & "' and ForDate >= '" & DateEdit1.DateTime.ToString("yyyy-MM-dd 00:00:00") & "' and Fordate <= '" & DateEdit2.DateTime.ToString("yyyy-MM-dd 23:59:59") & "'"

        If Common.IsNepali = "Y" Then
            Dim DC As New DateConverter()
            Try
                'DateEdit1.DateTime = DC.ToAD(New Date(ComboNepaliYearFrm.EditValue, ComboNEpaliMonthTo.SelectedIndex + 1, ComboNepaliDateTo.EditValue))
                DateEdit1.DateTime = DC.ToAD(ComboNepaliYearFrm.EditValue & "-" & ComboNEpaliMonthTo.SelectedIndex + 1 & "-" & ComboNepaliDateTo.EditValue)
            Catch ex As Exception
                XtraMessageBox.Show(ulf, "<size=10>Invalid From Date</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                ComboNepaliDateFrm.Select()
                Exit Sub
            End Try

            Try
                'DateEdit2.DateTime = DC.ToAD(New Date(ComboNepaliYearTo.EditValue, ComboNEpaliMonthTo.SelectedIndex + 1, ComboNepaliDateTo.EditValue))
                DateEdit2.DateTime = DC.ToAD(ComboNepaliYearTo.EditValue & "-" & ComboNEpaliMonthTo.SelectedIndex + 1 & "-" & ComboNepaliDateTo.EditValue)
            Catch ex As Exception
                XtraMessageBox.Show(ulf, "<size=10>Invalid To Date</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                ComboNepaliDateTo.Select()
                Exit Sub
            End Try

            Try
                'DateEdit3.DateTime = DC.ToAD(New Date(ComboNepaliYearApp.EditValue, ComboNEpaliMonthApp.SelectedIndex + 1, ComboNepaliDateApp.EditValue))
                DateEdit3.DateTime = DC.ToAD(ComboNepaliYearApp.EditValue & "-" & ComboNEpaliMonthApp.SelectedIndex + 1 & "-" & ComboNepaliDateApp.EditValue)
            Catch ex As Exception
                XtraMessageBox.Show(ulf, "<size=10>Invalid Approve Date</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                ComboNepaliDateTo.Select()
                Exit Sub
            End Try
        End If

        If LookUpEdit1.EditValue.ToString.Trim <> "" Then
            If Common.servername = "Access" Then
                sSql = "select PAYCODE from LeaveApplication where PAYCODE = '" & LookUpEdit1.EditValue.ToString.Trim & "' and FORMAT(ForDate, 'yyyy-MM-dd 00:00:00') >= '" & DateEdit1.DateTime.ToString("yyyy-MM-dd 00:00:00") & "' and FORMAT(Fordate,'yyyy-MM-dd 00:00:00') <= '" & DateEdit2.DateTime.ToString("yyyy-MM-dd 23:59:59") & "'"
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
            If ds.Tables(0).Rows.Count > 0 Then
                XtraMessageBox.Show(ulf, "<size=10>Leave already present for the day.</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                LookUpEdit1.Select()
                setDefault()
                Exit Sub
            Else

            End If
        End If

    End Sub
    Private Sub LoadLeaveHistoryGrid()
        Dim gridselet As String = "select * from LeaveApplication where PayCode = '" & LookUpEdit1.EditValue.ToString.Trim & "'"
        If Common.servername = "Access" Then
            Dim dataAdapter As New OleDbDataAdapter(gridselet, Common.con1)
            Dim WTDataTable As New DataTable("LeaveApplication")
            dataAdapter.Fill(WTDataTable)
            GridControl1.DataSource = WTDataTable
        Else
            Dim dataAdapter As New SqlClient.SqlDataAdapter(gridselet, Common.con)
            Dim WTDataTable As New DataTable("LeaveApplication")
            dataAdapter.Fill(WTDataTable)
            GridControl1.DataSource = WTDataTable
        End If
    End Sub
    Private Sub LoadLeaveQuota(paycode As String)
        Dim adap, adap1 As SqlDataAdapter
        Dim adapA, adapA1 As OleDbDataAdapter
        Dim sSql As String
        Dim ds, ds1 As DataSet

        sSql = "select EmployeeGroup.Id from TblEmployee, EmployeeGroup where TblEmployee.EmployeeGroupId=EmployeeGroup.GroupId and PAYCODE='" & paycode & "'"
        ds1 = New DataSet
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(ds1)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(ds1)
        End If
        If Common.EmpGrpArr(ds1.Tables(0).Rows(0).Item(0)).ISCOMPOFF = "Y" Then
            sSql = "select LEAVEFIELD, LEAVECODE, LEAVEDESCRIPTION, isCompOffType from tblLeaveMaster"
        Else
            sSql = "select LEAVEFIELD, LEAVECODE, LEAVEDESCRIPTION, isCompOffType from tblLeaveMaster where isCompOffType <> 'Y'"
        End If


        ds = New DataSet
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(ds)
        End If
        Dim isCompOffType As String '= ds.Tables(0).Rows(0).Item("isCompOffType").ToString.Trim
        Dim s As String = ""
        Dim s1 As String = ""
        Dim dt As DataTable = New DataTable
        dt.Columns.Add("LeaveCode")
        dt.Columns.Add("LeaveDescription")
        dt.Columns.Add("LeaveLeft")

        Dim LeaveLedgerYear As Integer = DateEdit1.DateTime.Year
        If Common.gLeaveFinancialYear = "Y" Then
            If DateEdit1.DateTime.Month < 4 Then
                LeaveLedgerYear = DateEdit1.DateTime.Year - 1
                'Else
                '    LeaveLedgerYear = DateEdit2.DateTime.Year
            End If
            'Else
            '    LeaveLedgerYear = DateEdit2.DateTime.Year
        End If
        For i As Integer = 1 To ds.Tables(0).Rows.Count
            isCompOffType = ds.Tables(0).Rows(i - 1).Item("isCompOffType").ToString.Trim
            Dim index As Integer = Convert.ToInt16(ds.Tables(0).Rows(i - 1).Item("LEAVEFIELD").ToString.TrimStart("L"))
            s = "L" & index.ToString("00")
            s1 = "L" & index.ToString("00") & "_ADD "
            Dim sS As String = "select " & s & " , " & s1 & " from tblLeaveLedger where PAYCODE = '" & paycode & "' and LYEAR = " & LeaveLedgerYear & ""
            'MsgBox(sS)
            If Common.servername = "Access" Then
                adapA1 = New OleDbDataAdapter(sS, Common.con1)
                ds1 = New DataSet
                adapA1.Fill(ds1)
            Else
                adap1 = New SqlDataAdapter(sS, Common.con)
                ds1 = New DataSet
                adap1.Fill(ds1)
            End If
            Dim lftLEave As String
            If ds1.Tables(0).Rows.Count = 0 Then
                'Try
                sSql = "select EmployeeGroupId from TblEmployee where PAYCODE ='" & paycode & "'"
                Dim adapX As SqlDataAdapter
                Dim adapAX As OleDbDataAdapter
                Dim dsX As DataSet = New DataSet
                Dim dsX1 As DataSet = New DataSet
                If Common.servername = "Access" Then
                    adapAX = New OleDbDataAdapter(sSql, Common.con1)
                    adapAX.Fill(dsX1)
                Else
                    adapX = New SqlDataAdapter(sSql, Common.con)
                    adapX.Fill(dsX1)
                End If
                'sSql = "SELECT * from EmployeeGroupLeaveLedger where GroupId = '" & dsX1.Tables(0).Rows(0).Item("EmployeeGroupId").ToString.Trim & "' and LYEAR=" & Now.Year & " "
                sSql = "SELECT * from EmployeeGroupLeaveLedger where GroupId = '" & dsX1.Tables(0).Rows(0).Item("EmployeeGroupId").ToString.Trim & "' and LYEAR=" & LeaveLedgerYear & " "
                dsX = New DataSet
                If Common.servername = "Access" Then
                    adapAX = New OleDbDataAdapter(sSql, Common.con1)
                    adapAX.Fill(dsX)
                Else
                    adapX = New SqlDataAdapter(sSql, Common.con)
                    adapX.Fill(dsX)
                End If
                If dsX.Tables(0).Rows.Count = 0 Then
                    XtraMessageBox.Show(ulf, "<size=10>Leave Accrual Not Prsent. Please Create Leave Accrual.</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    setDefault()
                    Exit Sub
                Else
                    Dim sSql3 As String = "insert into tblLeaveLedger (PAYCODE, L01_ADD, L02_ADD, L03_ADD, L04_ADD, L05_ADD, L06_ADD, L07_ADD, L08_ADD, L09_ADD, L10_ADD, L11_ADD, L12_ADD, L13_ADD, L14_ADD, L15_ADD, L16_ADD, L17_ADD, L18_ADD, L19_ADD, L20_ADD, ACC_FLAG, LYEAR, L01, L02, L03, L04, L05, L06, L07, L08, L09, L10, L11, L12, L13, L14, L15, L16, L17, L18, L19, L20) VALUES " & _
                                          "('" & paycode & "', " & dsX.Tables(0).Rows(0).Item("L01_ADD").ToString.Trim & "," & dsX.Tables(0).Rows(0).Item("L02_ADD").ToString.Trim & "," & dsX.Tables(0).Rows(0).Item("L03_ADD").ToString.Trim & "," & dsX.Tables(0).Rows(0).Item("L04_ADD").ToString.Trim & "," & dsX.Tables(0).Rows(0).Item("L05_ADD").ToString.Trim & "," & dsX.Tables(0).Rows(0).Item("L06_ADD").ToString.Trim & ", " & _
                                         "" & dsX.Tables(0).Rows(0).Item("L07_ADD").ToString.Trim & "," & dsX.Tables(0).Rows(0).Item("L08_ADD").ToString.Trim & "," & dsX.Tables(0).Rows(0).Item("L09_ADD").ToString.Trim & "," & dsX.Tables(0).Rows(0).Item("L10_ADD").ToString.Trim & "," & dsX.Tables(0).Rows(0).Item("L11_ADD").ToString.Trim & "," & dsX.Tables(0).Rows(0).Item("L12_ADD").ToString.Trim & "," & dsX.Tables(0).Rows(0).Item("L13_ADD").ToString.Trim & ", " & _
                                         "" & dsX.Tables(0).Rows(0).Item("L14_ADD").ToString.Trim & "," & dsX.Tables(0).Rows(0).Item("L15_ADD").ToString.Trim & "," & dsX.Tables(0).Rows(0).Item("L16_ADD").ToString.Trim & "," & dsX.Tables(0).Rows(0).Item("L17_ADD").ToString.Trim & "," & dsX.Tables(0).Rows(0).Item("L18_ADD").ToString.Trim & "," & dsX.Tables(0).Rows(0).Item("L19_ADD").ToString.Trim & "," & dsX.Tables(0).Rows(0).Item("L20_ADD").ToString.Trim & ",'N','" & LeaveLedgerYear & "', " & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & ") "
                    If Common.servername = "Access" Then
                        If Common.con1.State <> ConnectionState.Open Then
                            Common.con1.Open()
                        End If
                        cmd1 = New OleDbCommand(sSql3, Common.con1)
                        cmd1.ExecuteNonQuery()
                        If Common.con1.State <> ConnectionState.Closed Then
                            Common.con1.Close()
                        End If
                    Else
                        If Common.con.State <> ConnectionState.Open Then
                            Common.con.Open()
                        End If
                        cmd = New SqlCommand(sSql3, Common.con)
                        cmd.ExecuteNonQuery()
                        If Common.con.State <> ConnectionState.Closed Then
                            Common.con.Close()
                        End If
                    End If
                End If
                lftLEave = "0"
            Else
                lftLEave = ds1.Tables(0).Rows(0).Item(1).ToString - ds1.Tables(0).Rows(0).Item(0).ToString
            End If
            If isCompOffType = "Y" Then
                lftLEave = "0"
            End If
            dt.Rows.Add(ds.Tables(0).Rows(i - 1).Item("LEAVECODE").ToString.Trim, ds.Tables(0).Rows(i - 1).Item("LEAVEDESCRIPTION").ToString.Trim, lftLEave)
        Next
        Dim datase As DataSet = New DataSet()
        datase.Tables.Add(dt)
        GridLookUpEditLeave.Properties.DataSource = dt
        GridLookUpEditLeave.Properties.DisplayMember = "LeaveCode"
        GridLookUpEditLeave.Properties.ValueMember = "LeaveCode"
        GridLookUpEditLeave.EditValue = GridLookUpEditLeave.Properties.GetKeyValue(0)
    End Sub
    Private Sub GridLookUpEdit1_CustomDisplayText(sender As System.Object, e As DevExpress.XtraEditors.Controls.CustomDisplayTextEventArgs) Handles GridLookUpEditLeave.CustomDisplayText
        If (e.Value Is Nothing) Then
            Return
        End If

        Dim gridLookUpEdit = CType(sender, GridLookUpEdit)
        Dim dataRowView = CType(gridLookUpEdit.Properties.GetRowByKeyValue(e.Value), DataRowView)
        If (dataRowView Is Nothing) Then
            Return
        End If

        Dim row = CType(dataRowView.Row, DataRow)
        If (row Is Nothing) Then
            Return
        End If
        e.DisplayText = (row("LeaveCode").ToString & ("  " & row("LeaveDescription").ToString) & ((" - " & row("LeaveLeft").ToString)))
    End Sub
    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click
        Dim PAYCODE As String = LookUpEdit1.EditValue.ToString.Trim
        Dim FromDate As DateTime = DateEdit1.DateTime
        Dim ToDate As DateTime = DateEdit2.DateTime
        Dim ApproveDate As DateTime = DateEdit3.DateTime
        Dim voucherNo As String = lblVoucher.Text
        Dim LeaveCode As String = GridLookUpEditLeave.EditValue.ToString.Trim
        Dim appliedfor As String
        Dim halfttpe As String = ""
        Dim days As Double
        Dim reason As String = TextEdit1.Text.Trim


        Dim LeaveLedgerYear As Integer = DateEdit1.DateTime.Year
        If Common.gLeaveFinancialYear = "Y" Then
            If DateEdit1.DateTime.Month < 4 Then
                LeaveLedgerYear = DateEdit1.DateTime.Year - 1
            End If
            If DateEdit1.DateTime.Month <= 3 And DateEdit2.DateTime.Month > 3 Then
                XtraMessageBox.Show(ulf, "<size=10>Leave Can be applied for Same Financial year only</size>", "<size=10>Error</size>")
                Exit Sub
            End If
        End If



        If ComboCmpAgn.Visible = True Then
            If ComboCmpAgn.EditValue.ToString.Trim = "NA" Then
                XtraMessageBox.Show(ulf, "<size=10>Comp Off Not Available</size>", "<size=10>Error</size>")
                Exit Sub
            End If
        End If
        If Common.IsNepali = "Y" Then
            Dim DC As New DateConverter()
            Try
                'DateEdit1.DateTime = DC.ToAD(New Date(ComboNepaliYearFrm.EditValue, ComboNEpaliMonthFrm.SelectedIndex + 1, ComboNepaliDateFrm.EditValue))
                DateEdit1.DateTime = DC.ToAD(ComboNepaliYearFrm.EditValue & "-" & ComboNEpaliMonthFrm.SelectedIndex + 1 & "-" & ComboNepaliDateFrm.EditValue)
            Catch ex As Exception
                XtraMessageBox.Show(ulf, "<size=10>Invalid From Date</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                ComboNepaliDateFrm.Select()
                Exit Sub
            End Try

            Try
                'DateEdit2.DateTime = DC.ToAD(New Date(ComboNepaliYearTo.EditValue, ComboNEpaliMonthTo.SelectedIndex + 1, ComboNepaliDateTo.EditValue))
                DateEdit2.DateTime = DC.ToAD(ComboNepaliYearTo.EditValue & "-" & ComboNEpaliMonthTo.SelectedIndex + 1 & "-" & ComboNepaliDateTo.EditValue)
            Catch ex As Exception
                XtraMessageBox.Show(ulf, "<size=10>Invalid To Date</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                ComboNepaliDateTo.Select()
                Exit Sub
            End Try

            Try
                DateEdit3.DateTime = DC.ToAD(ComboNepaliYearApp.EditValue & "-" & ComboNEpaliMonthApp.SelectedIndex + 1 & "-" & ComboNepaliDateApp.EditValue)
            Catch ex As Exception
                XtraMessageBox.Show(ulf, "<size=10>Invalid Approve Date</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                ComboNepaliDateApp.Select()
                Exit Sub
            End Try
        End If

        Dim adap, adap1, adap2, adap3, adap4 As SqlDataAdapter
        Dim adapA, adapA1, adapA2, adapA3, adapA4 As OleDbDataAdapter
        Dim ds, ds1, WOHLD, SelSts, Quota As DataSet
        ds = New DataSet
        Dim leaveFieldSql As String = "select LEAVEFIELD, LEAVETYPE, ISOFFINCLUDE, ISHOLIDAYINCLUDE, ISLEAVEACCRUAL, FIXED, SMIN, SMAX,isCompOffType, CheckPreFix, CheckPostFix from tblLeaveMaster where LEAVECODE='" & GridLookUpEditLeave.EditValue & "'"
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(leaveFieldSql, Common.con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(leaveFieldSql, Common.con)
            adap.Fill(ds)
        End If
        Dim CheckPreFix As String = ds.Tables(0).Rows(0).Item("CheckPreFix").ToString.Trim
        Dim CheckPostFix As String = ds.Tables(0).Rows(0).Item("CheckPostFix").ToString.Trim
        Dim sSqlL As String
        If CheckPreFix = "Y" Then
            sSqlL = "select paycode from tblTimeRegister where DateOFFICE ='" & FromDate.AddDays(-1).ToString("yyyy-MM-dd") & "' and PAYCODE ='" & PAYCODE & "' and LEAVEVALUE=1 and LEAVECODE <> '" & LeaveCode & "'"
            ds1 = New DataSet
            If Common.servername = "Access" Then
                sSqlL = sSqlL.Replace("DateOFFICE", " FORMAT(DateOFFICE,'yyyy-MM-dd')")
                adapA = New OleDbDataAdapter(sSqlL, Common.con1)
                adapA.Fill(ds1)
            Else
                adap = New SqlDataAdapter(sSqlL, Common.con)
                adap.Fill(ds1)
            End If
            If ds1.Tables(0).Rows.Count > 0 Then
                XtraMessageBox.Show(ulf, "<size=10>Prefix to this Leave Not Allowed.</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If
        End If
        If CheckPostFix = "Y" Then
            sSqlL = "select paycode from tblTimeRegister where DateOFFICE ='" & ToDate.AddDays(1).ToString("yyyy-MM-dd") & "' and PAYCODE ='" & PAYCODE & "' and LEAVEVALUE=1 and LEAVECODE <> '" & LeaveCode & "'"
            ds1 = New DataSet
            If Common.servername = "Access" Then
                sSqlL = sSqlL.Replace("DateOFFICE", " FORMAT(DateOFFICE,'yyyy-MM-dd')")
                adapA = New OleDbDataAdapter(sSqlL, Common.con1)
                adapA.Fill(ds1)
            Else
                adap = New SqlDataAdapter(sSqlL, Common.con)
                adap.Fill(ds1)
            End If
            If ds1.Tables(0).Rows.Count > 0 Then
                XtraMessageBox.Show(ulf, "<size=10>Postfix to this Leave Not Allowed.</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If
        End If
        Dim isCompOffType As String = ds.Tables(0).Rows(0).Item("isCompOffType").ToString.Trim
        Dim status As String
        Dim LEAVEVALUE As Double
        Dim ABSENTVALUE As Double
        Dim LEAVEFIELD As String = ds.Tables(0).Rows(0).Item("LEAVEFIELD").ToString.Trim
        Dim LEAVETYPE As String = ds.Tables(0).Rows(0).Item("LEAVETYPE").ToString.Trim
        Dim LEAVETYPE1 As String = Nothing
        Dim LEAVETYPE2 As String = Nothing

        Dim LEAVEAMOUNT1 As Double = 0
        Dim LEAVEAMOUNT2 As Double = 0
        Dim LEAVEAPRDate As DateTime = DateEdit3.DateTime.ToString("yyyy-MM-dd 00:00:00")
        Dim LUNCHSTARTTIME As DateTime
        Dim LUNCHENDTIME As DateTime
        Dim SHIFTSTARTTIME As DateTime
        Dim SHIFTENDTIME As DateTime
        Dim FIRSTHALFLEAVECODE As String = Nothing
        Dim SECONDHALFLEAVECODE As String = Nothing
        Dim VOUCHER_NO As String = lblVoucher.Text

        If CheckEdit1.Checked = True Then
            appliedfor = "Quater"
            days = 0.25
            status = "Q_" & GridLookUpEditLeave.EditValue.ToString.Trim
            LEAVEVALUE = 0.25
            ABSENTVALUE = 0.75
        ElseIf CheckEdit2.Checked = True Then
            appliedfor = "Half"
            If CheckEdit5.Checked Then
                halfttpe = "First Half"
                LEAVETYPE1 = ds.Tables(0).Rows(0).Item("LEAVETYPE").ToString.Trim
                LEAVEAMOUNT1 = 0.5
                FIRSTHALFLEAVECODE = GridLookUpEditLeave.EditValue.ToString.Trim
            Else
                halfttpe = "Second Half"
                LEAVETYPE2 = ds.Tables(0).Rows(0).Item("LEAVETYPE").ToString.Trim
                LEAVEAMOUNT2 = 0.5
                SECONDHALFLEAVECODE = GridLookUpEditLeave.EditValue.ToString.Trim
            End If
            days = 0.5
            status = "H_" & GridLookUpEditLeave.EditValue.ToString.Trim
            LEAVEVALUE = 0.5
            ABSENTVALUE = 0.5
        ElseIf CheckEdit3.Checked = True Then
            appliedfor = "Three Forth"
            days = 0.75
            status = "T_" & GridLookUpEditLeave.EditValue.ToString.Trim
            LEAVEVALUE = 0.75
            ABSENTVALUE = 0.25
        ElseIf CheckEdit4.Checked = True Then
            appliedfor = "Full Day"
            days = 1
            status = GridLookUpEditLeave.EditValue.ToString.Trim
            LEAVEVALUE = 1
            ABSENTVALUE = 0
        End If
        Dim datediff As TimeSpan = DateEdit2.DateTime.Subtract(DateEdit1.DateTime)
        Dim LEAVEAMOUNT As Double = days
        Dim WOHLDAlowdSql As String = "SELECT ISOFFINCLUDE, ISHOLIDAYINCLUDE from tblLeaveMaster where LEAVECODE = '" & GridLookUpEditLeave.EditValue.ToString.Trim & "'"
        WOHLD = New DataSet
        If Common.servername = "Access" Then
            adapA2 = New OleDbDataAdapter(WOHLDAlowdSql, Common.con1)
            adapA2.Fill(WOHLD)
        Else
            adap2 = New SqlDataAdapter(WOHLDAlowdSql, Common.con)
            adap2.Fill(WOHLD)
        End If
        Dim WO As Integer = 0
        Dim HLD As Integer = 0
        Dim SelectStatus As String
        Dim tmpDate As DateTime
        For x As Integer = 0 To datediff.Days
            tmpDate = FromDate.AddDays(x).ToString("yyyy-MM-dd 00:00:00")
            SelectStatus = "select STATUS from tblTimeRegister where PAYCODE='" & PAYCODE & "' and DateOFFICE='" & tmpDate.ToString("yyyy-MM-dd 00:00:00") & "' "
            SelSts = New DataSet
            If Common.servername = "Access" Then
                SelectStatus = "select STATUS from tblTimeRegister where PAYCODE='" & PAYCODE & "' and FORMAT(DateOFFICE, 'yyyy-MM-dd 00:00:00') ='" & tmpDate.ToString("yyyy-MM-dd 00:00:00") & "' "
                adapA3 = New OleDbDataAdapter(SelectStatus, Common.con1)
                adapA3.Fill(SelSts)
            Else
                adap3 = New SqlDataAdapter(SelectStatus, Common.con)
                adap3.Fill(SelSts)
            End If
            If SelSts.Tables(0).Rows.Count = 0 Then
                XtraMessageBox.Show(ulf, "<size=10>No Roster present for employee." & vbCrLf & "Create Roster or leave apply date is less than date of joining of employee.</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                setDefault()
                Exit Sub
            End If
            If SelSts.Tables(0).Rows(0).Item("STATUS").ToString.Trim = "WO" Then
                If WOHLD.Tables(0).Rows(0).Item("ISOFFINCLUDE").ToString = "N" Then
                    WO = WO + 1
                End If
            ElseIf SelSts.Tables(0).Rows(0).Item("STATUS").ToString.Trim = "HLD" Then
                If WOHLD.Tables(0).Rows(0).Item("ISHOLIDAYINCLUDE").ToString = "N" Then
                    HLD = HLD + 1
                End If
            End If
        Next

        Dim totaldays As Double = (datediff.Days - WO - HLD + 1) * days

        If ds.Tables(0).Rows(0).Item("ISLEAVEACCRUAL").ToString.Trim = "Y" And ds.Tables(0).Rows(0).Item("FIXED").ToString.Trim = "Y" Then
            If totaldays < ds.Tables(0).Rows(0).Item("SMIN").ToString.Trim Or totaldays > ds.Tables(0).Rows(0).Item("SMAX").ToString.Trim Then
                XtraMessageBox.Show(ulf, "<size=10>Total days is not in range of Leave accrual limites</size>")
                Exit Sub
            End If
        End If

        Dim findQuotaSql As String = "SELECT " & LEAVEFIELD & "_ADD - " & LEAVEFIELD & " from tblLeaveLedger where PAYCODE='" & PAYCODE & "' and LYEAR = " & LeaveLedgerYear
        Quota = New DataSet
        If Common.servername = "Access" Then
            adapA4 = New OleDbDataAdapter(findQuotaSql, Common.con1)
            adapA4.Fill(Quota)
        Else
            adap4 = New SqlDataAdapter(findQuotaSql, Common.con)
            adap4.Fill(Quota)
        End If
        'MsgBox(findQuotaSql)
        'MsgBox(Quota.Tables(0).Rows(0).Item(0))
        If isCompOffType <> "Y" Then
            If totaldays > Quota.Tables(0).Rows(0).Item(0) Then
                'XtraMessageBox.Show(ulf, "<size=10>Insufficient leave balance</size>", "<size=9>Error</size>")
                'setDefault()
                'Exit Sub
                If XtraMessageBox.Show(ulf, "<size=10>Insufficient leave balance " & vbCrLf & " Still want to continue</size>", "<size=9>Confirm</size>", _
                               MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then
                    'setdefault()
                    Exit Sub
                End If
            End If
        End If
        'MsgBox(datediff.Days)
        Dim InsertinleaveApp As String
        Dim updatetblregiter As String
        If Common.servername = "Access" Then
            If Common.con1.State <> ConnectionState.Open Then
                Common.con1.Open()
            End If
        Else
            If Common.con.State <> ConnectionState.Open Then
                Common.con.Open()
            End If
        End If
        Dim DateOFFICE As DateTime
        Dim FindShiftSql As String
        Dim i As Integer = 0
        For i = 0 To datediff.Days
            DateOFFICE = FromDate.AddDays(i).ToString("yyyy-MM-dd 00:00:00")
            ds1 = New DataSet
            FindShiftSql = "select tblTimeRegister.SHIFT, (select STARTTIME from tblShiftMaster where tblShiftMaster.SHIFT = tblTimeRegister.SHIFT) as shiftstarttime, " & _
                        "(select ENDTIME from tblShiftMaster where tblShiftMaster.SHIFT = tblTimeRegister.SHIFT) as shiftendtime, " & _
                        "(select LUNCHTIME from tblShiftMaster where tblShiftMaster.SHIFT = tblTimeRegister.SHIFT) as lunchstarttime, " & _
                        "(select LUNCHENDTIME from tblShiftMaster where tblShiftMaster.SHIFT = tblTimeRegister.SHIFT) as lunchendtime, " & _
                        "STATUS from tblTimeRegister where PAYCODE='" & PAYCODE & "' and DateOFFICE='" & DateOFFICE.ToString("yyyy-MM-dd 00:00:00") & "' "
            If Common.servername = "Access" Then
                FindShiftSql = "select tblTimeRegister.SHIFT, (select STARTTIME from tblShiftMaster where tblShiftMaster.SHIFT = tblTimeRegister.SHIFT) as shiftstarttime, " & _
                       "(select ENDTIME from tblShiftMaster where tblShiftMaster.SHIFT = tblTimeRegister.SHIFT) as shiftendtime, " & _
                       "(select LUNCHTIME from tblShiftMaster where tblShiftMaster.SHIFT = tblTimeRegister.SHIFT) as lunchstarttime, " & _
                       "(select LUNCHENDTIME from tblShiftMaster where tblShiftMaster.SHIFT = tblTimeRegister.SHIFT) as lunchendtime, " & _
                       "STATUS from tblTimeRegister where PAYCODE='" & PAYCODE & "' and FORMAT(DateOFFICE, 'yyyy-MM-dd 00:00:00')='" & DateOFFICE.ToString("yyyy-MM-dd 00:00:00") & "' "
                adapA1 = New OleDbDataAdapter(FindShiftSql, Common.con1)
                adapA1.Fill(ds1)
            Else
                adap1 = New SqlDataAdapter(FindShiftSql, Common.con)
                adap1.Fill(ds1)
            End If
            'MsgBox(ds1.Tables(0).Rows(0).Item("STATUS").ToString & " , " & ds1.Tables(0).Rows(0).Item("STATUS").ToString.Length)
            If ds1.Tables(0).Rows(0).Item("STATUS").ToString.Trim = "WO" Or ds1.Tables(0).Rows(0).Item("STATUS").ToString.Trim = "HLD" Then
                If ds.Tables(0).Rows(0).Item("ISOFFINCLUDE").ToString.Trim = "Y" Then
                    updatetblregiter = "update tblTimeRegister set  STATUS='" & status & "', " & _
                  "LEAVETYPE='" & LEAVETYPE & "', LEAVETYPE1='" & LEAVETYPE1 & "', LEAVETYPE2='" & LEAVETYPE2 & "', REASON='" & reason & "', LEAVEVALUE=" & LEAVEVALUE & ", ABSENTVALUE=" & ABSENTVALUE & ", LEAVEAMOUNT=" & LEAVEAMOUNT & ", LEAVEAMOUNT1=" & LEAVEAMOUNT1 & ", " & _
                  "LEAVEAMOUNT2=" & LEAVEAMOUNT2 & ", LEAVECODE='" & LeaveCode & "', LEAVEAPRDate='" & LEAVEAPRDate.ToString("yyyy-MM-dd HH:mm:ss") & "', VOUCHER_NO = '" & VOUCHER_NO & "', FIRSTHALFLEAVECODE = '" & FIRSTHALFLEAVECODE & "', SECONDHALFLEAVECODE='" & SECONDHALFLEAVECODE & "' " & _
                  "where PAYCODE='" & PAYCODE & "' and "
                    GoTo tmp
                End If
                If ds.Tables(0).Rows(0).Item("ISHOLIDAYINCLUDE").ToString.Trim = "Y" Then
                    updatetblregiter = "update tblTimeRegister set  STATUS='" & status & "', " & _
               "LEAVETYPE='" & LEAVETYPE & "', LEAVETYPE1='" & LEAVETYPE1 & "', LEAVETYPE2='" & LEAVETYPE2 & "', REASON='" & reason & "', LEAVEVALUE=" & LEAVEVALUE & ", ABSENTVALUE=" & ABSENTVALUE & ", LEAVEAMOUNT=" & LEAVEAMOUNT & ", LEAVEAMOUNT1=" & LEAVEAMOUNT1 & ", " & _
               "LEAVEAMOUNT2=" & LEAVEAMOUNT2 & ", LEAVECODE='" & LeaveCode & "', LEAVEAPRDate='" & LEAVEAPRDate.ToString("yyyy-MM-dd HH:mm:ss") & "', VOUCHER_NO = '" & VOUCHER_NO & "', FIRSTHALFLEAVECODE = '" & FIRSTHALFLEAVECODE & "', SECONDHALFLEAVECODE='" & SECONDHALFLEAVECODE & "' " & _
               "where PAYCODE='" & PAYCODE & "' and "
                    GoTo tmp
                End If
                XtraMessageBox.Show(ulf, "<size=10>WO or Holiday assigned on date " & DateOFFICE.ToString("dd MMM yyyy") & "</size>")
                Continue For
            End If
            'MsgBox(ds1.Tables(0).Rows(0).Item("shiftstarttime").ToString)
            'MsgBox(Convert.ToDateTime(ds1.Tables(0).Rows(0).Item("shiftstarttime").ToString).ToString("HH:mm:ss"))
            Dim SHIFTSTARTTIMETemp As String
            Dim SHIFTENDTIMETemp As String
            If ds1.Tables(0).Rows(0).Item("shiftstarttime").ToString.Trim = "" Then
                SHIFTSTARTTIMETemp = "00:00:00"
            Else
                SHIFTSTARTTIMETemp = Convert.ToDateTime(ds1.Tables(0).Rows(0).Item("shiftstarttime").ToString.Trim).ToString("HH:mm:ss")
            End If

            If ds1.Tables(0).Rows(0).Item("shiftendtime").ToString.Trim = "" Then
                SHIFTENDTIMETemp = "00:00:00"
            Else
                SHIFTENDTIMETemp = Convert.ToDateTime(ds1.Tables(0).Rows(0).Item("shiftendtime").ToString.Trim).ToString("HH:mm:ss")
            End If
            'SHIFTSTARTTIME = Convert.ToDateTime(DateOFFICE.ToString("yyyy-MM-dd") & " " & Convert.ToDateTime(ds1.Tables(0).Rows(0).Item("shiftstarttime").ToString).ToString("HH:mm:ss"))
            'SHIFTENDTIME = Convert.ToDateTime(DateOFFICE.ToString("yyyy-MM-dd") & " " & Convert.ToDateTime(ds1.Tables(0).Rows(0).Item("shiftendtime").ToString).ToString("HH:mm:ss"))
            SHIFTSTARTTIME = Convert.ToDateTime(DateOFFICE.ToString("yyyy-MM-dd") & " " & SHIFTSTARTTIMETemp)
            SHIFTENDTIME = Convert.ToDateTime(DateOFFICE.ToString("yyyy-MM-dd") & " " & SHIFTENDTIMETemp)

            Dim LUNCHSTARTTIMETemp As String
            If ds1.Tables(0).Rows(0).Item("lunchstarttime").ToString.Trim = "" Then
                LUNCHSTARTTIMETemp = "00:00:00"
            Else
                LUNCHSTARTTIMETemp = Convert.ToDateTime(ds1.Tables(0).Rows(0).Item("lunchstarttime").ToString.Trim).ToString("HH:mm:ss")
            End If
            Dim LUNCHENDTIMETemp As String
            If ds1.Tables(0).Rows(0).Item("lunchendtime").ToString.Trim = "" Then
                LUNCHENDTIMETemp = "00:00:00"
            Else
                LUNCHENDTIMETemp = Convert.ToDateTime(ds1.Tables(0).Rows(0).Item("lunchendtime").ToString.Trim).ToString("HH:mm:ss")
            End If
            'LUNCHSTARTTIME = Convert.ToDateTime(DateOFFICE.ToString("yyyy-MM-dd") & " " & Convert.ToDateTime(ds1.Tables(0).Rows(0).Item("lunchstarttime").ToString).ToString("HH:mm:ss"))
            'LUNCHENDTIME = Convert.ToDateTime(DateOFFICE.ToString("yyyy-MM-dd") & " " & Convert.ToDateTime(ds1.Tables(0).Rows(0).Item("lunchendtime").ToString).ToString("HH:mm:ss"))
            LUNCHSTARTTIME = Convert.ToDateTime(DateOFFICE.ToString("yyyy-MM-dd") & " " & LUNCHSTARTTIMETemp)
            LUNCHENDTIME = Convert.ToDateTime(DateOFFICE.ToString("yyyy-MM-dd") & " " & LUNCHENDTIMETemp)

            'updatetblregiter = "update tblTimeRegister set SHIFTSTARTTIME = '" & SHIFTSTARTTIME.ToString("yyyy-MM-dd HH:mm:ss") & "', SHIFTENDTIME ='" & SHIFTENDTIME.ToString("yyyy-MM-dd HH:mm:ss") & "', LUNCHSTARTTIME='" & LUNCHSTARTTIME.ToString("yyyy-MM-dd HH:mm:ss") & "', LUNCHENDTIME='" & LUNCHENDTIME.ToString("yyyy-MM-dd HH:mm:ss") & "', STATUS='" & status & "', " & _
            '        "LEAVETYPE='" & LEAVETYPE & "', LEAVETYPE1='" & LEAVETYPE1 & "', LEAVETYPE2='" & LEAVETYPE2 & "', REASON='" & reason & "', LEAVEVALUE=" & LEAVEVALUE & ", ABSENTVALUE=" & ABSENTVALUE & ", LEAVEAMOUNT=" & LEAVEAMOUNT & ", LEAVEAMOUNT1=" & LEAVEAMOUNT1 & ", " & _
            '        "LEAVEAMOUNT2=" & LEAVEAMOUNT2 & ", LEAVECODE='" & LeaveCode & "', LEAVEAPRDate='" & LEAVEAPRDate.ToString("yyyy-MM-dd HH:mm:ss") & "', VOUCHER_NO = '" & VOUCHER_NO & "', FIRSTHALFLEAVECODE = '" & FIRSTHALFLEAVECODE & "', SECONDHALFLEAVECODE='" & SECONDHALFLEAVECODE & "' " & _
            '        "where PAYCODE='" & PAYCODE & "' and DateOFFICE ='" & DateOFFICE.ToString("yyyy-MM-dd HH:mm:ss") & "'"

            updatetblregiter = "update tblTimeRegister set SHIFTSTARTTIME = '" & SHIFTSTARTTIME.ToString("yyyy-MM-dd HH:mm:ss") & "', SHIFTENDTIME ='" & SHIFTENDTIME.ToString("yyyy-MM-dd HH:mm:ss") & "', LUNCHSTARTTIME='" & LUNCHSTARTTIME.ToString("yyyy-MM-dd HH:mm:ss") & "', LUNCHENDTIME='" & LUNCHENDTIME.ToString("yyyy-MM-dd HH:mm:ss") & "', STATUS='" & status & "', " & _
                   "LEAVETYPE='" & LEAVETYPE & "', LEAVETYPE1='" & LEAVETYPE1 & "', LEAVETYPE2='" & LEAVETYPE2 & "', REASON='" & reason & "', LEAVEVALUE=" & LEAVEVALUE & ", ABSENTVALUE=" & ABSENTVALUE & ", LEAVEAMOUNT=" & LEAVEAMOUNT & ", LEAVEAMOUNT1=" & LEAVEAMOUNT1 & ", " & _
                   "LEAVEAMOUNT2=" & LEAVEAMOUNT2 & ", LEAVECODE='" & LeaveCode & "', LEAVEAPRDate='" & LEAVEAPRDate.ToString("yyyy-MM-dd HH:mm:ss") & "', VOUCHER_NO = '" & VOUCHER_NO & "', FIRSTHALFLEAVECODE = '" & FIRSTHALFLEAVECODE & "', SECONDHALFLEAVECODE='" & SECONDHALFLEAVECODE & "' " & _
                   "where PAYCODE='" & PAYCODE & "' and "

tmp:        If isCompOffType = "Y" Then
                Dim COFUsedAgainst As DateTime = Convert.ToDateTime(ComboCmpAgn.EditValue.ToString.Substring(0, 10) & " 00:00:00")
                InsertinleaveApp = "insert INTO LeaveApplication (VOUCHER_NO, PayCode, LeaveCode, ForDate, Days, AppliedFor, HalfType, Reason, ApprovedDate, LastModifiedBy, LastModifiedDate, COFUsedAgainst) VALUES " & _
                  "('" & VOUCHER_NO & "','" & PAYCODE & "','" & LeaveCode & "','" & DateOFFICE.ToString("yyyy-MM-dd HH:mm:ss") & "'," & days & ",'" & appliedfor & "','" & halfttpe & "','" & reason & "','" & ApproveDate.ToString("yyyy-MM-dd HH:mm:ss") & "','admin','" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "','" & COFUsedAgainst.ToString("yyyy-MM-dd HH:mm:ss") & "')"
            Else
                InsertinleaveApp = "insert INTO LeaveApplication (VOUCHER_NO, PayCode, LeaveCode, ForDate, Days, AppliedFor, HalfType, Reason, ApprovedDate, LastModifiedBy, LastModifiedDate) VALUES " & _
                  "('" & VOUCHER_NO & "','" & PAYCODE & "','" & LeaveCode & "','" & DateOFFICE.ToString("yyyy-MM-dd HH:mm:ss") & "'," & days & ",'" & appliedfor & "','" & halfttpe & "','" & reason & "','" & ApproveDate.ToString("yyyy-MM-dd HH:mm:ss") & "','admin','" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "')"
            End If
            If Common.servername = "Access" Then
                updatetblregiter = updatetblregiter & "FORMAT(DateOFFICE, 'yyyy-MM-dd HH:mm:ss') ='" & DateOFFICE.ToString("yyyy-MM-dd HH:mm:ss") & "'"

                cmd1 = New OleDbCommand(updatetblregiter, Common.con1)
                cmd1.ExecuteNonQuery()

                cmd1 = New OleDbCommand(InsertinleaveApp, Common.con1)
                cmd1.ExecuteNonQuery()
            Else
                updatetblregiter = updatetblregiter & "DateOFFICE ='" & DateOFFICE.ToString("yyyy-MM-dd HH:mm:ss") & "'"
                cmd = New SqlCommand(updatetblregiter, Common.con)
                cmd.ExecuteNonQuery()

                cmd = New SqlCommand(InsertinleaveApp, Common.con)
                cmd.ExecuteNonQuery()
            End If
            Common.LogPost("Leave Apply; Paycode:" & PAYCODE & ", For Date:" & DateOFFICE.ToString("yyyy-MM-dd HH:mm:ss"))
            If ComboCmpAgn.Visible = True And ComboCmpAgn.EditValue <> "NA" Then
                Dim sSqlX As String
                Dim sSqlY As String
                Dim xDate As Date = Convert.ToDateTime(ComboCmpAgn.EditValue.ToString.Substring(0, 10) & " 00:00:00")
                If Common.servername = "Access" Then
                    sSqlX = "update tblTimeRegister set COFUsed='Y' where PAYCODE='" & PAYCODE & "' and FORMAT(DateOFFICE, 'yyyy-MM-dd HH:mm:ss') ='" & xDate.ToString("yyyy-MM-dd HH:mm:ss") & "'"
                    sSqlY = "update tblTimeRegister set COFUsedAgainst='" & xDate.ToString("yyyy-MM-dd HH:mm:ss") & "' where PAYCODE='" & PAYCODE & "' and FORMAT(DateOFFICE, 'yyyy-MM-dd HH:mm:ss') ='" & DateOFFICE.ToString("yyyy-MM-dd HH:mm:ss") & "'"
                    cmd1 = New OleDbCommand(sSqlX, Common.con1)
                    cmd1.ExecuteNonQuery()
                Else
                    sSqlX = "update tblTimeRegister set COFUsed='Y' where PAYCODE='" & PAYCODE & "' and DateOFFICE ='" & xDate.ToString("yyyy-MM-dd HH:mm:ss") & "'"
                    sSqlY = "update tblTimeRegister set COFUsedAgainst='" & xDate.ToString("yyyy-MM-dd HH:mm:ss") & "' where PAYCODE='" & PAYCODE & "' and DateOFFICE ='" & DateOFFICE.ToString("yyyy-MM-dd HH:mm:ss") & "'"
                    cmd = New SqlCommand(sSqlX, Common.con)
                    cmd.ExecuteNonQuery()
                End If
            End If
        Next
        'Dim x As Double = i * days
        Dim updateLeaveLedger As String = "update tblLeaveLedger set " & LEAVEFIELD & " = " & LEAVEFIELD & " + " & totaldays & "  where PAYCODE = '" & LookUpEdit1.EditValue.ToString.Trim & "' and LYEAR = " & LeaveLedgerYear & ""
        'MsgBox("i = " & i & vbCrLf & "days = " & days & vbCrLf & "x = " & x & vbCrLf & updateLeaveLedger)

        If Common.servername = "Access" Then
            cmd1 = New OleDbCommand(updateLeaveLedger, Common.con1)
            cmd1.ExecuteNonQuery()
        Else
            cmd = New SqlCommand(updateLeaveLedger, Common.con)
            cmd.ExecuteNonQuery()
        End If


        If Common.servername = "Access" Then
            Common.con1.Close()
        Else
            Common.con.Close()
        End If

        Dim comclass As Common = New Common
        Dim sSql As String = "select tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK,EmployeeGroup.ID from tblEmployeeShiftMaster, TblEmployee, EmployeeGroup where tblEmployeeShiftMaster.PAYCODE='" & LookUpEdit1.EditValue.ToString.Trim & "' and tblEmployeeShiftMaster.PAYCODE= TblEmployee.PAYCODE and TblEmployee.EmployeeGroupId = EmployeeGroup.GroupId"
        '"select ISROUNDTHECLOCKWORK from tblEmployeeShiftMaster where PAYCODE = '" & LookUpEdit1.EditValue.ToString.Trim & "'"
        ds = New DataSet
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(ds)
        End If
        If ds.Tables(0).Rows(0).Item("ISROUNDTHECLOCKWORK").ToString = "Y" Then
            comclass.Process_AllRTC(DateEdit1.DateTime.AddDays(-1), Now.Date, LookUpEdit1.EditValue.ToString.Trim, LookUpEdit1.EditValue.ToString.Trim, ds.Tables(0).Rows(0).Item("Id"))
        Else
            comclass.Process_AllnonRTC(DateEdit1.DateTime.AddDays(-1), DateEdit2.DateTime, LookUpEdit1.EditValue.ToString.Trim, LookUpEdit1.EditValue.ToString.Trim, ds.Tables(0).Rows(0).Item("Id"))
            If Common.EmpGrpArr(ds.Tables(0).Rows(0).Item("Id")).SHIFTTYPE = "M" Then
                comclass.Process_AllnonRTCMulti(DateEdit1.DateTime.AddDays(-1), DateEdit2.DateTime, LookUpEdit1.EditValue.ToString.Trim, LookUpEdit1.EditValue.ToString.Trim, ds.Tables(0).Rows(0).Item("Id"))
            End If
        End If
        XtraMasterTest.LabelControlStatus.Text = ""
        Application.DoEvents()
        XtraMessageBox.Show(ulf, "<size=10> Saved Successfully </size> ", "<size=9> Success </size>")
        setDefault()
        'MsgBox(GridLookUpEdit1.EditValue)
    End Sub
    Private Sub DateEdit1_Leave(sender As System.Object, e As System.EventArgs) Handles DateEdit1.Leave
        'MsgBox(DateEdit1.DateTime.ToString)
        If DateEdit1.DateTime.ToString("yyyy-MM-dd") = "0001-01-01" Then
            DateEdit1.EditValue = Now
        End If
        DateEdit2.EditValue = DateEdit1.EditValue
    End Sub
    Private Sub SimpleButton2_Click(sender As System.Object, e As System.EventArgs)
        'Common.XtraMsgBox(Me)
        'DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.Show(ulf, "<size=10>message</size>", DefaultBoolean.Default)
    End Sub
    Private Sub LookUpEdit1_Leave(sender As System.Object, e As System.EventArgs) Handles LookUpEdit1.Leave
        If Not leaveFlage Then Return
        leaveFlage = False
        Dim adap, adap1 As SqlDataAdapter
        Dim adapA, adapA1 As OleDbDataAdapter
        Dim ds As DataSet = New DataSet
        Dim ds1 As DataSet
        Dim sSql As String = "select PAYCODE from TblEmployee where PAYCODE = '" & LookUpEdit1.EditValue.ToString.Trim & "'"
        If LookUpEdit1.EditValue.ToString.Trim <> "" Then
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
            If ds.Tables(0).Rows.Count = 0 Then
                If LookUpEdit1.EditValue.ToString.Trim <> "" Then
                    XtraMessageBox.Show(ulf, "<size=10>No such Employee</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    LookUpEdit1.Select()
                    setDefault()
                    leaveFlage = True
                    Exit Sub
                End If
            Else
                Dim sSql1 As String = "select EMPNAME, PRESENTCARDNO, (select GroupName from EmployeeGroup where EmployeeGroup.GroupId = TblEmployee.EmployeeGroupId) as EmpGrp, (select DEPARTMENTNAME from tblDepartment where tblDepartment.DEPARTMENTCODE = TblEmployee.DEPARTMENTCODE) as DEPName, (select CATAGORYNAME from tblCatagory WHERE tblCatagory.CAT = TblEmployee.CAT) as CATName, DESIGNATION  from TblEmployee where PAYCODE = '" & LookUpEdit1.EditValue.ToString.Trim & "'"
                sSql1 = "select EMPNAME, PRESENTCARDNO, " & _
                       "(SELECT COMPANYNAME from tblCompany WHERE tblCompany.COMPANYCODE = TblEmployee.COMPANYCODE) as CompName, " & _
                       "(select GroupName from EmployeeGroup where EmployeeGroup.GroupId = TblEmployee.EmployeeGroupId) as EmpGrp," & _
                       "(select DEPARTMENTNAME from tblDepartment where tblDepartment.DEPARTMENTCODE = TblEmployee.DEPARTMENTCODE) as DEPName, " & _
                       "(select CATAGORYNAME from tblCatagory WHERE tblCatagory.CAT = TblEmployee.CAT) as CATName, " & _
                       "(select GradeName from tblGrade WHERE tblGrade.GradeCode = TblEmployee.GradeCode) as GrdName, DESIGNATION  from TblEmployee where PAYCODE = '" & LookUpEdit1.EditValue.ToString.Trim & "'"
                If Common.servername = "Access" Then
                    sSql1 = "select T.EMPNAME, T.PRESENTCARDNO, C.COMPANYNAME  as CompName,  T.DESIGNATION, E.GroupName as EmpGrp, D.DEPARTMENTNAME as DEPName, cat.CATAGORYNAME as CATName, grd.GradeName as GrdName  from TblEmployee T,  tblCompany C, EmployeeGroup E, tblDepartment D, tblCatagory cat, tblGrade grd where C.COMPANYCODE = T.COMPANYCODE  and E.GroupId = T.EmployeeGroupId and D.DEPARTMENTCODE = T.DEPARTMENTCODE and cat.CAT = T.CAT and grd.GradeCode = T.GradeCode and T.Paycode='" & LookUpEdit1.EditValue.ToString.Trim & "'"
                    adapA1 = New OleDbDataAdapter(sSql1, Common.con1)
                    ds1 = New DataSet
                    adapA1.Fill(ds1)
                Else
                    adap1 = New SqlDataAdapter(sSql1, Common.con)
                    ds1 = New DataSet
                    adap1.Fill(ds1)
                End If
                lblName.Text = ds1.Tables(0).Rows(0).Item("EMPNAME").ToString
                lblCardNum.Text = ds1.Tables(0).Rows(0).Item("PRESENTCARDNO").ToString
                lblComp.Text = ds1.Tables(0).Rows(0).Item("CompName").ToString
                lblCat.Text = ds1.Tables(0).Rows(0).Item("CATName").ToString
                lblDept.Text = ds1.Tables(0).Rows(0).Item("DEPName").ToString
                lblEmpGrp.Text = ds1.Tables(0).Rows(0).Item("EmpGrp").ToString
                lblDesi.Text = ds1.Tables(0).Rows(0).Item("DESIGNATION").ToString
                lblGrade.Text = ds1.Tables(0).Rows(0).Item("GrdName").ToString
                LoadLeaveHistoryGrid()
                LoadLeaveQuota(LookUpEdit1.EditValue.ToString.Trim)
                SimpleButton1.Enabled = True
            End If
        End If
        leaveFlage = True
    End Sub
    'Private Sub GridView1_RowDeleted(sender As System.Object, e As DevExpress.Data.RowDeletedEventArgs) Handles GridView1.RowDeleted
    '    Me.LeaveApplication1TableAdapter1.Update(Me.SSSDBDataSet.LeaveApplication1)
    '    Me.LeaveApplicationTableAdapter.Update(Me.SSSDBDataSet.LeaveApplication)
    'End Sub
    'Private Sub GridView1_RowUpdated(sender As System.Object, e As DevExpress.XtraGrid.Views.Base.RowObjectEventArgs) Handles GridView1.RowUpdated
    '    Me.LeaveApplication1TableAdapter1.Update(Me.SSSDBDataSet.LeaveApplication1)
    '    Me.LeaveApplicationTableAdapter.Update(Me.SSSDBDataSet.LeaveApplication)
    'End Sub
    Private Sub GridControl1_EmbeddedNavigator_ButtonClick(sender As System.Object, e As DevExpress.XtraEditors.NavigatorButtonClickEventArgs) Handles GridControl1.EmbeddedNavigator.ButtonClick
        If e.Button.ButtonType = DevExpress.XtraEditors.NavigatorButtonType.Remove Then
            If XtraMessageBox.Show(ulf, "<size=10>" & Common.res_man.GetString("askdelete", Common.cul) & "</size>", Common.res_man.GetString("confirmdeleteion", Common.cul), _
                              MessageBoxButtons.YesNo, MessageBoxIcon.Question) <> DialogResult.Yes Then
                Me.Validate()
                e.Handled = True
            Else
                Dim adap As SqlDataAdapter
                Dim adapA As OleDbDataAdapter
                Dim ds As DataSet = New DataSet
                Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
                Dim LeaveCode As String = row("LeaveCode").ToString.Trim
                Dim Days As String = row("Days").ToString.Trim
                Dim VOUCHER_NO As String = row("VOUCHER_NO").ToString.Trim
                Dim paycode As String = row("PAYCODE").ToString.Trim
                Dim ForDateYear As String = Convert.ToDateTime(row("ForDate").ToString.Trim).Year
                If Common.gLeaveFinancialYear = "Y" Then
                    If Convert.ToDateTime(row("ForDate").ToString.Trim).Month < 4 Then
                        ForDateYear = Convert.ToDateTime(row("ForDate").ToString.Trim).Year - 1
                    End If
                   
                End If

                Dim sSql As String = "select LEAVEFIELD from tblLeaveMaster where LEAVECODE = '" & LeaveCode & "'"
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(ds)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(ds)
                End If
                Dim sSql1 As String = "DELETE from LeaveApplication where VOUCHER_NO = '" & VOUCHER_NO & "' and PayCode='" & paycode & "' and LeaveCode='" & LeaveCode & "' and ForDate='" & Convert.ToDateTime(row("ForDate").ToString.Trim).ToString("yyyy-MM-dd 00:00:00") & "'"
                If ds.Tables(0).Rows.Count > 0 Then
                    sSql = "update tblLeaveLedger set " & ds.Tables(0).Rows(0).Item("LEAVEFIELD").ToString.Trim & " = " & ds.Tables(0).Rows(0).Item("LEAVEFIELD").ToString.Trim & " - " & Days & " WHERE PAYCODE = '" & LookUpEdit1.EditValue.ToString.Trim & "' and LYEAR = " & ForDateYear & ""
                    If Common.servername = "Access" Then
                        Common.con1.Open()
                        cmd1 = New OleDbCommand(sSql, Common.con1)
                        cmd1.ExecuteNonQuery()

                        cmd1 = New OleDbCommand(sSql1, Common.con1)
                        cmd1.ExecuteNonQuery()
                        Common.con1.Close()
                    Else
                        Common.con.Open()
                        cmd = New SqlCommand(sSql, Common.con)
                        cmd.ExecuteNonQuery()

                        cmd = New SqlCommand(sSql1, Common.con)
                        cmd.ExecuteNonQuery()
                        Common.con.Close()
                    End If
                    Common.LogPost("Leave Delete; Paycode:" & LookUpEdit1.EditValue.ToString.Trim & " For Date:" & Convert.ToDateTime(row("ForDate").ToString.Trim).ToString("yyyy-MM-dd HH:mm:ss"))
                End If

                Dim Leavecodetmp As String = ""
                Dim LEAVETYPE As String = ""
                Dim Voucher_Notmp As String = ""
                Dim LEAVETYPE1 As String = ""
                Dim LEAVETYPE2 As String = ""
                Dim FIRSTHALFLEAVECODE As String = ""
                Dim SECONDHALFLEAVECODE As String = ""
                Dim leaveamount = 0
                Dim leaveamount1 = 0
                Dim leaveamount2 = 0

                If Common.servername = "Access" Then
                    sSql = "update tblTimeRegister set  Leavecode='" & Leavecodetmp & "', LEAVETYPE= '" & LEAVETYPE & "', Voucher_No='" & Voucher_Notmp & "', LEAVETYPE1='" & LEAVETYPE1 & "', LEAVETYPE2='" & LEAVETYPE2 & "', FIRSTHALFLEAVECODE='" & FIRSTHALFLEAVECODE & "', SECONDHALFLEAVECODE='" & SECONDHALFLEAVECODE & "', leaveamount=" & leaveamount & ", leaveamount1 = " & leaveamount1 & ", leaveamount2=" & leaveamount2 & " where PAYCODE='" & paycode & "' and FORMAT(DateOFFICE,'yyyy-MM-dd HH:mm:ss')='" & Convert.ToDateTime(row("ForDate").ToString.Trim).ToString("yyyy-MM-dd HH:mm:ss") & "'"
                    If Common.con1.State <> ConnectionState.Open Then
                        Common.con1.Open()
                    End If
                    cmd1 = New OleDbCommand(sSql, Common.con1)
                    cmd1.ExecuteNonQuery()
                    If Common.con1.State <> ConnectionState.Closed Then
                        Common.con1.Close()
                    End If
                Else
                    sSql = "update tblTimeRegister set Leavecode='" & Leavecodetmp & "', LEAVETYPE= '" & LEAVETYPE & "', Voucher_No='" & Voucher_Notmp & "', LEAVETYPE1='" & LEAVETYPE1 & "', LEAVETYPE2='" & LEAVETYPE2 & "', FIRSTHALFLEAVECODE='" & FIRSTHALFLEAVECODE & "', SECONDHALFLEAVECODE='" & SECONDHALFLEAVECODE & "', leaveamount=" & leaveamount & ", leaveamount1 = " & leaveamount1 & ", leaveamount2=" & leaveamount2 & " where PAYCODE='" & paycode & "' and DateOFFICE='" & Convert.ToDateTime(row("ForDate").ToString.Trim).ToString("yyyy-MM-dd HH:mm:ss") & "'"
                    If Common.con.State <> ConnectionState.Open Then
                        Common.con.Open()
                    End If
                    cmd = New SqlCommand(sSql, Common.con)
                    cmd.ExecuteNonQuery()
                    If Common.con.State <> ConnectionState.Closed Then
                        Common.con.Close()
                    End If
                End If
                Dim COFUsedAgainst As String = row("COFUsedAgainst").ToString.Trim
                If COFUsedAgainst.Trim <> "" Then  ' update if compoff deleted 
                    COFUsedAgainst = Convert.ToDateTime(COFUsedAgainst).ToString("yyyy-MM-dd HH:mm:ss")
                    If Common.servername = "Access" Then
                        sSql = "update tblTimeRegister set  COFUsed='N' where PAYCODE='" & paycode & "' and FORMAT(DateOFFICE,'yyyy-MM-dd HH:mm:ss')='" & COFUsedAgainst & "'"
                        If Common.con1.State <> ConnectionState.Open Then
                            Common.con1.Open()
                        End If
                        cmd1 = New OleDbCommand(sSql, Common.con1)
                        cmd1.ExecuteNonQuery()
                        If Common.con1.State <> ConnectionState.Closed Then
                            Common.con1.Close()
                        End If
                    Else
                        sSql = "update tblTimeRegister set  COFUsed='N' where PAYCODE='" & paycode & "' and DateOFFICE='" & COFUsedAgainst & "'"
                        If Common.con.State <> ConnectionState.Open Then
                            Common.con.Open()
                        End If
                        cmd = New SqlCommand(sSql, Common.con)
                        cmd.ExecuteNonQuery()
                        If Common.con.State <> ConnectionState.Closed Then
                            Common.con.Close()
                        End If
                    End If
                End If



                sSql = "select tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK,EmployeeGroup.ID from tblEmployeeShiftMaster, TblEmployee, EmployeeGroup where tblEmployeeShiftMaster.PAYCODE='" & paycode & "' and tblEmployeeShiftMaster.PAYCODE= TblEmployee.PAYCODE and TblEmployee.EmployeeGroupId = EmployeeGroup.GroupId"
                '"select ISROUNDTHECLOCKWORK from tblEmployeeShiftMaster where PAYCODE = '" & LookUpEdit1.EditValue.ToString.Trim & "'"
                ds = New DataSet
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(ds)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(ds)
                End If
                Dim comclass As Common = New Common
                If ds.Tables(0).Rows(0).Item("ISROUNDTHECLOCKWORK").ToString = "Y" Then
                    comclass.Process_AllRTC(Convert.ToDateTime(row("ForDate").ToString.Trim).AddDays(-1), Now.Date, LookUpEdit1.EditValue.ToString.Trim, LookUpEdit1.EditValue.ToString.Trim, ds.Tables(0).Rows(0).Item("Id"))
                Else
                    comclass.Process_AllnonRTC(Convert.ToDateTime(row("ForDate").ToString.Trim), Now.Date, LookUpEdit1.EditValue.ToString.Trim, LookUpEdit1.EditValue.ToString.Trim, ds.Tables(0).Rows(0).Item("Id"))
                    If Common.EmpGrpArr(ds.Tables(0).Rows(0).Item("Id")).SHIFTTYPE = "M" Then
                        comclass.Process_AllnonRTCMulti(Convert.ToDateTime(row("ForDate").ToString.Trim), Now.Date, LookUpEdit1.EditValue.ToString.Trim, LookUpEdit1.EditValue.ToString.Trim, ds.Tables(0).Rows(0).Item("Id"))
                    End If
                End If
                XtraMasterTest.LabelControlStatus.Text = ""
                Application.DoEvents()
                LoadLeaveHistoryGrid()
                LoadLeaveQuota(paycode)
                XtraMessageBox.Show(ulf, "<size=10>" & Common.res_man.GetString("deletesuccess", Common.cul) & "</size>", Common.res_man.GetString("msgsuccess", Common.cul))
            End If
        End If
    End Sub
    Private Sub ComboNepaliYearFrm_Leave(sender As System.Object, e As System.EventArgs) Handles ComboNepaliYearFrm.Leave

        If Common.IsNepali = "Y" Then
            Dim DC As New DateConverter()
            Try
                'DateEdit1.DateTime = DC.ToAD(New Date(ComboNepaliYearFrm.EditValue, ComboNEpaliMonthFrm.SelectedIndex + 1, ComboNepaliDateFrm.EditValue))
                DateEdit1.DateTime = DC.ToAD(ComboNepaliYearFrm.EditValue & "-" & ComboNEpaliMonthFrm.SelectedIndex + 1 & "-" & ComboNepaliDateFrm.EditValue)
            Catch ex As Exception
                XtraMessageBox.Show(ulf, "<size=10>Invalid From Date</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                ComboNepaliDateFrm.Select()
                Exit Sub
            End Try
        End If

        If DateEdit1.DateTime.ToString("yyyy-MM-dd") = "0001-01-01" Then
            DateEdit1.EditValue = Now
        End If
        DateEdit2.EditValue = DateEdit1.EditValue

        If Common.IsNepali = "Y" Then
            Dim DC As New DateConverter()
            Dim doj As String = DC.ToBS(New Date(DateEdit2.DateTime.Year, DateEdit2.DateTime.Month, DateEdit2.DateTime.Day))
            Dim dojTmp() As String = doj.Split("-")
            ComboNepaliYearTo.EditValue = dojTmp(0)
            ComboNEpaliMonthTo.SelectedIndex = dojTmp(1) - 1
            ComboNepaliDateTo.EditValue = dojTmp(2)
        End If
    End Sub
    Private Sub GridView1_CustomColumnDisplayText(sender As System.Object, e As DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs) Handles GridView1.CustomColumnDisplayText
        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
        Dim view As ColumnView = TryCast(sender, ColumnView)
        If Common.IsNepali = "Y" Then
            Me.Cursor = Cursors.WaitCursor
            If e.Column.FieldName = "ForDate" Then
                If row("ForDate").ToString.Trim <> "" Then
                    Dim DC As New DateConverter()
                    Dim dt As DateTime = Convert.ToDateTime(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "ForDate").ToString().Trim)
                    Try
                        Dim Vstart As String = DC.ToBS(New Date(dt.Year, dt.Month, dt.Day))
                        'e.DisplayText = Vstart.Day & "-" & Common.NepaliMonth(Vstart.Month - 1).ToString & "-" & Vstart.Year
                        Dim dojTmp() As String = Vstart.Split("-")
                        e.DisplayText = dojTmp(2) & "-" & Common.NepaliMonth(dojTmp(1) - 1) & "-" & dojTmp(0)

                    Catch ex As Exception
                    End Try
                End If
            End If

            If e.Column.FieldName = "ApprovedDate" Then
                If row("ApprovedDate").ToString.Trim <> "" Then
                    Dim DC As New DateConverter()
                    Dim dt As DateTime = Convert.ToDateTime(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "ApprovedDate").ToString().Trim)
                    Try
                        Dim Vstart As String = DC.ToBS(New Date(dt.Year, dt.Month, dt.Day))
                        'e.DisplayText = Vstart.Day & "-" & Common.NepaliMonth(Vstart.Month - 1).ToString & "-" & Vstart.Year
                        Dim dojTmp() As String = Vstart.Split("-")
                        e.DisplayText = dojTmp(2) & "-" & Common.NepaliMonth(dojTmp(1) - 1) & "-" & dojTmp(0)
                    Catch ex As Exception
                    End Try
                End If
            End If
            Me.Cursor = Cursors.Default
        End If
    End Sub
    Private Sub Fill_COMOFF(ByVal ExpiryDays As String)
        Dim NoDay As Integer = Convert.ToInt16(ExpiryDays)
        Dim dd As DataSet = New DataSet
        Dim dt As DateTime = DateEdit1.DateTime ' System.DateTime.Now.Date
        Dim ddt As String = dt.ToString("yyyy-MM-dd")
        Dim dt1 As DateTime = dt.AddDays((-NoDay))
        Dim ddt1 As String = dt1.ToString("yyyy-MM-dd")
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter

       
        Dim sSql As String
        Dim ds1 As DataSet
        sSql = "select EmployeeGroup.Id from TblEmployee, EmployeeGroup where TblEmployee.EmployeeGroupId=EmployeeGroup.GroupId and PAYCODE='" & LookUpEdit1.EditValue.ToString.Trim & "'"
        ds1 = New DataSet
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(ds1)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(ds1)
        End If
        Dim strsql As String
        If Common.EmpGrpArr(ds1.Tables(0).Rows(0).Item(0)).MISCOMPOFF = "Y" Then
            strsql = "select Convert(varchar(10),dateoffice,120)  +'-'+ STATUS 'Datenstatus',Convert(varchar(10),dateoffice,120) 'dt' from " & _
                     "tblTimeRegister where DATEOFFICE  BETWEEN '" & ddt1.ToString().Trim() & "' and '" & ddt.ToString().Trim() & "'and " & _
                     "paycode= '" & LookUpEdit1.EditValue.ToString.Trim & "' AND  (MPRESENTVALUE>0 OR STATUS IN ('POW','POH') ) and " & _
                     "(COFUsed is NULL or COFUsed='N' )"
        Else
            strsql = "select Convert(varchar(10),dateoffice,120)  +'-'+ STATUS 'Datenstatus',Convert(varchar(10),dateoffice,120) 'dt' from tblTimeRegister where DATEOFFICE  BETWEEN '" & ddt1.ToString().Trim() & "' and '" & ddt.ToString().Trim() & "'and paycode= '" & LookUpEdit1.EditValue.ToString.Trim & "' AND STATUS IN('POH','POW') and (COFUsed is NULL or COFUsed='N' )"
        End If

        If Common.servername = "Access" Then
            If Common.EmpGrpArr(ds1.Tables(0).Rows(0).Item(0)).MISCOMPOFF = "Y" Then
                strsql = "select FORMAT(dateoffice,'yyyy-MM-dd'))  +'-'+ STATUS as Datenstatus ,FORMAT(dateoffice,'yyyy-MM-dd') as dt from " & _
                         "tblTimeRegister where DATEOFFICE  BETWEEN #" & ddt1.ToString().Trim() & "# and #" & ddt.ToString().Trim() & "# and " & _
                         "paycode= '" & LookUpEdit1.EditValue.ToString.Trim & "' AND  (MPRESENTVALUE>0 OR STATUS IN ('POW','POH') ) and " & _
                         "(COFUsed is NULL or COFUsed='N' )"
            Else
                strsql = "select FORMAT(dateoffice,'yyyy-MM-dd')  +'-'+ STATUS as Datenstatus ,FORMAT(dateoffice,'yyyy-MM-dd') as dt from tblTimeRegister where DATEOFFICE  BETWEEN #" & ddt1.ToString().Trim() & "# and #" & ddt.ToString().Trim() & "# and paycode= '" & LookUpEdit1.EditValue.ToString.Trim & "' AND STATUS IN('POH','POW') and (COFUsed is NULL or COFUsed='N' )"
            End If
        End If

        Dim ds As DataSet = New DataSet
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(ds)
        End If
        ComboCmpAgn.Properties.Items.Clear()
        If ds.Tables(0).Rows.Count > 0 Then
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                Dim compval As String = ds.Tables(0).Rows(i).Item("Datenstatus").ToString.Trim
                ComboCmpAgn.Properties.Items.Add(compval)
            Next
        Else
            ComboCmpAgn.Properties.Items.Add("NA")
        End If


        ComboCmpAgn.SelectedIndex = 0
        'dd = cn.FillDataSet(strsql)
        'ddlcomofflist.DataSource = dd
        'ddlcomofflist.DataTextField = "Datenstatus"
        'ddlcomofflist.DataValueField = "dt"
        'ddlcomofflist.DataBind()
    End Sub
    Private Sub GridLookUpEditLeave_EditValueChanged(sender As System.Object, e As System.EventArgs) Handles GridLookUpEditLeave.EditValueChanged
        If GridLookUpEditLeave.EditValue <> "" Then
            Dim sSql As String
            Dim adap As SqlDataAdapter
            Dim adapA As OleDbDataAdapter
            Dim ds1 As DataSet = New DataSet
           
                Dim ds As DataSet = New DataSet
            sSql = "select isCompOffType,ExpiryDays from tblLeaveMaster where LEAVECODE='" & GridLookUpEditLeave.EditValue & "'"
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(ds)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(ds)
                End If

                If ds.Tables(0).Rows(0).Item(0).ToString.Trim = "Y" Then
                    LabelControl13.Visible = True
                    ComboCmpAgn.Visible = True
                    DateEdit2.DateTime = DateEdit1.DateTime
                    DateEdit2.Enabled = False
                    GroupControl1.Enabled = False
                    CheckEdit4.Checked = True
                    Fill_COMOFF(ds.Tables(0).Rows(0).Item("ExpiryDays").ToString.Trim)

                Else
                    LabelControl13.Visible = False
                    ComboCmpAgn.Visible = False
                    DateEdit2.Enabled = True
                    GroupControl1.Enabled = True
                End If

            End If       
    End Sub
End Class
