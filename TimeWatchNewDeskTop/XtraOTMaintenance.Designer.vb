﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class XtraOTMaintenance
    Inherits DevExpress.XtraEditors.XtraUserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(XtraOTMaintenance))
        Me.SplitContainerControl1 = New DevExpress.XtraEditors.SplitContainerControl()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.TblTimeRegisterBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SSSDBDataSet = New iAS.SSSDBDataSet()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colPAYCODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDateOFFICE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemDateEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit()
        Me.colSHIFT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSTATUS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLATEARRIVAL = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colEARLYDEPARTURE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colIN1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemDateEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit()
        Me.colOUT1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colIN2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colOUT2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colHOURSWORKED = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colOTDURATION = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colManOTDuration = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colOTApprove = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.SidePanel2 = New DevExpress.XtraEditors.SidePanel()
        Me.XtraScrollableControl1 = New DevExpress.XtraEditors.XtraScrollableControl()
        Me.PopupContainerControlBranch = New DevExpress.XtraEditors.PopupContainerControl()
        Me.GridControlBranch = New DevExpress.XtraGrid.GridControl()
        Me.GridViewBranch = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colBRANCHCODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBRANCHNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTimeEdit7 = New DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit()
        Me.RepositoryItemDateEdit3 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit()
        Me.PopupContainerControlEmp = New DevExpress.XtraEditors.PopupContainerControl()
        Me.GridControlEmp = New DevExpress.XtraGrid.GridControl()
        Me.TblEmployeeBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GridViewEmp = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colEMPNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTimeEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit()
        Me.PopupContainerControlDept = New DevExpress.XtraEditors.PopupContainerControl()
        Me.GridControlDept = New DevExpress.XtraGrid.GridControl()
        Me.GridViewDept = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colDEPARTMENTCODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDEPARTMENTNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTimeEdit3 = New DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit()
        Me.CheckEdit1 = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.CheckEdit2 = New DevExpress.XtraEditors.CheckEdit()
        Me.PopupContainerEditLocation = New DevExpress.XtraEditors.PopupContainerEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.PopupContainerEditEmp = New DevExpress.XtraEditors.PopupContainerEdit()
        Me.PopupContainerEditGrade = New DevExpress.XtraEditors.PopupContainerEdit()
        Me.PopupContainerControlGrade = New DevExpress.XtraEditors.PopupContainerControl()
        Me.GridControlGrade = New DevExpress.XtraGrid.GridControl()
        Me.GridViewGrade = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTimeEdit6 = New DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit()
        Me.PopupContainerEditComp = New DevExpress.XtraEditors.PopupContainerEdit()
        Me.PopupContainerControlComp = New DevExpress.XtraEditors.PopupContainerControl()
        Me.GridControlComp = New DevExpress.XtraGrid.GridControl()
        Me.GridViewComp = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colCOMPANYCODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCOMPANYNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTimeEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.PopupContainerEditDept = New DevExpress.XtraEditors.PopupContainerEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.PopupContainerEditCat = New DevExpress.XtraEditors.PopupContainerEdit()
        Me.PopupContainerControlCat = New DevExpress.XtraEditors.PopupContainerControl()
        Me.GridControlCat = New DevExpress.XtraGrid.GridControl()
        Me.GridViewCat = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colCAT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCATAGORYNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTimeEdit4 = New DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit()
        Me.SidePanel1 = New DevExpress.XtraEditors.SidePanel()
        Me.BtnLoad = New DevExpress.XtraEditors.SimpleButton()
        Me.DateEdit2 = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.DateEdit1 = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.SimpleButtonSave = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButtonAdd = New DevExpress.XtraEditors.SimpleButton()
        Me.TextMOT = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit2 = New DevExpress.XtraEditors.TextEdit()
        Me.MemoEdit1 = New DevExpress.XtraEditors.MemoEdit()
        Me.TblTimeRegisterTableAdapter = New iAS.SSSDBDataSetTableAdapters.tblTimeRegisterTableAdapter()
        Me.TblTimeRegister1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblTimeRegister1TableAdapter()
        Me.TblEmployee1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.TblEmployee1TableAdapter()
        Me.TblEmployeeTableAdapter = New iAS.SSSDBDataSetTableAdapters.TblEmployeeTableAdapter()
        Me.TblDepartmentBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TblGradeBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TblCatagoryBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TblDepartmentTableAdapter = New iAS.SSSDBDataSetTableAdapters.tblDepartmentTableAdapter()
        Me.TblDepartment1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblDepartment1TableAdapter()
        Me.TblCatagoryTableAdapter = New iAS.SSSDBDataSetTableAdapters.tblCatagoryTableAdapter()
        Me.TblGradeTableAdapter = New iAS.SSSDBDataSetTableAdapters.tblGradeTableAdapter()
        Me.TblCatagory1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblCatagory1TableAdapter()
        Me.TblGrade1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblGrade1TableAdapter()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl1.SuspendLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblTimeRegisterBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit2.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit1.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SidePanel2.SuspendLayout()
        Me.XtraScrollableControl1.SuspendLayout()
        CType(Me.PopupContainerControlBranch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PopupContainerControlBranch.SuspendLayout()
        CType(Me.GridControlBranch, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewBranch, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTimeEdit7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit3.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerControlEmp, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PopupContainerControlEmp.SuspendLayout()
        CType(Me.GridControlEmp, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblEmployeeBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewEmp, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTimeEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerControlDept, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PopupContainerControlDept.SuspendLayout()
        CType(Me.GridControlDept, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewDept, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTimeEdit3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerEditLocation.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerEditEmp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerEditGrade.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerControlGrade, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PopupContainerControlGrade.SuspendLayout()
        CType(Me.GridControlGrade, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewGrade, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTimeEdit6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerEditComp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerControlComp, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PopupContainerControlComp.SuspendLayout()
        CType(Me.GridControlComp, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewComp, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTimeEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerEditDept.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerEditCat.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerControlCat, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PopupContainerControlCat.SuspendLayout()
        CType(Me.GridControlCat, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewCat, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTimeEdit4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SidePanel1.SuspendLayout()
        CType(Me.DateEdit2.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit1.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextMOT.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblDepartmentBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblGradeBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblCatagoryBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SplitContainerControl1
        '
        Me.SplitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainerControl1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainerControl1.Name = "SplitContainerControl1"
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.GridControl1)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.SidePanel2)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.SidePanel1)
        Me.SplitContainerControl1.Panel1.Text = "Panel1"
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.MemoEdit1)
        Me.SplitContainerControl1.Panel2.Text = "Panel2"
        Me.SplitContainerControl1.Size = New System.Drawing.Size(1250, 568)
        Me.SplitContainerControl1.SplitterPosition = 1165
        Me.SplitContainerControl1.TabIndex = 4
        Me.SplitContainerControl1.Text = "SplitContainerControl1"
        '
        'GridControl1
        '
        Me.GridControl1.DataSource = Me.TblTimeRegisterBindingSource
        Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControl1.EmbeddedNavigator.Buttons.Append.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.Edit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.Remove.Visible = False
        Me.GridControl1.Location = New System.Drawing.Point(0, 142)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemDateEdit1, Me.RepositoryItemDateEdit2})
        Me.GridControl1.Size = New System.Drawing.Size(1165, 426)
        Me.GridControl1.TabIndex = 2
        Me.GridControl1.UseEmbeddedNavigator = True
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'TblTimeRegisterBindingSource
        '
        Me.TblTimeRegisterBindingSource.DataMember = "tblTimeRegister"
        Me.TblTimeRegisterBindingSource.DataSource = Me.SSSDBDataSet
        '
        'SSSDBDataSet
        '
        Me.SSSDBDataSet.DataSetName = "SSSDBDataSet"
        Me.SSSDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GridView1
        '
        Me.GridView1.Appearance.TopNewRow.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.GridView1.Appearance.TopNewRow.ForeColor = System.Drawing.Color.Blue
        Me.GridView1.Appearance.TopNewRow.Options.UseFont = True
        Me.GridView1.Appearance.TopNewRow.Options.UseForeColor = True
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colPAYCODE, Me.colDateOFFICE, Me.colSHIFT, Me.colSTATUS, Me.colLATEARRIVAL, Me.colEARLYDEPARTURE, Me.colIN1, Me.colOUT1, Me.colIN2, Me.colOUT2, Me.colHOURSWORKED, Me.colOTDURATION, Me.colManOTDuration, Me.colOTApprove})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.NewItemRowText = "Click here to add new Holiday"
        Me.GridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.EditingMode = DevExpress.XtraGrid.Views.Grid.GridEditingMode.EditForm
        Me.GridView1.OptionsView.ColumnAutoWidth = False
        '
        'colPAYCODE
        '
        Me.colPAYCODE.FieldName = "PayCode"
        Me.colPAYCODE.Name = "colPAYCODE"
        Me.colPAYCODE.Visible = True
        Me.colPAYCODE.VisibleIndex = 0
        '
        'colDateOFFICE
        '
        Me.colDateOFFICE.Caption = "Date"
        Me.colDateOFFICE.ColumnEdit = Me.RepositoryItemDateEdit2
        Me.colDateOFFICE.FieldName = "DateOffice"
        Me.colDateOFFICE.Name = "colDateOFFICE"
        Me.colDateOFFICE.Visible = True
        Me.colDateOFFICE.VisibleIndex = 1
        Me.colDateOFFICE.Width = 104
        '
        'RepositoryItemDateEdit2
        '
        Me.RepositoryItemDateEdit2.AutoHeight = False
        Me.RepositoryItemDateEdit2.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit2.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit2.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemDateEdit2.Name = "RepositoryItemDateEdit2"
        '
        'colSHIFT
        '
        Me.colSHIFT.Caption = "Shift"
        Me.colSHIFT.FieldName = "Shift"
        Me.colSHIFT.Name = "colSHIFT"
        Me.colSHIFT.Visible = True
        Me.colSHIFT.VisibleIndex = 2
        Me.colSHIFT.Width = 104
        '
        'colSTATUS
        '
        Me.colSTATUS.Caption = "Status"
        Me.colSTATUS.FieldName = "Status"
        Me.colSTATUS.Name = "colSTATUS"
        Me.colSTATUS.Visible = True
        Me.colSTATUS.VisibleIndex = 3
        Me.colSTATUS.Width = 104
        '
        'colLATEARRIVAL
        '
        Me.colLATEARRIVAL.Caption = "Late Arrival"
        Me.colLATEARRIVAL.FieldName = "LATEARRIVAL"
        Me.colLATEARRIVAL.Name = "colLATEARRIVAL"
        Me.colLATEARRIVAL.Width = 104
        '
        'colEARLYDEPARTURE
        '
        Me.colEARLYDEPARTURE.Caption = "Early Departure"
        Me.colEARLYDEPARTURE.FieldName = "EARLYDEPARTURE"
        Me.colEARLYDEPARTURE.Name = "colEARLYDEPARTURE"
        Me.colEARLYDEPARTURE.Width = 104
        '
        'colIN1
        '
        Me.colIN1.Caption = "In Time"
        Me.colIN1.ColumnEdit = Me.RepositoryItemDateEdit1
        Me.colIN1.FieldName = "In1"
        Me.colIN1.Name = "colIN1"
        Me.colIN1.Visible = True
        Me.colIN1.VisibleIndex = 4
        Me.colIN1.Width = 104
        '
        'RepositoryItemDateEdit1
        '
        Me.RepositoryItemDateEdit1.AutoHeight = False
        Me.RepositoryItemDateEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit1.Mask.EditMask = "HH:mm"
        Me.RepositoryItemDateEdit1.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemDateEdit1.Name = "RepositoryItemDateEdit1"
        '
        'colOUT1
        '
        Me.colOUT1.Caption = "Lunch Out"
        Me.colOUT1.ColumnEdit = Me.RepositoryItemDateEdit1
        Me.colOUT1.FieldName = "Out1"
        Me.colOUT1.Name = "colOUT1"
        Me.colOUT1.Width = 104
        '
        'colIN2
        '
        Me.colIN2.Caption = "Lunch In"
        Me.colIN2.ColumnEdit = Me.RepositoryItemDateEdit1
        Me.colIN2.FieldName = "In2"
        Me.colIN2.Name = "colIN2"
        Me.colIN2.Width = 104
        '
        'colOUT2
        '
        Me.colOUT2.Caption = "Out Time"
        Me.colOUT2.ColumnEdit = Me.RepositoryItemDateEdit1
        Me.colOUT2.FieldName = "Out2"
        Me.colOUT2.Name = "colOUT2"
        Me.colOUT2.Visible = True
        Me.colOUT2.VisibleIndex = 5
        Me.colOUT2.Width = 104
        '
        'colHOURSWORKED
        '
        Me.colHOURSWORKED.Caption = "Worked Hours"
        Me.colHOURSWORKED.FieldName = "HOURSWORKED"
        Me.colHOURSWORKED.Name = "colHOURSWORKED"
        Me.colHOURSWORKED.Visible = True
        Me.colHOURSWORKED.VisibleIndex = 6
        Me.colHOURSWORKED.Width = 104
        '
        'colOTDURATION
        '
        Me.colOTDURATION.Caption = "Over Time"
        Me.colOTDURATION.FieldName = "OTDURATION"
        Me.colOTDURATION.Name = "colOTDURATION"
        Me.colOTDURATION.Visible = True
        Me.colOTDURATION.VisibleIndex = 7
        Me.colOTDURATION.Width = 104
        '
        'colManOTDuration
        '
        Me.colManOTDuration.Caption = "Manual OT"
        Me.colManOTDuration.FieldName = "ManOTDuration"
        Me.colManOTDuration.Name = "colManOTDuration"
        Me.colManOTDuration.Visible = True
        Me.colManOTDuration.VisibleIndex = 8
        Me.colManOTDuration.Width = 130
        '
        'colOTApprove
        '
        Me.colOTApprove.Caption = "OT Approve"
        Me.colOTApprove.FieldName = "OTApprove"
        Me.colOTApprove.Name = "colOTApprove"
        Me.colOTApprove.Visible = True
        Me.colOTApprove.VisibleIndex = 9
        Me.colOTApprove.Width = 100
        '
        'SidePanel2
        '
        Me.SidePanel2.Controls.Add(Me.XtraScrollableControl1)
        Me.SidePanel2.Dock = System.Windows.Forms.DockStyle.Top
        Me.SidePanel2.Location = New System.Drawing.Point(0, 53)
        Me.SidePanel2.Name = "SidePanel2"
        Me.SidePanel2.Size = New System.Drawing.Size(1165, 89)
        Me.SidePanel2.TabIndex = 1
        Me.SidePanel2.Text = "SidePanel2"
        '
        'XtraScrollableControl1
        '
        Me.XtraScrollableControl1.Controls.Add(Me.PopupContainerControlBranch)
        Me.XtraScrollableControl1.Controls.Add(Me.PopupContainerControlEmp)
        Me.XtraScrollableControl1.Controls.Add(Me.PopupContainerControlDept)
        Me.XtraScrollableControl1.Controls.Add(Me.CheckEdit1)
        Me.XtraScrollableControl1.Controls.Add(Me.LabelControl8)
        Me.XtraScrollableControl1.Controls.Add(Me.CheckEdit2)
        Me.XtraScrollableControl1.Controls.Add(Me.PopupContainerEditLocation)
        Me.XtraScrollableControl1.Controls.Add(Me.LabelControl1)
        Me.XtraScrollableControl1.Controls.Add(Me.LabelControl9)
        Me.XtraScrollableControl1.Controls.Add(Me.PopupContainerEditEmp)
        Me.XtraScrollableControl1.Controls.Add(Me.PopupContainerEditGrade)
        Me.XtraScrollableControl1.Controls.Add(Me.PopupContainerEditComp)
        Me.XtraScrollableControl1.Controls.Add(Me.LabelControl4)
        Me.XtraScrollableControl1.Controls.Add(Me.PopupContainerEditDept)
        Me.XtraScrollableControl1.Controls.Add(Me.LabelControl6)
        Me.XtraScrollableControl1.Controls.Add(Me.LabelControl5)
        Me.XtraScrollableControl1.Controls.Add(Me.PopupContainerEditCat)
        Me.XtraScrollableControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.XtraScrollableControl1.Location = New System.Drawing.Point(0, 0)
        Me.XtraScrollableControl1.Name = "XtraScrollableControl1"
        Me.XtraScrollableControl1.Size = New System.Drawing.Size(1165, 88)
        Me.XtraScrollableControl1.TabIndex = 1
        '
        'PopupContainerControlBranch
        '
        Me.PopupContainerControlBranch.Controls.Add(Me.GridControlBranch)
        Me.PopupContainerControlBranch.Location = New System.Drawing.Point(648, 75)
        Me.PopupContainerControlBranch.Name = "PopupContainerControlBranch"
        Me.PopupContainerControlBranch.Size = New System.Drawing.Size(300, 300)
        Me.PopupContainerControlBranch.TabIndex = 38
        '
        'GridControlBranch
        '
        Me.GridControlBranch.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControlBranch.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridControlBranch.Location = New System.Drawing.Point(0, 0)
        Me.GridControlBranch.MainView = Me.GridViewBranch
        Me.GridControlBranch.Name = "GridControlBranch"
        Me.GridControlBranch.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTimeEdit7, Me.RepositoryItemDateEdit3})
        Me.GridControlBranch.Size = New System.Drawing.Size(300, 300)
        Me.GridControlBranch.TabIndex = 6
        Me.GridControlBranch.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewBranch})
        '
        'GridViewBranch
        '
        Me.GridViewBranch.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colBRANCHCODE, Me.colBRANCHNAME})
        Me.GridViewBranch.GridControl = Me.GridControlBranch
        Me.GridViewBranch.Name = "GridViewBranch"
        Me.GridViewBranch.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewBranch.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewBranch.OptionsBehavior.Editable = False
        Me.GridViewBranch.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridViewBranch.OptionsSelection.MultiSelect = True
        Me.GridViewBranch.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridViewBranch.OptionsView.ColumnAutoWidth = False
        Me.GridViewBranch.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colBRANCHCODE, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colBRANCHCODE
        '
        Me.colBRANCHCODE.Caption = "Location Code"
        Me.colBRANCHCODE.FieldName = "BRANCHCODE"
        Me.colBRANCHCODE.Name = "colBRANCHCODE"
        Me.colBRANCHCODE.Visible = True
        Me.colBRANCHCODE.VisibleIndex = 1
        Me.colBRANCHCODE.Width = 100
        '
        'colBRANCHNAME
        '
        Me.colBRANCHNAME.Caption = "Name"
        Me.colBRANCHNAME.FieldName = "BRANCHNAME"
        Me.colBRANCHNAME.Name = "colBRANCHNAME"
        Me.colBRANCHNAME.Visible = True
        Me.colBRANCHNAME.VisibleIndex = 2
        Me.colBRANCHNAME.Width = 120
        '
        'RepositoryItemTimeEdit7
        '
        Me.RepositoryItemTimeEdit7.AutoHeight = False
        Me.RepositoryItemTimeEdit7.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemTimeEdit7.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTimeEdit7.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemTimeEdit7.Name = "RepositoryItemTimeEdit7"
        '
        'RepositoryItemDateEdit3
        '
        Me.RepositoryItemDateEdit3.AutoHeight = False
        Me.RepositoryItemDateEdit3.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit3.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit3.Mask.EditMask = "HH:mm"
        Me.RepositoryItemDateEdit3.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemDateEdit3.Name = "RepositoryItemDateEdit3"
        '
        'PopupContainerControlEmp
        '
        Me.PopupContainerControlEmp.Controls.Add(Me.GridControlEmp)
        Me.PopupContainerControlEmp.Location = New System.Drawing.Point(342, 75)
        Me.PopupContainerControlEmp.Name = "PopupContainerControlEmp"
        Me.PopupContainerControlEmp.Size = New System.Drawing.Size(300, 300)
        Me.PopupContainerControlEmp.TabIndex = 37
        '
        'GridControlEmp
        '
        Me.GridControlEmp.DataSource = Me.TblEmployeeBindingSource
        Me.GridControlEmp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControlEmp.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridControlEmp.Location = New System.Drawing.Point(0, 0)
        Me.GridControlEmp.MainView = Me.GridViewEmp
        Me.GridControlEmp.Name = "GridControlEmp"
        Me.GridControlEmp.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTimeEdit1})
        Me.GridControlEmp.Size = New System.Drawing.Size(300, 300)
        Me.GridControlEmp.TabIndex = 6
        Me.GridControlEmp.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewEmp})
        '
        'TblEmployeeBindingSource
        '
        Me.TblEmployeeBindingSource.DataMember = "TblEmployee"
        Me.TblEmployeeBindingSource.DataSource = Me.SSSDBDataSet
        '
        'GridViewEmp
        '
        Me.GridViewEmp.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn3, Me.colEMPNAME})
        Me.GridViewEmp.GridControl = Me.GridControlEmp
        Me.GridViewEmp.Name = "GridViewEmp"
        Me.GridViewEmp.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewEmp.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewEmp.OptionsBehavior.Editable = False
        Me.GridViewEmp.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridViewEmp.OptionsSelection.MultiSelect = True
        Me.GridViewEmp.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridViewEmp.OptionsView.ColumnAutoWidth = False
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "Paycode"
        Me.GridColumn3.FieldName = "PAYCODE"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Visible = True
        Me.GridColumn3.VisibleIndex = 1
        '
        'colEMPNAME
        '
        Me.colEMPNAME.Caption = "Name"
        Me.colEMPNAME.FieldName = "EMPNAME"
        Me.colEMPNAME.Name = "colEMPNAME"
        Me.colEMPNAME.Visible = True
        Me.colEMPNAME.VisibleIndex = 2
        Me.colEMPNAME.Width = 125
        '
        'RepositoryItemTimeEdit1
        '
        Me.RepositoryItemTimeEdit1.AutoHeight = False
        Me.RepositoryItemTimeEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemTimeEdit1.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTimeEdit1.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemTimeEdit1.Name = "RepositoryItemTimeEdit1"
        '
        'PopupContainerControlDept
        '
        Me.PopupContainerControlDept.Controls.Add(Me.GridControlDept)
        Me.PopupContainerControlDept.Location = New System.Drawing.Point(14, 78)
        Me.PopupContainerControlDept.Name = "PopupContainerControlDept"
        Me.PopupContainerControlDept.Size = New System.Drawing.Size(300, 300)
        Me.PopupContainerControlDept.TabIndex = 15
        '
        'GridControlDept
        '
        Me.GridControlDept.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControlDept.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridControlDept.Location = New System.Drawing.Point(0, 0)
        Me.GridControlDept.MainView = Me.GridViewDept
        Me.GridControlDept.Name = "GridControlDept"
        Me.GridControlDept.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTimeEdit3})
        Me.GridControlDept.Size = New System.Drawing.Size(300, 300)
        Me.GridControlDept.TabIndex = 6
        Me.GridControlDept.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewDept})
        '
        'GridViewDept
        '
        Me.GridViewDept.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colDEPARTMENTCODE, Me.colDEPARTMENTNAME})
        Me.GridViewDept.GridControl = Me.GridControlDept
        Me.GridViewDept.Name = "GridViewDept"
        Me.GridViewDept.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewDept.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewDept.OptionsBehavior.Editable = False
        Me.GridViewDept.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridViewDept.OptionsSelection.MultiSelect = True
        Me.GridViewDept.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridViewDept.OptionsView.ColumnAutoWidth = False
        Me.GridViewDept.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colDEPARTMENTCODE, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colDEPARTMENTCODE
        '
        Me.colDEPARTMENTCODE.Caption = "Depatment Code"
        Me.colDEPARTMENTCODE.FieldName = "DEPARTMENTCODE"
        Me.colDEPARTMENTCODE.Name = "colDEPARTMENTCODE"
        Me.colDEPARTMENTCODE.Visible = True
        Me.colDEPARTMENTCODE.VisibleIndex = 1
        Me.colDEPARTMENTCODE.Width = 100
        '
        'colDEPARTMENTNAME
        '
        Me.colDEPARTMENTNAME.Caption = "Name"
        Me.colDEPARTMENTNAME.FieldName = "DEPARTMENTNAME"
        Me.colDEPARTMENTNAME.Name = "colDEPARTMENTNAME"
        Me.colDEPARTMENTNAME.Visible = True
        Me.colDEPARTMENTNAME.VisibleIndex = 2
        Me.colDEPARTMENTNAME.Width = 120
        '
        'RepositoryItemTimeEdit3
        '
        Me.RepositoryItemTimeEdit3.AutoHeight = False
        Me.RepositoryItemTimeEdit3.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemTimeEdit3.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTimeEdit3.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemTimeEdit3.Name = "RepositoryItemTimeEdit3"
        '
        'CheckEdit1
        '
        Me.CheckEdit1.EditValue = True
        Me.CheckEdit1.Location = New System.Drawing.Point(730, 31)
        Me.CheckEdit1.Name = "CheckEdit1"
        Me.CheckEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEdit1.Properties.Appearance.Options.UseFont = True
        Me.CheckEdit1.Properties.Caption = "Active Employee"
        Me.CheckEdit1.Size = New System.Drawing.Size(155, 19)
        Me.CheckEdit1.TabIndex = 33
        Me.CheckEdit1.Visible = False
        '
        'LabelControl8
        '
        Me.LabelControl8.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl8.Appearance.Options.UseFont = True
        Me.LabelControl8.Location = New System.Drawing.Point(364, 31)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(84, 14)
        Me.LabelControl8.TabIndex = 36
        Me.LabelControl8.Text = "Select Location"
        '
        'CheckEdit2
        '
        Me.CheckEdit2.Location = New System.Drawing.Point(958, 31)
        Me.CheckEdit2.Name = "CheckEdit2"
        Me.CheckEdit2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEdit2.Properties.Appearance.Options.UseFont = True
        Me.CheckEdit2.Properties.Caption = "Inactive Employee"
        Me.CheckEdit2.Size = New System.Drawing.Size(155, 19)
        Me.CheckEdit2.TabIndex = 34
        Me.CheckEdit2.Visible = False
        '
        'PopupContainerEditLocation
        '
        Me.PopupContainerEditLocation.Location = New System.Drawing.Point(486, 28)
        Me.PopupContainerEditLocation.Name = "PopupContainerEditLocation"
        Me.PopupContainerEditLocation.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditLocation.Properties.Appearance.Options.UseFont = True
        Me.PopupContainerEditLocation.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditLocation.Properties.AppearanceDropDown.Options.UseFont = True
        Me.PopupContainerEditLocation.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditLocation.Properties.AppearanceFocused.Options.UseFont = True
        Me.PopupContainerEditLocation.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PopupContainerEditLocation.Properties.PopupControl = Me.PopupContainerControlBranch
        Me.PopupContainerEditLocation.Size = New System.Drawing.Size(225, 20)
        Me.PopupContainerEditLocation.TabIndex = 35
        Me.PopupContainerEditLocation.ToolTip = "Leave blank if want for all Employees"
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(14, 9)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(91, 14)
        Me.LabelControl1.TabIndex = 21
        Me.LabelControl1.Text = "Select Employee"
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl9.Appearance.Options.UseFont = True
        Me.LabelControl9.Location = New System.Drawing.Point(364, 54)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(70, 14)
        Me.LabelControl9.TabIndex = 32
        Me.LabelControl9.Text = "Select Grade"
        '
        'PopupContainerEditEmp
        '
        Me.PopupContainerEditEmp.Location = New System.Drawing.Point(111, 6)
        Me.PopupContainerEditEmp.Name = "PopupContainerEditEmp"
        Me.PopupContainerEditEmp.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditEmp.Properties.Appearance.Options.UseFont = True
        Me.PopupContainerEditEmp.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditEmp.Properties.AppearanceDropDown.Options.UseFont = True
        Me.PopupContainerEditEmp.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditEmp.Properties.AppearanceFocused.Options.UseFont = True
        Me.PopupContainerEditEmp.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PopupContainerEditEmp.Properties.PopupControl = Me.PopupContainerControlEmp
        Me.PopupContainerEditEmp.Size = New System.Drawing.Size(225, 20)
        Me.PopupContainerEditEmp.TabIndex = 22
        Me.PopupContainerEditEmp.ToolTip = "Leave blank if want for all Employees"
        '
        'PopupContainerEditGrade
        '
        Me.PopupContainerEditGrade.Location = New System.Drawing.Point(486, 51)
        Me.PopupContainerEditGrade.Name = "PopupContainerEditGrade"
        Me.PopupContainerEditGrade.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditGrade.Properties.Appearance.Options.UseFont = True
        Me.PopupContainerEditGrade.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditGrade.Properties.AppearanceDropDown.Options.UseFont = True
        Me.PopupContainerEditGrade.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditGrade.Properties.AppearanceFocused.Options.UseFont = True
        Me.PopupContainerEditGrade.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PopupContainerEditGrade.Properties.PopupControl = Me.PopupContainerControlGrade
        Me.PopupContainerEditGrade.Size = New System.Drawing.Size(225, 20)
        Me.PopupContainerEditGrade.TabIndex = 31
        Me.PopupContainerEditGrade.ToolTip = "Leave blank if want for all Employees"
        '
        'PopupContainerControlGrade
        '
        Me.PopupContainerControlGrade.Controls.Add(Me.GridControlGrade)
        Me.PopupContainerControlGrade.Location = New System.Drawing.Point(765, 44)
        Me.PopupContainerControlGrade.Name = "PopupContainerControlGrade"
        Me.PopupContainerControlGrade.Size = New System.Drawing.Size(300, 300)
        Me.PopupContainerControlGrade.TabIndex = 24
        '
        'GridControlGrade
        '
        Me.GridControlGrade.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControlGrade.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridControlGrade.Location = New System.Drawing.Point(0, 0)
        Me.GridControlGrade.MainView = Me.GridViewGrade
        Me.GridControlGrade.Name = "GridControlGrade"
        Me.GridControlGrade.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTimeEdit6})
        Me.GridControlGrade.Size = New System.Drawing.Size(300, 300)
        Me.GridControlGrade.TabIndex = 6
        Me.GridControlGrade.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewGrade})
        '
        'GridViewGrade
        '
        Me.GridViewGrade.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2})
        Me.GridViewGrade.GridControl = Me.GridControlGrade
        Me.GridViewGrade.Name = "GridViewGrade"
        Me.GridViewGrade.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewGrade.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewGrade.OptionsBehavior.Editable = False
        Me.GridViewGrade.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridViewGrade.OptionsSelection.MultiSelect = True
        Me.GridViewGrade.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridViewGrade.OptionsView.ColumnAutoWidth = False
        Me.GridViewGrade.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.GridColumn1, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Grade Code"
        Me.GridColumn1.FieldName = "GradeCode"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 1
        Me.GridColumn1.Width = 100
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Name"
        Me.GridColumn2.FieldName = "GradeName"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 2
        Me.GridColumn2.Width = 120
        '
        'RepositoryItemTimeEdit6
        '
        Me.RepositoryItemTimeEdit6.AutoHeight = False
        Me.RepositoryItemTimeEdit6.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemTimeEdit6.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTimeEdit6.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemTimeEdit6.Name = "RepositoryItemTimeEdit6"
        '
        'PopupContainerEditComp
        '
        Me.PopupContainerEditComp.Location = New System.Drawing.Point(111, 30)
        Me.PopupContainerEditComp.Name = "PopupContainerEditComp"
        Me.PopupContainerEditComp.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditComp.Properties.Appearance.Options.UseFont = True
        Me.PopupContainerEditComp.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditComp.Properties.AppearanceDropDown.Options.UseFont = True
        Me.PopupContainerEditComp.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditComp.Properties.AppearanceFocused.Options.UseFont = True
        Me.PopupContainerEditComp.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PopupContainerEditComp.Properties.PopupControl = Me.PopupContainerControlComp
        Me.PopupContainerEditComp.Size = New System.Drawing.Size(225, 20)
        Me.PopupContainerEditComp.TabIndex = 23
        Me.PopupContainerEditComp.ToolTip = "Leave blank if want for all Employees"
        '
        'PopupContainerControlComp
        '
        Me.PopupContainerControlComp.Controls.Add(Me.GridControlComp)
        Me.PopupContainerControlComp.Location = New System.Drawing.Point(309, 39)
        Me.PopupContainerControlComp.Name = "PopupContainerControlComp"
        Me.PopupContainerControlComp.Size = New System.Drawing.Size(300, 300)
        Me.PopupContainerControlComp.TabIndex = 26
        '
        'GridControlComp
        '
        Me.GridControlComp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControlComp.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridControlComp.Location = New System.Drawing.Point(0, 0)
        Me.GridControlComp.MainView = Me.GridViewComp
        Me.GridControlComp.Name = "GridControlComp"
        Me.GridControlComp.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTimeEdit2})
        Me.GridControlComp.Size = New System.Drawing.Size(300, 300)
        Me.GridControlComp.TabIndex = 6
        Me.GridControlComp.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewComp})
        '
        'GridViewComp
        '
        Me.GridViewComp.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colCOMPANYCODE, Me.colCOMPANYNAME})
        Me.GridViewComp.GridControl = Me.GridControlComp
        Me.GridViewComp.Name = "GridViewComp"
        Me.GridViewComp.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewComp.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewComp.OptionsBehavior.Editable = False
        Me.GridViewComp.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridViewComp.OptionsSelection.MultiSelect = True
        Me.GridViewComp.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridViewComp.OptionsView.ColumnAutoWidth = False
        Me.GridViewComp.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colCOMPANYCODE, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colCOMPANYCODE
        '
        Me.colCOMPANYCODE.Caption = "Company Code"
        Me.colCOMPANYCODE.FieldName = "COMPANYCODE"
        Me.colCOMPANYCODE.Name = "colCOMPANYCODE"
        Me.colCOMPANYCODE.Visible = True
        Me.colCOMPANYCODE.VisibleIndex = 1
        Me.colCOMPANYCODE.Width = 100
        '
        'colCOMPANYNAME
        '
        Me.colCOMPANYNAME.Caption = "Name"
        Me.colCOMPANYNAME.FieldName = "COMPANYNAME"
        Me.colCOMPANYNAME.Name = "colCOMPANYNAME"
        Me.colCOMPANYNAME.Visible = True
        Me.colCOMPANYNAME.VisibleIndex = 2
        Me.colCOMPANYNAME.Width = 120
        '
        'RepositoryItemTimeEdit2
        '
        Me.RepositoryItemTimeEdit2.AutoHeight = False
        Me.RepositoryItemTimeEdit2.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemTimeEdit2.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTimeEdit2.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemTimeEdit2.Name = "RepositoryItemTimeEdit2"
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl4.Appearance.Options.UseFont = True
        Me.LabelControl4.Location = New System.Drawing.Point(14, 32)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(88, 14)
        Me.LabelControl4.TabIndex = 24
        Me.LabelControl4.Text = "Select Company"
        '
        'PopupContainerEditDept
        '
        Me.PopupContainerEditDept.Location = New System.Drawing.Point(486, 5)
        Me.PopupContainerEditDept.Name = "PopupContainerEditDept"
        Me.PopupContainerEditDept.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditDept.Properties.Appearance.Options.UseFont = True
        Me.PopupContainerEditDept.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditDept.Properties.AppearanceDropDown.Options.UseFont = True
        Me.PopupContainerEditDept.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditDept.Properties.AppearanceFocused.Options.UseFont = True
        Me.PopupContainerEditDept.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PopupContainerEditDept.Properties.PopupControl = Me.PopupContainerControlDept
        Me.PopupContainerEditDept.Size = New System.Drawing.Size(225, 20)
        Me.PopupContainerEditDept.TabIndex = 25
        Me.PopupContainerEditDept.ToolTip = "Leave blank if want for all Employees"
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl6.Appearance.Options.UseFont = True
        Me.LabelControl6.Location = New System.Drawing.Point(14, 56)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(86, 14)
        Me.LabelControl6.TabIndex = 28
        Me.LabelControl6.Text = "Select Catagory"
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl5.Appearance.Options.UseFont = True
        Me.LabelControl5.Location = New System.Drawing.Point(364, 8)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(104, 14)
        Me.LabelControl5.TabIndex = 26
        Me.LabelControl5.Text = "Select Department"
        '
        'PopupContainerEditCat
        '
        Me.PopupContainerEditCat.Location = New System.Drawing.Point(111, 54)
        Me.PopupContainerEditCat.Name = "PopupContainerEditCat"
        Me.PopupContainerEditCat.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditCat.Properties.Appearance.Options.UseFont = True
        Me.PopupContainerEditCat.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditCat.Properties.AppearanceDropDown.Options.UseFont = True
        Me.PopupContainerEditCat.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditCat.Properties.AppearanceFocused.Options.UseFont = True
        Me.PopupContainerEditCat.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PopupContainerEditCat.Properties.PopupControl = Me.PopupContainerControlCat
        Me.PopupContainerEditCat.Size = New System.Drawing.Size(225, 20)
        Me.PopupContainerEditCat.TabIndex = 27
        Me.PopupContainerEditCat.ToolTip = "Leave blank if want for all Employees"
        '
        'PopupContainerControlCat
        '
        Me.PopupContainerControlCat.Controls.Add(Me.GridControlCat)
        Me.PopupContainerControlCat.Location = New System.Drawing.Point(3, 39)
        Me.PopupContainerControlCat.Name = "PopupContainerControlCat"
        Me.PopupContainerControlCat.Size = New System.Drawing.Size(300, 300)
        Me.PopupContainerControlCat.TabIndex = 25
        '
        'GridControlCat
        '
        Me.GridControlCat.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControlCat.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridControlCat.Location = New System.Drawing.Point(0, 0)
        Me.GridControlCat.MainView = Me.GridViewCat
        Me.GridControlCat.Name = "GridControlCat"
        Me.GridControlCat.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTimeEdit4})
        Me.GridControlCat.Size = New System.Drawing.Size(300, 300)
        Me.GridControlCat.TabIndex = 6
        Me.GridControlCat.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewCat})
        '
        'GridViewCat
        '
        Me.GridViewCat.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colCAT, Me.colCATAGORYNAME})
        Me.GridViewCat.GridControl = Me.GridControlCat
        Me.GridViewCat.Name = "GridViewCat"
        Me.GridViewCat.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewCat.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewCat.OptionsBehavior.Editable = False
        Me.GridViewCat.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridViewCat.OptionsSelection.MultiSelect = True
        Me.GridViewCat.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridViewCat.OptionsView.ColumnAutoWidth = False
        Me.GridViewCat.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colCAT, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colCAT
        '
        Me.colCAT.Caption = "Catagory Code"
        Me.colCAT.FieldName = "CAT"
        Me.colCAT.Name = "colCAT"
        Me.colCAT.Visible = True
        Me.colCAT.VisibleIndex = 1
        Me.colCAT.Width = 100
        '
        'colCATAGORYNAME
        '
        Me.colCATAGORYNAME.Caption = "Name"
        Me.colCATAGORYNAME.FieldName = "CATAGORYNAME"
        Me.colCATAGORYNAME.Name = "colCATAGORYNAME"
        Me.colCATAGORYNAME.Visible = True
        Me.colCATAGORYNAME.VisibleIndex = 2
        Me.colCATAGORYNAME.Width = 120
        '
        'RepositoryItemTimeEdit4
        '
        Me.RepositoryItemTimeEdit4.AutoHeight = False
        Me.RepositoryItemTimeEdit4.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemTimeEdit4.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTimeEdit4.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemTimeEdit4.Name = "RepositoryItemTimeEdit4"
        '
        'SidePanel1
        '
        Me.SidePanel1.Controls.Add(Me.PopupContainerControlComp)
        Me.SidePanel1.Controls.Add(Me.PopupContainerControlCat)
        Me.SidePanel1.Controls.Add(Me.PopupContainerControlGrade)
        Me.SidePanel1.Controls.Add(Me.BtnLoad)
        Me.SidePanel1.Controls.Add(Me.DateEdit2)
        Me.SidePanel1.Controls.Add(Me.LabelControl3)
        Me.SidePanel1.Controls.Add(Me.DateEdit1)
        Me.SidePanel1.Controls.Add(Me.LabelControl2)
        Me.SidePanel1.Controls.Add(Me.SimpleButtonSave)
        Me.SidePanel1.Controls.Add(Me.SimpleButtonAdd)
        Me.SidePanel1.Controls.Add(Me.TextMOT)
        Me.SidePanel1.Controls.Add(Me.TextEdit2)
        Me.SidePanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.SidePanel1.Location = New System.Drawing.Point(0, 0)
        Me.SidePanel1.Name = "SidePanel1"
        Me.SidePanel1.Size = New System.Drawing.Size(1165, 53)
        Me.SidePanel1.TabIndex = 0
        Me.SidePanel1.Text = "SidePanel1"
        '
        'BtnLoad
        '
        Me.BtnLoad.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.BtnLoad.Appearance.Options.UseFont = True
        Me.BtnLoad.Location = New System.Drawing.Point(466, 15)
        Me.BtnLoad.Name = "BtnLoad"
        Me.BtnLoad.Size = New System.Drawing.Size(100, 23)
        Me.BtnLoad.TabIndex = 3
        Me.BtnLoad.Text = "Load Data"
        '
        'DateEdit2
        '
        Me.DateEdit2.EditValue = Nothing
        Me.DateEdit2.Location = New System.Drawing.Point(326, 16)
        Me.DateEdit2.Name = "DateEdit2"
        Me.DateEdit2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.DateEdit2.Properties.Appearance.Options.UseFont = True
        Me.DateEdit2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit2.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit2.Size = New System.Drawing.Size(100, 20)
        Me.DateEdit2.TabIndex = 2
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl3.Appearance.Options.UseFont = True
        Me.LabelControl3.Location = New System.Drawing.Point(264, 19)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(45, 14)
        Me.LabelControl3.TabIndex = 12
        Me.LabelControl3.Text = "To Date"
        '
        'DateEdit1
        '
        Me.DateEdit1.EditValue = Nothing
        Me.DateEdit1.Location = New System.Drawing.Point(111, 16)
        Me.DateEdit1.Name = "DateEdit1"
        Me.DateEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.DateEdit1.Properties.Appearance.Options.UseFont = True
        Me.DateEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit1.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit1.Size = New System.Drawing.Size(100, 20)
        Me.DateEdit1.TabIndex = 1
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl2.Appearance.Options.UseFont = True
        Me.LabelControl2.Location = New System.Drawing.Point(26, 19)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(57, 14)
        Me.LabelControl2.TabIndex = 10
        Me.LabelControl2.Text = "From Date"
        '
        'SimpleButtonSave
        '
        Me.SimpleButtonSave.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonSave.Appearance.Options.UseFont = True
        Me.SimpleButtonSave.Location = New System.Drawing.Point(808, 15)
        Me.SimpleButtonSave.Name = "SimpleButtonSave"
        Me.SimpleButtonSave.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButtonSave.TabIndex = 6
        Me.SimpleButtonSave.Text = "Save"
        '
        'SimpleButtonAdd
        '
        Me.SimpleButtonAdd.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonAdd.Appearance.Options.UseFont = True
        Me.SimpleButtonAdd.Location = New System.Drawing.Point(727, 15)
        Me.SimpleButtonAdd.Name = "SimpleButtonAdd"
        Me.SimpleButtonAdd.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButtonAdd.TabIndex = 5
        Me.SimpleButtonAdd.Text = "Add"
        '
        'TextMOT
        '
        Me.TextMOT.EditValue = "00:00"
        Me.TextMOT.Location = New System.Drawing.Point(623, 16)
        Me.TextMOT.Name = "TextMOT"
        Me.TextMOT.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextMOT.Properties.Appearance.Options.UseFont = True
        Me.TextMOT.Properties.Mask.EditMask = "HH:mm"
        Me.TextMOT.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TextMOT.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextMOT.Properties.MaxLength = 5
        Me.TextMOT.Size = New System.Drawing.Size(72, 20)
        Me.TextMOT.TabIndex = 4
        '
        'TextEdit2
        '
        Me.TextEdit2.Location = New System.Drawing.Point(630, 41)
        Me.TextEdit2.Name = "TextEdit2"
        Me.TextEdit2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEdit2.Properties.Appearance.Options.UseFont = True
        Me.TextEdit2.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextEdit2.Properties.MaxLength = 12
        Me.TextEdit2.Size = New System.Drawing.Size(129, 20)
        Me.TextEdit2.TabIndex = 3
        Me.TextEdit2.Visible = False
        '
        'MemoEdit1
        '
        Me.MemoEdit1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MemoEdit1.EditValue = resources.GetString("MemoEdit1.EditValue")
        Me.MemoEdit1.Location = New System.Drawing.Point(0, 0)
        Me.MemoEdit1.Name = "MemoEdit1"
        Me.MemoEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.MemoEdit1.Properties.Appearance.Options.UseFont = True
        Me.MemoEdit1.Properties.MaxLength = 100000
        Me.MemoEdit1.Properties.ReadOnly = True
        Me.MemoEdit1.Size = New System.Drawing.Size(80, 568)
        Me.MemoEdit1.TabIndex = 4
        '
        'TblTimeRegisterTableAdapter
        '
        Me.TblTimeRegisterTableAdapter.ClearBeforeFill = True
        '
        'TblTimeRegister1TableAdapter1
        '
        Me.TblTimeRegister1TableAdapter1.ClearBeforeFill = True
        '
        'TblEmployee1TableAdapter1
        '
        Me.TblEmployee1TableAdapter1.ClearBeforeFill = True
        '
        'TblEmployeeTableAdapter
        '
        Me.TblEmployeeTableAdapter.ClearBeforeFill = True
        '
        'TblDepartmentBindingSource
        '
        Me.TblDepartmentBindingSource.DataMember = "tblDepartment"
        Me.TblDepartmentBindingSource.DataSource = Me.SSSDBDataSet
        '
        'TblGradeBindingSource
        '
        Me.TblGradeBindingSource.DataMember = "tblGrade"
        Me.TblGradeBindingSource.DataSource = Me.SSSDBDataSet
        '
        'TblCatagoryBindingSource
        '
        Me.TblCatagoryBindingSource.DataMember = "tblCatagory"
        Me.TblCatagoryBindingSource.DataSource = Me.SSSDBDataSet
        '
        'TblDepartmentTableAdapter
        '
        Me.TblDepartmentTableAdapter.ClearBeforeFill = True
        '
        'TblDepartment1TableAdapter1
        '
        Me.TblDepartment1TableAdapter1.ClearBeforeFill = True
        '
        'TblCatagoryTableAdapter
        '
        Me.TblCatagoryTableAdapter.ClearBeforeFill = True
        '
        'TblGradeTableAdapter
        '
        Me.TblGradeTableAdapter.ClearBeforeFill = True
        '
        'TblCatagory1TableAdapter1
        '
        Me.TblCatagory1TableAdapter1.ClearBeforeFill = True
        '
        'TblGrade1TableAdapter1
        '
        Me.TblGrade1TableAdapter1.ClearBeforeFill = True
        '
        'XtraOTMaintenance
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.SplitContainerControl1)
        Me.Name = "XtraOTMaintenance"
        Me.Size = New System.Drawing.Size(1250, 568)
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl1.ResumeLayout(False)
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblTimeRegisterBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit2.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit1.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SidePanel2.ResumeLayout(False)
        Me.XtraScrollableControl1.ResumeLayout(False)
        Me.XtraScrollableControl1.PerformLayout()
        CType(Me.PopupContainerControlBranch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PopupContainerControlBranch.ResumeLayout(False)
        CType(Me.GridControlBranch, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewBranch, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTimeEdit7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit3.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerControlEmp, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PopupContainerControlEmp.ResumeLayout(False)
        CType(Me.GridControlEmp, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblEmployeeBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewEmp, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTimeEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerControlDept, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PopupContainerControlDept.ResumeLayout(False)
        CType(Me.GridControlDept, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewDept, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTimeEdit3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerEditLocation.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerEditEmp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerEditGrade.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerControlGrade, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PopupContainerControlGrade.ResumeLayout(False)
        CType(Me.GridControlGrade, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewGrade, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTimeEdit6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerEditComp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerControlComp, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PopupContainerControlComp.ResumeLayout(False)
        CType(Me.GridControlComp, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewComp, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTimeEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerEditDept.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerEditCat.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerControlCat, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PopupContainerControlCat.ResumeLayout(False)
        CType(Me.GridControlCat, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewCat, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTimeEdit4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SidePanel1.ResumeLayout(False)
        Me.SidePanel1.PerformLayout()
        CType(Me.DateEdit2.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit1.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextMOT.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblDepartmentBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblGradeBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblCatagoryBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SplitContainerControl1 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents MemoEdit1 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents SidePanel1 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents TextEdit2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents SidePanel2 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents TblTimeRegisterBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SSSDBDataSet As iAS.SSSDBDataSet
    Friend WithEvents colPAYCODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDateOFFICE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSHIFT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSTATUS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLATEARRIVAL As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colEARLYDEPARTURE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colIN1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colOUT1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colIN2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colOUT2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colHOURSWORKED As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colOTDURATION As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TblTimeRegisterTableAdapter As iAS.SSSDBDataSetTableAdapters.tblTimeRegisterTableAdapter
    Friend WithEvents TblTimeRegister1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.tblTimeRegister1TableAdapter
    Friend WithEvents RepositoryItemDateEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents RepositoryItemDateEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents TblEmployee1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.TblEmployee1TableAdapter
    Friend WithEvents TblEmployeeBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblEmployeeTableAdapter As iAS.SSSDBDataSetTableAdapters.TblEmployeeTableAdapter
    Friend WithEvents XtraScrollableControl1 As DevExpress.XtraEditors.XtraScrollableControl
    Friend WithEvents colManOTDuration As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colOTApprove As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TextMOT As DevExpress.XtraEditors.TextEdit
    Friend WithEvents SimpleButtonSave As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButtonAdd As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents DateEdit2 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents DateEdit1 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents BtnLoad As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PopupContainerEditEmp As DevExpress.XtraEditors.PopupContainerEdit
    Friend WithEvents PopupContainerEditGrade As DevExpress.XtraEditors.PopupContainerEdit
    Friend WithEvents PopupContainerEditComp As DevExpress.XtraEditors.PopupContainerEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PopupContainerEditDept As DevExpress.XtraEditors.PopupContainerEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PopupContainerEditCat As DevExpress.XtraEditors.PopupContainerEdit
    Friend WithEvents CheckEdit1 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents CheckEdit2 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents PopupContainerEditLocation As DevExpress.XtraEditors.PopupContainerEdit
    Friend WithEvents PopupContainerControlGrade As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents GridControlGrade As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridViewGrade As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemTimeEdit6 As DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit
    Friend WithEvents PopupContainerControlCat As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents GridControlCat As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridViewCat As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colCAT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCATAGORYNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemTimeEdit4 As DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit
    Friend WithEvents PopupContainerControlComp As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents GridControlComp As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridViewComp As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colCOMPANYCODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCOMPANYNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemTimeEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit
    Friend WithEvents PopupContainerControlDept As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents GridControlDept As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridViewDept As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colDEPARTMENTCODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDEPARTMENTNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemTimeEdit3 As DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit
    Friend WithEvents PopupContainerControlEmp As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents GridControlEmp As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridViewEmp As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colEMPNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemTimeEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit
    Friend WithEvents PopupContainerControlBranch As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents GridControlBranch As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridViewBranch As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colBRANCHCODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBRANCHNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemTimeEdit7 As DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit
    Friend WithEvents RepositoryItemDateEdit3 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents TblDepartmentBindingSource As BindingSource
    Friend WithEvents TblGradeBindingSource As BindingSource
    Friend WithEvents TblCatagoryBindingSource As BindingSource
    Friend WithEvents TblDepartmentTableAdapter As SSSDBDataSetTableAdapters.tblDepartmentTableAdapter
    Friend WithEvents TblDepartment1TableAdapter1 As SSSDBDataSetTableAdapters.tblDepartment1TableAdapter
    Friend WithEvents TblCatagoryTableAdapter As SSSDBDataSetTableAdapters.tblCatagoryTableAdapter
    Friend WithEvents TblGradeTableAdapter As SSSDBDataSetTableAdapters.tblGradeTableAdapter
    Friend WithEvents TblCatagory1TableAdapter1 As SSSDBDataSetTableAdapters.tblCatagory1TableAdapter
    Friend WithEvents TblGrade1TableAdapter1 As SSSDBDataSetTableAdapters.tblGrade1TableAdapter
End Class
